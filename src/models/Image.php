<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * Image class implements base methods for Image object. 
 * 
 */

class Image {
    private $imageId;                                   // Unique image Identifier
    private $imageDateCreated;                          // Date image was created
    private $imageDateUpdated;                          // Date image was last updated
    private $imagePath;                                 // Image file path on server
    private $imageMiniPath;                             // Image miniature path on server
    private $imageLegend;                               // Image legend - displayed with image and used for search purposes
    private $imageText;                                 // Additional Image text

    /******************************************* Getters and setters  *************************************************/

    /**
     * Get the value of imageId
     */ 
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * Get the value of imageDateCreated
     */ 
    public function getImageDateCreated()
    {
        return $this->imageDateCreated;
    }


    /**
     * Get the value of imagePath
     */ 
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set the value of imagePath
     *
     * @return  self
     */ 
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get the value of imageMiniPath
     */ 
    public function getImageMiniPath()
    {
        return $this->imageMiniPath;
    }

    /**
     * Set the value of imageMiniPath
     *
     * @return  self
     */ 
    public function setImageMiniPath($imageMiniPath)
    {
        $this->imageMiniPath = $imageMiniPath;

        return $this;
    }
    /**
     * Get the value of imageLegend
     */ 
    public function getImageLegend()
    {
        return $this->imageLegend;
    }

    /**
     * Set the value of imageLegend
     *
     * @return  self
     */ 
    public function setImageLegend($imageLegend)
    {
        $this->imageLegend = $imageLegend;

        return $this;
    }

    /**
     * Get the value of imageText
     */ 
    public function getImageText()
    {
        return $this->imageText;
    }

    /**
     * Set the value of imageText
     *
     * @return  self
     */ 
    public function setImageText($imageText)
    {
        $this->imageText = $imageText;

        return $this;
    }

    /********************************************************* CRUD *************************************************/

    public function create() {
        $db = new Database();
        $request= "INSERT INTO images (imagePath,imageMiniPath,imageLegend, imageText) VALUES (?,?,?,?)";
        $args=[
            $this->imagePath,
            $this->imageMiniPath, 
            $this->imageLegend,
            $this->imageText
        ];
        $result= $db->insert(
            $request,
            $args
            );
        $this->imageId=$db->getLastId();
        return $result;   
    }

    public static function read ($imageId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from images WHERE imageId LIKE ?",             // SQL request
            array($imageId),                                           // SQL request parameters
            "Image"                                                    // class to be used
    );  
    }


    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE images SET 
                        imageLegend=?,
                        imageText=?,
                        imageDateUpdated=?
                        WHERE imageId=?";
        $args=[
            $this->imageLegend,
            $this->imageText,
            $updateDate,
            $this->imageId
        ];

        return $db->update(
            $request,
            $args
            );

    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM images WHERE imageId=?";
        $params=[
            $this->imageId,
        ];
        return $db->delete($request,$params);      
    }

    public static function readAll() {
        $db=new Database();
        $images=$db->getMany(
            "SELECT * from images",
            "Image" 
        );
        return $images;   
    }

    public static function readManyRandom($qty=15) {
        $db=new Database();
        $images=$db->getMany(
            "SELECT * from images ORDER BY RAND() LIMIT ".$qty,
            "Image" 
        );
        return $images;   
    }
}