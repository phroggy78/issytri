<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * 
 * Top Level class used to dynamically build the website. 
 * 
 * Each Menu item points to pages which are build dynamicall. The Menu element allows to build the overall scheme
 */

class Menu {
    private $menuId ;                                       // Unique menu ID
    private $menuTitle ;                                    // Menu title as displayed in menu navigation bar
    private $menuOrder;                                     // Defines order in which menu items are displayed in navBar
    private $menuDateCreated ;                              // Date menu item created
    private $menuDateUpdated ;                              // Date menu item was last updated


    /************************************************ Getters and setters *************************************************/

    /**
     * Get the value of menuId
     */ 
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Get the value of menuTitle
     */ 
    public function getMenuTitle()
    {
        return $this->menuTitle;
    }

    /**
     * Set the value of menuTitle
     *
     * @return  self
     */ 
    public function setMenuTitle($menuTitle)
    {
        $this->menuTitle = $menuTitle;

        return $this;
    }

    /**
     * Get the value of menuDateCreated
     */ 
    public function getMenuDateCreated()
    {
        return $this->menuDateCreated;
    }

    /**
     * Get the value of menuDateUpdated
     */ 
    public function getMenuDateUpdated()
    {
        return $this->menuDateUpdated;
    }

    /**
     * Set the value of menuDateUpdated
     *
     * @return  self
     */ 
    public function setMenuDateUpdated($menuDateUpdated)
    {
        $this->menuDateUpdated = $menuDateUpdated;

        return $this;
    }

    /**
     * Get the value of menuOrder
     */ 
    public function getMenuOrder()
    {
        return $this->menuOrder;
    }

    /**
     * Set the value of menuOrder
     *
     * @return  self
     */ 
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }

    /*************************************** CRUD and other admin methods  ********************************/
    
    public function create() {
        $db = new Database();
        $request= "INSERT INTO menus (menuTitle, menuOrder) VALUES (?,?)";
        $args=[
            $this->menuTitle,
            $this->menuOrder,
        ];
        return $db->insert(
            $request,
            $args
            );
    }

    public static function read($menuId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from menus WHERE menuId LIKE ?",                // SQL request
            array($menuId),                                           // SQL request parameters
            "Menu"                                                    // class to be used
        );                 
    }

    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE menus SET menuTitle=?, menuOrder=? , menuDateUpdated=? WHERE menuId=?";
        $args=[
            $this->menuTitle,
            $this->menuOrder,
            $updateDate,
            $this->menuId
        ];

        return $db->update(
            $request,
            $args
            );
    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM menus WHERE menuId=?";
        $params=[
            $this->menuId,
        ];
        return $db->delete($request,$params);
    }

    public static function readAll () :array 
    {
        $db=new Database();
        $menus=$db->getMany(
            "SELECT * from menus order by menuOrder",
            "Menu" 
        );
        return $menus;
    }


}
