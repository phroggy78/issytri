<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * Document class implements base methods for Document object. 
 * 
 */

class Document {
    private $documentId;                                   // Unique document Identifier
    private $documentDateCreated;                          // Date document was created
    private $documentDateUpdated;                          // Date document was last updated
    private $documentPath;                                 // Document file path on server
    private $documentLegend;                               // Document legend - displayed with document and used for search purposes
    private $documentText;                                 // Additional Document text

    /******************************************* Getters and setters  *************************************************/

    /**
     * Get the value of documentId
     */ 
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Get the value of documentDateCreated
     */ 
    public function getDocumentDateCreated()
    {
        return $this->documentDateCreated;
    }


    /**
     * Get the value of documentPath
     */ 
    public function getDocumentPath()
    {
        return $this->documentPath;
    }

    /**
     * Set the value of documentPath
     *
     * @return  self
     */ 
    public function setDocumentPath($documentPath)
    {
        $this->documentPath = $documentPath;

        return $this;
    }


    /**
     * Get the value of documentLegend
     */ 
    public function getDocumentLegend()
    {
        return $this->documentLegend;
    }

    /**
     * Set the value of documentLegend
     *
     * @return  self
     */ 
    public function setDocumentLegend($documentLegend)
    {
        $this->documentLegend = $documentLegend;

        return $this;
    }

    /**
     * Get the value of documentText
     */ 
    public function getDocumentText()
    {
        return $this->documentText;
    }

    /**
     * Set the value of documentText
     *
     * @return  self
     */ 
    public function setDocumentText($documentText)
    {
        $this->documentText = $documentText;

        return $this;
    }

    /********************************************************* CRUD *************************************************/

    public function create() {

        $db = new Database();
        $request= "INSERT INTO documents (documentPath,documentLegend, documentText) VALUES (?,?,?)";
        $args=[
            $this->documentPath,
            $this->documentLegend,
            $this->documentText
        ];
        $result= $db->insert(
            $request,
            $args
            );
        $this->documentId=$db->getLastId();
        return $result;   
    }

    public static function read ($documentId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from documents WHERE documentId LIKE ?",             // SQL request
            array($documentId),                                           // SQL request parameters
            "Document"                                                    // class to be used
    );  
    }


    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE documents SET 
                        documentLegend=?,
                        documentText=?,
                        documentDateUpdated=?
                        WHERE documentId=?";
        $args=[
            $this->documentLegend,
            $this->documentText,
            $updateDate,
            $this->documentId
        ];

        return $db->update(
            $request,
            $args
            );

    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM documents WHERE documentId=?";
        $params=[
            $this->documentId,
        ];
        return $db->delete($request,$params);      
    }

    public static function readAll() {
        $db=new Database();
        $documents=$db->getMany(
            "SELECT * from documents",
            "Document" 
        );
        return $documents;   
    }
}