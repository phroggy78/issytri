<?php 

/**
 * Author : Philippe Roggeband - March 2020
 * Version 1.0
 * Change History : 
 * 
 * Class used to associate images in the library with objects : Page, Section, Article or Tile
 * 
 * Methods will be used by page render and front page view to retrieve images and display them
 * 
 * Multiple images can be associated with a signle object. It is up to the calling controller to select the appropriate display mode : 
 *  Single image
 *  Single random image
 *  Image Slider
 * 
 * The image is known by it's identifier
 * The object is known by the combination (class, id)
 * 
 */

 class ImageObject {
     private $imageObjectId ;                       // Unique identifier for relationship     
     private $imageObjectDateCreated;               // Creation date
     private $imageId;                              // Pointer to image
     private $objectClass;                          // Object class
     private $objectId;                             // Unique object Id within class

     /********************************************* Getters and setters  *********************************************/

     

     /**
      * Get the value of imageObjectId
      */ 
     public function getImageObjectId()
     {
          return $this->imageObjectId;
     }

     /**
      * Get the value of imageObjectDateCreated
      */ 
     public function getImageObjectDateCreated()
     {
          return $this->imageObjectDateCreated;
     }

     /**
      * Get the value of imageId
      */ 
     public function getImageId()
     {
          return $this->imageId;
     }

     /**
      * Set the value of imageId
      *
      * @return  self
      */ 
     public function setImageId($imageId)
     {
          $this->imageId = $imageId;

          return $this;
     }

     /**
      * Get the value of objectClass
      */ 
     public function getObjectClass()
     {
          return $this->objectClass;
     }

     /**
      * Set the value of objectClass
      *
      * @return  self
      */ 
     public function setObjectClass($objectClass)
     {
          $this->objectClass = $objectClass;

          return $this;
     }

     /**
      * Get the value of objectId
      */ 
     public function getObjectId()
     {
          return $this->objectId;
     }

     /**
      * Set the value of objectId
      *
      * @return  self
      */ 
     public function setObjectId($objectId)
     {
          $this->objectId = $objectId;

          return $this;
     }

     /*************************************************** CRUD methods  ************************************/

     public function create() {
        $db = new Database();
        $request= "INSERT INTO imageObjects (imageId, objectClass, objectId) VALUES (?,?,?)";
        $args=[
            $this->imageId,
            $this->objectClass,
            $this->objectId
        ];
        return $db->insert(
            $request,
            $args
            );
     }

     public static function read ( $imageObjectId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from imageObjects WHERE tagObjectId LIKE ?",           // SQL request
            array($imageObjectId),                                           // SQL request parameters
            "ImageObject"                                                    // class to be used
        ); 
     }

     public function delete ():bool {
        $db=new Database();
        $request="DELETE FROM imageObjects WHERE imageObjectId=?";
        $params=[
            $this->imageObjectId,
        ];
        return $db->delete($request,$params);    

     }

     /************************************************** Base methods  *************************************/

     /**
      * Return all relationships for a given image 
      *
      */
    public static function readAllByImageId (int $imageId) {
        $db=new Database();
        $request= "SELECT * from imageObjects where imageId=? ";
        $args=[
            $imageId,
        ];
        $imageObjects=$db->getMany(
            $request,
            "ImageObject",
            $args
        );
        return $imageObjects;   
    }

    /**
     * Return all relationships for a given object
     */
    public static function readAllByObject ($objectClass, $objectId) {
        $db=new Database();
        $request= "SELECT * FROM imageObjects WHERE objectClass=? AND objectId=? ";
        $args=[
            $objectClass,
            $objectId
        ];
        $imageObjects=$db->getMany(
            $request,
            "ImageObject",
            $args
        );
        return $imageObjects;   
    }

    public static function deleteImageObjectsByImageId ( $imageId) {
        $db=new Database();
        $request="DELETE FROM imageObjects WHERE imageId=?  ";
        $params=[
            $imageId
        ];
        return $db->delete($request,$params);   
    }

    public static function deleteImageObject ( $imageId,  $objectClass, $objectId) {
        $db=new Database();
        $request="DELETE FROM imageObjects WHERE imageId=? AND objectClass=? AND objectId=? ";
        $params=[
            $imageId,
            $objectClass,
            $objectId
        ];
        return $db->delete($request,$params);   
    }


    public static function deleteImageObjectsByObject ( $objectClass, $objectId) {
        $db=new Database();
        $request="DELETE FROM imageObjects WHERE objectClass=? AND objectId=? ";
        $params=[
            $objectClass,
            $objectId
        ];
        return $db->delete($request,$params);   
    }
 }