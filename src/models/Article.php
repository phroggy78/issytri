<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change history 
 * 
 * Article class - An article is the base element of the site. 
 * 
 * Implements base CRUD methods for article method
 * 
 * Additional methods : 
 *  search for articles by parent section ID
 */
class Article {
    private $articleId ;                                    // Unique article ID
    private $articleTitle ;                                 // 
    private $articleDateCreated ;                           // Date article was created
    private $articlePublicationDate ;                       // Date at which article should be made visible to the users
    private $articleDateUpdated ;                           // Date article was last updated
    private $articleExpiryDate ;                            // Date after which article should no longer be visible to users
    private $articleContent ;                               // Article content in HTML
    private $articleOrder ;                                 // Display order for article (low order articles get displayed first)
    private $articleStatus ;                                // Article can be hidden (regardless of dates), preview (only admin sees it) or public
    private $articleSectionId ;                             // Article's parent section ID

    /*********************************************** Getters and setters ************************************************/
    /**
     * Get the value of articleId
     */ 
    public function getArticleId()
    {
        return $this->articleId;
    }

    /**
     * Get the value of articleTitle
     */ 
    public function getArticleTitle()
    {
        return $this->articleTitle;
    }

    /**
     * Set the value of articleTitle
     *
     * @return  self
     */ 
    public function setArticleTitle($articleTitle)
    {
        $this->articleTitle = $articleTitle;

        return $this;
    }

    /**
     * Get the value of articleDateCreated
     */ 
    public function getArticleDateCreated()
    {
        return $this->articleDateCreated;
    }


    /**
     * Get the value of articleDateUpdated
     */ 
    public function getArticleDateUpdated()
    {
        return $this->articleDateUpdated;
    }

    /**
     * Set the value of articleDateUpdated
     *
     * @return  self
     */ 
    public function setArticleDateUpdated($articleDateUpdated)
    {
        $this->articleDateUpdated = $articleDateUpdated;

        return $this;
    }

    /**
     * Get the value of articlePublicationDate
     */ 
    public function getArticlePublicationDate()
    {
        return $this->articlePublicationDate;
    }

    /**
     * Set the value of articlePublicationDate
     *
     * @return  self
     */ 
    public function setArticlePublicationDate($articlePublicationDate)
    {
        $this->articlePublicationDate = $articlePublicationDate;

        return $this;
    }

    /**
     * Get the value of articleExpiryDate
     */ 
    public function getArticleExpiryDate()
    {
        return $this->articleExpiryDate;
    }

    /**
     * Set the value of articleExpiryDate
     *
     * @return  self
     */ 
    public function setArticleExpiryDate($articleExpiryDate)
    {
        $this->articleExpiryDate = $articleExpiryDate;

        return $this;
    }

    /**
     * Get the value of articleContent
     */ 
    public function getArticleContent()
    {
        return $this->articleContent;
    }

    /**
     * Set the value of articleContent
     *
     * @return  self
     */ 
    public function setArticleContent($articleContent)
    {
        $this->articleContent = $articleContent;

        return $this;
    }

    /**
     * Get the value of articleOrder
     */ 
    public function getArticleOrder()
    {
        return $this->articleOrder;
    }

    /**
     * Set the value of articleOrder
     *
     * @return  self
     */ 
    public function setArticleOrder($articleOrder)
    {
        $this->articleOrder = $articleOrder;

        return $this;
    }

    /**
     * Get the value of articleStatus
     */ 
    public function getArticleStatus()
    {
        return $this->articleStatus;
    }

    /**
     * Set the value of articleStatus
     *
     * @return  self
     */ 
    public function setArticleStatus($articleStatus)
    {
        $this->articleStatus = $articleStatus;

        return $this;
    }



    /**
     * Get the value of articleSectionId
     */ 
    public function getArticleSectionId()
    {
        return $this->articleSectionId;
    }

    /**
     * Set the value of articleSectionId
     *
     * @return  self
     */ 
    public function setArticleSectionId($articleSectionId)
    {
        $this->articleSectionId = $articleSectionId;

        return $this;
    }

     /*************************************** CRUD and other admin methods  ********************************/
    
     public function create() {
        $db = new Database();
        $request= "INSERT INTO articles (articleTitle, articleOrder, articleContent, articleSectionId, articlePublicationDate, articleExpiryDate, articleStatus) VALUES (?,?,?,?,?,?,?)";
        $args=[
            $this->articleTitle,
            $this->articleOrder,
            $this->articleContent,
            $this->articleSectionId,
            $this->articlePublicationDate,
            $this->articleExpiryDate,
            $this->articleStatus
        ];
        $result= $db->insert(
            $request,
            $args
            );
        $this->articleId=$db->getLastId();
        return $result;   

    }

    public static function read($articleId) : Article {
        $db=new Database();
        $result= $db->getOne (
            "SELECT * from articles WHERE articleId LIKE ?",             // SQL request
            array($articleId),                                           // SQL request parameters
            "Article"                                                    // class to be used
            );  
        return $result;
    }

    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE articles SET articleTitle=?, 
                        articleOrder=?,
                        articleContent=?,
                        articleSectionId=?,
                        articlePublicationDate=?, 
                        articleExpiryDate=?,
                        articleStatus=?,
                        articleDateUpdated=? 
                        WHERE articleId=?";
        $args=[
            $this->articleTitle,
            $this->articleOrder,
            $this->articleContent,
            $this->articleSectionId,
            $this->articlePublicationDate,
            $this->articleExpiryDate,
            $this->articleStatus,
            $updateDate,
            $this->articleId
        ];

        return $db->update(
            $request,
            $args
            );
    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM articles WHERE articleId=?";
        $params=[
            $this->articleId,
        ];
        return $db->delete($request,$params);      
    }

    public static  function readAll () : array 
    {
        $db=new Database();
        $articles=$db->getMany(
            "SELECT * from articles order by articleSectionId, articleOrder",
            "Article" 
        );
        return $articles;   
    }

    public static function readAllBySectionId ($sectionId) : array 
    {
        $db=new Database();
        $request= "SELECT * from articles where articleSectionId=? order by articleOrder";
        $args=[
            $sectionId,
        ];
        $articles=$db->getMany(
            $request,
            "Article",
            $args
        );
        return $articles;   
    }
}