<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0 
 * Change History : 
 * 
 * 
 * Model to define website user profiles. Initial version is only used for admin profiles. 
 */

class User {
    private $userId ;
    private $lastName ;
    private $firstName ;
    private $email ;
    private $password ;
    private $role ; 
    private $passwordChanged;


    /********************************************** Getters and setters  ************************************************/
    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get the value of lastName
     */ 
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @return  self
     */ 
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Get the value of firstName
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @return  self
     */ 
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get the value of role
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    /**
     * Set the value of role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }


    /**
     * Get the value of passwordChanged
     */ 
    public function getPasswordChanged()
    {
        return $this->passwordChanged;
    }

    /**
     * Set the value of passwordChanged
     *
     * @return  self
     */ 
    public function setPasswordChanged($passwordChanged)
    {
        $this->passwordChanged = $passwordChanged;

        return $this;
    }
    /************************************CRUD methods  *****************************/

    public function create() {
        $db = new Database();
        $request= "INSERT INTO users (lastName, firstName, email, `password`, `role`, passwordChanged) VALUES (?,?,?,?,?,?)";
        $args=[
            $this->lastName,
            $this->lastName,
            $this->email,
            password_hash($this->password, PASSWORD_BCRYPT),
            $this->role,
            false
        ];
        return $db->insert(
            $request,
            $args
            );
    }

    public static function read($userId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from users WHERE userId LIKE ?",                // SQL request
            array($userId),                                           // SQL request parameters
            "User"                                                 // class to be used
        );
    }

    public function update() {

        $db = new Database();
        $request= "UPDATE users SET email=?, firstName=?, lastName=?, `role`=? WHERE userId=?";
        $args=[
            $this->email,
            $this->firstName,
            $this->lastName,
            $this->role,
            $this->userId
        ];

        return $db->update(
            $request,
            $args
            );

    }

    /**
     * Update Password - rehash  before updating database
     *
     * @return boolval
     */
    public function updatePassword() : bool {

        $db = new Database();
        $request= "UPDATE users SET `password`=?, passwordChanged=? WHERE userId=?";

        $args=[
            password_hash($this->password, PASSWORD_BCRYPT),
            intval($this->passwordChanged),                 // false if password is reset by admin for the user, true if password is changed by user himself
            $this->userId
        ];

        $result= $db->update(
            $request,
            $args
            );
        return $result; 
    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM users WHERE userId=?";
        $params=[
            $this->userId,
        ];
        return $db->delete($request,$params);
    }

    /************************************* Other methods *****************************/

    /**
     * return array of Users from database
     *
     * @return array
     */
    public static function readAll():array {
        $db=new Database();
        $users=$db->getMany(
            "SELECT * from users",
            "User" 
        );
        return $users;
    
    }

    /**
     * Retrieve user details using Email address as the key 
     *
     * @param string $email
     * @return void
     */
    public static function readByEmail(string $email)
    {
       $db=new Database();
       $user= $db->getOne(
           "SELECT * from users WHERE email LIKE ?",                // SQL request
           array($email),                                          // SQL request parameters
           "User"                                                   // class to be used
       );
       return $user;
    }



}