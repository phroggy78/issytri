<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History
 *  June 2020 - Add layout attribute
 * A section is used to group articles with a similar topic. 
 * A section is the child of a a page. 
 */
class Section {
    private $sectionId ;                                // Unique section ID
    private $sectionTitle ;                             // SectionTitle
    private $sectionDateCreated ;                       // Date section was created
    private $sectionDatePublished ;                     // Date on which section should be published (not used in initial version)
    private $sectionDateUpdated ;                       // Date section was last updated
    private $sectionContent ;                           // Section text / content in HTML code
    private $sectionOrder ;                             // Order in which sections should be displayed in a page
    private $sectionPageId ;                            // Parent page ID
    private $sectionLayout ;                            // Define layout format for section

    /*********************************************** Getters and setters **********************************************/
    /**
     * Get the value of sectionId
     */ 
    public function getSectionId()
    {
        return $this->sectionId;
    }

    /**
     * Get the value of sectionTitle
     */ 
    public function getSectionTitle()
    {
        return $this->sectionTitle;
    }

    /**
     * Set the value of sectionTitle
     *
     * @return  self
     */ 
    public function setSectionTitle($sectionTitle)
    {
        $this->sectionTitle = $sectionTitle;

        return $this;
    }

    /**
     * Get the value of sectionDateCreated
     */ 
    public function getSectionDateCreated()
    {
        return $this->sectionDateCreated;
    }

    /**
     * Get the value of sectionDatePublished
     */ 
    public function getSectionDatePublished()
    {
        return $this->sectionDatePublished;
    }

    /**
     * Set the value of sectionDatePublished
     *
     * @return  self
     */ 
    public function setSectionDatePublished($sectionDatePublished)
    {
        $this->sectionDatePublished = $sectionDatePublished;

        return $this;
    }

    /**
     * Get the value of sectionDateUpdated
     */ 
    public function getSectionDateUpdated()
    {
        return $this->sectionDateUpdated;
    }

    /**
     * Set the value of sectionDateUpdated
     *
     * @return  self
     */ 
    public function setSectionDateUpdated($sectionDateUpdated)
    {
        $this->sectionDateUpdated = $sectionDateUpdated;

        return $this;
    }

    /**
     * Get the value of sectionContent
     */ 
    public function getSectionContent()
    {
        return $this->sectionContent;
    }

    /**
     * Set the value of sectionContent
     *
     * @return  self
     */ 
    public function setSectionContent($sectionContent)
    {
        $this->sectionContent = $sectionContent;

        return $this;
    }

    /**
     * Get the value of sectionOrder
     */ 
    public function getSectionOrder()
    {
        return $this->sectionOrder;
    }

    /**
     * Set the value of sectionOrder
     *
     * @return  self
     */ 
    public function setSectionOrder($sectionOrder)
    {
        $this->sectionOrder = $sectionOrder;

        return $this;
    }

    /**
     * Get the value of sectionPageId
     */ 
    public function getSectionPageId()
    {
        return $this->sectionPageId;
    }

    /**
     * Set the value of sectionPageId
     *
     * @return  self
     */ 
    public function setSectionPageId($sectionPageId)
    {
        $this->sectionPageId = $sectionPageId;

        return $this;
    }



     /*************************************** CRUD and other admin methods  ********************************/
    
     public function create() {
        $db = new Database();
        $request= "INSERT INTO sections (sectionTitle, sectionOrder, sectionContent, sectionPageId, sectionLayout) VALUES (?,?,?,?,?)";
        $args=[
            $this->sectionTitle,
            $this->sectionOrder,
            $this->sectionContent,
            $this->sectionPageId,
            $this->sectionLayout
        ];
        $result= $db->insert(
            $request,
            $args
            );
        $this->sectionId=$db->getLastId();
        return $result;   

    }

    public static function read($sectionId) : Section {
        $db=new Database();
        return $db->getOne (
            "SELECT * from sections WHERE sectionId LIKE ?",                // SQL request
            array($sectionId),                                           // SQL request parameters
            "Section"                                                    // class to be used
    );   
    }

    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE sections SET sectionTitle=?, 
                                        sectionOrder=?,
                                        sectionLayout=?,
                                        sectionContent=?,
                                        sectionPageId=?,
                                        sectionDateUpdated=?  
                                        WHERE sectionId=?";
        $args=[
            $this->sectionTitle,
            $this->sectionOrder,
            $this->sectionLayout,
            $this->sectionContent,
            $this->sectionPageId,
            $updateDate,
            $this->sectionId
        ];

        return $db->update(
            $request,
            $args
            );


    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM sections WHERE sectionId=?";
        $params=[
            $this->sectionId,
        ];
        return $db->delete($request,$params);    
    }

    public static function readAll () : array 
    {
        $db=new Database();
        $sections=$db->getMany(
            "SELECT * from sections order by sectionPageId, sectionOrder",
            "Section" 
        );
        return $sections;   
    }

    public static function readAllByPageId ($pageId) : array 
    {
        $db=new Database();
        $request= "SELECT * from sections where sectionPageId=? order by sectionOrder";
        $args=[
            $pageId,
        ];
        $sections=$db->getMany(
            $request,
            "Section",
            $args
        );
        return $sections;   
    }


    /**
     * Get the value of sectionLayout
     */ 
    public function getSectionLayout()
    {
        return $this->sectionLayout;
    }

    /**
     * Set the value of sectionLayout
     *
     * @return  self
     */ 
    public function setSectionLayout($sectionLayout)
    {
        $this->sectionLayout = $sectionLayout;

        return $this;
    }
}