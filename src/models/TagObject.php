<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * Class used to create a relationship between a tag and an object (page, section,article or image)
 * 
 * this class is used by the search engine to perform searches on keywords (tags) associated with the objects
 */
class TagObject {
    private $tagObjectId ;                      
    private $tagObjectDateCreated;
    private $tagId;
    private $objectClass;
    private $objectId;

    /**
     * Get the value of tagObjectId
     */ 
    public function getTagObjectId()
    {
        return $this->tagObjectId;
    }

    /**
     * Get the value of tagObjectDateCreated
     */ 
    public function getTagObjectDateCreated()
    {
        return $this->tagObjectDateCreated;
    }

    /**
     * Get the value of tagId
     */ 
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * Set the value of tagId
     *
     * @return  self
     */ 
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;

        return $this;
    }

    /**
     * Get the value of objectClass
     */ 
    public function getObjectClass()
    {
        return $this->objectClass;
    }

    /**
     * Set the value of objectClass
     *
     * @return  self
     */ 
    public function setObjectClass($objectClass)
    {
        $this->objectClass = $objectClass;

        return $this;
    }

    /**
     * Get the value of objectId
     */ 
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set the value of objectId
     *
     * @return  self
     */ 
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /***************************************************** CRUD functions ******************************/

    /**
     * Create a relationship between a tag and an object.
     *
     * @return boolean
     */
    public function create() :bool {
        $db = new Database();
        $request= "INSERT INTO tagObjects (tagId, objectClass, objectId) VALUES (?,?,?)";
        $args=[
            $this->tagId,
            $this->objectClass,
            $this->objectId
        ];
        return $db->insert(
            $request,
            $args
            );
    }

    public static function read($tagObjectId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from tagObjects WHERE tagObjectId LIKE ?",           // SQL request
            array($tagObjectId),                                           // SQL request parameters
            "TagObject"                                                    // class to be used
        ); 
    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM tagObjects WHERE tagObjectId=?";
        $params=[
            $this->tagObjectId,
        ];
        return $db->delete($request,$params);    
    }

    /********************************* search methods  **************************************************/

    /**
     * Returns an array 
     *
     * @param [type] $tagId
     * @return void
     */
    public static function readAllByTagId ($tagId) {
        $db=new Database();
        $request= "SELECT * from tagObjects where tagId=? ";
        $args=[
            $tagId,
        ];
        $tagObjects=$db->getMany(
            $request,
            "TagObject",
            $args
        );
        return $tagObjects;   
    }

    public static function readAllByObject ($objectClass,$objectId) {
        $db=new Database();
        $request= "SELECT * FROM tagObjects WHERE objectClass=? AND objectId=? ";
        $args=[
            $objectClass,
            $objectId
        ];
        $tagObjects=$db->getMany(
            $request,
            "TagObject",
            $args
        );
        return $tagObjects;   
    }

    public static function readAllByTagObject ($tagId,$objectClass,$objectId) {
        $db=new Database();
        $request= "SELECT * FROM tagObjects WHERE tagId=? AND objectClass=? AND objectId=? ";
        $args=[
            $tagId,
            $objectClass,
            $objectId
        ];

        $tagObjects=$db->getMany(
            $request,
            "TagObject",
            $args
        );
        return $tagObjects;   
    }

    public static function deleteTagObjectsByTagId ($tagId) {
        $db=new Database();
        $request="DELETE FROM tagObjects WHERE tagId=?  ";
        $params=[
            $tagId
        ];
        return $db->delete($request,$params);   
    }

    public static function untagObject ($tagId, $objectClass,$objectId) {
        $db=new Database();
        $request="DELETE FROM tagObjects WHERE tagId=? AND objectClass=? AND objectId=? ";
        $params=[
            $tagId,
            $objectClass,
            $objectId
        ];
        $result= $db->delete($request,$params);   
        return $result;
    }


    public static function deleteTagObjectsByObject ($objectClass,$objectId) {
        $db=new Database();
        $request="DELETE FROM tagObjects WHERE objectClass=? AND objectId=? ";
        $params=[
            $objectClass,
            $objectId
        ];
        return $db->delete($request,$params);   
    }
}