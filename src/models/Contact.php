<?php 

class Contact {
    private $contactId;
    private $contactLastName;
    private $contactFirstName;
    private $contactEmail;
    private $contactPrivacyStatement;               // 1 = accepted by viewer
    private $contactMessage;
    private $contactStatus;                         // 1 = created, 0 = processed
    private $contactDateCreated;
    private $contactDateUpdated;


    /******************************************** Getters and setters  *******************************************/

    

    /**
     * Get the value of contactId
     */ 
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Get the value of contactLastName
     */ 
    public function getContactLastName()
    {
        return $this->contactLastName;
    }

    /**
     * Set the value of contactLastName
     *
     * @return  self
     */ 
    public function setContactLastName($contactLastName)
    {
        $this->contactLastName = $contactLastName;

        return $this;
    }

    /**
     * Get the value of contactFirstName
     */ 
    public function getContactFirstName()
    {
        return $this->contactFirstName;
    }

    /**
     * Set the value of contactFirstName
     *
     * @return  self
     */ 
    public function setContactFirstName($contactFirstName)
    {
        $this->contactFirstName = $contactFirstName;

        return $this;
    }

    /**
     * Get the value of contactEmail
     */ 
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set the value of contactEmail
     *
     * @return  self
     */ 
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get the value of contactPrivacyStatement
     */ 
    public function getContactPrivacyStatement()
    {
        return $this->contactPrivacyStatement;
    }

    /**
     * Set the value of contactPrivacyStatement
     *
     * @return  self
     */ 
    public function setContactPrivacyStatement($contactPrivacyStatement)
    {
        $this->contactPrivacyStatement = $contactPrivacyStatement;

        return $this;
    }

    /**
     * Get the value of contactMessage
     */ 
    public function getContactMessage()
    {
        return $this->contactMessage;
    }

    /**
     * Set the value of contactMessage
     *
     * @return  self
     */ 
    public function setContactMessage($contactMessage)
    {
        $this->contactMessage = $contactMessage;

        return $this;
    }

    /**
     * Get the value of contactStatus
     */ 
    public function getContactStatus()
    {
        return $this->contactStatus;
    }

    /**
     * Set the value of contactStatus
     *
     * @return  self
     */ 
    public function setContactStatus($contactStatus)
    {
        $this->contactStatus = $contactStatus;

        return $this;
    }

    /**
     * Get the value of contactDateCreated
     */ 
    public function getContactDateCreated()
    {
        return $this->contactDateCreated;
    }

    /**
     * Get the value of contactDateUpdated
     */ 
    public function getContactDateUpdated()
    {
        return $this->contactDateUpdated;
    }

    /**
     * Set the value of contactDateUpdated
     *
     * @return  self
     */ 
    public function setContactDateUpdated($contactDateUpdated)
    {
        $this->contactDateUpdated = $contactDateUpdated;

        return $this;
    }


    /********************************************* CRUD  ******************************************************/

    public function create() {
        $db = new Database();
        $request= "INSERT INTO contacts (contactLastName,
                                         contactFirstName,
                                         contactEmail,
                                         contactPrivacyStatement,
                                         contactMessage,
                                         contactStatus) VALUES (?,?,?,?,?,?)";
        $args=[
            $this->contactLastName,
            $this->contactFirstName,
            $this->contactEmail,
            $this->contactPrivacyStatement,
            $this->contactMessage,
            $this->contactStatus
        ];
        $result= $db->insert(
            $request,
            $args
            );
        $this->contactId=$db->getLastId();
        return $result;   
    }

    public static function read($contactId){
        $db=new Database();
        return $db->getOne (
            "SELECT * from contacts WHERE contactId LIKE ?",             // SQL request
            array($contactId),                                           // SQL request parameters
            "Contact"                                                    // class to be used
    );  
    }

    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE contacts SET 
                        contactLastName=?,
                        contactFirstName=?,
                        contactEmail=?,
                        contactPrivacyStatement=?,
                        contactMessage=?,
                        contactStatus=?,
                        contactDateUpdated=?
                        WHERE contactId=?";
        $args=[
            $this->contactLastName,
            $this->contactFirstName,
            $this->contactEmail,
            $this->contactPrivacyStatement,
            $this->contactMessage,
            $this->contactStatus,
            $updateDate,
            $this->contactId
        ];

        return $db->update(
            $request,
            $args
            );
    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM contacts WHERE contactId=?";
        $params=[
            $this->contactId,
        ];
        return $db->delete($request,$params);      
    }

    /************************************************ Additional methods  **************************************/

    public static function readAll() {
        $db=new Database();
        $contacts=$db->getMany(
            "SELECT * from contacts",
            "Contact" 
        );
        return $contacts;   
    }

    public static function readAllByStatus ($contactStatus) {
        $db=new Database();
        $request= "SELECT * from contacts where contactStatus=? ";
        $args=[
            $contactStatus,
        ];
        $contacts=$db->getMany(
            $request,
            "Contact",
            $args
        );
        return $contacts;   
    }
}