<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History
 * 
 *  Page model. Top level content element in web site. 
 *  A page belongs to a menu. NavBar is built using menu titles and page titles. 
 * 
 *  A page contains sections. 
 * 
 */

class Page {
    
    private $pageId ;                                   // Unique identifier
    private $pageTitle ;                                // Page title heading
    private $pageOrder;                                 // Order number used to build the dynamic menu
    private $pageDateCreated;                           // Default timestamp
    private $pageDateUpdated;                           // Date page was last updated 
    private $pageContent;                               // Page intro text
    private $pageMenuId;                                // Menu from which this page is accessed

    /**
     * Get the value of pageId
     */ 
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * Get the value of pageTitle
     */ 
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * Set the value of pageTitle
     *
     * @return  self
     */ 
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * Get the value of pageOrder
     */ 
    public function getPageOrder()
    {
        return $this->pageOrder;
    }
    /**
     * Set the value of pageOrder
     *
     * @return  self
     */ 
    public function setPageOrder($pageOrder)
    {
        $this->pageOrder = $pageOrder;

        return $this;
    }

    /**
     * Get the value of pageDateCreated
     */ 
    public function getPageDateCreated()
    {
        return $this->pageDateCreated;
    }

    /**
     * Get the value of pageDateUpdated
     */ 
    public function getPageDateUpdated()
    {
        return $this->pageDateUpdated;
    }

    /**
     * Set the value of pageDateUpdated
     *
     * @return  self
     */ 
    public function setPageDateUpdated($pageDateUpdated)
    {
        $this->pageDateUpdated = $pageDateUpdated;

        return $this;
    }

    /**
     * Get the value of pageContent
     */ 
    public function getPageContent()
    {
        return $this->pageContent;
    }

    /**
     * Set the value of pageContent
     *
     * @return  self
     */ 
    public function setPageContent($pageContent)
    {
        $this->pageContent = $pageContent;

        return $this;
    }

    /**
     * Get the value of pageMenuId
     */ 
    public function getPageMenuId()
    {
        return $this->pageMenuId;
    }

    /**
     * Set the value of pageMenuId
     *
     * @return  self
     */ 
    public function setPageMenuId($pageMenuId)
    {
        $this->pageMenuId = $pageMenuId;

        return $this;
    }

        /*************************************** CRUD and other admin methods  ********************************/
    
        public function create() {

            $db = new Database();
            $request= "INSERT INTO pages (pageTitle, pageOrder, pageContent, pageMenuId) VALUES (?,?,?,?)";
            $args=[
                $this->pageTitle,
                $this->pageOrder,
                $this->pageContent,
                $this->pageMenuId,
            ];
            $result= $db->insert(
                $request,
                $args
                );
            $this->pageId=$db->getLastId();
            return $result;   
        }
    
        public static function read($pageId) : Page {
            $db=new Database();
            return $db->getOne (
                "SELECT * from pages WHERE pageId LIKE ?",                // SQL request
                array($pageId),                                           // SQL request parameters
                "Page"                                                    // class to be used
        );     
        }
    
        public function update() {
            $today=new DateTime('now');
            $updateDate=$today->format('Y-m-d H:i:s');
            $db = new Database();
            $request= "UPDATE pages SET pageTitle=?, pageOrder=?,pageContent=?,pageMenuId=?,pageDateUpdated=?  WHERE pageId=?";
            $args=[
                $this->pageTitle,
                $this->pageOrder,
                $this->pageContent,
                $this->pageMenuId,
                $updateDate,
                $this->pageId
            ];
    
            return $db->update(
                $request,
                $args
                );
    
        }
    
        public function delete() {
            $db=new Database();
            $request="DELETE FROM pages WHERE pageId=?";
            $params=[
                $this->pageId,
            ];
            return $db->delete($request,$params);    
        }

        /**
         * Retrieve list of all pages in the database as an array
         *
         * @return array
         */
        public static function readAll () : array 
        {
            $db=new Database();
            $pages=$db->getMany(
                "SELECT * from pages order by pageMenuId, pageOrder",
                "Page" 
            );
            return $pages;            
        }

        public static function readAllByMenuId ($menuId) : array 
        {
            $db=new Database();
            $request= "SELECT * from pages where pageMenuId=? order by pageOrder";
            $args=[
                $menuId,
            ];
            $pages=$db->getMany(
                $request,
                "Page",
                $args
            );
            return $pages;   
        }
}