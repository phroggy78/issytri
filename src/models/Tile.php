<?php 

/** 
 * Author : Philippe Roggeband - March 2020
 * Version 1.0 
 * Change History:
 * 
 * This class is used for important news items, or informations to be highlighted, which typicall only have a temporary validity. 
 * The mechanism used is a combination of cards for top-level content, and Modal for the more detailed content. 
 * 
 * Tiles only have one image (illusration) associated with them. They can however point to an article which may have more detailed content. 
 * 
 */

 class Tile {
     private $tileId;                               // Unique system allocated identifier
     private $tileTitle;                            // Tile title 
     private $tileDateCreated;                      //     
     private $tileDateUpdated ;                     // Date of last updated
     private $tileOrder;                            // Display order 
     private $tileImportance;                       // Determines background display color
     private $tileImage;                            // Image displayed as part of tile
     private $tileContent;                          // Detailed content displayed in modal mode
     private $tileStatus;                           // Tiles can be in preview mode, hidden, or active
     private $tilePublicationDate;                  // Date after which a tile should be made visible
     private $tileExpiryDate;                       // Date after which a tile should no longer be visible


     /*************************************** Getters and setters  *************************************************/
     

     /**
      * Get the value of tileId
      */ 
     public function getTileId()
     {
          return $this->tileId;
     }

     /**
      * Get the value of tileDateCreated
      */ 
     public function getTileDateCreated()
     {
          return $this->tileDateCreated;
     }

     /**
      * Get the value of tileDateUpdated
      */ 
     public function getTileDateUpdated()
     {
          return $this->tileDateUpdated;
     }

     /**
      * Set the value of tileDateUpdated
      *
      * @return  self
      */ 
     public function setTileDateUpdated($tileDateUpdated)
     {
          $this->tileDateUpdated = $tileDateUpdated;

          return $this;
     }

     /**
      * Get the value of tileTitle
      */ 
     public function getTileTitle()
     {
          return $this->tileTitle;
     }

     /**
      * Set the value of tileTitle
      *
      * @return  self
      */ 
     public function setTileTitle($tileTitle)
     {
          $this->tileTitle = $tileTitle;

          return $this;
     }

     /**
      * Get the value of tileOrder
      */ 
     public function getTileOrder()
     {
          return $this->tileOrder;
     }

     /**
      * Set the value of tileOrder
      *
      * @return  self
      */ 
     public function setTileOrder($tileOrder)
     {
          $this->tileOrder = $tileOrder;

          return $this;
     }

     /**
      * Get the value of tileImportance
      */ 
     public function getTileImportance()
     {
          return $this->tileImportance;
     }

     /**
      * Set the value of tileImportance
      *
      * @return  self
      */ 
     public function setTileImportance($tileImportance)
     {
          $this->tileImportance = $tileImportance;

          return $this;
     }

     /**
      * Get the value of tileImage
      */ 
      public function getTileImage()
      {
           return $this->tileImage;
      }
 
      /**
       * Set the value of tileImage
       *
       * @return  self
       */ 
      public function setTileImage($tileImage)
      {
           $this->tileImage = $tileImage;
 
           return $this;
      }

     /**
      * Get the value of tileContent
      */ 
     public function getTileContent()
     {
          return $this->tileContent;
     }

     /**
      * Set the value of tileContent
      *
      * @return  self
      */ 
     public function setTileContent($tileContent)
     {
          $this->tileContent = $tileContent;

          return $this;
     }

     /**
      * Get the value of tileStatus
      */ 
     public function getTileStatus()
     {
          return $this->tileStatus;
     }

     /**
      * Set the value of tileStatus
      *
      * @return  self
      */ 
     public function setTileStatus($tileStatus)
     {
          $this->tileStatus = $tileStatus;

          return $this;
     }

     /**
      * Get the value of tilePublicationDate
      */ 
     public function getTilePublicationDate()
     {
          return $this->tilePublicationDate;
     }

     /**
      * Set the value of tilePublicationDate
      *
      * @return  self
      */ 
     public function setTilePublicationDate($tilePublicationDate)
     {
          $this->tilePublicationDate = $tilePublicationDate;

          return $this;
     }

     /**
      * Get the value of tileExpiryDate
      */ 
      public function getTileExpiryDate()
      {
           return $this->tileExpiryDate;
      }
 
      /**
       * Set the value of tileExpiryDate
       *
       * @return  self
       */ 
      public function setTileExpiryDate($tileExpiryDate)
      {
           $this->tileExpiryDate = $tileExpiryDate;
 
           return $this;
      }

     /******************************************* CRUD methods  *******************************************/

     public function create() {

        $db = new Database();
        $request= "INSERT INTO tiles (  tileTitle, 
                                        tileOrder, 
                                        tileImportance, 
                                        tileImage,
                                        tileContent, 
                                        tileStatus, 
                                        tilePublicationDate, 
                                        tileExpiryDate) 
                                        VALUES (?,?,?,?,?,?,?,?)";                              
        $args=[
            $this->tileTitle,
            $this->tileOrder,
            $this->tileImportance,
            $this->tileImage,
            $this->tileContent,
            $this->tileStatus,
            $this->tilePublicationDate,
            $this->tileExpiryDate
        ];

        $result= $db->insert(
            $request,
            $args
            );
        $this->tileId=$db->getLastId();
        return $result;   

    }

    public static function read($tileId) : Tile {
        $db=new Database();
        $result= $db->getOne (
            "SELECT * from tiles WHERE tileId LIKE ?",             // SQL request
            array($tileId),                                           // SQL request parameters
            "Tile"                                                    // class to be used
            );  
        return $result;
    }

    public function update() {

        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE tiles SET 
                        tileTitle=?, 
                        tileOrder=?,
                        tileImportance=?,
                        tileImage=?,
                        tileContent=?,
                        tileStatus=?,
                        tilePublicationDate=?,
                        tileExpiryDate=?,
                        tileDateUpdated=?
                        WHERE tileId=?";
        $args=[
            $this->tileTitle,
            $this->tileOrder,
            $this->tileImportance,
            $this->tileImage,
            $this->tileContent,
            $this->tileStatus,
            $this->tilePublicationDate,
            $this->tileExpiryDate,
            $updateDate,
            $this->tileId
        ];

        return $db->update(
            $request,
            $args
            );
    }

    public function delete() {
        $db=new Database();
        $request="DELETE FROM tiles WHERE tileId=?";
        $params=[
            $this->tileId,
        ];
        return $db->delete($request,$params);      
    }

    public static function readAll () : array 
    {
        $db=new Database();
        $tiles=$db->getMany(
            "SELECT * from tiles order by tileOrder",
            "Tile" 
        );
        return $tiles;   
    }

    public static function readAllByImportance ($importance) : array 
    {
        $db=new Database();
        $request= "SELECT * from tiles WHERE tileImportance=? order by tileOrder";
        $params=[$importance];
        $tiles=$db->getMany(
            $request,
            "Tile",
            $params
        );
        return $tiles;   
    }
 }