<?php 


/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History:
 * 
 * 
 * Tag Class - can be attached to any item, is used for search purposes
 * 
 */

 class Tag {
    private $tagId;                                         // Unique Tag identifier
    private $tagName;                                       // Keyword to tag an item
    private $tagDescription;                                // Description - May be filled automatically by a controller at tag creation
    private $tagDateCreated;                                // Date tag was created
    private $tagDateUpdated;                                // Date tag was last updated (not used)


    /******************************************************* Getters and setters ******************************/
    /**
     * Get the value of tagId
     */ 
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * Get the value of tagName
     */ 
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * Set the value of tagName
     *
     * @return  self
     */ 
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;

        return $this;
    }

    /**
     * Get the value of tagDescription
     */ 
    public function getTagDescription()
    {
        return $this->tagDescription;
    }

    /**
     * Set the value of tagDescription
     *
     * @return  self
     */ 
    public function setTagDescription($tagDescription)
    {
        $this->tagDescription = $tagDescription;

        return $this;
    }

    /**
     * Get the value of tagDateCreated
     */ 
    public function getTagDateCreated()
    {
        return $this->tagDateCreated;
    }

    /**
     * Get the value of tagDateUpdated
     */ 
    public function getTagDateUpdated()
    {
        return $this->tagDateUpdated;
    }

    /**
     * Set the value of tagDateUpdated
     *
     * @return  self
     */ 
    public function setTagDateUpdated($tagDateUpdated)
    {
        $this->tagDateUpdated = $tagDateUpdated;

        return $this;
    }

    /******************************************** CRUD methods **********************************************/

    public function create() {
        $db = new Database();
        $request= "INSERT INTO tags (tagName, tagDescription) VALUES (?,?)";
        $args=[
            $this->tagName,
            $this->tagDescription,
        ];
        $result= $db->insert(
            $request,
            $args
            );
        $this->tagId=$db->getLastId();      // set Tag ID in object 
        return $result;
    }

    public static function read($tagId) {
        $db=new Database();
        return $db->getOne (
            "SELECT * from tags WHERE tagId LIKE ?",                // SQL request
            array($tagId),                                           // SQL request parameters
            "Tag"                                                    // class to be used
        );  
    }

    public function update() {
        $today=new DateTime('now');
        $updateDate=$today->format('Y-m-d H:i:s');
        $db = new Database();
        $request= "UPDATE tags SET tagName=?, tagDescription=? , tagDateUpdated=? WHERE tagId=?";
        $args=[
            $this->tagName,
            $this->tagDescription,
            $updateDate,
            $this->tagId
        ];

        return $db->update(
            $request,
            $args
            );
    }

    /**
     * The calling controller has the responsability of checking if any dependencies still exist
     *
     * @return void
     */
    public function delete() {
        $db=new Database();
        $request="DELETE FROM tags WHERE tagId=?";
        $params=[
            $this->tagId,
        ];
        return $db->delete($request,$params);
    }

    public static function readAll() {
        $db=new Database();
        $tags=$db->getMany(
            "SELECT * from tags",
            "Tag" 
        );
        return $tags;
    }

    public static function readByTagName($tagName){
        $db=new Database();
        return $db->getOne (
            "SELECT * from tags WHERE tagName LIKE ?",                // SQL request
            array($tagName),                                         // SQL request parameters
            "Tag"                                                     // class to be used
        );  
    }
 }
 
