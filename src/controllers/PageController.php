<?php

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * 
 * Change history:
 * 
 * 
 * This class implements Controller methods for Page objects.
 * 
 * All top-level functions are called via a URL of the following type : 
 *      admin.php?class=Page&action=<method>[&id=<pageId>]  for admin methods
 *      index.php?class=Page&action=<method>[&id=<pageId>]  for user methods
 * 
 * A page's parent is the menu in which it will appear. 
 * A page's direct child is a section. 
 * A page can be attached to tags via tagObjects. 
 * a page can include images, via imageObjects. 
 * 
 * It is built on top of model-implemented CRUD methods. 
 * 
 * Admin methods : 
 *      Index - Lists all pages, including parent menu
 *      Create - 
 *          Validate page parameters : title, content
 *          Tie Page to a menu object
 *          Tag page
 *          Associate images with page
 *      Update -
 *          Modify page content
 *          Tie page to a different menu
 *          Add / remove tags
 *          Add / remove images
 *      Delete -
 *          If page has section children, refuse delete
 *          Else 
 *              untag page
 *              remove image associations
 *              Delete page object
 * 
 *  User methods : 
 *      Display page -
 *       
 * 
 */

class PageController extends Controller implements Icontroller {


    /*********************************************************** Admin methods  ************************************/

    /**
     * List all pages, and the menu in which they are placed
     *
     * @return void
     */
    function index(){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            $this->redirectTo("User","login");
        } else
        {
            $pagesList=Page::readAll();                         // Retrieve list of pages  
            $menusList=Menu::readAll();                         // Retrieve list of menus  
            // build the table to be rendered
            $pages=[];

            foreach ($pagesList as $page) {
                array_push ( $pages,
                [
                    "pageId"=>$page->getPageId(),
                    "pageTitle"=>Lib::cleanHtml($page->getPageTitle()),
                    "pageOrder"=>$page->getPageOrder(),
                    "pageMenu"=>(Menu::read($page->getPageMenuId()))->getMenuTitle()
                ]
                );
            }
            $this->render("admin/pageIndex",$pages);        // Display list of users 
        }
    }

    /**
     * Create a new page record
     *
     * @return void
     */
    function create() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            $this->redirectTo("User","login");
        } 
        else {
            // if user has submitted a new page, enter it in the database
            if (isset($_POST['submit'])) {
                // check if all parameters have been entered
                if (isset($_POST['pageTitle'])&&
                    isset($_POST['pageOrder'])&&
                    isset($_POST['menuId'])&&
                    isset($_POST['pageContent'])
                )
                {
                    // Create Page object and set attributes
                    $newPage=new Page();
                    $newPage->setPageTitle(Lib::cleanHtml($_POST['pageTitle']));
                    $newPage->setPageOrder($_POST['pageOrder']);
                    $newPage->setPageMenuId($_POST['menuId']);
                    $newPage->setPageContent(htmlentities(Lib::cleanHtml($_POST['pageContent'])));
                    // Write object to database and retrieve id which is set by page->create() 
                    if ($newPage->create()) {
                        $newPageId=$newPage->getPageId();
                        $flash->success("Page $newPageId created");
                        // Add tags to the page object 
                        //
                        if (!empty($_POST["pageTags"])) {
                            $pageTags = explode (",", $_POST['pageTags']);

                            foreach ($pageTags as $pageTag) {
                                Lib::tagObject(ucfirst(strtolower($pageTag)),"Page",$newPageId);
                            }
                        }
                        //
                        $newImages=explode(",",$_POST['objectImages']);
                        foreach ($newImages as $newImage) {
                            Lib::imageObject($newImage,"Page",$newPageId) ;
                        }
                    }
                    else {
                        $flash->error("Error creating page");
                    }
                }
                else {
                    $flash->error("Missing parameters");
                } 
            }

            // Display page creation form 
            $menusList=Menu::readAll();                         // Retrieve list of menus  
            $menuTitles=[];
            foreach ($menusList as $menu) {
                array_push ( $menuTitles, 
                [
                    'menuId'=>$menu->getMenuId(),
                    'menuTitle'=>$menu->getMenuTitle()
                ]);
            }
            $this->render("admin/pageCreate", $menuTitles);                    // Display user add form              else {

        }
    }     

    /**
     * Update Page record 
     * 
     * @param [type] $pageId
     * @return void
     */
    function update($pageId) {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            $this->redirectTo("User","login");
        } 
        else {
            // prepare update page render 
            //
            $page=Page::read($pageId);
            $pageTags=TagObject::readAllByObject("Page",$pageId);      // Get the section Tags before Update
            $pageTagsArray=[];
            // convert to an array of tags for comparison with updated field 
            foreach ($pageTags as $pageTag) {
                array_push($pageTagsArray,Tag::read($pageTag->getTagId())->getTagName());
            }
            // Convert array to CSV format for display
            $pageTagsList=implode(",",$pageTagsArray);   
            if (isset($_POST['submit'])) {
                // check if all parameters have been entered
                if (isset($_POST['pageTitle'])&&
                    isset($_POST['pageOrder'])&&
                    isset($_POST['menuId'])&&
                    isset($_POST['pageContent'])
                    ) {
                        $page->setPageTitle(Lib::cleanHtml($_POST['pageTitle']));
                        $page->setPageOrder($_POST['pageOrder']);
                        $page->setPageMenuId($_POST['menuId']);
                        $page->setPageContent(htmlentities(Lib::cleanHtml($_POST['pageContent'])));
                        if($page->update()) {
                            $flash->success("Page updated");
                            // 
                            // Check if tags have been changed.
                            //
                            $updatedPageTags = explode (",", $_POST['pageTags']);
                            // Convert all tags to the same Format
                            for ($i=0; $i< count($updatedPageTags);$i++) {
                                $updatedPageTags[$i]=ucfirst(strtolower($updatedPageTags[$i]));
                            }

                            $newTags=array_diff($updatedPageTags,$pageTagsArray);
                            $removedTags=array_diff($pageTagsArray,$updatedPageTags);

                            foreach ($newTags as $newTag) {
                                Lib::tagObject($newTag,"Page",$pageId) ;
                            }

                            foreach($removedTags as $removedTag) {
                                Lib::untagObject($removedTag,"Page",$pageId) ;
                            }
                            $pageTagsArray=$updatedPageTags ;
                            $pageTagsList=implode(",",$pageTagsArray);
                            //
                            // Check if any images have been changed 
                            // Get images associated with page before update
                            //
                            $pageImages=ImageObject::readAllByObject("Page",$pageId);
                            $imagesList=[];
                            $imagesIds=[];
                            foreach($pageImages as $pageImage) {
                                array_push($imagesList, 
                                    [
                                        $pageImage->getImageId(),               // get imageId
                                        Image::read($pageImage->getImageId())->getImageMiniPath()
                                    ]);
                                array_push($imagesIds,$pageImage->getImageId());
                            }
                            // Get images associated with page after update
                            $updatedImagesList=explode(",",$_POST['objectImages']);
                            //
                            $newImages=array_diff($updatedImagesList, $imagesIds);
                            $removedImages=array_diff($imagesIds,$updatedImagesList);

                            foreach ($newImages as $newImage) {
                                Lib::imageObject($newImage,"Page",$pageId) ;
                            }

                            foreach ($removedImages as $removedImage) {
                                Lib::unimageObject($removedImage, "Page", $pageId);
                            }              
                        }
                        else {
                            $flash->error("Page was not updated");
                        }
                    }
                else {
                    $flash->error("Missing Parameters");
                }

            }
            // Display page update form 
            $menusList=Menu::readAll();                         // Retrieve list of menus  
            $menuTitles=[];
            foreach ($menusList as $menu) {
                array_push ( $menuTitles, 
                [
                    'menuId'=>$menu->getMenuId(),
                    'menuTitle'=>$menu->getMenuTitle()
                ]);
            }
            //
            // Get images associated with page
            //
            $pageImages=ImageObject::readAllByObject("Page",$pageId);
            $imagesList=[];
            foreach($pageImages as $pageImage) {
                array_push($imagesList, 
                    [
                        $pageImage->getImageId(),               // get imageId
                        Image::read($pageImage->getImageId())->getImageMiniPath()
                    ]);
            }
            //
            // arguments required to update page
            $params=[
                "pageId"=>$page,                              // page object
                "menuTitles"=>$menuTitles,                    // array of menu items
                "pageTags"=>$pageTagsList,                    // page tags
                "pageImages"=>$imagesList                     // page images
            ];
            $this->render("admin/pageUpdate",$params);
        }
    }

     /**
     * function called by URL admin.php?class=User&action=delete&id=<id of record to be deleted>
     *
     * @return void
     */
    public function delete() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            $this->redirectTo("User","login");
        } else
        {
            if (isset($_GET["id"])){
                // Make sure page has no sections which would be left orphaned
                if (Section::readAllByPageId($_GET['id'])) {
                    $flash->error("This page contains sections. Please move sections before deleting");

                }
                else {
                    $page=Page::read($_GET["id"]) ;
                    if ($page->delete()) {
                        $flash->warning("Page record deleted");
                        //
                        // remove any tags which were attached to the page
                        //
                        TagObject::deleteTagObjectsByObject("Page",$_GET["id"]);
                        // remove any images which were attached to the page
                        ImageObject::deleteImageObjectsByObject("Page",$_GET["id"]);
                    }
                    else {
                        $flash->error("Error deleting page record");
                    }
                }
            } 
            else $flash->error("Missing parameter: page ID");
            //
            $this->redirectTo("Page");
        }
    }

    /************************************************************ User Methods  *************************************/

    /**
     * Display a page : 
     *      retrieve page content
     *      retrieve list of sections belonging to the page
     * 
     *      render page content with sections in collapsed mode
     * 
     *  this method is called by URL index.php?class=Page&action=display&id=<pageId>
     *
     * @return void
     */
    function display(){
        $flash=new SessionFlash(); 
        // Check if ID was specified
        if (isset($_GET['id'])) {
            if ($page=Page::read($_GET['id'])) {

                // Get all tags for this page
                $tagObjects=TagObject::readAllByObject("Page",$page->getPageId());
                $tags=[];
                foreach ($tagObjects as $tagObject) {
                    array_push($tags,Tag::read($tagObject->getTagId()));
                }
                // Get all images for this page
                $imageObjects=ImageObject::readAllByObject("Page",$page->getPageId());
                $images=[];
                foreach ($imageObjects as $imageObject) {
                    array_push($images,Image::read($imageObject->getImageId()));
                }
                // Get all children Sections 
                $sections=Section::readAllByPageId($page->getPageId());

                $params=[
                    "page"=>$page,
                    "sections"=>$sections,
                    "tags"=>$tags,
                    "images"=>$images
                ];
                // render page to screen
                $this->render("front/pageDisplay",$params);
            }
            else {              // page does not exist
                $flash->error("This page does not exist");
                $this->redirectTo("Front"); 
            }
        }
        else {
            $flash->error("Missing page ID");
            $this->redirectTo("Front"); 
        }
    }
}