<?php 

/**
 * Author : Christophe Mathias  - February 2022
 * Version 1.0
 * Change history :
 * 
 * Controller to manage partners
 */

 class PartnerController extends Controller implements Icontroller {

    function index() {
        $flash=new SessionFlash();
        require ("views/front/partners.php");
    }

 }