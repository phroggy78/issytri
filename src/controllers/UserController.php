<?php


/**
 * Author : Philippe Roggeband February 2020 
 * Version 1.0
 * Change history : 
 * 
 * Admin methods to list, create, update and delete administrator profiles. 
 * Will be extended at some point for additional profiles, to be determined.
 * 
 * User methods : 
 *  login
 *  logout
 */ 

use Ballen\Plexity\Plexity as PasswordValidator;


class UserController extends Controller implements Icontroller 
{
    
    
    public function login()
    {
        $flash=new SessionFlash(); 
        if(session_status() !== PHP_SESSION_ACTIVE) session_start();
        if(isset($_POST['email']) && isset($_POST['password']))
        {
            $user =User::readByEmail($_POST["email"]);
            if (!$user) {
            //  erreur - probablement login invalide
                $flash->error("Adresse email invalide");
            }
            //  valider que les mots de passe correspondent
            else if (! password_verify($_POST["password"],$user->getPassword())) 
                $flash->warning("Erreur de mot de passe");
            else {
                // Compte est valide, on logge l'utilisateur
                $_SESSION=[
                    "firstName"=>$user->getFirstName(),
                    "lastName"=>$user->getLastName(),
                    "email"=>$user->getEmail(),
                    "role"=>$user->getRole(),
                    "logged"=>true,
                    "userId"=>$user->getUserId(),
                    "loginTime"=>time()
                ] ; 

                if ($user->getPasswordChanged()==0) {
                    $flash->warning("Please reset your password");
                    header ("location:admin.php?class=User&action=updatePassword");
                    exit;
                }
                else {
                    var_dump ($_SESSION);
                    $flash->success("User ".$_SESSION['email']." logged in");   
                }
            }
        }
        header ("location:admin.php") ;
        exit;
    }

    /**
     * Return TRUE is user is logged in with any profile
     *
     * @return boolean
     */
    public static function isLogged() : bool 
    {
        if(session_status() !== PHP_SESSION_ACTIVE) session_start();
        $result=false;
        if (isset($_SESSION["logged"])) {
            if ($_SESSION["logged"]) 
                {
                    // Check inactivity timer
                    $time=time()-$_SESSION["loginTime"];
                    if ($time >= 600) {
                        $flash=new SessionFlash();
                        $flash->Error("You have been logged out due to inactivity for $time seconds");
                        header ("location:admin.php?class=User&action=logout") ;
                        exit;
                    } 
                    else {
                        $_SESSION['loginTime']=time();              // reset inactivity timer
                        $result=true;
                        return $result;
                    }
                }
        }
        return $result;
    }

    /**
     * Return TRUE is user is logged in with Admin profile
     *
     * @return boolean
     */
    public static function isLoggedAdmin() : bool 
    {
        if(session_status() !== PHP_SESSION_ACTIVE) session_start();
        $result=false;
        if (isset($_SESSION["logged"])) {
            if (($_SESSION["logged"]) && ($_SESSION["role"]=="Admin")) 
                {
                    // Check inactivity timer
                    $time=time()-$_SESSION["loginTime"];
                    if ($time >= 600) {
                        $flash=new SessionFlash();
                        $flash->Error("You have been logged out due to inactivity for $time seconds");
                        header ("location:admin.php?class=User&action=logout") ;
                        exit;
                    } 
                    else {
                        $_SESSION['loginTime']=time();              // reset inactivity timer
                        $result=true;
                        return $result;
                    }
                }
        }
        return $result;
    }

    /**
     * Return TRUE is user is logged in with Editor profile
     *
     * @return boolean
     */
    public static function isLoggedEditor() : bool 
    {
        if(session_status() !== PHP_SESSION_ACTIVE) session_start();
        $result=false;
        if (isset($_SESSION["logged"])) {
            if (($_SESSION["logged"]) && ($_SESSION["role"]=="Editor")) 
                // Check inactivity timer
                $time=time()-$_SESSION["loginTime"];
                if ($time >= 600) {
                    $flash=new SessionFlash();
                    $flash->Error("You have been logged out due to inactivity for $time seconds");
                    header ("location:admin.php?class=User&action=logout") ;
                    exit;
                } 
                else {
                    $_SESSION['loginTime']=time();              // reset inactivity timer
                    $result=true;
                    return $result;
                }
        }
        return $result;
    }

    /**
     * Logout function
     *
     * @return void
     */
    public function logout()
    {
        // open the session and checked if user is logged in 
        // we should never be here unless this is the case
        if(session_status() !== PHP_SESSION_ACTIVE) session_start();
        $flash=new SessionFlash();
        if (isset($_SESSION["logged"])) {
            if ($_SESSION["logged"])  {
                $_SESSION["logged"]=false;        // clear session
                $flash->success("User ".$_SESSION['email']." Logged out");
                header ("location:admin.php") ;
                exit;
            }
        }  
    }

    /**
     * Method for admin or editor to modify password
     *
     * @return void
     */
    public function updatePassword() {
        // open the session and checked if user is logged in 
        // we should never be here unless this is the case
        if(session_status() !== PHP_SESSION_ACTIVE) session_start();
        $flash=new SessionFlash();
        if (!UserController::islogged()) {
            $flash->error("You must log in to execute this function");
            require ("views/admin/userLogin.php");
        }
        else {
            if (isset($_POST['submit'])) {


                // Read user record from database
                if ($user=User::read($_SESSION["userId"])) {
                    $errorFlag=false ;
                    }
                else {
                    $flash->error("Session holds an invalid user Id");
                    $errorFlag=true;
                }
                
                /**
                 * Verify old password
                 */
                if (!$errorFlag) {
                    if (! password_verify($_POST["oldPassword"],$user->getPassword())) {
                        $flash->error("Invalid old Password");
                        $errorFlag=true;
                    }
                }

                /**
                 * Make sur new password and new password check match
                 */
                if (!$errorFlag) {
                    if ($_POST['newPassword'] !== $_POST['newPasswordCheck']) {
                        $flash->error("New passwords do not match");
                        $errorFlag=true;
                    }
                }


                /**
                 * Check password meets strength requirements
                 */
                if (!$errorFlag) {

                    $password = new PasswordValidator();
                    $password->requireSpecialCharacters() // We want the password to contain (atleast 1) special characters.
                        //->requireSpecialCharacters(5), // We could also specify a specific number of special characters.
                        ->requireUpperCase() // Requires the password to contains more than one upper case characters.
                        ->requireLowerCase(2) // Requires the password to contains atleast 2 lower case characters.
                        ->requireNumericChataters(3); // We want to ensure that the password uses at least 3 numbers!
            
                    // An example of passing a password history array, if the password exists in here then we'll disallow it!
                    $password->notIn([
                        'test_password',
                        'Issy_triathlon',
                        $_SESSION['email']
                    ]);
                    // You can optionally pass in an implementation of PasswordHistoryInterface like so:
                    //$password->notIn(new CustomPasswordHistoryDatastore()); // Must implement Ballen\Plexity\Interfaces\PasswordHistoryInterface
            
                    try {
                        $password->check($_POST['newPassword']);
                        $flash->success("Great news! The password passed validation!");
                    } catch (Ballen\Plexity\Exceptions\ValidationException $exception) {
                        $flash->error("Invalid password:".$exception->getMessage());
                        $errorFlag=true;
                    }
                }

                /**
                 * If no errors were found, update user password and write to database
                 */
                if (!$errorFlag) {
                    $user->setPassword($_POST['newPassword']);
                    $user->setPasswordChanged(true);
                    if ($user->updatePassword())
                        $flash->success("Password updated");
                    else {
                        $flash->error("Error updating user record");
                        $flash->error("Password was not reset");
                    }
                }
            }
            $this->render("admin/userUpdatePassword");
        }
    }

    /******************************************** Administrator functino : CRUD  ************************/

    /**
     * Index (default method) : List all users in the data base
     *
     * @return void
     */

    public function index()
    {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $usersList=User::readAll();                         // Retrieve list of users        
            $this->render("admin/userIndex",$usersList);        // Display list of users 
        }
    }

    /**
     * User Create function - Called either as a URL from admin.php (Class=User&action=create)
     *                      or from userAdd.php form 
     *
     * @return void
     */
    public function create() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            // if user has submitted a new page, the call came from the submit button , enter form data in the database
            if (isset($_POST['submit'])) {
                if( isset($_POST['userEmail'])&&
                    isset($_POST['userPassword'])&&
                    isset($_POST['firstName'])&&       
                    isset($_POST['lastName'])&& 
                    isset($_POST["role"])   
                    ) 
                {
                    // create newu User object, set attributs and write to database.
                    $newUser=new User();
                    $newUser->setEmail($_POST['userEmail']);
                    $newUser->setPassword($_POST['userPassword']);
                    $newUser->setfirstName($_POST['firstName']);
                    $newUser->setlastName($_POST['lastName']);
                    $newUser->setRole($_POST["role"]);
                    $newUser->setPasswordChanged(false);
                    // Check if creation worked 
                    if ($newUser->create()) {
                        $flash->success("User Profile added");
                    } else  { 
                        $flash->error("Error creating User Profile"); 
                    }
                }
                else {
                    $flash->error("Missing parameters");
                }
            }  
            // In all cases, return to userAdd form 
            $this->render("admin/userCreate");                    // Display user add form   
        }             
    }

    

    /**
     * Function called by URL admin.php?class=User&action=update&id=<user Id of database entry to update>
     * $_POST contains form parameters  entered by the user if form has been filled in. It's empty if we just have to 
     * display the update form. 
     *
     * @param [type] $userId
     * @return void
     */
    public function update($userId) {
        $flash=new SessionFlash();
        // Check if admin is logged in
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            // get user record from database
            $user=User::read($userId) ;
            // Check if we are handling a submtted form or displaying a form to be edited
            if (isset($_POST['submit'])) {
                 // set parameters in clear
                $user->setFirstName($_POST["firstName"]);
                $user->setLastName($_POST["lastName"]);
                $user->setEmail($_POST["userEmail"]);
                $user->setRole($_POST["role"]);
                $user->setPasswordChanged(false);                   // Force user to change his password on first login

                // update initial set of parameters
                if ($user->update()) {$flash->success("User data updated");}
                else {$flash->error("User data not updated");}

                // check if password needs to be updated 
                if ($_POST["userPassword"] !="************") {
                    $user->setPassword($_POST["userPassword"]);
                    if ($user->updatePassword()) {$flash->success("User password updated");}
                    else {($flash->error("User password was not updated"));}
                }
            } 
            $this->render("admin/userUpdate",array($user)) ;          // render form to edit user data
        }
    }

    /**
     * function called by URL admin.php?class=User&action=delete&id=<id of record to be deleted>
     *
     * @return void
     */
    public function delete() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $user=User::read($_GET["id"]) ;
                if ($user->delete()) {
                    $flash->warning("User record deleted");
                }
                else {
                    $flash->error("Error deleting user record");
                }
            } 
            else $flash->error("Missing parameter: user ID");
            //
            $this->redirectTo("User");
        }
    }
}