<?php 

class ArticleController extends Controller implements Icontroller {

    /**
     * Author : Philippe Roggeband - February 2020
     * v1.0
     * 
     * The article is the base component of a page. It can be tagged or can have images associated with it. 
     * 
     * An article is always a child of a section. 
     * 
     *  Methods in this controller are called via a URL of the format  index.php?class=Article&action=<method>[&id=<articleId>]
     * 
     *  Base functions : index - list all articles
     *                   create - add an article to the database,
     *                            attach tags, 
     *                            attach images
     *                   update - modify an entry
     *                            attach / remove tags, 
     *                            attach / remove images
     *                   delete - delete an article
     *                            remove attached tags
     *                            remove attached images
     */


    /**
     * List all articles, and the section / page / menu in which they are placed
     *
     * @return void
     */
    function index(){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $articlesList=Article::readAll();                         // Retrieve list of articles   
            $articles=[];
            foreach ($articlesList as $article) {
                $sectionId=$article->getArticleSectionId();
                $sectionTitle= (Section::read($sectionId)->getSectionTitle());
                $pageId=(Section::read($sectionId)->getSectionPageId());
                $pageTitle=(Page::read($pageId))->getPageTitle();
                $menuId=(Page::read($pageId))->getPageMenuId();
                $menuTitle=Lib::cleanHtml(Menu::read($menuId)->getMenuTitle());
                // date format conversions
                $articlePublicationDate = date("d-m-Y", strtotime($article->getArticlePublicationDate()));                
                $articleExpiryDate = date("d-m-Y", strtotime($article->getArticleExpiryDate()));

                array_push ( $articles,
                [
                    "articleId"=>$article->getArticleId(),
                    "articleTitle"=>Lib::cleanHtml($article->getArticleTitle()),
                    "articleOrder"=>$article->getArticleOrder(),
                    "articleSection"=>Lib::cleanHtml($sectionTitle),
                    "articlePage"=>Lib::cleanHtml($pageTitle),
                    "articleMenu"=>Lib::cleanHtml($menuTitle),
                    "articleStatus"=>$article->getArticleStatus(),
                    "articlePublicationDate"=>$articlePublicationDate,
                    "articleExpiryDate"=>$articleExpiryDate
                ]
                );
            }
            $this->render("admin/articleIndex",$articles);        // Display list of users 
        }
    }
    /**
     * create article record 
     *
     * @return void
     */
    function create(){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            // if admin has submitted a new article, enter it in the database
            if (isset($_POST['submit'])) {
                // check if all mandatory parameters have been entered
                if (isset($_POST['articleTitle'])&&
                    isset($_POST['articleOrder'])&&
                    isset($_POST['articleSectionId'])&&
                    isset($_POST['articleStatus'])&&
                    isset($_POST['articleContent'])
                )
                {
                    // convert input date formats to expected format for SQL insertion          

                    $articlePublicationDate = date("Y-m-d", strtotime($_POST['articlePublicationDate']));                
                    $articleExpiryDate = date("Y-m-d", strtotime($_POST['articleExpiryDate']));

                    // Create Section object and set attributes
                    $newArticle=new Article();
                    $newArticle->setArticleTitle(Lib::cleanHtml($_POST['articleTitle']));
                    $newArticle->setArticleOrder($_POST['articleOrder']);
                    $newArticle->setArticleSectionId($_POST['articleSectionId']);

                    // sanitize HTML content before encoding
                    $newArticle->setArticleContent(htmlentities(Lib::cleanHtml($_POST['articleContent'])));
                    $newArticle->setArticlePublicationDate($articlePublicationDate);     // publication date in "YYYY-MM-DD" format
                    $newArticle->setArticleExpiryDate($articleExpiryDate);               // expiry date in "YYYY-MM-DD" format
                    $newArticle->setArticleStatus($_POST['articleStatus']);         // 0 = hidden, 1 = active, 2 = preview
                    // Write object to database and retrieve id which is set by page->create() 
                    if ($newArticle->create()) {
                        $newArticleId=$newArticle->getArticleId();
                        $flash->success("Article $newArticleId created");
                        // 
                        // Add tags to the article object if created 
                        //
                        if (!empty($_POST["articleTags"])) {
                            $articleTags = explode (",", $_POST['articleTags']);
                            foreach ($articleTags as $articleTag) {
                                Lib::tagObject(ucfirst(strtolower($articleTag)), "Article",$newArticleId);
                            }
                        }
                        // 
                        // add images to last created article
                        //
                        $newImages=explode(",",$_POST['objectImages']);
                        foreach ($newImages as $newImage) {
                            Lib::imageObject($newImage,"Article",$newArticleId) ;
                        }
                    }
                    else {
                        $flash->error("Error creating Article");
                    }
                }
                else {
                    $flash->error("Missing parameters");
                } 
            }
           
            // in all cases, Display Section creation form 
            $sectionList=Section::readAll();                         // Retrieve list of menus  
            $sectionTitles=[];
            foreach ($sectionList as $section) {
                $sectionId=$section->getSectionId();
                $sectionTitle=Lib::cleanHtml($section->getSectionTitle());
                $pageId=$section->getSectionPageId();
                $pageTitle=Lib::cleanHtml((Page::read($pageId)->getPageTitle()));        
                $menuId=(Page::read($pageId))->getPageMenuId();
                $menuTitle=Lib::cleanHtml((Menu::read($menuId))->getMenuTitle());
                array_push ( $sectionTitles, 
                [
                    'sectionId'=>$section->getSectionId(),
                    'sectionTitle'=>"$menuTitle>$pageTitle>$sectionTitle"
                ]);
            }
            $this->render("admin/articleCreate", $sectionTitles);                    // Display section create form 
        }

    }

    /**
     * Update Section record 
     * 
     * @param [type] $sectionId
     * @return void
     */
    function update($articleId) {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        }
        else {
            $article=Article::read($articleId);                                 // Get the article before Update
            $articleTags=TagObject::readAllByObject("Article",$articleId);      // Get the article Tags before Update
            // initialise tag checking variables
            $articleTagsArray=[];
            $newTags=[];
            $removedTags=[];
            // convert to an array of tags for comparison with updated field 
            foreach ($articleTags as $articleTag) {
                array_push($articleTagsArray,Tag::read($articleTag->getTagId())->getTagName());
            }
            // Convert array to CSV format for display
            $articleTagsList=implode(",",$articleTagsArray);                    

            // Check if form has been filled. If yes, update article. If not, display article for update. 
            if (isset($_POST['submit'])) {
                // check if all mandatory parameters have been entered
                if (isset($_POST['articleTitle'])&&
                    isset($_POST['articleOrder'])&&
                    isset($_POST['articleSectionId'])&&
                    isset($_POST['articleStatus'])&&
                    isset($_POST['articleContent'])
                )
                {
                    // convert input date formats to expected format for SQL insertion          

                    $articlePublicationDate = date("Y-m-d", strtotime($_POST['articlePublicationDate']));                
                    $articleExpiryDate = date("Y-m-d", strtotime($_POST['articleExpiryDate']));

                    // Create Section object and set attributes
                    $article->setArticleTitle(Lib::cleanHtml($_POST['articleTitle']));
                    $article->setArticleOrder($_POST['articleOrder']);
                    $article->setArticleSectionId($_POST['articleSectionId']);
                    // santize HTML content before encoding 
                    $article->setArticleContent(htmlentities(Lib::cleanHtml($_POST['articleContent'])));
                    $article->setArticlePublicationDate($articlePublicationDate);     // publication date in "YYYY-MM-DD" format
                    $article->setArticleExpiryDate($articleExpiryDate);               // expiry date in "YYYY-MM-DD" format
                    $article->setArticleStatus($_POST['articleStatus']);              // 0 = hidden, 1 = active, 2 = preview
                    // Write object to database and retrieve id which is set by page->create() 
                    if ($article->update()) {
                        $flash->success("Article updated");
                        // 
                        // Check if tags have been changed.
                        //
                        $updatedArticleTags = explode (",", $_POST['articleTags']);
                        // convert all tags to the same format 
                        for ($i=0; $i< count($updatedArticleTags);$i++) {
                            $updatedArticleTags[$i]=ucfirst(strtolower($updatedArticleTags[$i]));
                        }

                        // Check if any tags have been added or removed
                        $newTags=array_diff($updatedArticleTags,$articleTagsArray);

                        $removedTags=array_diff($articleTagsArray,$updatedArticleTags);

                        foreach ($newTags as $newTag) {
                            Lib::tagObject($newTag,"Article",$articleId) ;
                        }

                        foreach($removedTags as $removedTag) {
                            Lib::untagObject($removedTag,"Article",$articleId) ;
                        }
                        // retrieve updated tags list for render view
                        $articleTagsArray=$updatedArticleTags ;
                        $articleTagsList=implode(",",$articleTagsArray);
                        //
                        // Check if any images have been changed 
                        // Get images associated with section before update
                        //
                        $articleImages=ImageObject::readAllByObject("Article",$articleId);
                        $imagesList=[];
                        $imagesIds=[];
                        foreach($articleImages as $articleImage) {
                            array_push($imagesList, 
                                [
                                    $articleImage->getImageId(),               // get imageId
                                    Image::read($articleImage->getImageId())->getImageMiniPath()
                                ]);
                            array_push($imagesIds,$articleImage->getImageId());
                        }
                        // Get images associated with page after update
                        $updatedImagesList=explode(",",$_POST['objectImages']);
                        //
                        $newImages=array_diff($updatedImagesList, $imagesIds);
                        $removedImages=array_diff($imagesIds,$updatedImagesList);

                        foreach ($newImages as $newImage) {
                            Lib::imageObject($newImage,"Article",$articleId) ;
                        }

                        foreach ($removedImages as $removedImage) {
                            Lib::unimageObject($removedImage, "Article", $articleId);
                        }  
                    }
                    else {
                        $flash->error("Error updating Article");
                    }
                }
                else {
                    $flash->error("Missing parameters");
                } 
            }

            // in all cases, Display Article Update form 
            $sectionList=Section::readAll();                         // Retrieve list of menus  
            $sectionTitles=[];
            foreach ($sectionList as $section) {
                $sectionId=$section->getSectionId();
                $sectionTitle=Lib::cleanHtml($section->getSectionTitle());
                $pageId=$section->getSectionPageId();
                $pageTitle=Lib::cleanHtml((Page::read($pageId))->getPageTitle());
                $menuId=(Page::read($pageId))->getPageMenuId();
                $menuTitle=Lib::cleanHtml((Menu::read($menuId))->getMenuTitle());

                array_push ( $sectionTitles, 
                [
                    'sectionId'=>$section->getSectionId(),
                    'sectionTitle'=>"$menuTitle>$pageTitle>$sectionTitle"
                ]);
            }
            //
            // Get images associated with page
            //
            $articleImages=ImageObject::readAllByObject("Article",$articleId);
            $imagesList=[];
            foreach($articleImages as $articleImage) {
                array_push($imagesList, 
                    [
                        $articleImage->getImageId(),               // get imageId
                        Image::read($articleImage->getImageId())->getImageMiniPath()
                    ]);
            }
            // arguments required to update page
            $params=[
                'article'=>$article,                            // section object
                'sections'=>$sectionTitles,                     // array of parent page items
                'articleTags'=>$articleTagsList,                // array of article tags
                'articleImages'=>$imagesList                    // images associated with article
            ];
            $this->render("admin/articleUpdate",$params);
        }
    } 

    /**
     * function called by URL admin.php?class=Article&action=delete&id=<id of record to be deleted>
     *
     * @return void
     */
    public function delete() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $article=Article::read($_GET["id"]) ;
                if ($article->delete()) {
                    $flash->warning("Article record deleted");
                    //
                    // Remove all tagObjects pointing to this article
                    TagObject::deleteTagObjectsByObject("Article",$article->getArticleId());
                    // Remove all links to images
                    //
                    ImageObject::deleteImageObjectsByObject("Article",$article->getArticleId());
                }
                else {
                    $flash->error("Error deleting Article record");
                }
            } 
            else $flash->error("Missing parameter: Article ID");
            //
            $this->redirectTo("Article");
        }
    }

    /********************************************* display functions  ****************************************/

    /**
     * Checks an article's status. Returns true if it should be displayed, false if it should be hidden.
     *
     * @param [type] $article
     * @return boolean
     */
    public static function displayArticle($article):bool {
        $result=false;   // initialise result
        switch ($article->getArticleStatus()) {
           case 0 : // Article is set as  hidden
               $result=false;
               break ;
           case 1 : // Article is active. Display if date is valid.
               $result=Lib::validateDate($article->getArticlePublicationDate(),$article->getArticleExpiryDate());
               break;
           case 2 : // Article is in preview mode. Display if user is loggd in as admin or editor 
               $result=UserController::isLogged();
               break;
        }
       return $result ;
    }

}