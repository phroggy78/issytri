<?php 

/**
 * Author: Philippe Roggeband - March 2020
 * Version 1.0
 * Change History:
 * 
 * Controls the display of tiles on the site front page.  
 * 
 * Administrator methods include : 
 *      index - list all existing tiles
 *      create - Add a new tile with it's required attributes
 *      update - Modify an existing tile
 *      delete - Delete a tile 
 *      
 */

 class TileController extends Controller implements Icontroller {

    /**
     * index - returns and displays the list of tiles
     *
     * @return void
     */
     public function index() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin to execute this function");
            $this->redirectTo("User","login");
        } else {
            $tiles=Tile::readAll();                     // get the list of all existing tiles
            $tilesList=[];                              // Initialise array used by view
            foreach ($tiles as $tile){  
                array_push ($tilesList, 
                    [
                        'tileId'=>$tile->getTileId(),
                        'tileTitle'=>Lib::cleanHtml($tile->getTileTitle()),
                        'tileOrder'=>$tile->getTileOrder(),
                        'tileStatus'=>$tile->getTileStatus(),
                        'tileImportance'=>$tile->getTileImportance(),
                        'tilePublicationDate'=>$tile->getTilePublicationDate(),
                        'tileExpiryDate'=>$tile->getTileExpiryDate()
                    ]
                );
            }
            $this->render("admin/tileIndex",$tilesList);
        }
     }

     /**
      * Create a new tile
      *     If form has been submitted
      *     - validate values passed in form
      *     - Check if image is valid, and upload it
      *     - If image was correctly uploaded, create tile Object in database
      *     Display create form 
      *
      * @return void
      */
     public function create() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            $this->redirectTo("User","login");
        } 
        else {    
            if (isset($_POST['submit'])) {
                if (isset ($_POST['tileTitle']) &&
                    isset ($_POST['tileOrder']) &&
                    isset ($_POST['tileContent']) &&
                    isset ($_POST['tileImportance']) &&
                    isset ($_POST['tilePublicationDate']) &&
                    isset ($_POST['tileExpiryDate']) &&
                    isset ($_POST['tileStatus']) &&
                    isset ($_FILES['tileImage'])
                ) {

                    $tile=new Tile();
                    //
                    // Retrieve form parameters
                    //
                    $tile->setTileTitle(Lib::cleanHtml($_POST['tileTitle']));
                    $tile->setTileOrder($_POST['tileOrder']);
                    $tile->setTileContent(htmlentities(Lib::cleanHtml($_POST['tileContent'])));

                    $tile->setTileImportance($_POST['tileImportance']);
                    $tile->setTileStatus($_POST['tileStatus']);
                    // Convert date formats for SQL insertion
                    $tilePublicationDate = date("Y-m-d", strtotime($_POST['tilePublicationDate']));                
                    $tileExpiryDate = date("Y-m-d", strtotime($_POST['tileExpiryDate']));
                    //
                    $tile->setTilePublicationDate($tilePublicationDate);
                    $tile->setTileExpiryDate($tileExpiryDate);
                    //
                    // validate image format and upload to server
                    //
                    $targetDir = "images/tiles/";
                    $targetFile = $targetDir.basename($_FILES["tileImage"]["name"]);
                    $uploadOk = 1;
                    $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
                    
                    // Check if image file is a actual image or fake image
                    $check = getimagesize($_FILES["tileImage"]["tmp_name"]);
                    if($check !== false) {
                        $uploadOk = 1;
                    } else {
                        $flash->error( "File is not an image.");
                        $uploadOk = 0;
                    }
        
                    // Check if file already exists
                    if (file_exists($targetFile)) {
                        $flash->error( "Sorry, file already exists.");
                        $uploadOk = 0;
                    }

                    // Check file size
                    if ($_FILES["tileImage"]["size"] > 500000) {
                        $flash->error( "Sorry, your file is too large.");
                        $uploadOk = 0;
                    }

                    // Allow certain file formats
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                        $flash->error( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                        $uploadOk = 0;
                    }

                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk !== 0) {

                        if (move_uploaded_file($_FILES["tileImage"]["tmp_name"], $targetFile)) {
                            $flash->success ("Image ". basename( $_FILES["tileImage"]["name"]). " has been uploaded.");
                            $tile->setTileImage($targetFile);
                            if ($tile->create()) {
                                $tileId=$tile->getTileId();
                                $flash->success("Tile $tileId Created");
                            } else {
                                $flash->error("Error creating tile");
                                }
                            }
                            else {
                                $flash->error ("Sorry, there was an error uploading your image file. Tile was not created");
                            }
                        }
                    
                        // 
                        // add secondary images to last created Tile
                        //

                        $newImages=explode(",",$_POST['objectImages']);
                        foreach ($newImages as $newImage) {
                            Lib::imageObject($newImage,"Tile",$tileId) ;
                        }
                    }
                    else {
                        $flash->error("Missing Parameters");
                    }
                }
            }
        $this->render("admin/tileCreate");
     }

     /**
      * Update Tile record 
      * 
      *
      * @param [type] $tileId
      * @return void
      */
     public function update ($tileId) {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            $this->redirectTo("User","login");
        } else
        {
            $tile=Tile::read($tileId);

            if (isset($_POST['submit'])) {
                if (isset ($_POST['tileTitle']) &&
                isset ($_POST['tileOrder']) &&
                isset ($_POST['tileContent']) &&
                isset ($_POST['tileImportance']) &&
                isset ($_POST['tilePublicationDate']) &&
                isset ($_POST['tileExpiryDate']) &&
                isset ($_POST['tileStatus']) &&
                isset ($_FILES['tileImage'])
            ) {
                //
                // Retrieve form parameters
                //
                $tile->setTileTitle(Lib::cleanHtml($_POST['tileTitle']));
                $tile->setTileOrder($_POST['tileOrder']);

                // sanitize html code entered for content before writing
                $tile->setTileContent(htmlentities(Lib::cleanHtml($_POST['tileContent'])));
                $tile->setTileImportance($_POST['tileImportance']);
                $tile->setTileStatus($_POST['tileStatus']);
                // Convert date formats for SQL insertion
                $tilePublicationDate = date("Y-m-d", strtotime($_POST['tilePublicationDate']));                
                $tileExpiryDate = date("Y-m-d", strtotime($_POST['tileExpiryDate']));
                //
                $tile->setTilePublicationDate($tilePublicationDate);
                $tile->setTileExpiryDate($tileExpiryDate);
                //
                // validate image format and upload to server
                //
                if ($_FILES["tileImage"]["name"]!=="") {
                    $targetDir = "images/tiles/";
                    $targetFile = $targetDir.basename($_FILES["tileImage"]["name"]);
                    //
                    // Check if image has been changed
                    // 
                    if ($targetFile !== $tile->getTileImage()) {
                        $uploadOk = 1;
                        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
                        
                        // Check if image file is a actual image or fake image
                        $check = getimagesize($_FILES["tileImage"]["tmp_name"]);
                        if($check !== false) {
                            $uploadOk = 1;
                        } else {
                            $flash->error( "File is not an image.");
                            $uploadOk = 0;
                        }
            
                        // Check if file already exists
                        if (file_exists($targetFile)) {
                            $flash->error( "Sorry, file already exists.");
                            $uploadOk = 0;
                        }
        
                        // Check file size
                        if ($_FILES["tileImage"]["size"] > 500000) {
                            $flash->error( "Sorry, your file is too large.");
                            $uploadOk = 0;
                        }
        
                        // Allow certain file formats
                        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif" ) {
                            $flash->error( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                            $uploadOk = 0;
                        }
        
                        // Check if $uploadOk is set to 0 by an error
                        if ($uploadOk !== 0) {
                            if (move_uploaded_file($_FILES["tileImage"]["tmp_name"], $targetFile)) {
                                $flash->success ("Image ". basename( $_FILES["tileImage"]["name"]). " has been uploaded.");
                                //
                                // Delete previous file
                                //
                                unlink($tile->getTileImage());
                                // Update record with new image file
                                $tile->setTileImage($targetFile); }
                                else {
                                    $flash->error ("Sorry, there was an error uploading your image file. Tile was not created");
                                }
                            }
                        }
                    }
                    //
                    // Check if any images have been changed 
                    // Get images associated with section before update
                    //
                    $tileImages=ImageObject::readAllByObject("Tile",$tileId);
                    $imagesList=[];
                    $imagesIds=[];
                    foreach($tileImages as $tileImage) {
                        array_push($imagesList, 
                            [
                                $tileImage->getImageId(),               // get imageId
                                Image::read($tileImage->getImageId())->getImageMiniPath()
                            ]);
                        array_push($imagesIds,$tileImage->getImageId());
                    }
                    // Get images associated with page after update
                    $updatedImagesList=explode(",",$_POST['objectImages']);
                    //
                    $newImages=array_diff($updatedImagesList, $imagesIds);
                    $removedImages=array_diff($imagesIds,$updatedImagesList);

                    foreach ($newImages as $newImage) {
                        Lib::imageObject($newImage,"Tile",$tileId) ;
                    }

                    foreach ($removedImages as $removedImage) {
                        Lib::unimageObject($removedImage, "Tile", $tileId);
                    } 
                    // Update Tile record in database
                
                    if ($tile->update()) {
                        $flash->success("Tile $tileId updated") ;
                    }
                    else {
                        $flash->error("Error updating tile $tileId");
                    }
                }
                 else {
                    $flash->error("Missing Parameters");
                }
            }
            //
            // Get images associated with page
            //
            $tileImages=ImageObject::readAllByObject("Tile",$tileId);
            $imagesList=[];
            foreach($tileImages as $tileImage) {
                array_push($imagesList, 
                    [
                        $tileImage->getImageId(),               // get imageId
                        Image::read($tileImage->getImageId())->getImageMiniPath()
                    ]);
            }
            $params=[
                'tile'=>$tile,
                'tileImages'=>$imagesList
            ];
            $this->render("admin/tileUpdate",$params);
        }
     }

     public function delete ($tileId) {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            $this->redirectTo("User","login");
        } else
        {
            $tile=Tile::read($tileId);
            ImageObject::deleteImageObjectsByObject("Tile",$tile->getTileId());
            if ( unlink ($tile->getTileImage()))  $flash->warning("Tile Image deleted");
            if ($tile->delete()) $flash->warning("Tile $tileId deleted");
        }
        $this->redirectTo("Tile");
     }

     /************************************************** User display methods  ******************************************/
   
    // If status is "hidden", (0) ignore tile. 
    // If status is "active", (1) display the tile if the current date is within range. 
    // If status is "preview", (2) display tile only if user is logged in as admin or editor

     public static function displayTile ($tile) :bool {
         $result=false;   // initialise result
         switch ($tile->getTileStatus()) {
            case 0 : // Tile is hidden
                $result=false;
                break ;
            case 1 : // Tile is active. Display if date is valid.
                $result=Lib::validateDate($tile->getTilePublicationDate(),$tile->getTileExpiryDate());
                break;
            case 2 : // Tile is in preview mode. Display if user is loggd in as admin. 
                $result=UserController::isLogged();
                break;
         }
        return $result ;
     }
 }