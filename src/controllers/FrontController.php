<?php

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0 
 * 
 *  Base controller used to display home page, contact page
 * 
 * Change History : 
 * 
 */

class FrontController extends Controller implements Icontroller
{
    
    public function index()
    {
        $this->render("front/index");
    }
    public function contact()
    {
        $this->render("front/contact");
    }
   
}