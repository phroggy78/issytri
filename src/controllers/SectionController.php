<?php 

class SectionController extends Controller implements Icontroller {

    /**
     * 
     * Author : Philippe Roggeband - February 2020
     * 
     * This controller implements all complex methods for section objects. 
     * A section object contains articles as children. 
     * A section object is the child of a page. 
     * 
     * Base methods : 
     *      - Index (list all sections)
     *      - Create
     *      - Update
     *      - Delete
     * 
     * A section can only be deleted if it has no children articles.
     * When a section is deleted, all ImageObjects and TagObjects pointing to the section are removed. 
     * 
     */

    /**
     * List all sections, and the page in which they are placed
     *
     * @return void
     */
    function index(){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $sectionsList=Section::readAll();                         // Retrieve list of sections  
            $pagesList=Page::readAll();                         // Retrieve list of parent pages  

            // build the table to be rendered
            $sections=[];

            foreach ($sectionsList as $section) {
                array_push ( $sections,
                [
                    "sectionId"=>$section->getSectionId(),
                    "sectionTitle"=>Lib::cleanHtml($section->getSectionTitle()),
                    "sectionOrder"=>$section->getSectionOrder(),
                    "sectionPage"=>Lib::cleanHtml((Page::read($section->getSectionPageId()))->getPageTitle())
                ]
                );
            }
            $this->render("admin/sectionIndex",$sections);        // Display list of users 
        }
    }

    /**
     * Create a new section record
     *
     * @return void
     */
    function create() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            // if admin has submitted a new section, enter it in the database
            if (isset($_POST['submit'])) {
                // check if all parameters have been entered
                if (isset($_POST['sectionTitle'])&&
                    isset($_POST['sectionOrder'])&&
                    isset($_POST['sectionLayout'])&&
                    isset($_POST['pageId'])&&
                    isset($_POST['sectionContent'])
                )
                {
                    // Create Section object and set attributes
                    $newSection=new Section();
                    $newSection->setSectionTitle(Lib::cleanHtml($_POST['sectionTitle']));
                    $newSection->setSectionOrder($_POST['sectionOrder']);
                    $newSection->setSectionLayout($_POST['sectionLayout']);
                    $newSection->setSectionPageId($_POST['pageId']);
                    // sanitize html code for section content before encoding
                    $newSection->setSectionContent(htmlentities(Lib::cleanHtml($_POST['sectionContent'])));
                    // Write object to database and retrieve id which is set by page->create() 
                    if ($newSection->create()) {
                        $newSectionId=$newSection->getSectionId();
                        $flash->success("Section $newSectionId created");
                                                // 
                        // Add tags to the section object if created 
                        //
                        if (!empty($_POST["sectionTags"])) {
                            $sectionTags = explode (",", $_POST['sectionTags']);
                                                        for ($i=0; $i< count($updatedPageTags);$i++) {
                                $updatedPageTags[$i]=ucfirst(strtolower($updatedPageTags[$i]));
                            }

                            foreach ($sectionTags as $sectionTag) {
                                Lib::tagObject(ucfirst(strtolower($sectionTag)),"Section",$newSectionId);
                            }
                        }
                        // 
                        // add images to last created Section
                        //
                        $newImages=explode(",",$_POST['objectImages']);
                        foreach ($newImages as $newImage) {
                            Lib::imageObject($newImage,"Section",$newSectionId) ;
                        }
                    }
                    else {
                        $flash->error("Error creating Section");
                    }
                }
                else {
                    $flash->error("Missing parameters");
                } 
            }

            // in all cases, Display Section creation form 
            $pageList=Page::readAll();                         // Retrieve list of menus  
            $pageTitles=[];
            foreach ($pageList as $page) {
                array_push ( $pageTitles, 
                [
                    'pageId'=>$page->getPageId(),
                    'pageTitle'=>Menu::read($page->getPageMenuId())->getMenuTitle().">".$page->getPageTitle()
                ]);
            }
            $this->render("admin/sectionCreate", $pageTitles);                    // Display section create form 
        }
    } 
    
    /**
     * Update Section record 
     * 
     * @param [type] $sectionId
     * @return void
     */
    function update($sectionId) {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as Admin or Editor to execute this function");
            require ("views/admin/userLogin.php");
        } 
        else {
            $section=Section::read($sectionId);
            $sectionTags=TagObject::readAllByObject("Section",$sectionId);      // Get the section Tags before Update
            $sectionTagsArray=[];
            // convert to an array of tags for comparison with updated field 
            foreach ($sectionTags as $sectionTag) {
                array_push($sectionTagsArray,Tag::read($sectionTag->getTagId())->getTagName());
            }
            // Convert array to CSV format for display
            $sectionTagsList=implode(",",$sectionTagsArray);   
            //
            // Check if the form has been submitted. If yes, process the form data, otherwise, display the form with database values
            //  
            if (isset($_POST['submit'])) {
                // check if all parameters have been entered
                if (isset($_POST['sectionTitle'])&&
                    isset($_POST['sectionOrder'])&&
                    isset($_POST['sectionLayout'])&&
                    isset($_POST['sectionId'])&&
                    isset($_POST['sectionContent'])
                    ) {
                        $section->setSectionTitle(Lib::cleanHtml($_POST['sectionTitle']));
                        $section->setSectionOrder($_POST['sectionOrder']);
                        $section->setSectionLayout($_POST['sectionLayout']);
                        $section->setSectionPageId($_POST['pageId']);
                        // sanitize html code for section content before encoding
                        $section->setSectionContent(htmlentities(Lib::cleanHtml($_POST['sectionContent'])));
                        if($section->update()) {
                            $flash->success("Section updated");
                            // 
                            // Check if tags have been changed.
                            //
                            $updatedSectionTags = explode (",", $_POST['sectionTags']);
                            for ($i=0; $i< count($updatedSectionTags);$i++) {
                                $updatedSectionTags[$i]=ucfirst(strtolower($updatedSectionTags[$i]));
                            }
                            
                            // Check if any tags have been added or removed
                            $newTags=array_diff($updatedSectionTags,$sectionTagsArray);
                            $removedTags=array_diff($sectionTagsArray,$updatedSectionTags);

                            foreach ($newTags as $newTag) {
                                Lib::tagObject($newTag,"Section",$sectionId) ;
                            }

                            foreach($removedTags as $removedTag) {
                                Lib::untagObject($removedTag,"Section",$sectionId) ;
                            }
                            $sectionTagsArray=$updatedSectionTags ;
                            $sectionTagsList=implode(",",$sectionTagsArray);
                            //
                            // Check if any images have been changed 
                            // Get images associated with section before update
                            //
                            $sectionImages=ImageObject::readAllByObject("Section",$sectionId);
                            $imagesList=[];
                            $imagesIds=[];
                            foreach($sectionImages as $sectionImage) {
                                array_push($imagesList, 
                                    [
                                        $sectionImage->getImageId(),               // get imageId
                                        Image::read($sectionImage->getImageId())->getImageMiniPath()
                                    ]);
                                array_push($imagesIds,$sectionImage->getImageId());
                            }
                            // Get images associated with page after update
                            $updatedImagesList=explode(",",$_POST['objectImages']);
                            //
                            $newImages=array_diff($updatedImagesList, $imagesIds);
                            $removedImages=array_diff($imagesIds,$updatedImagesList);

                            foreach ($newImages as $newImage) {
                                Lib::imageObject($newImage,"Section",$sectionId) ;
                            }

                            foreach ($removedImages as $removedImage) {
                                Lib::unimageObject($removedImage, "Section", $sectionId);
                            }  
                        }
                        else {
                            $flash->error("Section was not updated");
                        }
                    }
                else {
                    $flash->error("Missing Parameters");
                }

            }
            // Display page update form 
            $pageList=Page::readAll();                         // Retrieve list of menus  
            $pageTitles=[];
            foreach ($pageList as $page) {
                array_push ( $pageTitles, 
                [
                    'pageId'=>$page->getPageId(),
                    'pageTitle'=>Lib::cleanHtml(Menu::read($page->getPageMenuId())->getMenuTitle()).">".Lib::cleanHtml($page->getPageTitle())
                ]);
            }
            //
            // Get images associated with page
            //
            $sectionImages=ImageObject::readAllByObject("Section",$sectionId);
            $imagesList=[];
            foreach($sectionImages as $sectionImage) {
                array_push($imagesList, 
                    [
                        $sectionImage->getImageId(),               // get imageId
                        Image::read($sectionImage->getImageId())->getImageMiniPath()
                    ]);
            }
            //
            // arguments required to update page
            $params=[
                "section"=>$section,                            // section object
                "pageTitles"=>$pageTitles,                      // array of parent page items
                "sectionTagsList"=>$sectionTagsList,            // array of tag names
                "sectionImages"=>$imagesList                    // array of image ids & miniature paths
            ];
            $this->render("admin/sectionUpdate",$params);
        }
    } 
    
    /**
     * function called by URL admin.php?class=Section&action=delete&id=<id of record to be deleted>
     *
     * @return void
     */
    public function delete() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $section=Section::read($_GET["id"]) ;
                if ($articles=Article::readAllBySectionId($section->getSectionId())) {
                    $flash->error("This section has articles linked to it and cannot be deleted");
                }
                else {
                    if ($section->delete()) {
                        $flash->warning("Section record deleted");
                        // Remove all tagObjects pointing to this Section
                        TagObject::deleteTagObjectsByObject("Section",$section->getSectionId());
                        // Remove all imageObjects pointing to this Section
                        ImageObject::deleteImageObjectsByObject("Section",$section->getSectionId());
                    }
                    else {
                        $flash->error("Error deleting Section record");
                    }
                }
            } 
            else $flash->error("Missing parameter: Section ID");
            //
            $this->redirectTo("Section");
        }
    }

}