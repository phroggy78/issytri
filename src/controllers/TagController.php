<?php

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History : 
 * 
 * Controller used mainly for test purposes to create, update & delete tags. 
 * 
 * Tags are generally created by object controllers when an object is tagged
 */

class TagController extends Controller implements Icontroller {

    function index(){
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $tagsList=Tag::readAll();                         // Retrieve list of users        
            $this->render("admin/tagIndex",$tagsList);        // Display list of users 
        }
    }

    function create():bool {
        $flash=new SessionFlash(); 
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        }
        else {
            if (isset($_POST['submit'])) {
                if(isset($_POST['tagName'])&&
                isset($_POST["tagDescription"])   
                ) 
                {       
                    $newTag=new Tag();
                    $newTag->setTagName($_POST['tagName']);
                    $newTag->setTagDescription($_POST['tagDescription']);
                    if ($newTag->create()) {
                        $flash->success("Tag created");

                    } else  { 
                        $flash->error("Error creating Tag"); 
                    }
                }
                else {$flash->error("Missing parameters");
                }
            }
        }
        $this->render("admin/tagCreate");                    // Display user add form   
    }

     /**
     * Function called by URL admin.php?class=Menu&action=edit&id=<tag Id of database entry to update>
     * $_POST contains form parameters 
     *
     * @param [type] $tagId
     * @return void
     */
    public function update($tagId) {
        $flash=new SessionFlash();
        // Check admin rights
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            // get user record from database
            $tag=Tag::read($tagId) ;
            if (isset($_POST['submit'])) {
               // set parameters from form
                $tag->setTagName($_POST["tagName"]);
                $tag->setTagDescription($_POST["tagDescription"]);
               // update initial set of parameters
                if ($tag->update()) {$flash->success("Tag data updated");}
                else {$flash->error("Tag data not updated");}
            }
            $this->render("admin/tagUpdate",array($tag)) ;          // render form to edit menu data
        }
    }

    /**
     * function called by URL admin.php?class=Menu&action=delete&id=<id of record to be deleted>
     *
     * @return void
     */
    public function delete() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $tag=Tag::read($_GET["id"]) ;
                if ($tag->delete()) {
                    $flash->warning("Tag record deleted");
                    TagObject::deleteTagObjectsByTagId($_GET["id"]);
                }
                else {
                    $flash->error("Error deleting Tag record");
                }
            } 
            else $flash->error("Missing parameter: tag ID");
            //
            $this->redirectTo("Tag");
        }
    }

    /************************************************* user methods *************************************************/

}