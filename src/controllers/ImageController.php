<?php 

/**
 * Author : Philippe Roggeband February 2020
 * Version 1.0 
 * 
 * Controller to manage Image library 
 * 
 * Admin Methods :  
 * 
 *  Index
 *      List all image ojects in database
 * 
 *  Create
 *      Validate file format & size
 *      Upload file to server
 *      Create miniature version of file 
 *      Create image object and set attributes
 *      Store iamge obect in database
 *      add tags to image
 * 
 *  Update
 *      May be used to modify the image legend and description
 *      May be used to add / remove tags 
 * 
 *  Delete
 *      
 *      Remove all links to the image prior to delete it
 *      Delete file from server
 *      Delete image entry from database
 * 
 */

class ImageController extends Controller implements Icontroller {

    /**
     * Display list of images currently in library
     *
     * @return void
     */
    function index() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $imageList=Image::readAll();
            $images=[];
            //

            foreach ($imageList as $image) {
                // Get image use count 
                $imageObjects=ImageObject::readAllByImageId($image->getImageId());
                array_push($images, [
                    'imageId'=>$image->getImageId(),
                    'imagePath'=>$image->getImagePath(),
                    'imageLegend'=>$image->getImageLegend(),
                    'imageMiniPath'=>$image->getImageMiniPath(),
                    'imageUse'=>count($imageObjects)
                ]);
            }
            $this->render("admin/imageIndex",$images);
        }
    }

    /**
     * Add a new image to the library. Validate image size & format, and create a miniature.
     * Associate tags with image. 
     *
     * @return void
     */
    function create() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else {
            if (isset($_POST['submit']))  {

                // check mandatory parameters 
                if (isset($_FILES['imageFile']) &&
                    isset($_POST['imageLegend']) 
                ) {
                    // validate image format and upload to server
                    $targetDir = "images/";
                    $targetFile = $targetDir.basename($_FILES["imageFile"]["name"]);
                    $uploadOk = 1;
                    $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
                    
                    // Check if image file is a actual image or fake image
                    $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
                    if($check !== false) {
                        $uploadOk = 1;
                    } else {
                        $flash->error( "File is not an image.");
                        $uploadOk = 0;
                    }
        
                    // Check if file already exists
                    if (file_exists($targetFile)) {
                        $flash->error( "Sorry, file already exists.");
                        $uploadOk = 0;
                    }

                    // Check file size
                    if ($_FILES["imageFile"]["size"] > 1300000) {
                        $flash->error( "Sorry, your file is too large.");
                        $uploadOk = 0;
                    }

                    // Allow certain file formats
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                        $flash->error( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                        $uploadOk = 0;
                    }

                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        $flash->error("Sorry, your file was not uploaded.");
                    // if everything is ok, try to upload file
                    } else {
                        if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $targetFile)) {
                            $flash->success ("Image ". basename( $_FILES["imageFile"]["name"]). " has been uploaded.");
                            //
                            // Create Miniature 
                            //
                            $imageMini = new Gumlet\ImageResize($targetFile);
                            $imageMiniPath="images/mini/".basename($_FILES["imageFile"]["name"]);
                            $imageMini->resizeToHeight(60);
                            $imageMini->save($imageMiniPath);
                            //
                            // Create image object in database
                            $newImage=new Image();
                            $newImage->setImagePath($targetFile);
                            $newImage->setImageLegend($_POST['imageLegend']);
                            $newImage->setImageText($_POST['imageText']);
                            $newImage->setImageMiniPath($imageMiniPath);
                            if ($newImage->create()) {
                                $flash->success("Image successfully created");
                                $newImageId=$newImage->getImageId();
                                // add tags to image
                                if (!empty($_POST["imageTags"])) {
                                    $imageTags = explode (",", $_POST['imageTags']);
                                    
                                    foreach ($imageTags as $imageTag) {
                                        Lib::tagObject(ucfirst(strtolower($imageTag)),"Image", $newImageId);
                                    }
                                }
                            } else {
                                $flash->error("Error creating Image Object");
                            };
                        } else {
                            $flash->error ("Sorry, there was an error uploading your file.");
                        }
                    }
                }
            }
            $this->render("admin/imageCreate");
        }
    }
    

    /**
     * Update existing image in library.  Only attributes such as legend, description etc can be updated. The actual image file cannot be modified.
     *
     * @param [type] $imageId
     * @return void
     */
    function update($imageId){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $image=Image::read($imageId);
            $imageTags=TagObject::readAllByObject("Image",$imageId);      // Get the section Tags before Update
            $imageTagsArray=[];
            // convert to an array of tags for comparison with updated field 
            foreach ($imageTags as $imageTag) {
                array_push($imageTagsArray,Tag::read($imageTag->getTagId())->getTagName());
            }
            // Convert array to CSV format for display
            $imageTagsList=implode(",",$imageTagsArray);   
            if (isset($_POST['submit']))  {
                if (isset($_POST['imageLegend'])) {
                    $image->setImageLegend($_POST['imageLegend']);
                    $image->setImageText($_POST['imageText']);
                    if ($image->update()) {
                        $flash->success("Image record was updated");
                        // 
                        // Check if tags have been modified.
                        //
                        $updatedImageTags = explode (",", $_POST['imageTags']);
                        for ($i=0; $i< count($updatedImageTags);$i++) {
                            $updatedImageTags[$i]=ucfirst(strtolower($updatedImageTags[$i]));
                        }
                        
                        // Check if any tags have been added or removed
                        $newTags=array_diff($updatedImageTags,$imageTagsArray);
                        $removedTags=array_diff($imageTagsArray,$updatedImageTags);

                        foreach ($newTags as $newTag) {
                            Lib::tagObject($newTag,"Image",$imageId) ;
                        }

                        foreach($removedTags as $removedTag) {
                            Lib::untagObject($removedTag,"Image",$imageId) ;
                        }
                        $imageTagsArray=$updatedImageTags ;
                        $imageTagsList=implode(",",$imageTagsArray);

                    }
                    else {
                        $flash->error("Image record was not updated");
                    }
                }
                else {
                    $flash->error("Missing parameters");
                }
            }
            $params=[
                $image,
                $imageTagsList
            ];
            $this->render("admin/imageUpdate",$params);
        }
    }

    /**
     * Remove an image from the library, after deleting any relationships with other objects.
     *
     * @return void
     */
    function delete(){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $image=Image::read($_GET["id"]) ;
                // Delete image file
                if (unlink($image->getImagePath())) {
                    $flash->success("Image file ".$image->getImagePath()." deleted");
                } else {
                    $flash->error("Error deleting image file ".$image->getImagePath());
                }
                // Delete miniature file 
                if (unlink($image->getImageMiniPath())) {
                    $flash->success("Image file ".$image->getImageMiniPath()." deleted");
                } else {
                    $flash->error("Error deleting image file ".$image->getImageMiniPath());
                }
                // Remove tags from image.
                TagObject::deleteTagObjectsByObject("Image",$image->getImageId());
                // Delete image record from database
                if ($image->delete()) {
                    $flash->warning("Image record deleted");
                }
                else {
                    $flash->error("Error deleting Image record");
                }
            } 
            else $flash->error("Missing parameter: Image ID");
            //
            $this->redirectTo("Image");
        }
    }
}