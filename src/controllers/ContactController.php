<?php 

/**
 * Author : Philippe Roggeband  - April 2020
 * Version 1.0
 * Change history :
 * 
 * Controller to manage contact requests on website
 */

 class ContactController extends Controller implements Icontroller {

    function index() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $contacts=Contact::readAll();
            $params['contacts']=$contacts;
            $this->render("admin/contactIndex",$params);
        }

    }

    /**
     * User function to cteate a contact request by filling in a form
     *
     * @return void
     */
   function create() {
        $flash = new SessionFlash();
        $error=false;

        if (isset($_POST['submit'])) {
            // Verify Captcha 
            if(!isset($_POST['g-recaptcha-response'])){
                $flash->error("Veuillez valider la saisie avec reCAPTCHA");
                $error=true;         
            }
            if($_POST['g-recaptcha-response']==""){
                $flash->error("Veuillez valider la saisie avec reCAPTCHA");
                $error=true;         
            }

            if (!$error) {
                $url = 'https://www.google.com/recaptcha/api/siteverify';
                $data = array(
                    'secret' => '6LcoQvoZAAAAAGKcgwTsRrGkEiXM7XAvCHtnTlAp',
                    'response' => $_POST["g-recaptcha-response"]
                );
                $options = array(
                    'http' => array (
                        'method' => 'POST',
                        'content' => http_build_query($data)
                    )
                );
                $context  = stream_context_create($options);
                $verify = file_get_contents($url, false, $context);
                $captcha_success=json_decode($verify);

                if ($captcha_success->success==false) {
                    $flash->error("Robot détecté");
                    $error=true;
                }
            }

            // Verify parameters
            if (!$error) {
                if (!isset($_POST['lastName']) ||
                !isset($_POST['firstName']) ||
                !isset($_POST['email']) ||
                !isset($_POST['message'])
                ){
                    $error=true;
                    $flash->error("Veuillez saisir tous les champs");
                }
            }

            // Check email validity
            if (!$error) {
                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))  {
                    $error=true;
                    $flash->error("Adresse mail invalide");
                }
            }

            // check extension of email address. If (".ru"), eliminate, it's all spam ! 
            if (!$error) {
                if (substr($_POST["email"],-3)==".ru")  {
                    $error=true;
                    $flash->success("Votre demande de contact est envoyée");
                }
            }

            /*
            if (!$error) {
                $contact = new Contact();
                $contact->setContactLastName(Lib::cleanHtmlStrict($_POST['lastName']));
                $contact->setContactFirstName(Lib::cleanHtmlStrict($_POST['firstName']));
                $contact->setContactEmail(Lib::cleanHtmlStrict($_POST['email']));
                $contact->setContactMessage(Lib::cleanHtmlStrict($_POST['message']));
                $contact->setContactStatus(1);                              // Status = Created
                $contact->setContactPrivacyStatement(1);                    // Privacy statement accepted by user
            */

        
            if (!$error) {
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= "From: webmaster@issytriathlon.com\r\n";
                $message ="Message reçu de ".$_POST['email']."\n".Lib::cleanHtmlStrict($_POST['message']);
                mail('christophe226@gmail.com,secretariat@issytriathlon.com', 'contact Issy Tri v3', $message, $headers);
                $flash->success("Votre demande de contact est envoyée");
            }
        }

        $this->render("front/contactCreate");
    }

    /**
     * Admin function to update a contact request's status once the webmaster has responded
     *
     * @return void
     */
    function update($contactId) {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else {
            $contact=Contact::read($contactId) ;
            if (isset($_POST['submit'])) {
                $contact->setContactStatus($_POST['contactStatus']) ;
                if ($contact->update()) {
                    $flash->success("Contact ".$contact->getContactId()." updated");
                }
                else {
                    $flash->error("Error updating contact ".$contact->getContactId());
                }
            }
        }
        $params=['contact'=>$contact];
        $this->render("admin/contactUpdate",$params);
    }

    function delete($contactId){
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        }
        else {
            if ($contact=Contact::read($contactId)) {
                if ($contact->getContactStatus()==1) {
                    $flash->error("Please close contact request before deleting");
                }
                else {
                    if($contact->delete()) {
                        $flash->warning("Contact request ".$contact->getContactId()." deleted");
                    }
                    else {
                        $lash->error("Error deleting contact request ".$contact->getContactId());
                    }
                }
            }
            else{
                $flash->error("Invalid Contact Id");
            } 
            $this->redirectTo("Contact");
        }
    }
 }