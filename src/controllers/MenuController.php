<?php


class MenuController extends Controller implements Icontroller {

    function index(){
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $menusList=Menu::readAll();                         // Retrieve list of users        
            $this->render("admin/menuIndex",$menusList);        // Display list of users 
        }
    }

    function create():bool {
        $flash=new SessionFlash(); 
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        }
        else {
            if (isset($_POST['submit'])) {
                if(isset($_POST['menuTitle'])&&
                isset($_POST["menuOrder"])   
                ) 
                {       
                    $newMenu=new Menu();
                    $newMenu->setMenutitle(Lib::cleanHtml($_POST['menuTitle']));
                    $newMenu->setMenuOrder($_POST['menuOrder']);
                    if ($newMenu->create()) {
                        $flash->success("Menu created");

                    } else  { 
                        $flash->error("Error creating Menu"); 
                    }
                }
                else {$flash->error("Missing parameters");
                }
            }
        }
        $this->render("admin/menuCreate");                    // Display user add form   
    }

     /**
     * Function called by URL admin.php?class=Menu&action=edit&id=<menu Id of database entry to update>
     * $_POST contains form parameters 
     *
     * @param [type] $menuId
     * @return void
     */
    public function update($menuId) {
        $flash=new SessionFlash();
        // Check admin rights
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            // get user record from database
            $menu=Menu::read($menuId) ;
            if (isset($_POST['submit'])) {
               // set parameters from form
                $menu->setMenuTitle(Lib::cleanHtml($_POST["menuTitle"]));
                $menu->setMenuOrder($_POST["menuOrder"]);
               // update initial set of parameters
                if ($menu->update()) {$flash->success("Menu data updated");}
                else {$flash->error("Menu data not updated");}
            }
            $this->render("admin/menuUpdate",array($menu)) ;          // render form to edit menu data
        }
    }

    /**
     * function called by URL admin.php?class=Menu&action=delete&id=<id of record to be deleted>
     *
     * @return void
     */
    public function delete() {
        $flash=new SessionFlash();
        if (!UserController::isLoggedAdmin()) {
            $flash->error("You must log in as admin to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $menu=Menu::read($_GET["id"]) ;
                if ($menu->delete()) {
                    $flash->warning("Menu record deleted");
                }
                else {
                    $flash->error("Error deleting menu record");
                }
            } 
            else $flash->error("Missing parameter: menu ID");
            //
            $this->redirectTo("Menu");
        }
    }

    /************************************************* user methods *************************************************/

    // 
    public function buildNavBar() {
        $navBarHtml="";
        $menus=Menu::readAll();
        foreach($menus as $menu) {
            $pages=Page::readAllByMenuId($menu->getMenuId());
            $navBarHtml=$navBarHtml."<li class='nav-item dropdown text-white'>".
                "<a class='nav-link dropdown-toggle issyMenuBar' href='' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>".
                Lib::cleanHtml($menu->getMenuTitle()).
                "</a>".
                "<div class='dropdown-menu' aria-labelledby='navbarDropdown'>";
            foreach ($pages as $page) {
                switch ($page->getPageId()) {
                    case 7:
                    case 25:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=AdhesionAdulte'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                    case 12:
                    case 26:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=AdhesionJeune'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                    case 20:
                    case 27:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=AdhesionSportSanteFemmes'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                    case 28:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=AdhesionSportSanteHommes'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                    case 30:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=AdhesionTriKid'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                    case 19:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=Partner'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                    default:
                        $navBarHtml=$navBarHtml."<a class='dropdown-item' href='".
                        "index.php?class=Page&action=display&id=".$page->getPageId()."'>".
                        Lib::cleanHtml($page->getPageTitle())."</a>";
                        break;
                }

            }
        }
        $navBarHtml=$navBarHtml."</div></li>";
        return ($navBarHtml);
    }
}