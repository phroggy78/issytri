<?php 

/**
 * Author : Philippe Roggeband February 2020
 * Version 1.0 
 * 
 * Controller to manage Document library 
 * 
 * Admin Methods :  
 * 
 *  Index
 *      List all document ojects in database
 * 
 *  Create
 *      Validate file format & size
 *      Upload file to server
 *      Create document object and set attributes
 *      Store iamge obect in database
 *      add tags to document
 * 
 *  Update
 *      May be used to modify the document legend and description
 *      May be used to add / remove tags 
 * 
 *  Delete
 *      
 *      Remove all links to the document prior to delete it
 *      Delete file from server
 *      Delete document entry from database
 * 
 */

class DocumentController extends Controller implements Icontroller {

    /**
     * Display list of documents currently in library
     *
     * @return void
     */
    function index() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $documentList=Document::readAll();
            $documents=[];
            //

            foreach ($documentList as $document) {
                array_push($documents, [
                    'documentId'=>$document->getDocumentId(),
                    'documentPath'=>$document->getDocumentPath(),
                    'documentLegend'=>$document->getDocumentLegend(),
                ]);
            }
            $this->render("admin/documentIndex",$documents);
        }
    }

    /**
     * Add a new document to the library. Validate document size & format, 
     * Associate tags with document. 
     *
     * @return void
     */
    function create() {
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else {
            if (isset($_POST['submit']))  {

                // check mandatory parameters 
                if (isset($_FILES['documentFile']) &&
                    isset($_POST['documentLegend']) 
                ) {
                    // validate document format and upload to server
                    $targetDir = "documents/";
                    $targetFile = $targetDir.basename($_FILES["documentFile"]["name"]);
                    $uploadOk = 1;
                    $documentFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));

                    // Check if file already exists
                    if (file_exists($targetFile)) {
                        $flash->error( "Sorry, file already exists.");
                        $uploadOk = 0;
                    }

                    // Check file size
                    if ($_FILES["documentFile"]["size"] > 5000000) {
                        $flash->error( "Sorry, your file is too large.");
                        $uploadOk = 0;
                    }

                    // Allow certain file formats
                    if($documentFileType != "pdf" && $documentFileType != "docx" && $documentFileType != "doc" ) {
                        $flash->error( "Sorry, only pdf, doc and docx files are allowed.");
                        $uploadOk = 0;
                    }

                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        $flash->error("Sorry, your file was not uploaded.");
                    // if everything is ok, try to upload file
                    } else {
                        if (move_uploaded_file($_FILES["documentFile"]["tmp_name"], $targetFile)) {
                            $flash->success ("Document ". basename( $_FILES["documentFile"]["name"]). " has been uploaded.");

                            //
                            // Create document object in database
                            $newDocument=new Document();
                            $newDocument->setDocumentPath($targetFile);
                            $newDocument->setDocumentLegend($_POST['documentLegend']);
                            $newDocument->setDocumentText($_POST['documentText']);
                            if ($newDocument->create()) {
                                $flash->success("Document successfully created");
                                $newDocumentId=$newDocument->getDocumentId();
                                // add tags to document
                                if (!empty($_POST["documentTags"])) {
                                    $documentTags = explode (",", $_POST['documentTags']);
                                    
                                    foreach ($documentTags as $documentTag) {
                                        Lib::tagObject(ucfirst(strtolower($documentTag)),"Document", $newDocumentId);
                                    }
                                }
                            } else {
                                $flash->error("Error creating Document Object");
                            };
                        } else {
                            $flash->error ("Sorry, there was an error uploading your file.");
                        }
                    }
                }
            }
            $this->render("admin/documentCreate");
        }
    }
    

    /**
     * Update existing document in library.  Only attributes such as legend, description etc can be updated. The actual document file cannot be modified.
     *
     * @param [type] $documentId
     * @return void
     */
    function update($documentId){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            $document=Document::read($documentId);
            $documentTags=TagObject::readAllByObject("Document",$documentId);      // Get the section Tags before Update
            $documentTagsArray=[];
            // convert to an array of tags for comparison with updated field 
            foreach ($documentTags as $documentTag) {
                array_push($documentTagsArray,Tag::read($documentTag->getTagId())->getTagName());
            }
            // Convert array to CSV format for display
            $documentTagsList=implode(",",$documentTagsArray);   
            if (isset($_POST['submit']))  {
                if (isset($_POST['documentLegend'])) {
                    $document->setDocumentLegend($_POST['documentLegend']);
                    $document->setDocumentText($_POST['documentText']);
                    if ($document->update()) {
                        $flash->success("Document record was updated");
                        // 
                        // Check if tags have been modified.
                        //
                        $updatedDocumentTags = explode (",", $_POST['documentTags']);
                        for ($i=0; $i< count($updatedDocumentTags);$i++) {
                            $updatedDocumentTags[$i]=ucfirst(strtolower($updatedDocumentTags[$i]));
                        }
                        
                        // Check if any tags have been added or removed
                        $newTags=array_diff($updatedDocumentTags,$documentTagsArray);
                        $removedTags=array_diff($documentTagsArray,$updatedDocumentTags);

                        foreach ($newTags as $newTag) {
                            Lib::tagObject($newTag,"Document",$documentId) ;
                        }

                        foreach($removedTags as $removedTag) {
                            Lib::untagObject($removedTag,"Document",$documentId) ;
                        }
                        $documentTagsArray=$updatedDocumentTags ;
                        $documentTagsList=implode(",",$documentTagsArray);

                    }
                    else {
                        $flash->error("Document record was not updated");
                    }
                }
                else {
                    $flash->error("Missing parameters");
                }
            }
            $params=[
                $document,
                $documentTagsList
            ];
            $this->render("admin/documentUpdate",$params);
        }
    }

    /**
     * Remove an document from the library, after deleting any relationships with other objects.
     *
     * @return void
     */
    function delete(){
        $flash=new SessionFlash();
        if (!UserController::isLogged()) {
            $flash->error("You must log in as admin or editor to execute this function");
            require ("views/admin/userLogin.php");
        } else
        {
            if (isset($_GET["id"])){
                $document=Document::read($_GET["id"]) ;
                // Delete document file
                if (unlink($document->getDocumentPath())) {
                    $flash->success("Document file ".$document->getDocumentPath()." deleted");
                } else {
                    $flash->error("Error deleting document file ".$document->getDocumentPath());
                }

                // Remove tags from document.
                TagObject::deleteTagObjectsByObject("Document",$document->getDocumentId());
                // Delete document record from database
                if ($document->delete()) {
                    $flash->warning("Document record deleted");
                }
                else {
                    $flash->error("Error deleting Document record");
                }
            } 
            else $flash->error("Missing parameter: Document ID");
            //
            $this->redirectTo("Document");
        }
    }
}