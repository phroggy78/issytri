<?php 

/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History : 
 * 
 * This class implements the search engine 
 * 
 * Methods are called via URL 
 *      index.php?class=Search&action=<method> - Arguments are passed by POST
 *      
 * 
 * Methods : 
 *  Index - explode csv search strings into an array of search tags
 *          List all objects (pages, sections, articles and images ) tagged with at least one of the search tags
 *          If an object has multuple tags, only return the object once
 *  View includes JS script to extend search to titles of objects.
 */ 

class SearchController extends Controller implements Icontroller {

    /**
     * Renders an array of objects tagged with one of the keywords in the search string passed by POST method
     *
     * @return void
     */
    function index(){
        $flash=new SessionFlash();
        $params=[];
        // Check if parameters are passed by GET or POST
        if (isset($_POST['searchString'])) {
            if(!empty($_POST['searchString'])) {
                // Verify if searchString containts one or several tags
                $searchTags=explode(",",$_POST['searchString']);
            }
        } else if (isset($_GET['searchString'])) {
            if(!empty($_GET['searchString'])) {
                // Verify if searchString containts one or several tags
                $searchTags=array($_GET['searchString']);
            }
        }  else {
            // No parameters passed
            $flash->error("Search arguments missing");
            $this->redirectTo("Front"); 
        }
        
        $tagFound=false;
        $objectsFound=[];
        $pagesFound=[];
        $sectionsFound=[];
        $articlesFound=[];
        $imagesFound=[];
        $sanitizedSearchTags = [];
        foreach ($searchTags as $searchTag) {
            $sanitizedSearchTags[] = htmlspecialchars($searchTag, ENT_QUOTES, 'UTF-8');
        }
        foreach ($sanitizedSearchTags as $searchTag) {
            $searchTag=ucfirst(strtolower(trim($searchTag)));
            if ($tag=Tag::readByTagName($searchTag)) { 
                $tagObjects=TagObject::readAllByTagId($tag->getTagId());
                foreach ($tagObjects as $tagObject) {
                    switch ($tagObject->getObjectClass()) {
                        case "Page" :
                            // Check if page has already been selected for another tag
                            if (!in_array($tagObject->getObjectId(),$pagesFound)) {
                                // Check if page has not been deleted
                                if (Page::read($tagObject->getObjectId())) {
                                    array_push($pagesFound,$tagObject->getObjectId());
                                }
                            }
                            break;
                        case "Section" :

                            // Check if section has already been selected for another Tag
                            if (!in_array($tagObject->getObjectId(),$sectionsFound)) {
                                // Check if Section has not been deleted
                                if (Section::read($tagObject->getObjectId())) {
                                    array_push($sectionsFound,$tagObject->getObjectId());
                                }
                            }
                            break;
                        case "Article" :
                            // check if Article has already been selected for another Tag
                            if (!in_array($tagObject->getObjectId(),$articlesFound)) {
                                // Make sure article has not been deleted
                                if (Article::read($tagObject->getObjectId())) {
                                    array_push($articlesFound,$tagObject->getObjectId());
                                }

                            }
                            break;
                        case "Image" :
                            // Check if image has already been selected for another tag
                            if (!in_array($tagObject->getObjectId(),$imagesFound)) {
                                if (Image::read($tagObject->getObjectId())) {
                                    array_push($imagesFound,$tagObject->getObjectId());
                                }
                            }
                            break;
                    }
                }
            }
            else {
                $flash->warning("Tag $searchTag inconnu");
            }
        }
        $params=[
            $pagesFound,                                // Array of page ids with one or more tags
            $sectionsFound,                             // Array of section ids
            $articlesFound,                             // Array of article ids
            $imagesFound,                               // Array of image ids
            $sanitizedSearchTags                                 // Array of search keywords, used for additional searches in JavaScript
        ];

        $this->render("front/search",$params);
    }
}