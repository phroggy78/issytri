<?php


class SessionFlash
{
    public function __construct()
    {
        if(session_status() == PHP_SESSION_NONE)
        {
            session_start();
        }
        if(!array_key_exists("flash", $_SESSION))
        {
            $_SESSION["flash"] = [];
        } 
    }


    private function add($message, $type)
    {
        if(!array_key_exists("flash", $_SESSION))
        {
            $_SESSION["flash"] = [];
        } 
        array_push($_SESSION['flash'], ["type" => $type, "message" => $message]);
    }

/**
 * Undocumented function
 *
 * @return void
 */
    public function flash()
    {
        if(!empty($_SESSION['flash']))
        {
            foreach ($_SESSION['flash'] as $value) 
            {
                echo <<<HTML
                    <div class="alert alert-{$value['type']}" >
                        {$value['message']}
                    </div>
HTML;
            }
            unset($_SESSION['flash']);
        }
    }

    public function success($message)
    {
        $this->add($message, "success");
    }

    public function error($message)
    {
        $this->add($message, "danger");
    }

    public function information($message)
    {
        $this->add($message, "primary");
    }
    public function warning($message)
    {
        $this->add($message, "warning");
    }
}


