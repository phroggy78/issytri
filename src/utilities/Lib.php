<?php 

/**
 * This class is used to group a set of common functions used by several controllers, specifically those which tie together objects via
 * a relationshtip object, or those which build html code to display content in a given format
 * 
 */

use HtmlSanitizer\Extension\Basic\BasicExtension;
use HtmlSanitizer\Extension\Iframe\IframeExtension;
use HtmlSanitizer\Extension\Listing\ListExtension;
use HtmlSanitizer\Extension\Table\TableExtension;
use Sanitizer\Extension\Image\ImageExtension;
use Sanitizer\Extension\A\AExtension;
use HtmlSanitizer\SanitizerBuilder;

class Lib {

    /******************************************** Security functions  ********************************/

    /**
     * Sanitizes HTML code which has been entered with an editor. 
     * Dependency : https://github.com/tgalopin/html-sanitizer/blob/master/docs/1-getting-started.md 
     *
     * @param $untrustedHtml - code entered by the user 
     * @return  $safeHtml - sanitized code
     */
    public static function cleanHtml($untrustedHtml) {
        $builder = new SanitizerBuilder();
        $builder->registerExtension(new BasicExtension());
        $builder->registerExtension(new ListExtension());
        $builder->registerExtension(new TableExtension());
        $builder->registerExtension(new IframeExtension());
        $builder->registerExtension(new ImageExtension());
        $builder->registerExtension(new AExtension());
        $sanitizer = $builder->build(['extensions' => ['basic', 
                                                        'list', 
                                                        'table', 
                                                        'iframe',
                                                        'custom-image', 
                                                        'custom-a'],
                                                        'tags' =>[
                                                            'div' => ['allowed_attributes' => ['id','class', 'position'] ],
                                                            'a' => ['allowed_attributes' => ['target']],
                                                            'iframe' => ['allowed_attributes' => ['class', 'width', 'height', 'allow', 'allowfullscreen', 'position']]
                                                        ]]);
        // $sanitizer = HtmlSanitizer\Sanitizer::create(['extensions' => ['basic', 'list', 'table']]);
        $safeHtml = $sanitizer->sanitize($untrustedHtml);
        return $safeHtml;
    }


    /**
     * Sanitizes HTML code which has been entered with an editor. 
     * Dependency : https://github.com/tgalopin/html-sanitizer/blob/master/docs/1-getting-started.md 
     *
     * @param $untrustedHtml - code entered by the user 
     * @return  $safeHtml - sanitized code
     */
    public static function cleanHtmlStrict($untrustedHtml) {
        $builder = new SanitizerBuilder();
        $builder->registerExtension(new BasicExtension());
        $builder->registerExtension(new ListExtension());
        $builder->registerExtension(new TableExtension());
        $builder->registerExtension(new IframeExtension());
        $builder->registerExtension(new ImageExtension());
        $builder->registerExtension(new AExtension());
        $sanitizer = $builder->build(['extensions' => ['basic'],
                                'tags' => [
                                    'a' => [
                                        /*
                                        * If an array is provided, links targeting other hosts than one in this array
                                        * will be disabled (the `href` attribute will be blank). This can be useful if you want
                                        * to prevent links targeting external websites. Keep null to allow all hosts.
                                        * Any allowed domain also includes its subdomains.
                                        *
                                        * Example:
                                        *      'allowed_hosts' => ['trusted1.com', 'google.com'],
                                        */
                                        'allowed_hosts' => ['issytriathlon.com'],
                                        
                                        /*
                                        * If true, mailto links will be accepted.
                                        */
                                        'allow_mailto' => false,
                                    ],
                                    'img' => [
                                        /*
                                         * If an array is provided, images relying on other hosts than one in this array
                                         * will be disabled (the `src` attribute will be blank). This can be useful if you want
                                         * to prevent images contacting external websites. Keep null to allow all hosts.
                                         * Any allowed domain also includes its subdomains.
                                         *
                                         * Example:
                                         *      'allowed_hosts' => ['trusted1.com', 'google.com'],
                                         */
                                        'allowed_hosts' => null,
                                        
                                        /*
                                         * If true, images data-uri URLs will be accepted.
                                         */
                                        'allow_data_uri' => false,
                                    ],
                                    
                                    'iframe' => [
                                        /*
                                         * If an array is provided, iframes relying on other hosts than one in this array
                                         * will be disabled (the `src` attribute will be blank). This can be useful if you want
                                         * to prevent iframes contacting external websites.
                                         * Any allowed domain also includes its subdomains.
                                         *
                                         * Example:
                                         *      'allowed_hosts' => ['trusted1.com', 'google.com'],
                                         */
                                        'allowed_hosts' => null,
                                    ]
                                ]
                                    
                    ]);
        // $sanitizer = HtmlSanitizer\Sanitizer::create(['extensions' => ['basic', 'list', 'table']]);
        $safeHtml = $sanitizer->sanitize($untrustedHtml);
        return $safeHtml;
    }

    /********************************************Tagging functions  **********************************/

    public  static function tagObject ($tagName ,$objectClass,$objectId) {
        $flash=new SessionFlash();
        // check if tag already exists
        if ($tagName!="") {
            if ($tag=Tag::readByTagName($tagName)) { 
                $flash->warning("Tag $tagName already exists") ;}
            else {
            // if not, create it
                $tag=new Tag();
                $tag->setTagName($tagName);
                $tag->setTagDescription("Tag Created by $objectClass controller");
                if ($tag->create()) {
                $flash->success("Tag $tagName created"); }
                else {
                $flash->error("Error adding $tagName");
                }
            }
            // Create new Article Tag Object
            $tagObject=new TagObject();
            $tagObject->setTagId($tag->getTagId());
            $tagObject->setObjectClass($objectClass);
            $tagObject->setObjectId($objectId);
            $tagObject->create();
            $flash->success("Tag $tagName attached to $objectClass $objectId");
        }
        return;
    }

    /**
     * Untag an object : delete the link between the tag and the object from the database
     *
     * @param [type] $tag
     * @param [type] $sectionId
     * @return void
     */
    public  static function untagObject($tagName,$objectClass,$objectId) {
        if ($tagName!="") {
            $flash=new SessionFlash();
            TagObject::untagObject(Tag::readByTagName($tagName)->getTagId(),$objectClass,$objectId);
            $flash->warning("Tag $tagName removed from $objectClass $objectId");
        }
        return;
    }
    
    /**************************************************** Image functions  **********************************************/

    /**
     * Attach an image to an object. 
     *
     * @param [type] $imageId : image unique identifier
     * @param [type] $objectClass : class of object to which image must be attached
     * @param [type] $objectId : identifier of object to which image must be attached
     * @return void
     */
    public static function imageObject($imageId, $objectClass, $objectId) {
        if ($imageId !=""){
            $flash=new SessionFlash();
            $imageObject=new ImageObject();
            $imageObject->setImageId($imageId);
            $imageObject->setObjectClass($objectClass);
            $imageObject->setObjectId($objectId);
            if($imageObject->create()) {
                $flash->success("Image $imageId added to $objectClass $objectId") ;
            }
            else {
                $flash->error("Error adding image $imageId to $objectClass $objectId") ;
            }
            return ; 
        }
    }

    /**
     * detach an image from an object
     *
     * @param [type] $imageId
     * @param [type] $objectClass
     * @param [type] $objectId
     * @return void
     */
    public static function unimageObject($imageId,$objectClass, $objectId) {
        if ($imageId !=""){
            $flash=new SessionFlash();
            if (ImageObject::deleteImageObject($imageId,$objectClass, $objectId)) {
                $flash->warning("Image $imageId removed from $objectClass $objectId") ;
            }
            else {
                $flash->error("Error removing image $imageId from $objectClass $objectId") ;
            }
        }
    }

    /**
     * Build the HTML code to display images. If a single image, the image is displayed stand alone. 
     * If more than one image a carrousel is built. The calling view is responsable for displaying the HTML code produced.
     *
     * @param [type] $images
     * @return string  of HTML code
     */
    public static function displayImages($images ) {
        $imagesHtml="";
        $firstImage=True;

        if (count($images)!==0) {

                ob_start()?> 
                <!-- Build list of captions -->
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <!-- Build list of images --> 
                    <?php $firstImage = true; ?>
                    <div class="carousel-inner">
                        <?php foreach ($images as $image) :?>
                        <div class="carousel-item<?php if ($firstImage) { echo (' active'); $firstImage=false; } ?>">
                            <img src="<?=$image->getImagePath()?>" class="d-block w-100 img-fluid" alt="<?=$image->getImageLegend()?>">
                        </div>
                        <?php endforeach ?>                   
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Suiv.</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Prec.</span>
                    </a>
                </div>

        <?php $imagesHtml=ob_get_clean() ;
        }  
        return $imagesHtml ;
    }



    /**
     * Method to build HTML code to display the dynamic tiles at the top of a page
     *
     * @return string of HTML code
     */

    public static function buildDynamicTiles() {

        $tiles=Tile::readAll() ;                        // read all tiles
        $tilesHtml="";

        ob_start()?>
            <p></p>
            <div class="card-deck tile-flex">
            <?php $tiles=Tile::readAll()?>
            <?php foreach ($tiles as $tile) :?>
                <?php 
                    // Validate if tile should be displayed depending on status and date 
                    if ((TileController::displayTile($tile) && ($tile->getTileOrder() >= 10))) :?>

                        <!-- Prepare tiled display -->
                        <?php
                            switch ($tile->getTileImportance()) {
                                case 0 : 
                                    $importance="primary";
                                    $text="white";
                                break;

                                case 1 ;
                                    $importance="secondary";
                                    $text="white";
                                break;

                                case 2 ;
                                    $importance="danger";
                                    $text="white";
                                break;

                                case 3 ;
                                    $importance="warning";
                                    $text="white";
                                break;

                                case 4 :
                                    $importance="dark" ;
                                    $text="white";
                                break;

                                case 5: 
                                    $importance="light";
                                    $text="dark";
                                break;

                            }
                            // get tile images and convert to HTML//
                            $imagesHtml="";
                            $tileImageObjects=ImageObject::readAllByObject("Tile",$tile->getTileId());
                            $images=[];
                            foreach($tileImageObjects as $tileImageObject) {
                                array_push($images,Image::read($tileImageObject->getImageId()));
                            }
                            $imagesHtml=Lib::displayImages($images);
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 mb-1">
                            <div class="card mb-3 text-<?=$text?> bg-<?=$importance?>  tile" data-tileId="<?=$tile->getTileId()?> ">
                                <img src="<?=$tile->getTileImage()?>" class="card-img-top img-fluid d-none d-md-block">
                                <div class="card-header">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-<?=$importance?>" data-toggle="modal" data-target="#tile<?=$tile->getTileId()?>">
                                    <strong><?=$tile->getTileTitle()?> <i class="fas fa-info-circle"></i> </strong>
                                    </button>
                                </div>
                            </div>
                        </div>


                        <!-- Modal used to display tile extended content-->
                        <div class="modal fade" id="tile<?=$tile->getTileId()?>" tabindex="-1" role="dialog" aria-labelledby="tileLabel<?=$tile->getTileId()?>" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-<?=$importance?> text-<?=$text?>">
                                        <h5 class="modal-title " id="tileLabel<?=$tile->getTileId()?>"><?=$tile->getTileTitle()?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <?php if ($imagesHtml !=="") :?>
                                            <div>
                                                <?=$imagesHtml?>
                                            </div>
                                        <?php else :?>
                                            <div>
                                                <img src="<?=$tile->getTileImage()?>" class="img-fluid">
                                            </div>
                                        <?php endif ?>
                                        <div>
                                            <?=Lib::cleanHtml(html_entity_decode($tile->getTileContent()))?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php endif ?>
            <?php endforeach ?>

            </div>
        <?php $tilesHtml=ob_get_clean() ;
        return $tilesHtml;
    }

    /**
     * Method to build HTML code to display the dynamic tiles at the top of a page
     *
     * @return string of HTML code
     */

    public static function buildDynamicTilesCarousel() {


        $tiles=Tile::readAll() ;                        // read all tiles
        $tilesHtml="";
        $tilesPerPage=4;                                // Number of slides displayed 
        $tilesCount=0;
        $tilesCounter=0;                               
        $tiles=Tile::readAll();
        $dynamicTiles=[];
        //
        // Count tiles to be displayed
        //
        foreach ($tiles as $tile) {
            if ((TileController::displayTile($tile) && ($tile->getTileOrder() >= 10))) {
                array_push($dynamicTiles,$tile);
            }
        }
        $tilesCount=count($dynamicTiles);

        ob_start()?>
            <!--Carousel Wrapper-->
            <div id="tileCarousel" class="carousel slide mt-3" data-ride="carousel">

                <!--Controls-->
                <div class="controls-top buttons">
                    <a class="carousel-control-prev" href="#tileCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Suiv.</span>
                    </a>
                    <a class="carousel-control-next" href="#tileCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Prec.</span>
                    </a>
                </div>  <!--/.Controls-->              

                <!--Slides-->
                <div class="carousel-inner">
                    <?php foreach ($dynamicTiles as $tile) :?>
                        <?php 
                            $tilesCounter++;
                        ?>
                        <?php if ($tilesCounter==1) :?>
                            <!--Open First Carousel Item set-->
                            <div class="carousel-item active">
                            <div class="container">
                            <div class="card-group">
                        <?php endif ?>

                        <!-- Prepare tiled display -->
                        <?php
                            switch ($tile->getTileImportance()) {
                                case 0 : 
                                    $importance="primary";
                                    $text="white";
                                break;

                                case 1 ;
                                    $importance="secondary";
                                    $text="white";
                                break;

                                case 2 ;
                                    $importance="danger";
                                    $text="white";
                                break;

                                case 3 ;
                                    $importance="warning";
                                    $text="white";
                                break;

                                case 4 :
                                    $importance="dark" ;
                                    $text="white";
                                break;

                                case 5: 
                                    $importance="light";
                                    $text="dark";
                                break;

                            }
                            // get tile images and convert to HTML//
                            $imagesHtml="";
                            $tileImageObjects=ImageObject::readAllByObject("Tile",$tile->getTileId());
                            $images=[];
                            foreach($tileImageObjects as $tileImageObject) {
                                array_push($images,Image::read($tileImageObject->getImageId()));
                            }
                            $imagesHtml=Lib::displayImages($images);
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 mb-1">
                            <div class="card text-<?=$text?> bg-<?=$importance?>  tile" data-tileId="<?=$tile->getTileId()?> ">
                                <img src="<?=$tile->getTileImage()?>" class="card-img-top img-fluid d-none d-md-block">
                                <div class="card-header">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-<?=$importance?>" data-toggle="modal" data-target="#tile<?=$tile->getTileId()?>">
                                    <strong><?=$tile->getTileTitle()?> <i class="fas fa-info-circle"></i> </strong>
                                    </button>
                                </div> <!-- /.card-header -->
                            </div> <!-- /.card-->
                        </div> <!-- /.col -->


                        <!-- Modal used to display tile extended content -->
                        <div class="modal fade" id="tile<?=$tile->getTileId()?>" tabindex="-1" role="dialog" aria-labelledby="tileLabel<?=$tile->getTileId()?>" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-<?=$importance?> text-<?=$text?>">
                                        <h5 class="modal-title " id="tileLabel<?=$tile->getTileId()?>"><?=$tile->getTileTitle()?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>   <!-- /.modal-header -->

                                    <div class="modal-body">
                                        <?php if ($imagesHtml !=="") :?>
                                            <?=$imagesHtml?>
                                        <?php else :?>
                                            <img src="<?=$tile->getTileImage()?>" class="img-fluid">
                                        <?php endif ?>

                                            <?=Lib::cleanHtml(html_entity_decode($tile->getTileContent()))?>
                                    </div> <!-- /.modal-body -->
                                </div> <!-- /.modal-content -->
                            </div> <!-- /.modal-dialog -->
                        </div> <!-- /.modal -->
                        
                        <!-- check if we should close current tileset item -->
                        <?php if ((($tilesCounter % $tilesPerPage)==0) || ($tilesCounter == $tilesCount)):?>
                            </div> <!-- /.card-group -->
                            </div> <!-- /.container -->
                        </div>          <!-- /.carousel-item -->
                            <!-- check if we should open a new tileset item -->
                            <?php if ($tilesCounter < $tilesCount) :?>
                                <div class="carousel-item"> <!-- Open second (inactive) Carousel Item set -->
                                <div class="container">
                                <div class="card-group">
                            <?php endif ?>
                        <?php endif ?> 

                    <?php endforeach ?>
                </div> <!-- /.carousel-inner -->
            </div> <!-- /.carousel -->
        <?php $tilesHtml=ob_get_clean() ;
        return $tilesHtml;
    }


    /**
     * Returns Html code to display static tiles (those with order number < 10) 
     *
     * @return html strieng
     */
    public static function buildStaticTiles() {

        $tiles=Tile::readAll() ;                        // read all tiles
        $tilesHtml="";
        ob_start()?>
            <p></p>
            <div class="card-column">
            <?php $tiles=Tile::readAll()?>
            <?php foreach ($tiles as $tile) :?>
                <?php 
                    // Validate if tile should be displayed depending on status and date 
                    if ((TileController::displayTile($tile) && ($tile->getTileOrder() <10))) :?>

                        <!-- Prepare tiled display -->
                        <?php
                            switch ($tile->getTileImportance()) {
                                case 0 : 
                                    $importance="primary";
                                    $text="white";
                                break;

                                case 1 ;
                                    $importance="secondary";
                                    $text="white";
                                break;

                                case 2 ;
                                    $importance="danger";
                                    $text="white";
                                break;

                                case 3 ;
                                    $importance="warning";
                                    $text="white";
                                break;

                                case 4 :
                                    $importance="dark" ;
                                    $text="white";
                                break;

                                case 5: 
                                    $importance="light";
                                    $text="dark";
                                break;

                            }
                            // get tile images and convert to HTML//
                            $imagesHtml="";
                            $tileImageObjects=ImageObject::readAllByObject("Tile",$tile->getTileId());
                            $images=[];
                            foreach($tileImageObjects as $tileImageObject) {
                                array_push($images,Image::read($tileImageObject->getImageId()));
                            }
                            $imagesHtml=Lib::displayImages($images);
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-3">
                            <div class="card text-<?=$text?> bg-<?=$importance?>  tile" data-tileId="<?=$tile->getTileId()?> ">
                                <img src="<?=$tile->getTileImage()?>" class="card-img-top img-fluid d-none d-md-block">
                                <div class="card-header">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-<?=$importance?>" data-toggle="modal" data-target="#tile<?=$tile->getTileId()?>">
                                    <strong><?=Lib::cleanHtml($tile->getTileTitle())?> <i class="fas fa-info-circle"></i> </strong>
                                    </button>
                                </div> <!-- /.card-head -->
                            </div> <!-- /.card -->
                        </div> <!-- /.col -->


                        <!-- Modal used to display tile extended content-->
                        <div class="modal fade" id="tile<?=$tile->getTileId()?>" tabindex="-1" role="dialog" aria-labelledby="tileLabel<?=$tile->getTileId()?>" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-<?=$importance?> text-<?=$text?>">
                                        <h5 class="modal-title " id="tileLabel<?=$tile->getTileId()?>"><?=$tile->getTileTitle()?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div> <!-- /.modal-header -->

                                    <div class="modal-body">
                                        <?php if ($imagesHtml !=="") :?>

                                                <?=$imagesHtml?>

                                        <?php else :?>
                                            <div>
                                                <img src="<?=$tile->getTileImage()?>" class="img-fluid">
                                            </div>
                                        <?php endif ?>
                                        <div>
                                            <?=Lib::cleanHtml(html_entity_decode($tile->getTileContent()))?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php endif ?>
            <?php endforeach ?>

            </div>
        <?php $tilesHtml=ob_get_clean() ;
        return $tilesHtml;
    }

    public static function buildTilesByImportance() {
        $importance="light";
        $tilesHtml="";
        $text="dark";

        ob_start()?>
            <?php for ($category=0;$category<5;$category++) :?>
                <?php 
                    $first=true;
                    $closeDiv=false;
                    $tiles=Tile::readAllByImportance($category);
                    switch ($category) {
                        case 0 :
                            $categoryName="Actualités";
                            $categoryClass="actu";
                            $tileSize="4";
                        break;
                        case 1 :
                            $categoryName="Vie du Club";
                            $categoryClass="club";
                            $tileSize="3";
                        break;
                        case 2 :
                            $categoryName="Evénementiel";
                            $categoryClass="event";
                            $tileSize="3";
                        break;
                        default:
                            $categoryName="Divers";
                            $objectClass="divers";
                            $tileSize="3";
                    }
                ?>

                <?php foreach ($tiles as $tile) :?>
                    <?php if (TileController::displayTile($tile)) :?>
                        <?php if ($first) :?>
                            <div class="row">
                                <span class="<?=$categoryClass?> mb-3"> <?= $categoryName ?></span> 
                            </div>

                            <div class="card-deck">
                            <?php $first=false; ?>
                            <?php $closeDiv=true; ?>
                        <?php endif ?>
                        <?php 
                            // get tile images and convert to HTML//
                            $imagesHtml="";
                            $tileImageObjects=ImageObject::readAllByObject("Tile",$tile->getTileId());
                            $images=[];
                            foreach($tileImageObjects as $tileImageObject) {
                                array_push($images,Image::read($tileImageObject->getImageId()));
                            }
                            $imagesHtml=Lib::displayImages($images);
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-<?=$tileSize?> mb-3" >
                            <!-- Button trigger modal -->
                            <button type="button" class="btn" data-toggle="modal" data-target="#tile<?=$tile->getTileId()?>">
                                <div class="card text-<?=$text?> bg-white  tile" data-tileId="<?=$tile->getTileId()?> ">
                                    <img src="<?=$tile->getTileImage()?>" class="card-img-top tile-image-<?=$tileSize?>">
                                    <div class="card-header p-0 bg-white">
                                        <div class="tile-category"><?=$categoryName?></div>
                                        <div class="tile-title"><?=Lib::cleanHtml($tile->getTileTitle())?></div>
                                    </div> <!-- /.card-head -->
                                </div> <!-- /.card -->
                            </button>
                        </div> <!-- /.col -->


                        <!-- Modal used to display tile extended content-->
                        <div class="modal fade" id="tile<?=$tile->getTileId()?>" tabindex="-1" role="dialog" aria-labelledby="tileLabel<?=$tile->getTileId()?>" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body p-0">
                                        <div class="modal-header bg-white">
                                            <div class="modal-title" id="tileLabel<?=$tile->getTileId()?>">
                                            </div>

                                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">

                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div> <!-- /.modal-header -->
                                        <img src="<?=$tile->getTileImage()?>" class="tile-image-header">

                                        <p class="mt-3">
                                            <span class="<?=$categoryClass?> ml-3 mt-5"> <?= $categoryName ?></span> 
                                        </p>


                                        <div class="h5 ml-3 mt-3"><?=$tile->getTileTitle()?></div>
                                        <div class="p-3 tile-text mt-3">
                                            <?=Lib::cleanHtml(html_entity_decode($tile->getTileContent()))?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>

                <?php endforeach ?>
                
                <?php if ($closeDiv) :?>
                    </div> <!-- /.card-deck -->

                <?php endif ?>

            <?php endfor ?>
 
        <?php $tilesHtml=ob_get_clean() ;
        return $tilesHtml;
    }

    /************************************************* Methods used by Page Display  ****************************/

    public static function buildArticleModal($article,$articleImagesHtml,$articleTags) {
        ob_start()?>
            <div class="modal fade" id="article<?=$article->getArticleId()?>" tabindex="-1" role="dialog" aria-labelledby="articleLabel<?=$article->getArticleId()?>" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-secondary text-white">
                            <h5 class="modal-title " id="articleLabel<?=$article->getArticleId()?>"><?=Lib::cleanHtml($article->getArticleTitle())?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php if ($articleImagesHtml !=="") :?>
                                <div>
                                    <?=$articleImagesHtml?>
                                </div>

                            <?php endif ?>
                            <div>
                                    <?=Lib::cleanHtml(html_entity_decode($article->getArticleContent()))?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- List tags assocated with the article, with automated search for related items -->
                            <p>
                                <?php foreach ($articleTags as $tag) :?>
                                    <a href="index.php?class=Search&searchString=<?=$tag->getTagName()?>" class="btn btn-outline-secondary btn-sm" role="button"><?=$tag->getTagName()?></a>
                                <?php endforeach ?>
                            </p>

                        </div>
                    </div>
                </div>
            </div> 
        <?php $articleModal=ob_get_clean() ;
        return $articleModal;
    }

    public static function buildArticleModal2($article,$articleImages,$articleTags,$section) {
        ob_start()?>
            <div class="modal fade" id="article<?=$article->getArticleId()?>" tabindex="-1" role="dialog" aria-labelledby="articleLabel<?=$article->getArticleId()?>" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <?php $imagesCount=count($articleImages); ?>

                        <div class="modal-body p-0">
                            <div class="modal-header bg-white">
                                <button type="button" class="close text-white z-top" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="container-fluid">
                                <?php if ($imagesCount==0) :?> <!-- no images associated with article -->

                                    <div class="row">
                                        <img src="img/default.jpg" class="articleBanner" alt="">
                                    </div>

                                <?php elseif ($imagesCount==1) :?> <!-- only one banner image associated with article -->
                                    <div class="row">

                                        <img src="<?=$articleImages[0]->getImagePath()?>" class="articleBanner" alt="">
                                    </div>
                                <?php else  :?> <!-- one banner image + one other image -->
                                    <div class="row">

                                        <img src="<?=$articleImages[0]->getImagePath()?>" class="articleBanner" alt="">
                                        <img src="<?=$articleImages[1]->getImagePath()?>" class="articleCircle" alt="">
                                    </div>
                                <?php endif ?>

                                <div class="row p-3 mt-5">
                                    <span class="calendar"><?=$section->getSectionTitle()?></span>
                                </div>
                                <div class="row p-3">
                                    <span class="title"><?=Lib::cleanHtml($article->getArticleTitle())?></span>
                                </div>
                                <div class="row p-3">
                                    <div class="tile-text">
                                        <?=Lib::cleanHtml(html_entity_decode($article->getArticleContent()))?>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- List tags assocated with the article, with automated search for related items -->
                            <p>
                                <?php foreach ($articleTags as $tag) :?>
                                    <a href="index.php?class=Search&searchString=<?=$tag->getTagName()?>" class="btn btn-outline-secondary btn-sm" role="button"><?=$tag->getTagName()?></a>
                                <?php endforeach ?>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        <?php $articleModal=ob_get_clean() ;
        return $articleModal;
    }

    /**
     * Return list of all images associated with a given objetct
     *
     * @param [type] $objectClass
     * @param [type] $objectId
     * @return array
     */
    public static function getObjectImages($objectClass,$objectId) :array{
        // Get all images for this section
        $imageObjects=ImageObject::readAllByObject($objectClass,$objectId);
        $images=[];
        foreach ($imageObjects as $imageObject) {
            array_push($images,Image::read($imageObject->getImageId()));
        }
        return $images;
    }

    /**
     *  Return list of tags associated with a given object 
     *
     * @param [type] $objectClass
     * @param [type] $objectId
     * @return array
     */
    public static function getObjectTags($objectClass,$objectId): array {
        $tagObjects=TagObject::readAllByObject($objectClass,$objectId);
        $tags=[];
        foreach ($tagObjects as $tagObject) {
            array_push($tags,Tag::read($tagObject->getTagId()));
        }
        return $tags;
    }

    /**
     * Return articles in a given section
     *
     * @param [type] $sectionId
     * @return void
     */
    public static function getSectionArticles($sectionId) {
        $articles=[];
        $articles=Article::readAllBySectionId($sectionId);
        return $articles;
    }

    /*************************************************** Other library functions ***************************************/

    /**
     * Check if today's date is within a range. 
     *
     * @param [type] $startDate
     * @param [type] $endDate
     * @return boolean 
     */
    public static function validateDate($startDate, $endDate) :bool {
        $rangeStart=new DateTime($startDate);
        $rangeEnd=new DateTime($endDate);
        $today=new DateTime("now");
        if (($today >= $rangeStart) && ($today<=$rangeEnd)) {
            $result=true ;
        } else {
            $result=false;
        }      
        return $result ;
    }

}