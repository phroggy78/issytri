<?php

class Controller
{
    /**
     * Display a view 
     *
     * @param [type] $vue View name
     * @param array $params parameters to be displayed in the view
     * @return void
     */
    public function render($vue, array $params = [])
    {
       if(!empty($params))
       {
            foreach($params as $key => $value)
            {
                ${$key} = $value;
            }
       }
        require "views/$vue.php";
    }

    /**
     * Redirect to a PhP file by constructing a URL to call the root index.php 
     *
     * @param [type] $class
     * @param string $action
     * @param array $params
     * @return void
     */
    public function redirectTo($class, $action = "index", array $params = [])
    {
        
        $attributes = "";
        if(!empty($params))
       {
            foreach($params as $key => $value)
            {
                $attributes = $attributes."&".$key."=".$value;    
                
            }
       }
        header("Location: index.php?class=$class&action=$action".$attributes);
    }
}