<?php

spl_autoload_register(function ($class)
{
    $sources = [
        "../src/controllers/$class.php",
        "../src/models/$class.php",
        "../src/utilities/$class.php"
    ];

    foreach($sources as $source)
    {
        if(file_exists($source))
        {
            require_once $source;
        }
    }
});