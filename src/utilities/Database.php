<?php
/**
 * General Database utility
 */

class Database
{

    private $cnx;
    private $lastId;

    public function __construct()
    {
        try {
            $this->cnx = new PDO("mysql:host=".DATABASE['host'].";dbname=".DATABASE['dbname'].";charset=utf8",DATABASE['user'],DATABASE['password'] );
        } catch (PDOException $e) {
            die($e->getMessage());
        }

    }

    /**
     * Reeturn ID (primaruy key) of last item inserted in the database
     *
     * @return integer
     */
    public function getLastId() : int
    {
        return $this->lastId;
    }

    /**
     * Insert row in the database
     *
     * @param string $request
     * @param array $params
     * @return boolean
     */
    public function insert(string $request, array $params) : bool
    {
        $stmt = $this->cnx->prepare($request);
        $result = $stmt->execute($params);
        $this->lastId = ($this->cnx)->lastInsertId();
        return $result;
    }

    /**
     * Read a single row from class/table
     * structure row as class object
     *
     * @param string $request
     * @param array $params
     * @param string $class
     * @return object|bool
     */
   public function getOne(string $request, array $params, string $class) 
   {
    $stmt = $this->cnx->prepare($request);
    $stmt->execute($params);
    $stmt->setFetchMode(PDO::FETCH_CLASS, $class);
    $result = $stmt->fetch();
     return $result;
   }

   /**
    * Return multuple rows from a table
    * structure as an array of objects
    *
    * @param string $request
    * @param array $params
    * @param string $class
    * @return array|bool
    */
   public function getMany(string $request, string $class, array $params = []) 
   {
    $stmt = $this->cnx->prepare($request);
    $stmt->execute($params);
    $result = $stmt->fetchAll(PDO::FETCH_CLASS, $class);
     return $result;
   }

   /**
    * Remove or or several rows from table
    *
    * @param string $request
    * @param array $params
    * @return boolean
    */
   public function delete(string $request, array $params) : bool
   {
       $stmt = $this->cnx->prepare($request);
       $result = $stmt->execute($params);
       return $result;
   }
   
   /**
    * update a single row in the database
    *
    * @param string $request
    * @param array $params
    * @return boolean
    */
   public function update(string $request, array $params) : bool
   {

       $stmt = $this->cnx->prepare($request);
       $result = $stmt->execute($params);
       return $result;
   }
   /**
    * Delete connection to database
    */
   public function __destruct()
   {
       $this->cnx = null;
       //unset($this->cnx)
   }
}