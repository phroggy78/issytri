-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  ven. 31 juil. 2020 à 09:35
-- Version du serveur :  5.7.26
-- Version de PHP :  7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `issytri`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `articleId` int(11) NOT NULL,
  `articleTitle` varchar(256) NOT NULL,
  `articleDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `articlePublicationDate` date DEFAULT NULL,
  `articleDateUpdated` datetime DEFAULT NULL,
  `articleExpiryDate` date DEFAULT NULL,
  `articleContent` text NOT NULL,
  `articleOrder` int(11) NOT NULL,
  `articleStatus` int(11) NOT NULL,
  `articleSectionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`articleId`, `articleTitle`, `articleDateCreated`, `articlePublicationDate`, `articleDateUpdated`, `articleExpiryDate`, `articleContent`, `articleOrder`, `articleStatus`, `articleSectionId`) VALUES
(1, 'Natation', '2020-01-28 17:15:56', '2020-01-31', '2020-04-30 12:29:31', '2025-12-31', '&lt;p&gt;Pas moins de&nbsp; 16 cr&eacute;neaux hebdomadaire de natation &amp;#34;Adultes&amp;#34; qui sont propos&eacute;s aux adh&eacute;rents de la section Loisirs. . Ceux-ci sont r&eacute;partis entre les piscines Alfred Sevestre et Aquazena, mis &agrave; disposition par la ville d&amp;#039;Issy-Les-Moulineaux.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Piscine Alfred Sevestre&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2626.6268423455704!2d2.264983115704173!3d48.82718091085779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67a84c99ab90b%3A0x88d24cc53a9d0e79!2sAlfred%20Sevestre%20Pool!5e0!3m2!1sen!2sfr!4v1588165535448!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Piscine Aquazena&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2627.1959231944215!2d2.2677011157038716!3d48.816323111621784!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67a7e3f958afb%3A0x7981204b0fac1653!2sPiscine%20Aquazena!5e0!3m2!1sen!2sfr!4v1588165575226!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 10, 1, 2),
(3, 'Stage de Pâques à la Colle s/loup', '2020-01-29 08:26:53', '2020-01-29', '2020-05-10 13:44:20', '2020-06-27', '&lt;p&gt;D&eacute;pechez-vous !! derni&egrave;res places disponibles&lt;/p&gt;\n\n&lt;p&gt;Il reste quelques places de libre pour le stage de La Colle sur Loup, contactez d&amp;#039;urgence Chris Web si vous souhaitez vous inscrire.&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2905.4968113987265!2d7.098064092192253!3d43.68088346800138!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12cdd32e85edda05%3A0xc48774dc6cc2b950!2sBelambra%20Clubs%20%22Les%20Terrasses%20de%20St%20Paul%20de%20Vence%22!5e0!3m2!1sen!2sfr!4v1587363683882!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n', 30, 1, 4),
(38, 'Prochaine sortie longue durée', '2020-03-04 14:56:40', '2020-03-07', '2020-03-08 16:31:34', '2020-03-08', '&lt;p&gt;La prochaine sortie se d&amp;eacute;roulera lundi soir &amp;agrave; la frontale.&lt;br /&gt;\r\nLe tout en maillot de bain, bien sur !!&lt;/p&gt;\r\n', 80, 2, 3),
(39, 'Vélo', '2020-03-21 17:19:48', '2020-03-21', '2020-07-30 12:44:59', '2020-12-31', '&lt;p&gt;Les entrainements de v&eacute;lo ont lieu tous les samedi matin &agrave; 9h00. Le point de rendez-vous est le Palais des Sports &agrave; Issy-Les-Moulineaux.&nbsp;&lt;/p&gt;\n', 20, 1, 2),
(40, 'Course à pied', '2020-03-21 17:23:00', '2020-03-21', NULL, '2020-12-31', '&lt;p&gt;Les entrainements de course &amp;agrave; pied ont lieu dans le parc Suzanne Lenglen, en attendant la reconstruction de notre stade Jean Bouin.&amp;nbsp;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Les cr&amp;eacute;neaux propos&amp;eacute;s nombreux : tous les soirs de semaine, ainsi que les mardi et jeudi midi.&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Au programme : du fractionn&amp;eacute;.&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Des sorties longues &amp;agrave; la frontale sont &amp;eacute;galement propos&amp;eacute;es le premier lundi du mois.&amp;nbsp;&lt;/p&gt;\r\n', 30, 1, 2),
(41, 'Christophe H.', '2020-03-23 13:17:47', '2020-03-23', '2020-04-20 10:08:14', '2022-02-28', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nChristophe. 38 ans. Directeur sportif du club.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nA pied car je ne savais pas encore nager&nbsp;;)&lt;/p&gt;\n\n&lt;p&gt;Apr&egrave;s 10 ans de foot et un petit raid dans la campagne nous sommes tomb&eacute;s au forum des associations sur le club Issy Triathlon. C&amp;#039;&eacute;tait il y &agrave; 20 ans et &agrave; l&amp;#039;&eacute;poque le plus jeune devait avoir 35 ans :)&lt;/p&gt;\n\n&lt;p&gt;Avec Guillaume (entraineur&nbsp;au club) on a pers&eacute;v&eacute;r&eacute; dans ce sport qui pour moi n&amp;#039;avait rien de ludique ni en lien avec mes capacit&eacute;s physique et mon esprit collectif mais quand on veux&nbsp;atteindre un objectif il faut s&amp;#039;en donner les moyens. Pour &ecirc;tre performant en RAID il faut s&amp;#039;entrainer&nbsp;dans les disciplines du triathlon. Voila maintenant je fais partie des murs :)&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nToutes m&ecirc;me si depuis quelques temps je n&amp;#039;entraine le v&eacute;lo qu&amp;#039;en stage&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nSurement l&amp;#039;arriv&eacute;e de mon Ironman en 2007 personne n&amp;#039;y croyait vu mon niveau d&amp;#039;entrainement. Ce jour l&agrave; certains ont du apprendre le mot humilit&eacute;, mental et quand tu provoques un coach attends toi &agrave; perdre :)&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp;&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nSurement&nbsp;le premier &agrave; Etampes et sa natation dans une grand flaque de boue&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nSportive, je dirai Jordan le mec a r&eacute;volutionn&eacute;&nbsp;son sport et force le respect.&nbsp; Autre: le Dalai Lama, Martin Luther King ou Nelson Mandela. Tous ont men&eacute;&nbsp;des combats spirituels, politiques et humains.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMa famille.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nFaites du sport pour vous en vivant le moment pr&eacute;sent mais que tu sois elite ou loisir tu dois &ecirc;tre r&eacute;gulier et objectif&nbsp;&lt;/p&gt;\n', 1, 1, 12),
(42, 'Audrey M.', '2020-03-23 13:33:39', '2020-03-23', '2020-04-02 20:43:27', '2022-03-24', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&amp;eacute;senter en trois lignes ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nJe m&amp;#39;appelle Audrey Merle et j&amp;#39;ai 24 ans. Je suis licenci&amp;eacute;e au club depuis 2017, club que j&amp;#39;ai d&amp;#39;abord rejoint pour son &amp;eacute;quipe Elite tout en m&amp;#39;entra&amp;icirc;nant au P&amp;ocirc;le France &amp;agrave; Montpellier, avant de prendre la d&amp;eacute;cision de m&amp;#39;y installer pour rejoindre mon entra&amp;icirc;neur actuel, Guillaume Lepors. En parall&amp;egrave;le, j&amp;#39;ai suivi un cursus universitaire en STAPS et obtenu en 2018 mon master en Entra&amp;icirc;nement Nutrition Biologie Sant&amp;eacute;.&amp;nbsp;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venue au Triathlon ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nJ&amp;#39;ai commenc&amp;eacute; la natation en club &amp;agrave; Chamali&amp;egrave;res (63) &amp;agrave; 6 ans. Depuis toute petite, j&amp;#39;ai suivi le syst&amp;egrave;me comp&amp;eacute;titif de cette discipline en int&amp;eacute;grant les diff&amp;eacute;rentes sections sportives. J&amp;#39;ai quitt&amp;eacute; le domicile familial &amp;agrave; 15 ans afin de rejoindre les EN Tours et continuer ma progression. A c&amp;ocirc;t&amp;eacute;, j&amp;#39;ai toujours aim&amp;eacute; courir, c&amp;#39;est pourquoi, les week-end, je m&amp;#39;inscrivais r&amp;eacute;guli&amp;egrave;rement sur les cross. En terminant troisi&amp;egrave;me des Championnats de France de Cross en Cadette et demi-finaliste aux Championnats de France Elite de Natation sur 200 papillon, la F&amp;eacute;d&amp;eacute;ration Fran&amp;ccedil;aise de Triathlon m&amp;#39;a contact&amp;eacute;e et propos&amp;eacute;e d&amp;#39;essayer l&amp;#39;Aquathlon. A ma grande surprise, je gagne ces Championnats. Il m&amp;#39;aura fallu un an avant de v&amp;eacute;ritablement choisir la voie du Triathlon en rejoignant le P&amp;ocirc;le &amp;agrave; Montpellier en 2012.&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &amp;agrave; Issy Triathlon ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nA Issy Triathlon, j&amp;#39;encadre actuellement le sport sant&amp;eacute; les mardi et jeudi en renforcement et/ou course &amp;agrave; pied, les &amp;quot;adultes&amp;quot; le mardi et les jeunes du groupe &amp;quot;performance&amp;quot; le lundi (en bin&amp;ocirc;me avec Guillaume) en natation, les poussins (avec Blandine et Marco) le mercredi apr&amp;egrave;s-midi dans les trois disciplines et enfin les jeunes du groupe &amp;quot;d&amp;eacute;couverte&amp;quot; le vendredi en natation.&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nSi je devais en choisir un seul, je dirais qu&amp;#39;il s&amp;#39;agit des Championnats du Monde de relais mixte en 2015 &amp;agrave; Hambourg. Si le r&amp;eacute;sultat ne pouvait &amp;ecirc;tre mieux puisque nous avons remport&amp;eacute; cette course, c&amp;#39;est surtout l&amp;#39;ambiance indescriptible de cette &amp;eacute;preuve qui m&amp;#39;aura marqu&amp;eacute;e &amp;agrave; vie. J&amp;#39;&amp;eacute;tais remplie d&amp;#39;&amp;eacute;motions avant m&amp;ecirc;me de prendre le d&amp;eacute;part !&amp;nbsp;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&amp;nbsp;&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nProbablement, une course de premi&amp;egrave;re division &amp;agrave; Embrun en 2017... j&amp;#39;&amp;eacute;tais au bout du rouleau physiquement et mentalement.&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&amp;eacute; (sportive ou autre) avec qui tu aimerais passer une soir&amp;eacute;e, et pourquoi ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nIl est difficile de faire un choix... donc je vais en citer trois : Martin Fourcade, Tony Estanguet et Teddy Riner. Tous les trois, bien qu&amp;#39;ils aient un palmar&amp;egrave;s exceptionnel, d&amp;eacute;gagent beaucoup de simplicit&amp;eacute; et t&amp;eacute;moignent &amp;eacute;galement que pour atteindre le tr&amp;egrave;s Haut Niveau, c&amp;#39;est bien souvent plus qu&amp;#39;un physique hors du commun. Ils ont chacun des personnalit&amp;eacute;s qui ont marqu&amp;eacute; le sport fran&amp;ccedil;ais voire mondial.&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &amp;icirc;le d&amp;eacute;serte, qu&amp;#39;emporterais-tu ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nMa soeur, une valeur s&amp;ucirc;re contre l&amp;#39;ennui (test&amp;eacute;e et approuv&amp;eacute;e en plein confinement...).&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&amp;eacute;rents ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nLe triathlon est un sport accessible par la diversit&amp;eacute; des &amp;eacute;preuves qui existent. Avec trois disciplines, beaucoup d&amp;#39;alternatives, il est tr&amp;egrave;s important de varier son entra&amp;icirc;nement et de s&amp;#39;avoir s&amp;#39;adapter aux conditions au quotidien.&amp;nbsp; La r&amp;eacute;gularit&amp;eacute; &amp;eacute;tant la clef d&amp;#39;une belle progression, je voudrais juste rappeler l&amp;#39;ensemble des possibilit&amp;eacute;s dont vous disposez pour prendre du plaisir et ne jamais vous lasser... On a la chance de pratiquer un sport vari&amp;eacute; alors profitons-en :)&amp;nbsp;&lt;/p&gt;\r\n', 3, 1, 12),
(43, 'Emmanuelle B ', '2020-03-23 13:48:01', '2020-03-23', '2020-04-03 16:15:53', '2022-12-31', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&amp;eacute;senter en trois lignes ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nM&amp;egrave;re de trois triathl&amp;egrave;tes, femme d&amp;#39;un triathl&amp;egrave;te et un peu triathl&amp;egrave;te moi-m&amp;ecirc;me&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venue au Triathlon ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\n&amp;Agrave; pied ;-)&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quel est ton r&amp;ocirc;le au sein du CoDIR Issy Triathlon?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nFaire des g&amp;acirc;teaux... Plus s&amp;eacute;rieusement, aider le club &amp;agrave; diff&amp;eacute;rentes t&amp;acirc;ches administratives, partager des id&amp;eacute;es, soutenir les id&amp;eacute;es des autres...&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nIl y en a trop&amp;hellip;. :-)&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ? &amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nTrialong 2016 : un autre triathl&amp;egrave;te est parti courir les 21km avec mes baskets en me laissant les siennes, beaucoup trop petites...&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &amp;icirc;le d&amp;eacute;serte, qu&amp;#39;emporterais-tu ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nMon v&amp;eacute;lo (m&amp;ecirc;me si, je vous l&amp;rsquo;accorde, il n&amp;rsquo;est pas facile de p&amp;eacute;daler dans le sable)&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&amp;eacute;rents ?&amp;nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\r\nAucun objectif n&amp;rsquo;est impossible, il faut juste en avoir envie.&lt;/p&gt;\r\n', 3, 1, 13),
(45, 'Brice C.', '2020-04-20 07:29:43', '2020-04-20', '2020-04-26 13:33:08', '2022-04-21', '&lt;p&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;br /&gt;\nJe m&rsquo;appelle Brice, j&rsquo;ai 34 ans. Je suis MNS en collectivit&eacute;&nbsp; J&rsquo;ai commenc&eacute; le triathlon il y a 7 ans.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;br /&gt;\nUn ami m&rsquo;a inscrit, sans que je le sache, &agrave; un triathlon. Il m&rsquo;a ramen&eacute; tout le mat&eacute;riel n&eacute;cessaire et je me suis retrouv&eacute; sur une ligne de d&eacute;part !&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon? &lt;/strong&gt;&lt;br /&gt;\nLa natation .&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;br /&gt;\nC&rsquo;est difficile d&rsquo;en retenir qu&rsquo;un... Il y a les stages club &eacute;videmment, ou l&rsquo;ambiance est fabuleuse, et les jolies ascensions en comp&eacute;tition (comme le ventoux ou l&rsquo;alpe d&rsquo;huez...).&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Et le pire ?&nbsp;&lt;/strong&gt;&lt;br /&gt;\nLes canicules lors de comp&eacute;tition.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ?&nbsp;&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;J&rsquo;h&eacute;site entre Christophe Huet et un autre...&lt;/p&gt;\n\n&lt;p&gt;Martin Fourcade sans aucun doute.&nbsp; Humainement et sportivement c&rsquo;est exemple pour tous biathletes et tous sportifs. Il a r&eacute;ussi &agrave; devenir une l&eacute;gende dans son sport et dans le sport fran&ccedil;ais. Il a aussi r&eacute;ussi &agrave; hisser son sport m&eacute;diatiquement jusqu&rsquo;&agrave; en faire un &eacute;v&eacute;nement incontournable ne l&rsquo;hiver.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;br /&gt;\nMa femme et mon fils&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ? &lt;/strong&gt;&lt;br /&gt;\nOn ne perd jamais, on apprend toujours. Se rappeler que tout &ccedil;a, &ccedil;a reste du sport et qu&rsquo;il y a plus important dans la vie.&lt;/p&gt;\n', 25, 1, 12),
(46, 'Multi-Enchainement', '2020-04-20 07:35:21', '2020-04-20', '2020-04-20 07:36:06', '2022-04-20', '&lt;p&gt;A partir de l&amp;#039;&eacute;t&eacute;, nous vous proposons une fois par semaine un entrainement sp&eacute;cifique enchainement v&eacute;lo / CAP. Celui-ci se d&eacute;roule &agrave; l&amp;#039;hippodrome de LongChamps.&lt;/p&gt;\n', 40, 1, 2),
(47, 'Guillaume L', '2020-04-20 09:49:55', '2020-04-20', '2020-04-20 16:06:07', '2025-04-21', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nGuillaume, entra&icirc;neur depuis un certain temps et responsable de la section sportive scolaire Victor Hugo.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nVenant des sports co, j&rsquo;ai d&eacute;but&eacute; le triathlon &agrave; 18 ans en entrant en STAPS.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJ&rsquo;entra&icirc;ne les 3 disciplines exclusivement chez les jeunes :-)&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nChaque jour...&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp; &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nChaque jour... non je plaisante. Rien ne me vient en t&ecirc;te. C&rsquo;est un sport difficile mais tellement enrichissant s&ucirc;r et pour soi-m&ecirc;me !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMike Horn peut &ecirc;tre... il aurait de belles aventures &agrave; me faire partager !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMon couteau Suisse ... S&eacute;rieusement, j&rsquo;en sais trop rien.&lt;/p&gt;\n', 46, 1, 12),
(48, 'Christophe M.', '2020-04-23 13:49:48', '2020-04-23', '2020-04-23 15:22:02', '2025-04-24', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJe fais partie des plus &amp;#34;anciens&amp;#34; du club, o&ugrave; j&amp;#039;ai tr&egrave;s vite &eacute;t&eacute; impliqu&eacute; en tant que charg&eacute; de mission - en charge de la toute 1&egrave;re version du site web du club, en 2003 - puis dans l&amp;#039;&eacute;quipe dirigeante.&lt;br /&gt;\nJe travaille dans la s&eacute;curit&eacute; informatique, et depuis 2 ans je suis &agrave; la t&ecirc;te de la soci&eacute;t&eacute; Overcoaching, dont la plate-forme sert notamment de support pour le SI du club et pour le suivi de certains athl&egrave;tes.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nPar hasard, apr&egrave;s avoir achet&eacute; un magazine de triathlon un jour o&ugrave; le libraire n&amp;#039;avait pas mon magazine de course &agrave; pied pr&eacute;f&eacute;r&eacute;. Quelques mois&nbsp;plus tard, en juin 2002, je tentais le triathlon D&eacute;couverte de G&eacute;rardmer : j&amp;#039;ai tout de suite &eacute;t&eacute; conquis ! D&eacute;cor magnifique, super m&eacute;t&eacute;o, grosse ambiance, il y avait tout pour me convaincre de continuer... et de chercher un club pour la rentr&eacute;e. Je vous laisse deviner lequel !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLa natation du vendredi midi. Ainsi que les s&eacute;ances v&eacute;lo / course &agrave; pied du groupe Longue Distance... voire Zwift en p&eacute;riode de confinement.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quel est ton r&ocirc;le au sein du CoDIR Issy Triathlon?&nbsp;&lt;/strong&gt;&nbsp; &lt;/em&gt;&lt;br /&gt;\nWebmaster, responsable comm&amp;#039; interne (oui, c&amp;#039;est moi qui inonde votre bo&icirc;te mail !), responsable inscriptions group&eacute;es et depuis cette ann&eacute;e je suis aussi en charge du groupe Longue Distance.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nEx-aequo entre mon 1er Ironman et mon dernier Ironman. Roth 2005 pour l&amp;#039;ambiance hallucinante et une d&eacute;couverte r&eacute;ussie de la distance, et Hawa&iuml; 2019 pour le mythe, les paysages, l&amp;#039;impression de vivre un r&ecirc;ve &eacute;veill&eacute; !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp; &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nDur &agrave; dire, j&amp;#039;ai tr&egrave;s peu de mauvais souvenirs, mais je dirais Roth 2008, &agrave; cause de la m&eacute;t&eacute;o tr&egrave;s tr&egrave;s humide : quand tu montes sur ton v&eacute;lo et que c&amp;#039;est le d&eacute;luge, que tu en as sans doute pour 6h comme &ccedil;a, et qu&amp;#039;au 1er virage tu te rends compte que les freins carbone &ccedil;a freine quand m&ecirc;me nettement moins bien que par temps sec, tu sens que le plaisir de participer n&amp;#039;est pas pour tout de suite... sans compter un d&eacute;but d&amp;#039;hypothermie 4h plus tard, &agrave; force de rouler avec des habits tremp&eacute;s qui te collent au corps.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMa cl&eacute; 4G et mon PC, pour pouvoir envoyer les mails du club ;-) (mais bien s&ucirc;r qu&amp;#039;on capte la 4G sur cette &icirc;le !)&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nM&ecirc;me si c&amp;#039;est plus facile &agrave; dire qu&amp;#039;&agrave; faire : ne pas se mettre la pression &agrave; s&amp;#039;en rendre malade avant une grosse &eacute;ch&eacute;ance sportive ! A de tr&egrave;s rares exceptions pr&egrave;s, on fait du sport pour notre plaisir et pour notre bien-&ecirc;tre, on a beaucoup de chance d&amp;#039;&ecirc;tre en forme et de pouvoir participer &agrave; de grands &eacute;v&eacute;nements sportifs, sachons appr&eacute;cier cette chance ! D&amp;#039;autant que si vous vous &ecirc;tes pr&eacute;par&eacute;(e) correctement, le plus dur est derri&egrave;re vous, il ne reste plus qu&amp;#039;&agrave; r&eacute;colter les fruits de votre pr&eacute;pa, &ccedil;a va se faire tout naturellement.&lt;/p&gt;\n', 30, 1, 12),
(49, 'Juan O.', '2020-04-23 13:58:04', '2020-04-23', '2020-04-23 14:38:46', '2025-04-24', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\n&amp;#039;Hola!&amp;#039;&amp;#039; - Passionn&eacute; de sport depuis toujours, j&amp;#039;ai tout d&amp;#039;abord pratiqu&eacute; le judo, le football et le cyclisme. Je me suis form&eacute; afin d&amp;#039;obtenir une licence en Entra&icirc;nement Sportif. Mon but est de transmettre ma passion de l&amp;#039;effort et du d&eacute;passement de soi dans la joie et la bonne humeur.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLors de ma seconde ann&eacute;e d&amp;#039;universit&eacute;, en 2013, je d&eacute;cide de faire du triathlon afin de coupler mes capacit&eacute;s d&amp;#039;endurance acquises en cyclisme et mes bonnes qualit&eacute;s de coureur &agrave; pied transmises par le football... Ne rester plus qu&amp;#039;&agrave; nager! Et j&amp;#039;ai ador&eacute;!&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJ&amp;#039;encadre sur les trois disciplines aussi bien chez les jeunes que les adultes.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMon r&eacute;veil &agrave; 4h30 du matin pour boucler l&amp;#039;IronMan Nice 2017. Cette sensation, d&egrave;s que tu ouvres les yeux, d&amp;#039;&ecirc;tre pr&ecirc;t au combat! &Eacute;veiller comme en pleine journ&eacute;e, un superbe tonus musculaire, une envie d&amp;#039;en d&eacute;coudre. La pr&eacute;paration avait &eacute;t&eacute; bonne. Je savais que j&amp;#039;allais passer une superbe journ&eacute;e!&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLe pire... Hummm... Je crois que je n&amp;#039;ai pas de pire.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJ&amp;#039;aime passer les soir&eacute;es avec tout types de personnalit&eacute;s. Super dynamique ou &amp;#039;chilleur&amp;#039; de 1&egrave;re classe, tant que le sourire et la convivialit&eacute; sont l&agrave;... Profitons des bonnes ondes et partageons un bon moment!&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nTant que j&rsquo;ai de l&rsquo;eau potable et des allumettes, je trouverai des solutions l&agrave;-bas.&nbsp;Et peut-&ecirc;tre un petit g&eacute;ranium en plus!&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLorsque la pente se durcit... L&egrave;ve les yeux et imagine Toi apr&egrave;s le sommet... Tu n&amp;#039;as plus qu&amp;#039;&agrave; appuyer sur les p&eacute;dales pour Te retrouver!&lt;/p&gt;\n', 40, 1, 12),
(50, 'Elise M', '2020-04-23 14:11:27', '2020-04-23', '2020-04-23 14:34:26', '2025-04-23', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJ&amp;#039;ai 32 ans, je suis Masseur-kin&eacute;sith&eacute;rapeute et Ost&eacute;opathe &agrave; Clamart depuis 10 ans. J&amp;#039;ai toujours aim&eacute; le sport, les challenges personnels et professionnels. Je compense ma petite taille (1m56) par mon &eacute;nergie et ma bonne humeur.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nAvec curiosit&eacute;, pouss&eacute;e par mon ch&eacute;ri, qui voulait reprendre le triathlon apr&egrave;s 5 ans d&amp;#039;arr&ecirc;t &agrave; cause de son boulot. Je faisais &agrave; l&amp;#039;&eacute;poque de l&amp;#039;&eacute;quitation, de la course &agrave; pied, beaucoup de fitness... Nous aimons tous les deux le sport et pouvoir en faire ensemble est un vrai plus pour nous.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quel est on r&ocirc;le au sein du CoDIR Issy Triathlon? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJe suis charg&eacute;e de mission. J&amp;#039;ai int&eacute;gr&eacute; le CoDir depuis la rentr&eacute;e de septembre 2019, donc pour moi c&amp;#039;est encore tout nouveau. Je participe aux r&eacute;unions du CoDir pour amener mon avis et mes id&eacute;es sur diff&eacute;rents sujets, notamment avec mon exp&eacute;rience d&amp;#039;adh&eacute;rente au club. Je peux avoir diff&eacute;rents r&ocirc;les d&amp;#039;aide logistique lors des &eacute;v&egrave;nements du club, mettre &agrave; jour des documents ou &ecirc;tre en relation avec des partenaires / intervenants pour le mat&eacute;riel du club.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMon meilleur souvenir en triathlon s&amp;#039;est d&eacute;roul&eacute; pendant l&amp;#039;half ironman de Vichy. Le d&eacute;part natation se faisant en rolling-start, la quantit&eacute; de triathl&egrave;tes &eacute;tait r&eacute;partie sur le parcours. Pendant le v&eacute;lo, j&amp;#039;ai pu ainsi remonter un nombre assez cons&eacute;quent de personnes ce qui &eacute;tait tr&egrave;s grisant. J&amp;#039;ai, ce jour l&agrave;, roul&eacute; plus fort sur 90km en solitaire que lors de mes entrainements en groupe. C&amp;#039;est un souvenir palpitant.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp; &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLe d&eacute;part de mon premier triathlon en eau libre : &agrave; Annecy. J&amp;#039;ai cru que je n&amp;#039;arriverais pas &agrave; faire les 100 premiers m&egrave;tres sans me noyer, y laisser mes lunettes, r&eacute;cup&eacute;rer un coquard dans la bataille... Il a fallu rester vraiment solide dans la t&ecirc;te pour me ressaisir, laisser avancer le flot des nageurs puis pouvoir poser ma nage ensuite. &Ccedil;a valait le coup, car le reste de la course a &eacute;t&eacute; super !!&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMike Horn, pour avoir l&amp;#039;occasion d&amp;#039;&eacute;changer avec lui sur tout ce qu&amp;#039;il a pu voir lors de ses exp&eacute;ditions, son &eacute;tat d&amp;#039;esprit... C&amp;#039;est quelqu&amp;#039;un qui m&amp;#039;inspire le plus grand respect.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMon ch&eacute;ri ! Avec lui, je suis s&ucirc;re de ne jamais m&amp;#039;ennuyer&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nDe vous amuser surtout !!!! On reste de grands enfants et le sport c&amp;#039;est avant tout pour se faire du bien et &ecirc;tre en bonne sant&eacute;. Alors soyez contents d&amp;#039;avoir boug&eacute; m&ecirc;me si vous n&amp;#039;avez pas couru aussi vite que voulu pendant cette s&eacute;ance de fractionn&eacute;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;Si votre pratique est plus tourn&eacute;e vers la comp&eacute;tition, n&amp;#039;oubliez jamais d&amp;#039;&ecirc;tre patient et progressif dans votre entra&icirc;nement !&lt;/p&gt;\n', 50, 1, 13),
(51, 'Christophe M.', '2020-04-26 10:00:55', '2020-04-26', NULL, '2025-04-27', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJe fais partie des plus &amp;#34;anciens&amp;#34; du club, o&ugrave; j&amp;#039;ai tr&egrave;s vite &eacute;t&eacute; impliqu&eacute; en tant que charg&eacute; de mission - en charge de la toute 1&egrave;re version du site web du club, en 2003 - puis dans l&amp;#039;&eacute;quipe dirigeante.&lt;br /&gt;\nJe travaille dans la s&eacute;curit&eacute; informatique, et depuis 2 ans je suis &agrave; la t&ecirc;te de la soci&eacute;t&eacute; Overcoaching, dont la plate-forme sert notamment de support pour le SI du club et pour le suivi de certains athl&egrave;tes.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nPar hasard, apr&egrave;s avoir achet&eacute; un magazine de triathlon un jour o&ugrave; le libraire n&amp;#039;avait pas mon magazine de course &agrave; pied pr&eacute;f&eacute;r&eacute;. Quelques mois&nbsp;plus tard, en juin 2002, je tentais le triathlon D&eacute;couverte de G&eacute;rardmer : j&amp;#039;ai tout de suite &eacute;t&eacute; conquis ! D&eacute;cor magnifique, super m&eacute;t&eacute;o, grosse ambiance, il y avait tout pour me convaincre de continuer... et de chercher un club pour la rentr&eacute;e. Je vous laisse deviner lequel !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLa natation du vendredi midi. Ainsi que les s&eacute;ances v&eacute;lo / course &agrave; pied du groupe Longue Distance... voire Zwift en p&eacute;riode de confinement.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quel est ton r&ocirc;le au sein du CoDIR Issy Triathlon?&nbsp;&lt;/strong&gt;&nbsp; &lt;/em&gt;&lt;br /&gt;\nWebmaster, responsable comm&amp;#039; interne (oui, c&amp;#039;est moi qui inonde votre bo&icirc;te mail !), responsable inscriptions group&eacute;es et depuis cette ann&eacute;e je suis aussi en charge du groupe Longue Distance.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nEx-aequo entre mon 1er Ironman et mon dernier Ironman. Roth 2005 pour l&amp;#039;ambiance hallucinante et une d&eacute;couverte r&eacute;ussie de la distance, et Hawa&iuml; 2019 pour le mythe, les paysages, l&amp;#039;impression de vivre un r&ecirc;ve &eacute;veill&eacute; !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp; &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nDur &agrave; dire, j&amp;#039;ai tr&egrave;s peu de mauvais souvenirs, mais je dirais Roth 2008, &agrave; cause de la m&eacute;t&eacute;o tr&egrave;s tr&egrave;s humide : quand tu montes sur ton v&eacute;lo et que c&amp;#039;est le d&eacute;luge, que tu en as sans doute pour 6h comme &ccedil;a, et qu&amp;#039;au 1er virage tu te rends compte que les freins carbone &ccedil;a freine quand m&ecirc;me nettement moins bien que par temps sec, tu sens que le plaisir de participer n&amp;#039;est pas pour tout de suite... sans compter un d&eacute;but d&amp;#039;hypothermie 4h plus tard, &agrave; force de rouler avec des habits tremp&eacute;s qui te collent au corps.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMa cl&eacute; 4G et mon PC, pour pouvoir envoyer les mails du club ;-) (mais bien s&ucirc;r qu&amp;#039;on capte la 4G sur cette &icirc;le !)&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nM&ecirc;me si c&amp;#039;est plus facile &agrave; dire qu&amp;#039;&agrave; faire : ne pas se mettre la pression &agrave; s&amp;#039;en rendre malade avant une grosse &eacute;ch&eacute;ance sportive ! A de tr&egrave;s rares exceptions pr&egrave;s, on fait du sport pour notre plaisir et pour notre bien-&ecirc;tre, on a beaucoup de chance d&amp;#039;&ecirc;tre en forme et de pouvoir participer &agrave; de grands &eacute;v&eacute;nements sportifs, sachons appr&eacute;cier cette chance ! D&amp;#039;autant que si vous vous &ecirc;tes pr&eacute;par&eacute;(e) correctement, le plus dur est derri&egrave;re vous, il ne reste plus qu&amp;#039;&agrave; r&eacute;colter les fruits de votre pr&eacute;pa, &ccedil;a va se faire tout naturellement.&lt;/p&gt;\n', 45, 1, 13),
(52, 'Guillaume L.', '2020-04-26 10:02:50', '2020-04-26', NULL, '2025-04-27', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nGuillaume, entra&icirc;neur depuis un certain temps et responsable de la section sportive scolaire Victor Hugo.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nVenant des sports co, j&rsquo;ai d&eacute;but&eacute; le triathlon &agrave; 18 ans en entrant en STAPS.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJ&rsquo;entra&icirc;ne les 3 disciplines exclusivement chez les jeunes :-)&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nChaque jour...&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp; &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nChaque jour... non je plaisante. Rien ne me vient en t&ecirc;te. C&rsquo;est un sport difficile mais tellement enrichissant s&ucirc;r et pour soi-m&ecirc;me !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMike Horn peut &ecirc;tre... il aurait de belles aventures &agrave; me faire partager !&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMon couteau Suisse ... S&eacute;rieusement, j&rsquo;en sais trop rien.&lt;/p&gt;\n', 35, 1, 13),
(53, 'Christophe H.', '2020-04-26 10:04:31', '2020-04-26', NULL, '2025-04-27', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nChristophe. 38 ans. Directeur sportif du club.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nA pied car je ne savais pas encore nager&nbsp;;)&lt;/p&gt;\n\n&lt;p&gt;Apr&egrave;s 10 ans de foot et un petit raid dans la campagne nous sommes tomb&eacute;s au forum des associations sur le club Issy Triathlon. C&amp;#039;&eacute;tait il y &agrave; 20 ans et &agrave; l&amp;#039;&eacute;poque le plus jeune devait avoir 35 ans :)&lt;/p&gt;\n\n&lt;p&gt;Avec Guillaume (entraineur&nbsp;au club) on a pers&eacute;v&eacute;r&eacute; dans ce sport qui pour moi n&amp;#039;avait rien de ludique ni en lien avec mes capacit&eacute;s physique et mon esprit collectif mais quand on veux&nbsp;atteindre un objectif il faut s&amp;#039;en donner les moyens. Pour &ecirc;tre performant en RAID il faut s&amp;#039;entrainer&nbsp;dans les disciplines du triathlon. Voila maintenant je fais partie des murs :)&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nToutes m&ecirc;me si depuis quelques temps je n&amp;#039;entraine le v&eacute;lo qu&amp;#039;en stage&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nSurement l&amp;#039;arriv&eacute;e de mon Ironman en 2007 personne n&amp;#039;y croyait vu mon niveau d&amp;#039;entrainement. Ce jour l&agrave; certains ont du apprendre le mot humilit&eacute;, mental et quand tu provoques un coach attends toi &agrave; perdre :)&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp;&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nSurement&nbsp;le premier &agrave; Etampes et sa natation dans une grand flaque de boue&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nSportive, je dirai Jordan le mec a r&eacute;volutionn&eacute;&nbsp;son sport et force le respect.&nbsp; Autre: le Dalai Lama, Martin Luther King ou Nelson Mandela. Tous ont men&eacute;&nbsp;des combats spirituels, politiques et humains.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMa famille.&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nFaites du sport pour vous en vivant le moment pr&eacute;sent mais que tu sois elite ou loisir tu dois &ecirc;tre r&eacute;gulier et objectif&nbsp;&lt;/p&gt;\n', 30, 1, 13),
(54, 'Blandine G.', '2020-04-26 13:10:08', '2020-04-26', '2020-04-26 13:16:46', '2025-04-27', '&lt;p&gt;&lt;em&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJe m&rsquo;appelle Blandine Guilloteau, j&rsquo;ai 20ans, j&rsquo;ai 5 fr&egrave;res et soeurs et je suis triathl&egrave;te. Je suis actuellement en 3&egrave;me ann&eacute;es de STAPS. Cela fait plus de 10 ans que j&rsquo;ai commenc&eacute; le triathlon au sein m&ecirc;me du club d&rsquo;Issy triathlon.&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;Comment es-tu venue au Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nJ&rsquo;ai commenc&eacute; le sport par le judo et le ski de fond. Puis, suite &agrave; un d&eacute;m&eacute;nagement sur Paris, mon beau p&egrave;re nous a tous inscrit &agrave; Issy Triathlon.&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\n&Agrave; Issy Triathlon je coach la natation principalement, mais &eacute;galement la course &agrave; pied et le v&eacute;lo au pr&egrave;s de baby triathl&egrave;tes.&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLa deuxi&egrave;me course &agrave; pied des championnats de France de Duathlon jeune o&ugrave; je fais podium en me classant 3&egrave;me. Ou alors mes premiers championnats de France de triathlon UNSS.&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;Et le pire ?&nbsp;&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nLa course &agrave; pied de mon premier triathlon M&hellip;&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nTeddy Riner parce que c&rsquo;est un exemple de r&eacute;ussite&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nMon attestation de d&eacute;placement d&eacute;rogatoire au cas o&ugrave;.&lt;br /&gt;\n&lt;br /&gt;\n&lt;em&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ?&lt;/strong&gt;&lt;/em&gt;&lt;br /&gt;\nArriver &agrave; l&rsquo;heure aux s&eacute;ances pour &eacute;viter les pompes ;) .&lt;/p&gt;\n', 47, 1, 12),
(55, 'Thibaut L.', '2020-04-26 13:23:09', '2020-04-26', '2020-04-26 13:30:09', '2025-04-27', '&lt;p&gt;&lt;strong&gt;Peux-tu te pr&eacute;senter en trois lignes ? &lt;/strong&gt;&lt;br /&gt;\nThibaut&nbsp;LEDANOIS, n&eacute; le 24/09/92&nbsp;, Entra&icirc;neur de Triathlon Dipl&ocirc;m&eacute; d&amp;#039;&eacute;tat et Doctorant de&nbsp;la FFTRI&nbsp;/ INSEP. Les diff&eacute;rentes&nbsp;activit&eacute;s physiques et d&amp;#039;esprit sont mon essentiel :&nbsp;foot, golf, musique, athl&eacute;tisme, triathlon, cyclisme, ...&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Comment es-tu venu au Triathlon ? &lt;/strong&gt;&lt;br /&gt;\nJe suis venu au triathlon, car&nbsp;c&amp;#039;est une discipline sportive qui m&amp;#039;a toujours attir&eacute;e par sa complexit&eacute; technique associ&eacute;e &agrave; une adaptabilit&eacute; toujours constante face &agrave; la situation.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Quelles sont les disciplines que tu coaches &agrave; Issy Triathlon? &lt;/strong&gt;&lt;br /&gt;\nJe participe &agrave; l&amp;#039;encadrement des groupes jeunes (Ecole de Triathlon et performance)&nbsp;sur les trois modes de locomotion,&nbsp;en Course &agrave; pied, en&nbsp;Natation et en Cyclisme.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Ton meilleur souvenir en Triathlon ?&lt;/strong&gt;&lt;br /&gt;\nDe mani&egrave;re g&eacute;n&eacute;rale, j&amp;#039;appr&eacute;cie la&nbsp;d&eacute;charge d&amp;#039;adr&eacute;naline du d&eacute;part, le start soit le moment qui va&nbsp;te permettre de mettre en&nbsp;application tout ce que tu travailles depuis plusieurs mois &agrave; l&amp;#039;entra&icirc;nement.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Et le pire ?&nbsp; &lt;/strong&gt;&lt;br /&gt;\nS&ucirc;rement&nbsp;quand j&amp;#039;ai crev&eacute; sur mon premier triathlon&nbsp;&agrave; Versailles,&nbsp;sur le circuit de la base de Satory.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;La personnalit&eacute; (sportive ou autre) avec qui tu aimerais passer une soir&eacute;e, et pourquoi ? &lt;/strong&gt;&lt;br /&gt;\nLilian Thuram, par sa reflexion et pour&nbsp;l&amp;#039;ambivalence de sa performance&nbsp;lors du&nbsp;match contre la Croatie en 98. Ou&nbsp;&Eacute;mile&nbsp;Zatopek, un&nbsp;monument...&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Sur une &icirc;le d&eacute;serte, qu&amp;#039;emporterais-tu ? &lt;/strong&gt;&lt;br /&gt;\nProbablement un bilboquet ?&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Un conseil ou une astuce que tu aimerais partager avec les adh&eacute;rents ? &lt;/strong&gt;&lt;br /&gt;\nL&amp;#039;application de la force sans technique n&amp;#039;est que perte d&amp;#039;&eacute;nergie. Technique sans force est vain.&lt;/p&gt;\n', 76, 1, 12),
(56, 'Piscine Alfred Sevestre', '2020-04-30 13:57:12', '2020-04-30', NULL, '2025-05-01', '&lt;p&gt;La piscine&lt;a href=&quot;https://www.a-sevestre.fr/&quot;&gt; Alfred Sevestre&lt;/a&gt; se situe pr&egrave;s du palais des sports en plein centre d&amp;#039;Issy-les-Moulineaux.&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Localisation&lt;/strong&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2626.627027458333!2d2.264983115948579!3d48.827177379284564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67a84c99ab90b%3A0x88d24cc53a9d0e79!2sAlfred%20Sevestre%20Pool!5e0!3m2!1sen!2sfr!4v1588248285000!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n', 10, 1, 15),
(57, 'Piscine Aquazena', '2020-04-30 13:59:56', '2020-04-30', '2020-04-30 14:24:29', '2025-05-01', '&lt;p&gt;La piscine &amp;#34;Fen chui&amp;#34; Aquazena se situe dans l&amp;#039;&eacute;co-quartier du fort.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Localisation&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2627.1959231944215!2d2.2677011157038716!3d48.816323111621784!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67a7e3f958afb%3A0x7981204b0fac1653!2sPiscine%20Aquazena!5e0!3m2!1sen!2sfr!4v1588255099273!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n', 20, 1, 15),
(58, 'Halle C Guillaume', '2020-04-30 14:02:47', '2020-04-30', '2020-04-30 14:03:46', '2025-05-01', '&lt;p&gt;La Halle C Guillaume met &agrave; notre disposition ses vestiaires lors des entrainements de course &agrave; pied, en attendant le reconstruction du stage Jean Bouin.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Localisation&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2626.6750023000695!2d2.262163815704164!3d48.82626211092245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67bb42d75433d%3A0x4cd8ad97b8147d28!2sHalle%20Christiane%20Guillaume!5e0!3m2!1sen!2sfr!4v1588255243978!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n', 30, 1, 15),
(59, 'Stade Suzanne Lenglen', '2020-04-30 14:12:47', '2020-04-30', NULL, '2025-05-01', '&lt;p&gt;En attendant la reconstruction de notre stade Jean Bouin &agrave; Issy-les-Moulineaux, nous utilisons la piste du Stade Suzanne Lenglen proche de l&amp;#039;h&eacute;liport, ainsi que le parc l&amp;#039;entourant.&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Localisation : &lt;/strong&gt;&lt;/p&gt;\n\n&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2626.455483804391!2d2.2691866441924726!3d48.83045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67075c29534bd%3A0xd0c21c72198a29f4!2sStade%20Suzanne%20Lenglen!5e0!3m2!1sen!2sfr!4v1588255932195!5m2!1sen!2sfr&quot; width=&quot;100%&quot; height=&quot;450&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;', 45, 1, 15),
(60, 'Gymnase Jules Guesde', '2020-04-30 14:19:36', '2020-04-30', NULL, '2025-05-01', '&lt;p&gt;Le gymnase Jules Guesde accueille entre autres nos entrainements de PPG (Pr&eacute;paration physique g&eacute;n&eacute;rale)&lt;/p&gt;\n\n&lt;p&gt;&lt;strong&gt;Localisation&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;iframe src=&quot;https://www.google.com/maps/embed?pb&amp;#61;!1m18!1m12!1m3!1d2626.9273651031704!2d2.2772755!3d48.821447299999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e6707ed97a5839%3A0x13f9b0bc21974a09!2sGymnase%20Jules%20Guesde!5e0!3m2!1sen!2sfr!4v1588256311245!5m2!1sen!2sfr&quot; allowfullscreen frameborder=&quot;0&quot; height=&quot;450&quot; width=&quot;100%&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;\n', 65, 1, 15);
INSERT INTO `articles` (`articleId`, `articleTitle`, `articleDateCreated`, `articlePublicationDate`, `articleDateUpdated`, `articleExpiryDate`, `articleContent`, `articleOrder`, `articleStatus`, `articleSectionId`) VALUES
(61, 'Témoignage', '2020-04-30 15:08:46', '2020-04-30', '2020-07-30 14:24:15', '2025-05-01', '&lt;p&gt;&lt;strong&gt;Marie&lt;/strong&gt;, &lt;strong&gt;C&eacute;cile&lt;/strong&gt; et &lt;strong&gt;Agn&egrave;s&lt;/strong&gt; ont particip&eacute; au triathlon indoor du club des expatri&eacute;s &agrave; Paris. Elles nous racontent leur exp&eacute;rience et pourquoi elles font du &laquo;&nbsp;&lt;strong&gt;sport sant&eacute;&lt;/strong&gt;&nbsp;&raquo; au club d&amp;#039;Issy Triathlon&lt;/p&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Comment avez vous connu le sport sant&eacute; &agrave; Issy Triathlon. Pourquoi et depuis combien de temps vous participez aux entrainements?&lt;/strong&gt;&lt;/em&gt;&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;&lt;strong&gt;MM&lt;/strong&gt;&nbsp;: Gr&acirc;ce &agrave; un licenci&eacute; Issy Tri (et parce que la licence Issy Tri me faisait peur) &ndash; Participation depuis sa cr&eacute;ation &amp;#61; 2 ans&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;CF&lt;/strong&gt;&nbsp;: Je connais le club depuis 5 ans environ, mes 2 gar&ccedil;ons&nbsp;de 10 et 13 ans et mon mari sont au club d&amp;#039;Issy Tri. J&amp;#039;ai re&ccedil;u&nbsp;il y a 2 ans un mail sur un sondage concernant l&amp;#039;&eacute;ventualit&eacute; de l&amp;#039;ouverture de cette section sp&eacute;ciale pour les femmes. J&amp;#039;ai tout de suite &eacute;t&eacute; s&eacute;duite par le programme et l&amp;#039;accompagnement propos&eacute;s. Avoir la garantie d&amp;#039;&ecirc;tre bien accompagn&eacute;e par des BONS coach et des super &eacute;quipements (Piscine, salle couverte...), je n&amp;#039;ai pas h&eacute;sit&eacute; une seconde, je me suis pr&eacute;-inscrite en esp&eacute;rant&nbsp;tr&egrave;s fort que la section ouvre. Je suis donc les entra&icirc;nements depuis le d&eacute;but..&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;AD&lt;/strong&gt;&nbsp;: j&amp;#039;ai connu le sport sant&eacute; via les communications faites juste avant sa cr&eacute;ation aux parents des enfants adh&eacute;rents. Il me semble aussi que cela avait &eacute;t&eacute; annonc&eacute; lors de l&amp;#039;AG il y a 2 ans. J&amp;#039;&eacute;tais impatiente que &ccedil;a se concr&eacute;tise. J&amp;#039;y suis donc inscrite depuis sa cr&eacute;ation il y a 1 an 1/2. Je cherchais &agrave; changer d&amp;#039;activit&eacute; sportive car je me lassais de celle que je pratiquais.&nbsp;Je souhaitais &eacute;galement progresser en course &agrave; pied et apprendre &agrave; nager le crawl. Je n&amp;#039;osais pas m&amp;#039;inscrire au triathlon qui me semblait trop exigeant. Le sport sant&eacute; me semblait plus adapt&eacute; &agrave; mon niveau et &agrave; mon emploi&nbsp;du temps.&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Vous avez particip&eacute; &agrave; une comp&eacute;tition sur un format triathlon, quelles &eacute;taient vos appr&eacute;hensions? Franchir la ligne d&amp;#039;arriv&eacute;e n&amp;#039;&eacute;tait surement pas le seul but pouvez vous nous dire vos &amp;#34;objectifs&amp;#34;?&lt;/strong&gt;&lt;/em&gt;&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;&lt;strong&gt;MM&nbsp;:&lt;/strong&gt; Toujours aucune capacit&eacute; &agrave; g&eacute;rer le stress. Donc la peur absolue &agrave; chaque d&eacute;part. Au cas particuliers, aucun objectif, juste le plaisir et surtout le plaisir de convertir quelques Torpilles.&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;CF&lt;/strong&gt;&nbsp;: Avec les filles, nous avions envie de se faire un petit d&eacute;fi. Le format super sprint est un bon format pour nous &amp;#34;tester&amp;#34;. J&amp;#039;ai appris &agrave; nager le crawl l&amp;#039;ann&eacute;e derni&egrave;re ...&nbsp; et pour avoir vu des d&eacute;parts&nbsp;de triathlon&nbsp;olympique, la possibilit&eacute; de nager &agrave; 2 par ligne c&amp;#039;est plus rassurant. Et je n&amp;#039;ai jamais fait de comp&eacute;tition de ma vie avant celle ci ! Mon objectif ... finir ! c&amp;#039;est sur ! mais en moins de 45 minutes ...&nbsp; Franchement j&amp;#039;ai ador&eacute;, m&ecirc;me si j&amp;#039;ai vraiment gal&eacute;r&eacute; en natation ... mais avec les Torpilles en soutien et la famille au bord du bassin, on y arrive !! Les sensations &agrave; chaque transition sont assez dingues finalement.&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;AD&lt;/strong&gt;&nbsp;: Je participe assez r&eacute;guli&egrave;rement &agrave; des comp&eacute;titions de course &agrave; pied, alors j&amp;#039;&eacute;tais impatiente d&amp;#039;essayer un autre format. Mes appr&eacute;hensions / objectifs : r&eacute;ussir &agrave; faire les 300m de natation, me tester au v&eacute;lo de course (jamais utilis&eacute; de v&eacute;lo de course auparavant), exp&eacute;rimenter les transitions, et puis essayer ce que l&amp;#039;on fait subir &agrave; nos enfants ;-).&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Certaines &amp;#34;mamans du club&amp;#34; ou certaines femmes n&amp;#039;osent pas franchir le pas. Qu&amp;#039;avez vous envie de leur dire?&lt;/strong&gt;&lt;/em&gt;&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;&lt;strong&gt;MM&lt;/strong&gt;&nbsp;: Une grand-m&egrave;re le fait alors pourquoi pas une maman et surtout une femme. Et puis nous avons toutes les conditions pour essayer avec nos 3 supers coachs &nbsp;et un groupe de filles formidables.&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;CF&lt;/strong&gt;&nbsp;: On est un groupe qui aiment passer du temps ensemble &agrave; notre rythme, on fait du sport pour des raisons diff&eacute;rentes, avec plus ou moins d&amp;#039;intensit&eacute;. Mais c&amp;#039;est ca&nbsp;qui est bien, tout le monde y trouve son compte et on progresse, on se sent bien dans son corps mais aussi parce qu&amp;#039;on partage des choses perso en groupe. Les torpilles, c&amp;#039;est plus qu&amp;#039;un groupe de femmes, de mamans qui font du sport ... ca va au del&agrave;. D&amp;#039;ailleurs merci &agrave; nos coachs de nous supporter (dans les 2 sens du terme ! :) Alors venez nous rejoindre ! Au pire, vous passerez un bon moment&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;AD&lt;/strong&gt;&nbsp;: - par rapport au sport sant&eacute; : c&amp;#039;est un groupe de femmes en or, solidaires, sans jugement. Chacune progresse &agrave; son rythme gr&acirc;ce &agrave; des coachs au top! Et enfin, le point le plus important : on rit beaucoup!&lt;br /&gt;\n	- par rapport aux comp&eacute;titions : &agrave; chacune, selon ses capacit&eacute;s physiques de trouver le format de comp&eacute;tition qui lui convient et se lancer. Certaines pr&eacute;f&egrave;reront commencer avec une marche, d&amp;#039;autres une course courte, ou un format relai, peu importe, l&amp;#039;essentiel &eacute;tant de commencer, de se faire plaisir, pour au final se rendre compte que l&amp;#039;on est capable et avoir envie de recommencer. C&amp;#039;est &eacute;galement plus facile de se motiver&nbsp;&agrave; plusieurs et avec les Torpilles, c&amp;#039;est le Fun assur&eacute;!&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 20, 1, 14),
(62, 'test', '2020-05-10 13:16:38', '2020-05-10', NULL, '2020-05-30', '&lt;p&gt;dsfgdsfg&lt;/p&gt;\n\n&lt;p&gt;sdfgdfsg&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n\n', 99, 0, 13);

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

CREATE TABLE `contacts` (
  `contactId` int(11) NOT NULL,
  `contactLastName` varchar(128) NOT NULL,
  `contactFirstName` varchar(128) NOT NULL,
  `contactEmail` varchar(128) NOT NULL,
  `contactPrivacyStatement` tinyint(4) NOT NULL,
  `contactMessage` text NOT NULL,
  `contactStatus` tinyint(4) NOT NULL,
  `contactDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contactDateUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`contactId`, `contactLastName`, `contactFirstName`, `contactEmail`, `contactPrivacyStatement`, `contactMessage`, `contactStatus`, `contactDateCreated`, `contactDateUpdated`) VALUES
(1, 'Roggeband', 'Philippe', 'proggeband&#64;gmail.com', 1, 'test', 1, '2020-04-19 16:10:03', NULL),
(3, 'Dupont', 'Jean', 'jdupont&#64;test.com', 1, 'qsdfhqsf', 0, '2020-04-22 13:54:30', '2020-05-24 13:42:38');

-- --------------------------------------------------------

--
-- Structure de la table `imageObjects`
--

CREATE TABLE `imageObjects` (
  `imageObjectId` int(11) NOT NULL,
  `imageObjectDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `imageId` int(11) NOT NULL,
  `objectClass` varchar(128) NOT NULL,
  `objectId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `imageObjects`
--

INSERT INTO `imageObjects` (`imageObjectId`, `imageObjectDateCreated`, `imageId`, `objectClass`, `objectId`) VALUES
(1, '2020-03-06 14:46:46', 4, 'Page', 12),
(15, '2020-03-06 19:20:29', 8, 'Page', 6),
(50, '2020-03-08 16:27:10', 7, 'Article', 38),
(51, '2020-03-08 16:27:20', 4, 'Article', 38),
(85, '2020-03-13 17:32:48', 11, 'Page', 6),
(89, '2020-03-21 14:25:22', 4, 'Section', 3),
(90, '2020-03-21 14:45:54', 11, 'Section', 3),
(91, '2020-03-21 15:27:50', 10, 'Article', 3),
(92, '2020-03-21 15:27:50', 8, 'Article', 3),
(94, '2020-03-22 18:18:57', 14, 'Article', 1),
(96, '2020-03-23 13:18:48', 15, 'Article', 41),
(98, '2020-03-23 13:34:23', 16, 'Article', 42),
(101, '2020-03-23 13:48:40', 17, 'Article', 43),
(102, '2020-03-23 13:48:40', 18, 'Article', 43),
(107, '2020-04-15 12:28:11', 22, 'Page', 16),
(108, '2020-04-20 09:49:55', 24, 'Article', 47),
(109, '2020-04-23 14:14:39', 25, 'Article', 50),
(110, '2020-04-23 14:34:26', 26, 'Article', 50),
(111, '2020-04-23 14:35:12', 27, 'Article', 49),
(112, '2020-04-23 14:38:46', 28, 'Article', 49),
(113, '2020-04-23 15:22:02', 29, 'Article', 48),
(114, '2020-04-26 10:00:55', 29, 'Article', 51),
(115, '2020-04-26 10:02:50', 24, 'Article', 52),
(116, '2020-04-26 10:04:31', 15, 'Article', 53),
(117, '2020-04-26 13:16:46', 31, 'Article', 54),
(118, '2020-04-26 13:30:09', 32, 'Article', 55),
(119, '2020-04-26 13:33:08', 33, 'Article', 45),
(120, '2020-04-26 13:45:18', 30, 'Page', 1),
(121, '2020-04-26 13:48:10', 34, 'Section', 12),
(122, '2020-04-29 11:51:06', 35, 'Page', 13),
(124, '2020-04-29 12:44:06', 4, 'Page', 6),
(125, '2020-04-29 12:45:15', 15, 'Section', 12),
(126, '2020-04-29 12:45:15', 16, 'Section', 12),
(127, '2020-04-29 12:45:15', 27, 'Section', 12),
(128, '2020-04-29 12:45:15', 31, 'Section', 12),
(129, '2020-04-29 12:45:15', 32, 'Section', 12),
(130, '2020-04-29 12:45:15', 29, 'Section', 12),
(131, '2020-04-29 12:46:54', 33, 'Section', 12),
(132, '2020-04-29 12:46:54', 24, 'Section', 12),
(133, '2020-04-29 12:59:04', 38, 'Section', 19),
(134, '2020-04-29 13:02:10', 40, 'Section', 2),
(135, '2020-04-29 13:14:58', 37, 'Section', 20),
(136, '2020-04-29 13:14:58', 36, 'Section', 20),
(137, '2020-04-30 13:57:12', 13, 'Article', 56),
(138, '2020-04-30 13:59:56', 14, 'Article', 57),
(139, '2020-04-30 14:03:46', 42, 'Article', 58),
(140, '2020-04-30 14:12:47', 43, 'Article', 59),
(141, '2020-04-30 14:19:36', 44, 'Article', 60),
(142, '2020-04-30 15:09:26', 45, 'Section', 14),
(143, '2020-05-10 13:44:20', 7, 'Article', 3),
(144, '2020-05-24 13:43:58', 4, 'Page', 15),
(145, '2020-07-30 09:09:36', 46, 'Section', 22),
(146, '2020-07-30 12:44:59', 25, 'Article', 39),
(147, '2020-07-30 13:35:37', 38, 'Section', 18),
(148, '2020-07-30 13:54:34', 22, 'Section', 13);

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `imageId` int(11) NOT NULL,
  `imageDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `imageDateUpdated` datetime DEFAULT NULL,
  `imagePath` varchar(512) NOT NULL,
  `imageMiniPath` varchar(512) NOT NULL,
  `imageLegend` varchar(512) NOT NULL,
  `imageText` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `images`
--

INSERT INTO `images` (`imageId`, `imageDateCreated`, `imageDateUpdated`, `imagePath`, `imageMiniPath`, `imageLegend`, `imageText`) VALUES
(4, '2020-02-12 16:09:47', '2020-04-05 08:12:44', 'images/IMG_0876.JPG', 'images/mini/IMG_0876.JPG', 'Les pieds dans l\'eau', 'Elle est bonne ? '),
(7, '2020-02-13 12:10:45', '2020-04-05 08:13:32', 'images/b6033296-ba8b-4aaa-8cb1-52a7429b7843.JPG', 'images/mini/b6033296-ba8b-4aaa-8cb1-52a7429b7843.JPG', 'Les garçons Elite', 'Prêts à plonger ! '),
(8, '2020-02-26 13:52:43', '2020-04-26 14:42:14', 'images/IMG_3078.JPG', 'images/mini/IMG_3078.JPG', 'Transition', ''),
(9, '2020-02-26 13:56:25', '2020-03-04 15:08:40', 'images/94accaa1-e04e-498b-801d-07a52321379d.JPG', 'images/mini/94accaa1-e04e-498b-801d-07a52321379d.JPG', 'Linda', 'Linda Guinoiseau'),
(10, '2020-03-06 18:54:41', '2020-04-26 14:44:38', 'images/IMG_2363.JPG', 'images/mini/IMG_2363.JPG', 'Swim', 'Concentration avant le départ'),
(11, '2020-03-06 19:33:15', '2020-04-05 08:15:21', 'images/df609a38-d16e-4fd2-9474-fe54e260cb93.JPG', 'images/mini/df609a38-d16e-4fd2-9474-fe54e260cb93.JPG', 'Friends', ''),
(12, '2020-03-21 17:51:17', '2020-04-05 08:15:58', 'images/3676bfa5-9ae1-4ddc-8223-aa32c3e431d2.JPG', 'images/mini/3676bfa5-9ae1-4ddc-8223-aa32c3e431d2.JPG', 'Groupe', ''),
(13, '2020-03-22 18:17:42', '2020-03-22 18:30:48', 'images/47154229_2038159802938735_226409252548247552_o.jpg', 'images/mini/47154229_2038159802938735_226409252548247552_o.jpg', 'Piscine Sevestre', ''),
(14, '2020-03-22 18:18:26', NULL, 'images/47398165_2047888835299165_7933717297737236480_o.jpg', 'images/mini/47398165_2047888835299165_7933717297737236480_o.jpg', 'Ligne Triathlon', ''),
(15, '2020-03-23 13:18:28', NULL, 'images/Chris H.jpg', 'images/mini/Chris H.jpg', 'Christophe H ', 'Directeur Sportif'),
(16, '2020-03-23 13:29:55', NULL, 'images/Audrey M.jpg', 'images/mini/Audrey M.jpg', 'Audrey M', 'Coach'),
(17, '2020-03-23 13:43:19', NULL, 'images/Manou 1.jpg', 'images/mini/Manou 1.jpg', 'Manou B - CoDir', ''),
(18, '2020-03-23 13:43:41', NULL, 'images/Manou 2.jpg', 'images/mini/Manou 2.jpg', 'Manou B - CoDir', ''),
(19, '2020-03-23 14:05:58', NULL, 'images/iut20_n1052.jpg', 'images/mini/iut20_n1052.jpg', 'IUT 2020', ''),
(20, '2020-04-02 18:06:31', NULL, 'images/Logo issy 2019.jpg', 'images/mini/Logo issy 2019.jpg', 'logo 2019', ''),
(21, '2020-04-03 14:01:39', NULL, 'images/OC-logo2.png', 'images/mini/OC-logo2.png', 'Overcoaching', 'Overcoaching'),
(22, '2020-04-13 12:06:57', '2020-04-15 12:28:47', 'images/logo2019_black.jpg', 'images/mini/logo2019_black.jpg', ' ', ' '),
(23, '2020-04-13 12:55:01', NULL, 'images/logo2019_dark.jpg', 'images/mini/logo2019_dark.jpg', 'Dark Logo', ''),
(24, '2020-04-20 09:46:22', NULL, 'images/Guillaume Lepors - sq.jpg', 'images/mini/Guillaume Lepors - sq.jpg', 'Guillaumes Lepors - coach', ''),
(25, '2020-04-23 14:06:09', NULL, 'images/Elise 6.jpg', 'images/mini/Elise 6.jpg', 'Elise M', ''),
(26, '2020-04-23 14:34:08', NULL, 'images/Elise 2.jpg', 'images/mini/Elise 2.jpg', 'Elise M', ''),
(27, '2020-04-23 14:34:58', NULL, 'images/Juan O 2.jpg', 'images/mini/Juan O 2.jpg', 'Juan O', ''),
(28, '2020-04-23 14:38:33', NULL, 'images/Juan O 3.jpg', 'images/mini/Juan O 3.jpg', 'Juan O', ''),
(29, '2020-04-23 15:21:44', NULL, 'images/Chris M 2.jpg', 'images/mini/Chris M 2.jpg', 'Chris Web', ''),
(30, '2020-04-23 22:02:34', '2020-04-27 16:29:28', 'images/chaussettes.jpg', 'images/mini/chaussettes.jpg', ' ', ''),
(31, '2020-04-26 13:16:34', NULL, 'images/Blandine G.jpg', 'images/mini/Blandine G.jpg', 'Blandine G.', ''),
(32, '2020-04-26 13:29:53', NULL, 'images/Thibaut L.jpg', 'images/mini/Thibaut L.jpg', 'Thibaut L', ''),
(33, '2020-04-26 13:32:50', NULL, 'images/Brice C.jpg', 'images/mini/Brice C.jpg', 'Brice C.', ''),
(34, '2020-04-26 13:47:58', NULL, 'images/Coach.jpg', 'images/mini/Coach.jpg', 'Coach', ''),
(35, '2020-04-29 11:48:23', NULL, 'images/65467515_2366281040126608_3097303570882494464_o.jpg', 'images/mini/65467515_2366281040126608_3097303570882494464_o.jpg', ' ', ''),
(36, '2020-04-29 12:56:37', NULL, 'images/stage jeunes.jpg', 'images/mini/stage jeunes.jpg', 'Stage Jeunes', ''),
(37, '2020-04-29 12:56:52', '2020-04-29 12:57:08', 'images/stage jeunes 2.jpg', 'images/mini/stage jeunes 2.jpg', 'Stage Jeunes', ''),
(38, '2020-04-29 12:57:50', NULL, 'images/jeunes 3.jpg', 'images/mini/jeunes 3.jpg', 'Groupe Jeunes', ''),
(39, '2020-04-29 12:58:23', NULL, 'images/jeunes 4.jpg', 'images/mini/jeunes 4.jpg', 'Jeunes', ''),
(40, '2020-04-29 13:01:46', NULL, 'images/Semaine type.jpg', 'images/mini/Semaine type.jpg', 'Semaine type adultes', ''),
(41, '2020-04-29 13:52:52', NULL, 'images/logos bw.jpg', 'images/mini/logos bw.jpg', 'logos partenaires', ''),
(42, '2020-04-30 14:03:30', '2020-04-30 14:16:39', 'images/Halle C Guillaume.jpg', 'images/mini/Halle C Guillaume.jpg', ' Halle Christiane Guillaume', ''),
(43, '2020-04-30 14:10:39', NULL, 'images/suzanne Lenglen.jpg', 'images/mini/suzanne Lenglen.jpg', 'Stade Suzanne Lenglen', ''),
(44, '2020-04-30 14:17:09', NULL, 'images/gymnase Jules Guesde.jpg', 'images/mini/gymnase Jules Guesde.jpg', 'Gymnase Jules Guesde', ''),
(45, '2020-04-30 15:02:38', NULL, 'images/torpilles.jpg', 'images/mini/torpilles.jpg', 'Torpilles au repos', ''),
(46, '2020-07-30 09:08:05', '2020-07-30 11:51:34', 'images/team.jpg', 'images/mini/team.jpg', 'Elites', '');

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

CREATE TABLE `menus` (
  `menuId` int(11) NOT NULL,
  `menuTitle` varchar(64) NOT NULL,
  `menuOrder` int(11) NOT NULL,
  `menuCreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `menuDateUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `menus`
--

INSERT INTO `menus` (`menuId`, `menuTitle`, `menuOrder`, `menuCreationDate`, `menuDateUpdated`) VALUES
(1, 'La vie du club', 10, '2020-01-08 16:49:26', '2020-04-15 12:25:55'),
(2, 'Adultes', 14, '2020-01-08 16:55:22', NULL),
(3, 'Jeunes', 18, '2020-01-08 17:03:23', '2020-01-27 17:00:27'),
(4, 'Nos partenaires', 35, '2020-03-22 14:11:47', '2020-04-15 12:25:06');

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `pageTitle` varchar(256) NOT NULL,
  `pageOrder` int(11) NOT NULL,
  `pageDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageDateUpdated` datetime DEFAULT NULL,
  `pageContent` text NOT NULL,
  `pageMenuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pages`
--

INSERT INTO `pages` (`pageId`, `pageTitle`, `pageOrder`, `pageDateCreated`, `pageDateUpdated`, `pageContent`, `pageMenuId`) VALUES
(1, 'Découvrez le club', 10, '2020-01-13 14:49:56', '2020-04-26 13:45:18', '&lt;p&gt;&lt;strong&gt;Issy Triathlon &lt;/strong&gt;a &eacute;t&eacute; cr&eacute;&eacute; en janvier 1988. En 1999, l&rsquo;&eacute;cole de triathlon obtenait son agr&eacute;ment et voyait arriver les premiers jeunes au club. En 2008, le club comportait 170 adh&eacute;rents. Sous l&rsquo;impulsion de l&rsquo;&eacute;quipe de direction en place depuis 10 ans, le club s&rsquo;est d&eacute;velopp&eacute; et les&nbsp; effectifs sont &agrave; pr&eacute;sent de &lt;strong&gt;647 licenci&eacute;s dont 210 jeunes&lt;/strong&gt;.&lt;/p&gt;\n\n&lt;p&gt;Au cours de ces 10 derni&egrave;res ann&eacute;es, le club a obtenu plusieurs titres de champion de France par &eacute;quipe de duathlon femmes, des titres individuels et par &eacute;quipes de champion de France de duathlon et triathlon chez les jeunes et les adultes, quatre titres de champion du monde (dont 3 en 2019) et quatre d&rsquo;Europe individuel femmes (dont 2 en 2019), un titre de champion d&rsquo;Europe de para-duathlon.&lt;/p&gt;\n\n&lt;p&gt;En 2019, en duathlon, le club a gagn&eacute; le titre de champion de France D1 &eacute;lite femmes, 15&egrave;me de la D1 &eacute;lite hommes. Sandra Levenez a &eacute;t&eacute; championne du monde &eacute;lite de duathlon femmes. En Triathlon, notre &eacute;quipe femme a fini 4&egrave;me de la D1 &eacute;lite et nos gar&ccedil;ons 15&egrave;me de la D1 &eacute;lite.&lt;/p&gt;\n\n&lt;p&gt;Emilie Morier a remport&eacute; les titres de championne du monde U23 de triathlon et de champion du monde de relais mixte de triathlon fin ao&ucirc;t 2019. Morgane Riou a gagn&eacute; les titres de championne d&rsquo;Europe de cross-triathlon et cross-duathlon.&lt;/p&gt;\n\n&lt;p&gt;Chez les jeunes, le club a fini vice-champion de France et d&rsquo;Ile de France des clubs jeunes. Nos &eacute;quipes benjamines filles ont &eacute;t&eacute; championnes de France d&rsquo;aquathlon et nos minimes gar&ccedil;ons ont &eacute;t&eacute; champions de France de triathlon.&lt;/p&gt;\n\n&lt;p&gt;Notre &lt;strong&gt;projet associatif&lt;/strong&gt; a pour ambition de permettre la pratique du triathlon &lt;strong&gt;au plus grand nombre &lt;/strong&gt;en se d&eacute;veloppant suivant trois axes:&lt;/p&gt;\n\n&lt;ol&gt;\n	&lt;li&gt;&lt;strong&gt;l&rsquo;&eacute;cole de triathlon &lt;/strong&gt;pour permettre aux enfants de pratiquer notre discipline et faire &eacute;merger les meilleurs &eacute;l&eacute;ments pour constituer autour d&rsquo;eux des &eacute;quipes comp&eacute;titives en mettant en oeuvre des partenariats avec l&rsquo;&eacute;ducation nationale&nbsp;;&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;La pratique loisir&lt;/strong&gt;&nbsp;: les pratiquants loisirs constituent le socle du club. Favoriser l&rsquo;acc&egrave;s au triathlon des pratiquants loisirs permet de contribuer au d&eacute;veloppement et &agrave; la reconnaissance du triathlon aupr&egrave;s du plus grand nombre, de proposer une pratique sportive originale aux Iss&eacute;ens et salari&eacute;s des entreprises de la ville.&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;Les &eacute;quipes &eacute;lites&nbsp;:&lt;/strong&gt; elles permettent &agrave; nos meilleurs jeunes la possibilit&eacute; de continuer &agrave; progresser &agrave; l&rsquo;int&eacute;rieur du club, d&rsquo;attirer les meilleurs triathl&egrave;tes des Hauts de Seine et des villes environnantes et elles sont un outil de communication.&lt;/li&gt;\n&lt;/ol&gt;\n\n&lt;p&gt;Enfin deux axes structurants transversaux guident notre d&eacute;veloppement :&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;&lt;strong&gt;D&eacute;velopper le sport au f&eacute;minin&nbsp;&lt;/strong&gt;: les femmes ont pris une place essentielle dans le club et sont un axe de d&eacute;veloppement fort, tant pour le haut niveau que pour la pratique loisir.&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;Am&eacute;liorer en permanence notre organisation&lt;/strong&gt;, former nos &eacute;ducateurs et d&eacute;velopper l&rsquo;emploi, tout en conservant une structure financi&egrave;re saine.&lt;/li&gt;\n&lt;/ul&gt;\n', 1),
(2, 'Evénements', 14, '2020-01-13 15:21:10', NULL, 'Liste des &eacute;v&eacute;nements &agrave; venir', 1),
(3, 'Partenaires Institutionnels', 10, '2020-01-13 15:35:54', '2020-04-22 15:48:51', '&lt;p&gt;Liste des partenariats non &eacute;quipementiers&lt;/p&gt;\n', 4),
(6, 'Activités', 20, '2020-01-27 11:21:51', '2020-04-29 12:44:06', '&lt;p&gt;Le club Issy Triathlon propose de nombreuses activit&eacute;s tournant autour du triple effort :&nbsp;&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Des entrainements r&eacute;guliers, avec un nombre de s&eacute;ances permettant &agrave; tous de pratiquer les trois sports confortablement et &agrave; des horaires lui convenant&lt;/li&gt;\n	&lt;li&gt;Des stages&nbsp;&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 2),
(7, 'Adhérer', 24, '2020-01-27 11:22:16', '2020-03-13 18:28:57', 'test\r\n', 2),
(8, 'Elite', 26, '2020-01-27 11:22:43', '2020-04-13 15:14:04', '&lt;p&gt;Tout ce qui se passe au niveau Elite&lt;/p&gt;\n', 2),
(12, 'Adhérer', 60, '2020-03-05 17:45:23', '2020-03-23 14:27:11', '&lt;p&gt;Informations sur les modalit&amp;eacute;s d&amp;#39;adh&amp;eacute;sion&lt;/p&gt;\r\n', 3),
(13, 'Ecole de Triathlon', 15, '2020-03-06 19:25:46', '2020-04-29 13:16:58', '&lt;p&gt;Issy Triathlon&nbsp; a mont&eacute; une &eacute;cole de triathlon qui est aujourd&rsquo;hui sa composante principale et sa priorit&eacute; pour les jeunes. Cette &eacute;cole a pour but de former les jeunes &agrave; la pratique du triathlon et des sports encha&icirc;n&eacute;s en g&eacute;n&eacute;ral. L&rsquo;accent est mis sur l&rsquo;apprentissage des techniques, sur l&rsquo;aspect ludique, sur l&rsquo;esprit de groupe pour in fine amener les jeunes &agrave; la comp&eacute;tition, le contr&ocirc;le de soi, la progression, le plaisir, le respect, l&rsquo;autonomie et la volont&eacute;. Les jeunes sont ainsi r&eacute;partis en groupes d&rsquo;&acirc;ges avec un formateur r&eacute;f&eacute;rent qui les suit sur toutes leurs activit&eacute;s.&lt;/p&gt;\n\n&lt;p&gt;L&rsquo;&eacute;cole comporte plus de 200 jeunes &agrave; partir de 6 ans ce qui en fait la plus importante &eacute;cole de triathlon en France (statistiques FFTri).&lt;/p&gt;\n\n&lt;p&gt;Les r&eacute;sultats collectifs de nos jeunes sont excellents et constants avec 11 victoires au challenge Avenir d&rsquo;Ile de France sur les 15 derni&egrave;res ann&eacute;es.&lt;/p&gt;\n\n&lt;p&gt;L&amp;#039;&eacute;cole a obtenu, en janvier 2015, apr&egrave;s 4 ann&eacute;es de travail, le label &laquo;&lt;strong&gt;&nbsp;Ecole de triathlon 3 &eacute;toiles&lt;/strong&gt;&nbsp;&raquo;. En compl&eacute;ment de cette labellisation &laquo;&nbsp;&Eacute;cole de Triathlon 3 &eacute;toiles&nbsp;&raquo;, le club a re&ccedil;u en 2019 le label &laquo;&nbsp;&lt;strong&gt;Excellence Jeune&nbsp;&lt;/strong&gt;&raquo; ce qui correspond &agrave; la reconnaissance du club comme &laquo;&nbsp;structure d&rsquo;accession au haut niveau&nbsp;&raquo; par la direction technique nationale et la f&eacute;d&eacute;ration fran&ccedil;aise de triathlon. Le label &laquo;&nbsp;Excellence Jeune&nbsp;&raquo; fait partie du &lt;strong&gt;Projet de Performance F&eacute;d&eacute;ral,&lt;/strong&gt; valid&eacute; par le Minist&egrave;re des Sports. Seuls 4 clubs ont aujourd&rsquo;hui ce label en France (Valence, Metz, Poissy et Issy-les-Moulineaux).&lt;/p&gt;\n\n&lt;p&gt;En parall&egrave;le, le club s&rsquo;est li&eacute; au Coll&egrave;ge Victor Hugo d&rsquo;Issy-les-Moulineaux via une convention permettant de faire profiter aux &eacute;l&egrave;ves de ce coll&egrave;ge de l&rsquo;encadrement et des infrastructures du club. Cette convention est un franc succ&egrave;s avec des retomb&eacute;es tr&egrave;s positives en termes d&rsquo;adh&eacute;sions et de notori&eacute;t&eacute;, et aussi vis-&agrave;-vis de la municipalit&eacute;. Depuis septembre 2011, le club a mis en place une convention du m&ecirc;me type avec le lyc&eacute;e Michelet (Vanves) afin de pouvoir permettre &agrave; nos jeunes de s&rsquo;inscrire dans une structure scolaire performante et d&rsquo;&eacute;largir le champ des jeunes pouvant avoir acc&egrave;s au triathlon.&lt;/p&gt;\n', 3),
(15, 'Partenaires équipementiers', 20, '2020-03-22 14:45:25', '2020-05-24 13:43:58', '&lt;p&gt;&lt;strong&gt;Issy Triathlon&lt;/strong&gt; a, depuis de nombreuses ann&eacute;es, nou&eacute; des accords avec des &eacute;quipementiers qui vous font b&eacute;n&eacute;ficier de tarifs pr&eacute;f&eacute;rentiels. N&amp;#039;h&eacute;sitez pas &agrave; les contacter pour plus d&amp;#039;informations.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 4),
(16, 'Questions fréquentes', 40, '2020-04-15 12:28:11', NULL, '', 1),
(17, 'Partenaires commerciaux', 30, '2020-04-22 15:49:50', NULL, '', 4),
(18, ' Test', 90, '2020-07-31 08:42:42', '2020-07-31 08:43:11', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

CREATE TABLE `sections` (
  `sectionId` int(11) NOT NULL,
  `sectionTitle` varchar(256) NOT NULL,
  `sectionDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sectionDatePublished` datetime DEFAULT NULL,
  `sectionDateUpdated` datetime DEFAULT NULL,
  `sectionContent` text NOT NULL,
  `sectionOrder` int(11) NOT NULL,
  `sectionPageId` int(11) NOT NULL,
  `sectionLayout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sections`
--

INSERT INTO `sections` (`sectionId`, `sectionTitle`, `sectionDateCreated`, `sectionDatePublished`, `sectionDateUpdated`, `sectionContent`, `sectionOrder`, `sectionPageId`, `sectionLayout`) VALUES
(2, 'Semaine type', '2020-01-27 13:45:24', NULL, '2020-07-30 14:17:01', '&lt;p&gt;Une semaine type comprend de la &lt;strong&gt;natation&lt;/strong&gt;, du &lt;strong&gt;v&eacute;lo&lt;/strong&gt; et de la &lt;strong&gt;course&agrave; pied&lt;/strong&gt;.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;Chaque activit&eacute; est propos&eacute;e sur plusieurs cr&eacute;neaux, permettant &agrave; tous de trouver chaussure &agrave; son pied. Ainsi, une s&eacute;ance de natation est propos&eacute;e tous les jours de semaine d&egrave;s 7h00 du matin pour les l&egrave;ve-t&ocirc;t. Trois fos par semaine, une deuxi&egrave;me s&eacute;ance est propos&eacute;e &agrave; l&amp;#039;heure du d&eacute;jeuner. Enfin, chaque soir, il est&nbsp; encore possible de nager.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;Des entrainements de Course &agrave; Pied sont propos&eacute;s chaque soir, en plus de deux entrainements &agrave; l&amp;#039;heure du d&eacute;jeuner.&nbsp;&lt;/p&gt;\n', 23, 6, 2),
(3, 'Section LD', '2020-01-27 13:51:41', NULL, '2020-07-30 13:33:38', '&lt;p&gt;Section sp&eacute;ciale d&eacute;di&eacute;e aux entrainements longue distance&lt;/p&gt;\n', 24, 6, 1),
(4, 'Stages', '2020-01-27 13:53:09', NULL, '2020-07-30 13:34:15', '&lt;p&gt;Le Club propose chaque ann&eacute;e 3 stages diff&eacute;rents &agrave; ses adh&eacute;rents adultes:&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Un premier stage, en Janvier, a lieu &agrave; la montagne et propose natation et ski de fond.&lt;/li&gt;\n	&lt;li&gt;Un deuxi&egrave;me stage, pendant les vacances de Printemps, propose des activit&eacute;s de type natation, v&eacute;lo et course &agrave; pied.&lt;/li&gt;\n	&lt;li&gt;Un troisi&egrave;me stage, d&eacute;but Juillet, propose &eacute;galement des activit&eacute;s de type natation, v&eacute;lo et course &agrave; pied.&lt;/li&gt;\n&lt;/ul&gt;\n', 28, 6, 1),
(5, 'Formulaire d&#039;adhésion', '2020-02-19 14:31:30', NULL, '2020-07-30 14:20:11', '&lt;p&gt;&lt;strong&gt;L&amp;#039;adh&eacute;sion se fait typiquement en d&eacute;but d&amp;#039;ann&eacute;e scolaire&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;Le processus comporte plusieurs &eacute;tapes :&nbsp;&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Inscription en ligne aupr&egrave;s de la f&eacute;d&eacute;ration&lt;/li&gt;\n	&lt;li&gt;Paiement de la cotisation&lt;/li&gt;\n	&lt;li&gt;etc&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 20, 7, 5),
(6, 'Ville  d&#039;Issy-les-Moulineaux', '2020-02-19 14:50:57', NULL, '2020-07-30 14:21:02', '&lt;p&gt;La &lt;a href=&quot;https://www.issy.com/&quot;&gt;ville d&amp;#039;Issy-les-Moulineaux&lt;/a&gt; est un &lt;strong&gt;partenaire historique&lt;/strong&gt; du club Issy Triathlon. Gr&acirc;ce au soutien de son maire, Monsieur Santini, nous b&eacute;n&eacute;ficions chaque semaine de 16 cr&eacute;neaux de natation, r&eacute;partis dans les deux piscines de la ville :&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;La &lt;a href=&quot;https://www.a-sevestre.fr/&quot;&gt;piscine Alfred Sevestre&lt;/a&gt;&lt;/li&gt;\n	&lt;li&gt;La &lt;a href=&quot;https://www.aquazena-issy.fr/&quot;&gt;piscine Aquazena&lt;/a&gt; dans l&amp;#039;&eacute;co-quartier du fort&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 10, 3, 1),
(12, 'Nos Coach', '2020-03-12 17:30:05', NULL, '2020-07-31 08:57:59', '&lt;p&gt;Le club fait appel &agrave; 25 entra&icirc;neurs, dont 3 sont salari&eacute;s.&lt;/p&gt;\n\n&lt;p&gt;Tous les entra&icirc;neurs sont qualifi&eacute;s et ont les dipl&ocirc;mes exig&eacute;s (BNSSA, BEESAN, licence STAPS, BF5, BF4 et BF3) pour offrir aux athl&egrave;tes, jeunes et adultes, l&rsquo;encadrement n&eacute;cessaire, en assurant la s&eacute;curit&eacute; et une grande qualit&eacute; d&rsquo;entra&icirc;nement.&lt;/p&gt;\n', 50, 1, 2),
(13, 'Le Comité  Directeur', '2020-03-19 21:27:54', NULL, '2020-07-30 14:20:26', '&lt;p&gt;&lt;strong&gt;ISSY TRIATHLON&lt;/strong&gt; est une association de type &laquo;&nbsp;loi 1901&nbsp;&raquo; dont le but est la pratique du triathlon, du duathlon et des sports encha&icirc;n&eacute;s en g&eacute;n&eacute;ral.&lt;/p&gt;\n\n&lt;p&gt;Le club est compos&eacute; d&rsquo;une section sportive unique et comprend une &eacute;cole de triathlon, reconnue par la f&eacute;d&eacute;ration fran&ccedil;aise de triathlon par le label &laquo;&nbsp;club formateur&nbsp;&raquo; depuis 1999, renomm&eacute; depuis 2010 &laquo;&nbsp;&eacute;cole de triathlon&nbsp;&raquo;.&lt;/p&gt;\n\n&lt;p&gt;La direction du club est assur&eacute;e par le bureau dont la composition est la suivante&nbsp;:&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;pr&eacute;sident,&lt;/li&gt;\n	&lt;li&gt;vice-pr&eacute;sident et directeur de l&rsquo;&eacute;cole de triathlon&lt;/li&gt;\n	&lt;li&gt;directeur sportif&lt;/li&gt;\n	&lt;li&gt;tr&eacute;sorier,&lt;/li&gt;\n	&lt;li&gt;secr&eacute;taire,&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;Ce bureau s&rsquo;appuie sur un comit&eacute; directeur, compos&eacute; des membres du bureau auxquels s&rsquo;ajoutent&nbsp;:&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;4 charg&eacute;es de mission&lt;/li&gt;\n	&lt;li&gt;responsable de la communication&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 10, 1, 4),
(14, 'Sport Santé', '2020-03-23 12:56:45', NULL, '2020-07-30 14:22:19', '&lt;p&gt;Le club Issy Triahlon a cr&eacute;&eacute; en 2018/2019 un groupe &lt;strong&gt;sport-sant&eacute;,&lt;/strong&gt; &lt;strong&gt;remise en forme&lt;/strong&gt; et &lt;strong&gt;renforcement musculaire&lt;/strong&gt; d&eacute;di&eacute; &lt;strong&gt;exclusivement aux femmes&lt;/strong&gt;, et plus particuli&egrave;rement aux mamans et conjointes de nos adh&eacute;rents. Ce groupe est compos&eacute; de 20 &agrave; 30 femmeset organis&eacute; autour de deux entra&icirc;nements sp&eacute;cifiques r&eacute;serv&eacute;s &agrave; ce groupe :&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;un entra&icirc;nement renforcement musculaire / course &agrave; pied le mardi soir&nbsp;&lt;/li&gt;\n	&lt;li&gt;un entra&icirc;nement natation le samedi soir.&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;Ce groupe est d&eacute;di&eacute; en priorit&eacute; aux femmes d&eacute;sirant d&eacute;buter une activit&eacute; sportive, ou la reprendre apr&egrave;s une interruption plus ou moins longue, en mettant l&rsquo;accent sur le renforcement musculaire (s&eacute;ance du mardi soir en gymnase), la course &agrave; pied et la natation.&lt;/p&gt;\n\n&lt;p&gt;L&rsquo;encadrement est assur&eacute; par les coachs dipl&ocirc;m&eacute;s du club ayant en particulier suivi une formation f&eacute;d&eacute;rale sport sant&eacute;. Des conseils nutritionnels pourront &ecirc;tre dispens&eacute;s par notre directeur sportif, titulaire d&rsquo;un dipl&ocirc;me universitaire en nutrition du sportif&lt;/p&gt;\n', 26, 6, 2),
(15, 'Sites d&#039;entrainement', '2020-04-20 16:10:10', NULL, '2020-07-30 13:02:12', '&lt;p&gt;Le club a acc&egrave;s gratuitement aux infrastructures mises &agrave; disposition par la ville d&rsquo;Issy-les-Moulineaux ou par le lyc&eacute;e Michelet de Vanves :&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;le stade d&rsquo;athl&eacute;tisme &lt;strong&gt;Jean Bouin&lt;/strong&gt; (Issy-les-Moulineaux) en cours de r&eacute;novation&lt;/li&gt;\n	&lt;li&gt;le stade d&rsquo;athl&eacute;tisme &lt;strong&gt;Suzanne Lenglen&lt;/strong&gt; (Paris 15&egrave;me)&lt;/li&gt;\n	&lt;li&gt;la Halle &lt;strong&gt;Christiane Guillaume&lt;/strong&gt; (Issy-les-Moulineaux)&lt;/li&gt;\n	&lt;li&gt;la piscine &lt;strong&gt;Aquazena&lt;/strong&gt; (Issy-les-Moulineaux)&lt;/li&gt;\n	&lt;li&gt;la piscine &lt;strong&gt;Sevestre&lt;/strong&gt; (Issy-les-Moulineaux)&lt;/li&gt;\n	&lt;li&gt;la piscine de la cit&eacute; scolaire &lt;strong&gt;Michelet&lt;/strong&gt; (Vanves)&lt;/li&gt;\n	&lt;li&gt;la salle de musculation du gymnase &lt;strong&gt;Jules Guesdes&lt;/strong&gt; (Issy-les-Moulineaux)&lt;/li&gt;\n	&lt;li&gt;un local mat&eacute;riels au coll&egrave;ge &lt;strong&gt;Victor Hugo&lt;/strong&gt; (v&eacute;los, combinaisons, etc&hellip;)&lt;/li&gt;\n	&lt;li&gt;un local administratif servant de bureau &agrave; la piscine Aquazena.&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;Le club a acc&egrave;s gratuitement aux parcs, for&ecirc;t et piste de cyclisme &agrave; proximit&eacute; de la ville pour les entra&icirc;nements de course &agrave; pied et les encha&icirc;nements v&eacute;lo/course &agrave; pied (CAP)&nbsp;:&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Circuit ferm&eacute; de &lt;strong&gt;cyclisme&lt;/strong&gt; autour de l&rsquo;hippodrome de Longchamp (situ&eacute; &agrave; 4 km de notre stade) permettant d&rsquo;organiser tous les mercredis soir &agrave; partir d&rsquo;avril des &lt;strong&gt;encha&icirc;nements&lt;/strong&gt; v&eacute;lo/CAP (un cr&eacute;neau jeune de 18h &agrave; 19h et un cr&eacute;neau adulte de 19h &agrave; 20h30).&lt;/li&gt;\n	&lt;li&gt;Parc de l&rsquo;&lt;strong&gt;Ile Saint-Germain &lt;/strong&gt;&agrave; Issy-les-Moulineaux (situ&eacute; &agrave; 500 m de notre stade) permettant de faire des entra&icirc;nements de course &agrave; pied dans un cadre naturel en alternance avec la piste d&rsquo;athl&eacute;tisme.&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;For&ecirc;t de Meudon&lt;/strong&gt; (situ&eacute; &agrave; 2 km de notre stade) permettant d&rsquo;organiser des entra&icirc;nements de course &agrave; pied de type trail d&rsquo;1h30 (le 1er lundi de chaque mois)&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;Parc sportif Suzanne Lenglen&lt;/strong&gt; (situ&eacute; &agrave; 500 m de notre stade) permettant d&rsquo;organiser des entra&icirc;nements de course &agrave; pied de type fractionn&eacute; en alternance avec la piste d&rsquo;athl&eacute;tisme.&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 60, 1, 5),
(16, 'Département des Hauts-de-Seine', '2020-04-22 15:52:22', NULL, '2020-07-30 14:21:22', '', 20, 3, 1),
(17, 'Région Ile-de-France', '2020-04-22 15:52:47', NULL, '2020-07-30 14:21:40', '', 30, 3, 1),
(18, 'Groupe découverte', '2020-04-29 12:03:22', NULL, '2020-07-31 08:00:33', '&lt;p&gt;Le groupe &laquo;&nbsp;d&eacute;couverte&nbsp;&raquo; correspond &agrave; une pratique orient&eacute;e vers le plaisir du sport, l&rsquo;apprentissage des trois disciplines du triathlon mais sans objectif de performance chiffr&eacute; ni de contraintes d&rsquo;entra&icirc;nements. Aucun niveau particulier n&amp;#039;est requis pour int&eacute;grer le groupe d&eacute;couverte.&lt;/p&gt;\n', 20, 13, 1),
(19, 'Groupes performance', '2020-04-29 12:07:59', NULL, '2020-07-31 08:01:13', '&lt;p&gt;&lt;strong&gt;Groupes &amp;#34;Performance&amp;#34; 1,2 et 3&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;Afin de permettre &agrave; nos meilleurs jeunes de progresser plus vite, le club a mis en place en d&eacute;but de saison 2012/2013 un groupe performance compos&eacute; de jeunes (11 ans &agrave; 18 ans). Son objectif est de permettre aux jeunes les plus motiv&eacute;s du club de disposer de cr&eacute;neaux d&rsquo;entra&icirc;nements suppl&eacute;mentaires et d&rsquo;un suivi particulier afin de leur permettre d&rsquo;atteindre le plus haut niveau r&eacute;gional (voire national pour les meilleurs). La contrepartie est qu&rsquo;ils doivent s&rsquo;engager sur ce projet pour la totalit&eacute; de l&rsquo;ann&eacute;e et respecter les obligations en termes de nombre d&rsquo;entra&icirc;nements et de comp&eacute;titions.&lt;br /&gt;\n&lt;br /&gt;\nPour faire partie du groupe &laquo;&nbsp;performance&nbsp;&raquo; il faut remplir plusieurs crit&egrave;res pr&eacute;cis.&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;&lt;strong&gt;Performance 1&nbsp;&lt;/strong&gt;: &Ecirc;tre qualifi&eacute; au championnat de France de Triathlon puis &ge; 150 points au Issy Young Ranking (IYR) ou TOP 10 CNJ (Classement National Jeune) ou TOP 10 Championnat de France&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;Performance 2&nbsp;&lt;/strong&gt;: &Ecirc;tre qualifi&eacute; &agrave; un des championnats de France et &ge; 140 points au IYR&lt;/li&gt;\n	&lt;li&gt;&lt;strong&gt;Performance 3&nbsp;:&lt;/strong&gt; Identification des coachs. R&eacute;serv&eacute;s aux Poussins, Pupilles et Benjamins.&lt;/li&gt;\n&lt;/ul&gt;\n', 30, 13, 2),
(20, 'Stages', '2020-04-29 13:14:58', NULL, '2020-07-30 14:19:35', '&lt;p&gt;Le Club propose chaque ann&eacute;e au moins 6 stages diff&eacute;rents &agrave; ses jeunes adh&eacute;rents.&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Aux vacances de la Toussaint, diff&eacute;rents stages de natation sur Issy-les-Moulineaux sont propos&eacute;es aux&nbsp; jeunes des groupes loisirs et performances.&lt;/li&gt;\n	&lt;li&gt;Aux vacances de No&euml;l, de nouveau un stage natation est propos&eacute;.&lt;/li&gt;\n	&lt;li&gt;En f&eacute;vrier, un stage complet associant natation, v&eacute;lo et course &agrave; pied est propos&eacute; aux jeunes des groupes performances 1 et 2.&lt;/li&gt;\n	&lt;li&gt;Aux vacances de Printemps, un stage complet associant natation, v&eacute;lo et course &agrave; pied est propos&eacute; &agrave; tous les jeunes adh&eacute;rents, &agrave; partir de l&rsquo;&acirc;ge de 10 ans.&lt;/li&gt;\n	&lt;li&gt;D&eacute;but Juillet, un stage permettant de pr&eacute;parer au mieux les enfants aux Championnat de France d&rsquo;Aquathlon est propos&eacute; et regroupe natation et course &agrave; pied.&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;Pour les stages enfants, le club prend en charge les frais de d&eacute;placement.&lt;/p&gt;\n', 60, 13, 1),
(21, 'Sections sportives', '2020-04-29 14:48:48', NULL, '2020-07-31 08:01:51', '&lt;p&gt;&lt;strong&gt;Sections sportives Coll&egrave;ge et Lyc&eacute;e&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 70, 13, 4),
(22, 'Equipe Elite', '2020-07-30 09:09:36', NULL, '2020-07-31 08:57:45', '&lt;p&gt;D&eacute;couvrez nos equipes Elite et les comp&eacute;titions auxquelles elles participent !&lt;/p&gt;\n', 15, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tagObjects`
--

CREATE TABLE `tagObjects` (
  `tagObjectId` int(11) NOT NULL,
  `tagObjectDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tagId` int(11) NOT NULL,
  `objectClass` varchar(64) NOT NULL,
  `objectId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tagObjects`
--

INSERT INTO `tagObjects` (`tagObjectId`, `tagObjectDateCreated`, `tagId`, `objectClass`, `objectId`) VALUES
(33, '2020-02-25 09:53:18', 23, 'Section', 6),
(50, '2020-02-25 19:40:06', 26, 'Image', 4),
(52, '2020-02-28 17:31:52', 37, 'Image', 9),
(55, '2020-02-28 17:32:44', 26, 'Image', 7),
(56, '2020-02-28 17:32:44', 39, 'Image', 7),
(136, '2020-03-04 15:08:19', 38, 'Image', 9),
(139, '2020-03-06 18:54:41', 47, 'Image', 10),
(140, '2020-03-06 18:54:41', 48, 'Image', 10),
(141, '2020-03-06 19:20:29', 48, 'Page', 6),
(142, '2020-03-06 19:20:29', 49, 'Page', 6),
(143, '2020-03-06 19:20:29', 50, 'Page', 6),
(144, '2020-03-06 19:20:29', 51, 'Page', 6),
(146, '2020-03-06 19:33:15', 48, 'Image', 11),
(154, '2020-03-12 16:49:40', 52, 'Section', 2),
(155, '2020-03-12 16:49:40', 48, 'Section', 2),
(156, '2020-03-12 16:49:40', 50, 'Section', 2),
(157, '2020-03-12 16:49:40', 51, 'Section', 2),
(158, '2020-03-12 16:49:40', 53, 'Section', 2),
(159, '2020-03-12 17:30:05', 54, 'Section', 12),
(160, '2020-03-12 17:30:05', 55, 'Section', 12),
(161, '2020-03-12 17:35:59', 56, 'Page', 3),
(163, '2020-03-19 21:27:54', 57, 'Section', 13),
(164, '2020-03-19 21:27:54', 58, 'Section', 13),
(165, '2020-03-21 17:17:21', 48, 'Article', 1),
(166, '2020-03-21 17:17:21', 59, 'Article', 1),
(167, '2020-03-21 17:19:48', 49, 'Article', 39),
(168, '2020-03-21 17:19:48', 2, 'Article', 39),
(170, '2020-03-21 17:55:40', 34, 'Page', 13),
(171, '2020-03-21 17:55:40', 28, 'Page', 13),
(172, '2020-03-22 14:45:25', 60, 'Page', 15),
(173, '2020-03-22 14:45:25', 61, 'Page', 15),
(174, '2020-03-22 18:17:42', 48, 'Image', 13),
(175, '2020-03-22 18:17:42', 59, 'Image', 13),
(177, '2020-03-22 18:18:26', 59, 'Image', 14),
(178, '2020-03-22 18:18:26', 48, 'Image', 14),
(179, '2020-03-22 18:18:26', 62, 'Image', 14),
(180, '2020-03-22 18:25:51', 63, 'Image', 13),
(181, '2020-03-23 12:56:45', 64, 'Section', 14),
(182, '2020-03-23 12:56:45', 65, 'Section', 14),
(183, '2020-03-23 13:17:47', 54, 'Article', 41),
(184, '2020-03-23 13:18:28', 54, 'Image', 15),
(185, '2020-03-23 13:29:55', 54, 'Image', 16),
(186, '2020-03-23 13:33:39', 54, 'Article', 42),
(187, '2020-03-23 13:35:15', 64, 'Article', 42),
(188, '2020-03-23 13:43:19', 57, 'Image', 17),
(189, '2020-03-23 13:43:41', 57, 'Image', 18),
(190, '2020-03-23 13:48:01', 57, 'Article', 43),
(191, '2020-03-23 13:51:15', 57, 'Article', 41),
(192, '2020-03-23 14:05:58', 25, 'Image', 19),
(193, '2020-03-23 14:05:58', 66, 'Image', 19),
(194, '2020-04-02 18:06:31', 67, 'Image', 20),
(195, '2020-04-03 14:01:39', 68, 'Image', 21),
(196, '2020-04-03 14:01:39', 69, 'Image', 21),
(197, '2020-04-03 14:01:39', 70, 'Image', 21),
(198, '2020-04-05 08:12:44', 48, 'Image', 4),
(199, '2020-04-05 08:12:44', 71, 'Image', 4),
(200, '2020-04-05 08:13:17', 71, 'Image', 7),
(201, '2020-04-05 08:13:17', 48, 'Image', 7),
(202, '2020-04-05 08:14:18', 72, 'Image', 8),
(203, '2020-04-05 08:14:18', 49, 'Image', 8),
(204, '2020-04-05 08:14:18', 73, 'Image', 8),
(205, '2020-04-05 08:14:18', 74, 'Image', 8),
(206, '2020-04-05 08:15:21', 74, 'Image', 11),
(207, '2020-04-05 08:15:58', 74, 'Image', 12),
(208, '2020-04-05 08:15:58', 75, 'Image', 12),
(209, '2020-04-05 08:15:58', 71, 'Image', 12),
(224, '2020-04-13 12:06:57', 67, 'Image', 22),
(225, '2020-04-13 12:55:01', 67, 'Image', 23),
(227, '2020-04-15 12:28:11', 77, 'Page', 16),
(228, '2020-04-20 07:29:43', 54, 'Article', 45),
(229, '2020-04-20 07:35:21', 78, 'Article', 46),
(230, '2020-04-20 07:35:21', 2, 'Article', 46),
(231, '2020-04-20 09:46:22', 54, 'Image', 24),
(232, '2020-04-20 09:49:55', 54, 'Article', 47),
(233, '2020-04-20 16:10:10', 2, 'Section', 15),
(234, '2020-04-22 15:49:50', 70, 'Page', 17),
(235, '2020-04-22 15:52:22', 70, 'Section', 16),
(236, '2020-04-22 15:52:47', 70, 'Section', 17),
(237, '2020-04-23 13:49:48', 54, 'Article', 48),
(238, '2020-04-23 13:58:04', 54, 'Article', 49),
(239, '2020-04-23 14:06:09', 57, 'Image', 25),
(240, '2020-04-23 14:12:46', 57, 'Article', 50),
(241, '2020-04-23 14:34:08', 57, 'Image', 26),
(242, '2020-04-23 14:34:58', 54, 'Image', 27),
(243, '2020-04-23 14:38:33', 54, 'Image', 28),
(244, '2020-04-23 15:21:44', 54, 'Image', 29),
(245, '2020-04-23 15:21:44', 57, 'Image', 29),
(246, '2020-04-23 22:02:34', 79, 'Image', 30),
(247, '2020-04-26 10:00:55', 57, 'Article', 51),
(248, '2020-04-26 10:02:50', 57, 'Article', 52),
(249, '2020-04-26 10:04:31', 57, 'Article', 53),
(250, '2020-04-26 13:10:08', 54, 'Article', 54),
(251, '2020-04-26 13:16:34', 54, 'Image', 31),
(252, '2020-04-26 13:23:09', 54, 'Article', 55),
(253, '2020-04-26 13:29:53', 54, 'Image', 32),
(254, '2020-04-26 13:32:50', 54, 'Image', 33),
(255, '2020-04-26 13:47:58', 54, 'Image', 34),
(256, '2020-04-29 11:48:23', 28, 'Image', 35),
(258, '2020-04-29 12:03:22', 28, 'Section', 18),
(259, '2020-04-29 12:07:59', 28, 'Section', 19),
(260, '2020-04-29 12:07:59', 80, 'Section', 19),
(261, '2020-04-29 12:56:37', 28, 'Image', 36),
(262, '2020-04-29 12:56:37', 35, 'Image', 36),
(263, '2020-04-29 12:57:08', 28, 'Image', 37),
(264, '2020-04-29 12:57:08', 35, 'Image', 37),
(265, '2020-04-29 12:57:50', 28, 'Image', 38),
(266, '2020-04-29 12:57:50', 80, 'Image', 38),
(267, '2020-04-29 12:58:23', 28, 'Image', 39),
(268, '2020-04-29 12:58:23', 80, 'Image', 39),
(272, '2020-04-29 13:14:58', 28, 'Section', 20),
(276, '2020-04-30 13:57:12', 48, 'Article', 56),
(277, '2020-04-30 13:57:12', 59, 'Article', 56),
(278, '2020-04-30 13:59:56', 59, 'Article', 57),
(279, '2020-04-30 13:59:56', 48, 'Article', 57),
(280, '2020-04-30 14:02:47', 51, 'Article', 58),
(281, '2020-04-30 14:10:39', 50, 'Image', 43),
(282, '2020-04-30 14:12:47', 50, 'Article', 59),
(283, '2020-04-30 14:12:47', 81, 'Article', 59),
(284, '2020-04-30 14:16:39', 50, 'Image', 42),
(285, '2020-04-30 14:19:36', 82, 'Article', 60),
(286, '2020-04-30 15:02:38', 65, 'Image', 45),
(287, '2020-04-30 15:08:46', 65, 'Article', 61),
(288, '2020-05-10 13:16:38', 83, 'Article', 62),
(289, '2020-05-24 13:44:40', 84, 'Section', 14),
(290, '2020-07-30 09:08:05', 39, 'Image', 46),
(291, '2020-07-30 09:09:36', 39, 'Section', 22),
(294, '2020-07-30 13:34:15', 35, 'Section', 4),
(295, '2020-07-30 13:34:15', 42, 'Section', 4);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `tagId` int(11) NOT NULL,
  `tagName` varchar(128) NOT NULL,
  `tagDescription` varchar(512) NOT NULL,
  `tagDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tagDateUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`tagId`, `tagName`, `tagDescription`, `tagDateCreated`, `tagDateUpdated`) VALUES
(2, 'Entrainement', 'Informations relatives à un entrainement (annulation, modification, matériel nécessaire, ...)', '2020-02-17 09:33:16', NULL),
(11, 'Emilie Morier', 'Tag Created by ArticleController', '2020-02-23 18:49:29', NULL),
(12, 'Audrey Merle', 'Tag Created by ArticleController', '2020-02-23 18:49:29', '2020-06-29 08:54:49'),
(13, 'WebMaster', 'Tag Created by ArticleController', '2020-02-23 18:51:55', NULL),
(23, 'Issy', 'Tag Created by SectionController', '2020-02-25 09:51:50', NULL),
(24, 'Issy-Les-Moulineaux', 'Tag Created by SectionController', '2020-02-25 09:51:50', NULL),
(25, 'IUT', 'Tag Created by SectionController', '2020-02-25 09:51:50', NULL),
(26, 'Issy Triathlon', 'Tag Created by PageController', '2020-02-25 10:32:23', NULL),
(27, 'Club', 'Tag Created by PageController', '2020-02-25 10:32:23', NULL),
(28, 'Jeunes', 'Tag Created by PageController', '2020-02-25 10:44:54', NULL),
(29, 'Victor Hugo', 'Tag Created by PageController', '2020-02-25 10:44:54', '2020-04-13 21:38:31'),
(30, 'Michelet', 'Tag Created by PageController', '2020-02-25 10:44:54', NULL),
(31, 'Lycée', 'Tag Created by PageController', '2020-02-25 10:44:54', NULL),
(32, 'Collège', 'Tag Created by PageController', '2020-02-25 10:44:54', NULL),
(33, 'UNSS', 'Tag Created by PageController', '2020-02-25 10:44:54', '2020-04-13 21:38:50'),
(34, 'Ecole', 'Tag Created by PageController', '2020-02-25 10:44:54', NULL),
(35, 'stage', 'Tag Created by ArticleController', '2020-02-25 11:05:24', NULL),
(37, 'Linda', 'Tag Created by ImageController', '2020-02-26 13:56:25', NULL),
(38, 'girls', 'Tag Created by ImageController', '2020-02-28 17:31:59', NULL),
(39, 'Elite', 'Tag Created by ImageController', '2020-02-28 17:32:44', NULL),
(40, 'boys', 'Tag Created by ImageController', '2020-02-28 17:33:36', NULL),
(41, 'Colle sur Loup', 'Tag Created by ArticleController', '2020-02-29 20:06:23', NULL),
(42, 'adultes', 'Tag Created by ArticleController', '2020-02-29 20:07:15', NULL),
(43, 'stages', 'Tag Created by Article controller', '2020-02-29 20:16:36', NULL),
(45, 'Autre', 'Tag Created by Article controller', '2020-03-04 14:47:37', NULL),
(46, 'LD', 'Tag Created by Article controller', '2020-03-04 14:56:40', NULL),
(47, 'Bonnet', 'Tag Created by Image controller', '2020-03-06 18:54:41', NULL),
(48, 'Natation', 'Tag Created by Image controller', '2020-03-06 18:54:41', NULL),
(49, 'Vélo', 'Tag Created by Page controller', '2020-03-06 19:20:29', NULL),
(50, 'Course', 'Tag Created by Page controller', '2020-03-06 19:20:29', NULL),
(51, 'Course à pied', 'Tag Created by Page controller', '2020-03-06 19:20:29', NULL),
(52, 'Run', 'Tag Created by Section controller', '2020-03-12 16:49:40', NULL),
(53, 'Entrainements', 'Tag Created by Section controller', '2020-03-12 16:49:40', NULL),
(54, 'Coach', 'Tag Created by Section controller', '2020-03-12 17:30:05', NULL),
(55, 'Entraineur', 'Tag Created by Section controller', '2020-03-12 17:30:05', NULL),
(56, 'Partenariats', 'Tag Created by Page controller', '2020-03-12 17:35:59', NULL),
(57, 'Codir', 'Tag Created by Section controller', '2020-03-19 21:27:54', NULL),
(58, 'Administration', 'Tag Created by Section controller', '2020-03-19 21:27:54', NULL),
(59, 'Piscine', 'Tag Created by Article controller', '2020-03-21 17:17:21', NULL),
(60, 'Equipement', 'Tag Created by Page controller', '2020-03-22 14:45:25', NULL),
(61, 'Matériel', 'Tag Created by Page controller', '2020-03-22 14:45:25', NULL),
(62, 'Aquazena', 'Tag Created by Image controller', '2020-03-22 18:17:42', NULL),
(63, 'Sevestre', 'Tag Created by Image controller', '2020-03-22 18:25:51', NULL),
(64, 'Sport-santé', 'Tag Created by Section controller', '2020-03-23 12:56:45', NULL),
(65, 'Torpilles', 'Tag Created by Section controller', '2020-03-23 12:56:45', NULL),
(66, 'Issy Urban Trail', 'Tag Created by Image controller', '2020-03-23 14:05:58', NULL),
(67, 'Logo', 'Tag Created by Image controller', '2020-04-02 18:06:31', NULL),
(68, 'MyIssyTri', 'Tag Created by Image controller', '2020-04-03 14:01:39', NULL),
(69, 'Overcoaching', 'Tag Created by Image controller', '2020-04-03 14:01:39', NULL),
(70, 'Partenaire', 'Tag Created by Image controller', '2020-04-03 14:01:39', NULL),
(71, 'Elites', 'Tag Created by Image controller', '2020-04-05 08:12:44', NULL),
(72, 'Transition', 'Tag Created by Image controller', '2020-04-05 08:14:18', NULL),
(73, 'Duathlon', 'Tag Created by Image controller', '2020-04-05 08:14:18', NULL),
(74, 'Compétition', 'Tag Created by Image controller', '2020-04-05 08:14:18', NULL),
(75, 'Podium', 'Tag Created by Image controller', '2020-04-05 08:15:58', NULL),
(76, 'Blabla', 'Tag Created by Page controller', '2020-04-08 11:59:23', NULL),
(77, 'Faq', 'Tag Created by Page controller', '2020-04-15 12:28:11', NULL),
(78, 'Multi', 'Tag Created by Article controller', '2020-04-20 07:35:21', NULL),
(79, 'Kiwami', 'Tag Created by Image controller', '2020-04-23 22:02:34', NULL),
(80, 'Ecole de triathlon', 'Tag Created by Section controller', '2020-04-29 12:07:59', NULL),
(81, 'Stade', 'Tag Created by Article controller', '2020-04-30 14:12:47', NULL),
(82, 'Ppg', 'Tag Created by Article controller', '2020-04-30 14:19:36', NULL),
(83, 'Test', 'Tag Created by Article controller', '2020-05-10 13:16:38', NULL),
(84, 'Femmes', 'Tag Created by Section controller', '2020-05-24 13:44:40', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tiles`
--

CREATE TABLE `tiles` (
  `tileId` int(11) NOT NULL,
  `tileTitle` varchar(256) NOT NULL,
  `tileDateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tileDateUpdated` datetime DEFAULT NULL,
  `tileOrder` int(11) NOT NULL,
  `tileImportance` int(11) NOT NULL,
  `tileImage` varchar(512) NOT NULL,
  `tileContent` text NOT NULL,
  `tileStatus` int(11) NOT NULL,
  `tilePublicationDate` date NOT NULL,
  `tileExpiryDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tiles`
--

INSERT INTO `tiles` (`tileId`, `tileTitle`, `tileDateCreated`, `tileDateUpdated`, `tileOrder`, `tileImportance`, `tileImage`, `tileContent`, `tileStatus`, `tilePublicationDate`, `tileExpiryDate`) VALUES
(2, 'Stages', '2020-03-11 17:24:05', '2020-07-08 10:10:23', 30, 4, 'images/tiles/stage avril 2019.jpg', '&lt;p&gt;Comme tous les ans, une semaine intensive de stage vous est propos&eacute;e. Au programme : de la natation, du v&eacute;lo (beaucoup), de la course &agrave; pied (pas mal) et de la PPG (un peu), le tout dans une ambiance d&eacute;contract&eacute;e.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;Les informations sont disponibles sur la page &amp;#34;&lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;6&quot;&gt;activit&eacute;s&lt;/a&gt;&amp;#34; de la section &amp;#34;adultes&amp;#34;.&nbsp; Comme d&amp;#039;habitude, les inscriptions se font sur otre espace adh&eacute;rents sur&nbsp; &lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;2&quot;&gt;Overcoaching.&lt;/a&gt;&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 0, '2020-03-21', '2022-04-04'),
(4, 'IUT 2020 : retour', '2020-03-11 18:01:13', '2020-07-08 10:08:25', 55, 2, 'images/tiles/Iut2020.jpg', '&lt;p&gt;Samedi 1er f&eacute;vrier, Issy Triathlon organisait la 5&egrave;me &eacute;dition de l&amp;#039;&lt;strong&gt;Issy Urban Trail&lt;/strong&gt;. Avec pr&egrave;s de &lt;strong&gt;1400 inscrits&lt;/strong&gt;, c&amp;#039;est un nombre record de concurrents qui ont pu (re-)d&eacute;couvrir les rues d&amp;#039;Issy-les-Moulineaux &agrave; la lumi&egrave;re d&amp;#039;une frontale, sur 6 formats allant de la marche 10km jusqu&amp;#039;&agrave; une course 30km.&lt;br /&gt;\n&lt;br /&gt;\nUn grand bravo &agrave; tous les concurrents, ainsi qu&amp;#039;aux b&eacute;n&eacute;voles et &agrave; l&amp;#039;&eacute;quipe d&amp;#039;organisation ! Retrouvez les&nbsp;&lt;a href=&quot;http://www.issytriathlon.com/iut/resultats.php&quot;&gt;r&eacute;sultats&lt;/a&gt;, ainsi qu&amp;#039;une premi&egrave;re s&eacute;rie de&nbsp;&lt;a href=&quot;https://flic.kr/s/aHsmL9at9n&quot;&gt;photos&lt;/a&gt;.&lt;/p&gt;\n', 1, '2020-03-21', '2021-05-30'),
(5, 'Classement', '2020-03-12 16:54:21', '2020-04-30 14:41:05', 40, 1, 'images/tiles/Course.JPG', '&lt;p&gt;Plus d&amp;#039;informations sur le classement&lt;/p&gt;\n\n&lt;table&gt;\n	&lt;thead&gt;\n		&lt;tr&gt;\n			&lt;th&gt;Divison&lt;/th&gt;\n			&lt;th&gt;Position&lt;/th&gt;\n		&lt;/tr&gt;\n	&lt;/thead&gt;\n	&lt;tbody&gt;\n		&lt;tr&gt;\n			&lt;td&gt;D1&lt;/td&gt;\n			&lt;td&gt;1&lt;/td&gt;\n		&lt;/tr&gt;\n		&lt;tr&gt;\n			&lt;td&gt;D2&lt;/td&gt;\n			&lt;td&gt;3&lt;/td&gt;\n		&lt;/tr&gt;\n	&lt;/tbody&gt;\n&lt;/table&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 1, '2020-03-21', '2020-10-03'),
(6, 'Infomations Coronavirus', '2020-03-12 17:41:22', '2020-07-08 10:07:06', 20, 0, 'images/tiles/covid19.jpg', '&lt;p&gt;En premier lieu, veuillez noter que le&nbsp;&lt;strong&gt;duathlon jeunes de Verri&egrave;res-le-Buisson&lt;/strong&gt;, qui devait avoir lieu dimanche,&nbsp;&lt;strong&gt;est annul&eacute;&lt;/strong&gt;, par d&eacute;cision du club organisateur et de la Municipalit&eacute;.&lt;/p&gt;\n\n&lt;p&gt;En second lieu : des f&eacute;d&eacute;rations proches de la FFTRI (natation et athl&eacute;tisme) ont d&eacute;j&agrave; pris des mesures d&amp;#039;interdiction de toute forme de regroupement (entra&icirc;nement, comp&eacute;tition, stage) ; la FFTRI, de son c&ocirc;t&eacute;, doit prendre une d&eacute;cision formelle dans la soir&eacute;e ; la Ville d&amp;#039;Issy-les-Moulineaux a d&amp;#039;ores et d&eacute;j&agrave; interdit les cr&eacute;neaux d&amp;#039;entra&icirc;nements jeunes.&lt;/p&gt;\n\n&lt;p&gt;Au regard de ces &eacute;l&eacute;ments, et sachant que nous serons oblig&eacute;s de nous conformer aux instructions qui seront donn&eacute;es par la FFTRI ce soir, le Comit&eacute; Directeur du club a d&eacute;cid&eacute;, &agrave; titre conservatoire, d&amp;#039;&lt;strong&gt;annuler tous les entra&icirc;nements jeunes, adultes et sport-sant&eacute;, et ce, avec effet imm&eacute;diat&lt;/strong&gt;, et&nbsp;&lt;strong&gt;jusqu&amp;#039;&agrave; nouvel ordre&lt;/strong&gt;. Donc en particulier, il n&amp;#039;y aura aucun entra&icirc;nement ce soir, ni jeunes ni adultes. Il en va de m&ecirc;me pour les comp&eacute;titions sous sa responsabilit&eacute; (courses jeunes).&lt;/p&gt;\n\n&lt;p&gt;La d&eacute;cision a &eacute;t&eacute; prise dans l&amp;#039;int&eacute;r&ecirc;t collectif de l&amp;#039;ensemble de nos adh&eacute;rents, et cela participe &agrave; l&amp;#039;effort collectif demand&eacute; &agrave; toute la Nation.&lt;/p&gt;\n\n&lt;p&gt;S&amp;#039;agissant des 2 stages d&amp;#039;avril organis&eacute;s par le club : la d&eacute;cision sera prise dans la semaine &agrave; venir, au vu des instructions de la FFTRI et de celles de la Direction R&eacute;gionale Jeunesse Sports Coh&eacute;sion Sociale (DRJSCS) de la Pr&eacute;fecture de R&eacute;gion. Nous ne manquerons pas de vous tenir inform&eacute;s.&lt;/p&gt;\n\n&lt;p&gt;N&amp;#039;oubliez pas les gestes barri&egrave;res, et prenez soin de vous.&lt;/p&gt;\n\n&lt;p&gt;Bien sportivement,&lt;/p&gt;\n\n&lt;p&gt;Didier Serrano&lt;/p&gt;\n\n&lt;p&gt;Pr&eacute;sident Issy Triathlon&lt;/p&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 1, '2020-03-12', '2020-12-30'),
(7, 'Nos Partenaires', '2020-03-26 10:22:33', '2020-04-30 14:49:59', 5, 5, 'images/tiles/logos bw.jpg', '&lt;p&gt;Un club comme Issy Triathlon ne peut se d&eacute;velopper sans un &eacute;cosyst&egrave;me solide de partenaires, qu&amp;#039;il soient publics ou priv&eacute;s. Certains ont un lien direct avec le triathlon, d&amp;#039;autres retrouvent dans notre sport des valeurs proches de celles de leur entreprise.&lt;/p&gt;\n\n&lt;p&gt;D&eacute;ocouvrez-les :&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Nos partenaires&lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;3&quot;&gt; institutionnels&lt;/a&gt;&lt;/li&gt;\n	&lt;li&gt;Les &lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;15&quot;&gt;&eacute;quipementiers &lt;/a&gt;&lt;/li&gt;\n	&lt;li&gt;Les partenaires &lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;17&quot;&gt;commerciaux&lt;/a&gt;&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p&gt;&nbsp;&lt;/p&gt;\n', 1, '2020-03-26', '2022-03-27'),
(8, 'Rejoignez-nous ! ', '2020-04-02 15:00:42', '2020-07-08 10:09:58', 1, 4, 'images/tiles/OC-logo2.png', '&lt;p&gt;Pour rejoindre les adultes du club,&nbsp; cliquez &lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;7&quot;&gt;ici&lt;/a&gt; afin de conna&icirc;tre les formalit&eacute;s, en particulier pour obtenir votre licence de la f&eacute;d&eacute;ration.&nbsp; Vous serez ensuite redirig&eacute; vers la plate-forme adh&eacute;rents de Overcoaching ou vous trouverez votre espace adh&eacute;rent.&lt;/p&gt;\n\n&lt;p&gt;Si vous souhaitez inscrire un jeune, cliquez &lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;12&quot;&gt;ici&lt;/a&gt;.&lt;/p&gt;\n', 0, '2020-04-02', '2025-04-03'),
(9, 'Back to work', '2020-04-11 14:33:19', '2020-07-08 09:34:24', 70, 0, 'images/tiles/47154229_2038159802938735_226409252548247552_o.jpg', '&lt;p&gt;Il est temps de retourner &agrave; la piscine !&lt;/p&gt;\n', 1, '2020-04-11', '2022-06-30'),
(10, 'Focus &#34;Jeunes&#34; ', '2020-04-18 14:04:48', '2020-07-08 10:08:59', 92, 1, 'images/tiles/jeunes 4.jpg', '&lt;p&gt;L&amp;#039;&eacute;cole de triathlon de Issy-Les-Moulineaux fait partie des axes de d&eacute;veloppement prioritaires du club.&nbsp; Si tu as entre 6 at 17 ans, et que tu souhaites te lancer ou te perfectionner, tu trouveras un groupe correspondant &agrave; ton &acirc;ge et &agrave; ton niveau. De plus, des conventions sign&eacute;es avec les coll&egrave;ges et lyc&eacute;es de Issy-Les-Moulineaux et de communes voisines permettent de trouver des cr&eacute;neaux d&amp;#039;entra&icirc;nement adapt&eacute;s &agrave; ton emploi du temps scolaire.&lt;/p&gt;\n\n&lt;p&gt;Plus d&amp;#039;informations sont disponibles sur la page &lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;13&quot;&gt;Ecole de Triathlon &lt;/a&gt;du site.&lt;/p&gt;\n', 1, '2020-04-18', '2022-04-19'),
(11, 'Devenir Arbitre', '2020-04-30 13:35:31', '2020-07-08 10:09:30', 80, 1, 'images/tiles/arbitres.jpg', '&lt;p&gt;Comme tout club, Issy Triathlon doit fournir chaque ann&eacute;e un nombre important de &amp;#34;journ&eacute;es d&amp;#039;arbitrage&amp;#34; sur des &eacute;preuves de la Ligue &amp;#34;Ile de France&amp;#34;.&nbsp; Il est donc important d&amp;#039;avoir un &amp;#34;vivier&amp;#34; d&amp;#039;arbitres au sein du club. La Commission R&eacute;gionale d&amp;#039;Arbitrage organise chaque ann&eacute;e des journ&eacute;es de formation, abordant de mani&egrave;re p&eacute;dagogique la r&egrave;glementation de la F&eacute;d&eacute;ration Fran&ccedil;aise de Triathlon, que nul n&amp;#039;est sens&eacute; ignorer si elle ou il souhaite participer &agrave; des &eacute;preuves.&nbsp;&lt;/p&gt;\n\n&lt;p&gt;Au-del&agrave; des connaissances plus approfondies, c&amp;#039;est aussi une autre mani&egrave;re de vivre une comp&eacute;tition. Pas besoin d&amp;#039;&ecirc;tre exp&eacute;riment&eacute;, il suffit d&amp;#039;un peu de curiosit&eacute;, de bonne volont&eacute;, et de quelques jours par an. Alors, pourquoi pas vous ?&nbsp; N&amp;#039;h&eacute;sitez pas &agrave; contacter le Codir si vous &ecirc;tes interess&eacute; !&lt;/p&gt;\n', 1, '2020-04-30', '2021-05-01'),
(12, 'Féminisation du triathlon', '2020-04-30 13:51:56', '2020-04-30 14:42:46', 60, 1, 'images/tiles/15145484-e3d7-40c6-989d-fe7cfa699e05.JPG', '&lt;p&gt;La &lt;strong&gt;f&eacute;minisation&lt;/strong&gt; de notre discipline est un axe important de d&eacute;veloppement du club de Issy Triathlon. Le club compte &lt;strong&gt;34% de femmes en moyenne&lt;/strong&gt; (28% chez les adultes et&nbsp; 46% chez les jeunes). Nous constatons que la participation f&eacute;minine est tr&egrave;s importante &agrave; ISSY TRIATHLON et sup&eacute;rieure &agrave; la moyenne de l&rsquo;IDF.&lt;/p&gt;\n\n&lt;p&gt;Le triathlon est un sport qui se pr&ecirc;te bien &agrave; une pratique par les femmes et les filles. Les qualit&eacute;s physiques et techniques requises font que les performances atteintes sont &eacute;quivalentes jusqu&rsquo;&agrave; un assez haut niveau. De m&ecirc;me, la qualit&eacute; de sport individuel permet une mixit&eacute; dans les entra&icirc;nements comme dans la plupart des comp&eacute;titions. De ce fait, aucune diff&eacute;rence ni s&eacute;paration n&rsquo;est faite et les femmes / filles se sentent &agrave; l&rsquo;aise, totalement int&eacute;gr&eacute;es dans le club.&lt;/p&gt;\n\n&lt;p&gt;Le triathlon est aussi un sport attirant pour les femmes car &lt;strong&gt;non agressif&lt;/strong&gt; pour la sant&eacute; et au contraire combinant 3 sports fondamentaux pour l&rsquo;entretien et le modelage du corps.&lt;br /&gt;\nEnfin, le triathlon laisse une grande libert&eacute; de pratique (absence de contrainte de disponibilit&eacute;, libert&eacute; dans les objectifs physiques) et s&rsquo;int&egrave;gre facilement dans l&rsquo;organisation hebdomadaire des femmes.&lt;/p&gt;\n\n&lt;p&gt;Le club est fier de compter parmi ses adh&eacute;rents des athl&egrave;tes reconnues comme Linda Guinoiseau, Emilie Morier, Sandra Levenez, Audrey Merle et&nbsp; d&amp;#039;autres encore. Mais au-del&agrave; de ces triathl&egrave;tes de haut niveau, c&amp;#039;est aussi notre volont&eacute; d&amp;#039;ouvrir notre sport &agrave; toutes, au travers de la section &amp;#34;&lt;a href=&quot;index.php?class&amp;#61;Page&amp;amp;action&amp;#61;display&amp;amp;id&amp;#61;6&quot;&gt;&lt;strong&gt;Sport Sant&eacute;&lt;/strong&gt;&lt;/a&gt;&amp;#34; et de ses &amp;#34;&lt;strong&gt;torpilles&lt;/strong&gt;&amp;#34;. D&eacute;couvrez-les dans la page &amp;#34;activit&eacute;s adultes&amp;#34; !&lt;/p&gt;\n', 1, '2020-04-30', '2022-05-01');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `lastName` varchar(256) NOT NULL,
  `firstName` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(512) NOT NULL,
  `role` varchar(16) NOT NULL,
  `passwordChanged` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`userId`, `lastName`, `firstName`, `email`, `password`, `role`, `passwordChanged`) VALUES
(1, 'Roggeband', 'Philippe', 'proggeband@gmail.com', '$2y$10$z7D3ohDQBQ51NP2doyE9N.nrBUftvqnB40GR2e5ru9BrKu3xlDN4C', 'Admin', 1),
(2, 'Mathias', 'Christophe', 'chris@web.com', '$2y$10$KyoQmsE73XOQ2Bov81W4Q.X1AXEiEOla7R1eDrBvxgNkrvmHcGRh.', 'Admin', 0),
(3, 'Junior', 'Editor', 'Edit@piaf.com', '$2y$10$siBKLM7SlZXdxF//vvsiw.d54MmxwyN0/KojMVUasFiy4fah5O6iu', 'Editor', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`articleId`,`articleSectionId`),
  ADD KEY `articleSectionId` (`articleSectionId`);

--
-- Index pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contactId`);

--
-- Index pour la table `imageObjects`
--
ALTER TABLE `imageObjects`
  ADD PRIMARY KEY (`imageObjectId`);

--
-- Index pour la table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`imageId`);

--
-- Index pour la table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`menuId`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`,`pageMenuId`),
  ADD KEY `pageMenuId` (`pageMenuId`);

--
-- Index pour la table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`sectionId`,`sectionPageId`),
  ADD KEY `sectionPageId` (`sectionPageId`);

--
-- Index pour la table `tagObjects`
--
ALTER TABLE `tagObjects`
  ADD PRIMARY KEY (`tagObjectId`,`tagId`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tagId`);

--
-- Index pour la table `tiles`
--
ALTER TABLE `tiles`
  ADD PRIMARY KEY (`tileId`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `articleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT pour la table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contactId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `imageObjects`
--
ALTER TABLE `imageObjects`
  MODIFY `imageObjectId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT pour la table `images`
--
ALTER TABLE `images`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT pour la table `menus`
--
ALTER TABLE `menus`
  MODIFY `menuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `sections`
--
ALTER TABLE `sections`
  MODIFY `sectionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `tagObjects`
--
ALTER TABLE `tagObjects`
  MODIFY `tagObjectId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `tagId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT pour la table `tiles`
--
ALTER TABLE `tiles`
  MODIFY `tileId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`articleSectionId`) REFERENCES `sections` (`sectionId`);

--
-- Contraintes pour la table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`pageMenuId`) REFERENCES `menus` (`menuId`);

--
-- Contraintes pour la table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`sectionPageId`) REFERENCES `pages` (`pageId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
