<?php
/**
 * 
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 *  Change History :
 * 
 * 
 *  Display page content, including children sections.  Section content may be collapsed. 
 * 
*          Clicking on the title uncollapses the section and displays article titles within the section (Ajax function) as well as section 
*          tags.
* 
*          If a single image is included, display image
*          If multiple images are associated, display image slider (to be added)
*          If tags are associated with the page, display the tags as buttons, which redirect the user to a search function with the tag name
*  
*/        
    // unwrap parameters passed by page display controller
    $page=$params["page"];
    $sections=$params["sections"];
    $tags=$params["tags"];
    $images=$params["images"];
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Issy Triathlon 3.0 <?=Lib::cleanHtml($page->getPageTitle())?></title>
<?php $meta = ob_get_clean() ?>


<?php ob_start() ?>  <!-- Build $content variable -->
    <?php if(count($sections)!==0) :?>
        <div class="subMenu mobile-hidden">
            <div class="container">
                <nav class="navbar">
                    <?php foreach ($sections as $section) :?>
                        <?php if ($section->getSectionLayout() != 0) :?> <!-- no submenu bar for WIDE format sections -->
                        <a class="issySubMenuBar" href="#section-<?=$section->getSectionId()?>"><?=$section->getSectionTitle()?></a>
                        <?php endif?>
                    <?php endforeach ?>
                </nav>

            </div>
        </div>
    <?php endif ?>
    
    <!-- Display page title only if text is not empty, otherwise start directly with 1st section -->

    <?php if($page->getPageContent()!=="") :?>
        <div class="container">
            <!-- display images associated with the page -->
            <?php $imagesHtml=Lib::displayImages($images) ?>
            <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->
            <div class="row">
                <p class="pageText"><span class="title ml-3"><?=Lib::cleanHtml($page->getPageTitle())?></span></p>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-5 pageText">
                    <?=Lib::cleanHtml(html_entity_decode($page->getPageContent()))?>
                </div>
                <div class="col-sm-12 col-md-7 ">
                    <?=$imagesHtml?>
                </div>
            </div>
        </div>
    <?php endif ?>


        <!-- BUild sections display.  -->
    <?php foreach ($sections as $section):?>
        <div id="section-<?=$section->getSectionId()?>"></div> <!-- Anchor for sub-menu links -->
        <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

        <!-- Get all section details -->
        <?php 
            $sectionImages=Lib::getObjectImages("Section",$section->getSectionId()) ;
            $sectionArticles=Lib::getSectionArticles($section->getSectionId());
            $sectionTags=Lib::getObjectTags("Section",$section->getSectionId());
            // Build Html for sections
            $sectionImagesHtml=Lib::displayImages($sectionImages);
            $sectionLayout=$section->getSectionLayout();
        ?>


        <?php switch($sectionLayout): 
        case 0: ?> <!-- Wide --> 
            <!-- Assumptions : Section has ONE image, a title to be display in a "cartridge", and a very short text , and NO associated articles -->

            <div class="mt-3 mb-3 imgContainer">
                <?php $sectionImage=$sectionImages[0] ; ?>
                <img src="<?=$sectionImage->getImagePath()?>" alt="" class="imgWide">
                <div class="filter"></div>
                <div class="top-left">
                    <span class="fondBlanc"><?=$section->getSectionTitle()?></span>
                </div>
                <div class="centered-left">
                    <span class="title text-white"><?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?></span>
                </div>
                <div class="bottom-right">
                    <span class="callToAction"><?=Lib::cleanHtml(html_entity_decode($sectionImage->getImageLegend()))?></span>
                </div>
                <div class="top-right mobile-hidden">
                    <img src="img/DoubleLine.png" alt="">
                </div>
            </div>


        <?php break; ?>

        <?php case 1: ?> <!-- L: Text - R: Image - No articles -->

            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <span class="title pageText"><?=$section->getSectionTitle()?></span>
                        <div class="mt-3 pageText">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$sectionImagesHtml?>
                    </div>
                </div>

            </div>


        <?php break; ?>

        <?php case 2: ?> <!-- 3 cols : Image text text -->

            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$sectionImagesHtml?>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-8">
                        <span class="title pageText mt-3"><?=$section->getSectionTitle()?></span>
                        <div class="tile-text mt-3">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                    <?php foreach ($sectionArticles as $article)  :?>

                        <?php if (ArticleController::displayArticle($article)) :?>
                            <?php 
                                // Get article details
                                $articleImages=Lib::getObjectImages("Article",$article->getArticleId());
                                $articleImagesHtml=Lib::displayImages($articleImages);
                                $articleTags=Lib::getObjectTags("Article",$article->getArticleId());
                                if (count($articleImages)==0) {
                                    $coverImage="img/LogoIssyTriathlon.jpg"; }
                                else {
                                    $coverImage=$articleImages[0]->getImagePath();
                                }
                            ?>
                            <!-- display article card -->

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="card mt-3 mb-3" >
                                    <!-- Button trigger modal -->

                                        <button type="button" class="btn btn-white" data-toggle="modal" data-target="#article<?=$article->getArticleId()?>">
                                            <img src="<?=$coverImage?>" class="articleCover" alt="">
                                        </button>
                                        <p class="text-center"><?=Lib::cleanHtml($article->getArticleTitle())?></p>

                                </div>
                            </div>

                            <!-- Set up modal for full article content --> 

                            <?=Lib::buildArticleModal2($article,$articleImages,$articleTags,$section) ?> 
                        <?php endif ?>                                                    
                    <?php endforeach ?>
                </div>

            </div> 

        <?php break; ?>

        <?php case 3: ?> <!-- L: Text - Articles -->
            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <span class="title pageText"><?=$section->getSectionTitle()?></span>
                        <div class="mt-3 pageText">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="card-deck">
                            <?php foreach ($sectionArticles as $article)  :?>

                                <?php if (ArticleController::displayArticle($article)) :?>
                                    <?php 
                                        // Get article details
                                        $articleImages=Lib::getObjectImages("Article",$article->getArticleId());
                                        $articleImagesHtml=Lib::displayImages($articleImages);
                                        $articleTags=Lib::getObjectTags("Article",$article->getArticleId());
                                        if (count($articleImages)==0) {
                                            $coverImage="img/LogoIssyTriathlon.jpg"; }
                                        else {
                                            $coverImage=$articleImages[0]->getImagePath();
                                        }
                                    ?>
                                    <!-- display article card -->

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="card mt-3 mb-3" >
                                            <!-- Button trigger modal -->

                                                <button type="button" class="btn btn-white" data-toggle="modal" data-target="#article<?=$article->getArticleId()?>">
                                                    <img src="<?=$coverImage?>" class="articleCover" alt="">
                                                </button>
                                                <p class="text-center"><?=Lib::cleanHtml($article->getArticleTitle())?></p>

                                        </div>
                                    </div>

                                    <!-- Set up modal for full article content --> 

                                    <?=Lib::buildArticleModal2($article,$articleImages,$articleTags,$section) ?> 
                                <?php endif ?>                                                    
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php break; ?>

        <?php case 4: ?> <!-- Text top, articles below, round -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <span class="title pageText"><?=$section->getSectionTitle()?></span>
                        <div class="mt-3 pageText">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                    <?php foreach ($sectionArticles as $article)  :?>

                        <?php if (ArticleController::displayArticle($article)) :?>
                            <?php 
                                // Get article details
                                $articleImages=Lib::getObjectImages("Article",$article->getArticleId());
                                $articleImagesHtml=Lib::displayImages($articleImages);
                                $articleTags=Lib::getObjectTags("Article",$article->getArticleId());
                                if (count($articleImages)==0) {
                                    $coverImage="img/LogoIssyTriathlon.jpg"; }
                                else {
                                    $coverImage=$articleImages[0]->getImagePath();
                                }
                            ?>
                            <!-- display article card -->

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="card mt-3 mb-3" >
                                    <!-- Button trigger modal -->

                                        <button type="button" class="btn btn-white" data-toggle="modal" data-target="#article<?=$article->getArticleId()?>">
                                            <img src="<?=$coverImage?>" class="articleCover" alt="">
                                        </button>
                                        <p class="text-center"><?=Lib::cleanHtml($article->getArticleTitle())?></p>

                                </div>
                            </div>

                            <!-- Set up modal for full article content --> 

                            <?=Lib::buildArticleModal2($article,$articleImages,$articleTags,$section) ?> 
                        <?php endif ?>                                                    
                    <?php endforeach ?>
                </div>

            </div>
        <?php break; ?>

        <?php case 5: ?> <!-- Text top, articles below, square -->
            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <span class="title pageText"><?=$section->getSectionTitle()?></span>
                        <div class="mt-3 pageText">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                    <?php foreach ($sectionArticles as $article)  :?>

                        <?php if (ArticleController::displayArticle($article)) :?>
                            <?php 
                                // Get article details
                                $articleImages=Lib::getObjectImages("Article",$article->getArticleId());
                                $articleImagesHtml=Lib::displayImages($articleImages);
                                $articleTags=Lib::getObjectTags("Article",$article->getArticleId());
                                if (count($articleImages)==0) {
                                    $coverImage="img/LogoIssyTriathlon.jpg"; }
                                else {
                                    $coverImage=$articleImages[0]->getImagePath();
                                }
                            ?>
                            <!-- display article card -->

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="card mt-3 mb-3" >
                                    <!-- Button trigger modal -->

                                    <button type="button" class="btn btn-white" data-toggle="modal" data-target="#article<?=$article->getArticleId()?>">
                                        <img src="<?=$coverImage?>" class="articleCoverSq" alt="">
                                    </button>
                                    <p class="text-center"><?=Lib::cleanHtml($article->getArticleTitle())?></p>

                                </div>
                            </div>

                            <!-- Set up modal for full article content --> 
                            <?=Lib::buildArticleModal2($article,$articleImages,$articleTags,$section) ?> 
                        <?php endif ?>                                                    
                    <?php endforeach ?>
                </div>

            </div>
        <?php break; ?>

        <?php default: ?> <!-- 3 cols : Image text text -->
                <p class="sectionTitle mt-5 ml-3 pageText"><?=Lib::cleanHtml($section->getSectionTitle())?> </p> 
                <?php if ($sectionImagesHtml=="") :?>
                    <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card-body">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>                              
                    </div>
                <?php else :?>
                    <div class="column col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="card-body">
                            <?=Lib::cleanHtml(html_entity_decode($section->getSectionContent()))?>
                        </div>                              
                    </div>
                    <div class="column col-xs12 col-sm-12 col-md-4 col-lg-4">
                        <?=$sectionImagesHtml ?>
                    </div>
                <?php endif ?>
                <!-- WHen a section unfolds, a card deck provides access to a list of articles belonging to the section. Each article can display as a modal -->
                <div class="card-deck">
                    <?php foreach ($sectionArticles as $article)  :?>

                        <?php if (ArticleController::displayArticle($article)) :?>
                            <?php 
                                // Get article details
                                $articleImages=Lib::getObjectImages("Article",$article->getArticleId());
                                $articleImagesHtml=Lib::displayImages($articleImages);
                                $articleTags=Lib::getObjectTags("Article",$article->getArticleId());
                                if (count($articleImages)==0) {
                                    $coverImage="img/LogoIssyTriathlon.jpg"; }
                                else {
                                    $coverImage=$articleImages[0]->getImagePath();
                                }
                            ?>
                            <!-- display article card -->

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="card mt-3 mb-3" >
                                    <!-- Button trigger modal -->
                                    <div class="card-body bg-white">
                                        <button type="button" class="btn btn-white" data-toggle="modal" data-target="#article<?=$article->getArticleId()?>">
                                            <img src="<?=$coverImage?>" class="articleCover ml-3" alt="">
                                        </button>
                                        <p class="text-center"><?=Lib::cleanHtml($article->getArticleTitle())?></p>
                                    </div>
                                </div>
                            </div>

                            <!-- Set up modal for full article content --> 

                            <div class="modal fade" id="article<?=$article->getArticleId()?>" tabindex="-1" role="dialog" aria-labelledby="articleLabel<?=$article->getArticleId()?>" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-secondary text-white">
                                            <h5 class="modal-title " id="articleLabel<?=$article->getArticleId()?>"><?=Lib::cleanHtml($article->getArticleTitle())?></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?php if ($articleImagesHtml !=="") :?>
                                                <div>
                                                    <?=$articleImagesHtml?>
                                                </div>

                                            <?php endif ?>
                                            <div>
                                                    <?=Lib::cleanHtml(html_entity_decode($article->getArticleContent()))?>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <!-- List tags assocated with the article, with automated search for related items -->
                                            <p>
                                                <?php foreach ($articleTags as $tag) :?>
                                                    <a href="index.php?class=Search&searchString=<?=$tag->getTagName()?>" class="btn btn-outline-secondary btn-sm" role="button"><?=$tag->getTagName()?></a>
                                                <?php endforeach ?>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div> 
                        <?php endif ?>                                                    
                    <?php endforeach ?>
                </div>
            </div>
            <?php break; ?>

        <?php endswitch; ?>

        <div class="container">
            <!-- List tags assocated with the section, with automated search for related items -->
            <p>
                <?php foreach ($sectionTags as $tag) :?>
                    <a href="index.php?class=Search&searchString=<?=$tag->getTagName()?>" class="btn btn-outline-secondary btn-sm" role="button"><?=$tag->getTagName()?></a>
                <?php endforeach ?>
            </p>
        </div>

    <?php endforeach ?>
    <div class="container">
        <div class="row">
            <p class="mt-3">
                <!-- List tags assocated with the page, with automated search for related items -->
                <?php foreach ($tags as $tag) :?>
                    <a href="index.php?class=Search&searchString=<?=$tag->getTagName()?>" class="btn btn-outline-secondary btn-sm" role="button"><?=$tag->getTagName()?></a>
                <?php endforeach ?>
            </p>
        </div>
    </div>
		
<?php $content=ob_get_clean() ?>

<?php ob_start() ?>
<script src="jsApps/dynClassement.js"></script>
<script src="jsApps/dynCoach.js"></script>

<?php $js=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";