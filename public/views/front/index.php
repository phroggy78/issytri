<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
	<title>Bienvenue sur le site Issy Triathlon 3.0</title>
	<?php var_dump ("here"); ?>
<?php $meta = ob_get_clean() ?>

<?php ob_start() ?>
	<div class="row noMargins">
		<div class="indexBanner mobile-hidden">
			<div class="row noMargins">
				<div class="col-md-4">
					<img src="img/DoubleLine.png" class="ml-5 mt-5" alt="">
				</div>
				<div class="col-md-8 mt-5">
					<div class="mt-5 text-left">
						<span class="title text-white mt-auto mb-auto">Bienvenue sur le site d'Issy Triathlon</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $banner=ob_get_clean() ?>

<?php ob_start() ?>  <!-- Build $content variable -->

	<div class="device">     <!-- Based on device size, order of display is reversed --> 
		<div class="container">
			<div class="row">
				<div class="mt-5">
					<div class="title mt-3 mb-3">LES INFOS UTILES
					</div>
				</div>
			</div> <!-- /.row -->

			<!-- ************************************  Get dynamic tiles from library ***************************************************** --> 
			<?php $tilesHtml=Lib::buildTilesByImportance() ?>
			<?= $tilesHtml ?> 		

		</div> <!-- /.container -->

	<!-- ************************************   Get static tiles from library and dynamic calendar content *********************** -->
		<div class="container">
			<div class="row mt-3 mb-3">
				<p><span class="calendar">Calendrier</span></p>
			</div>
			<div class="row">	
				<div><span class="title mobile-hidden">SEMAINE <?=date('W')?> </span>
					<button id="calendarPrevious" class="btn ml-3 mb-3 icon-IssyBlue"><i class="fas fa-arrow-left"></i></button>
					<button id="calendarToday" class="btn mb-3 icon-IssyBlue"><i class="fas fa-stop"></i></button>
					<button id="calendarNext" class="btn mb-3 icon-IssyBlue"><i class="fas fa-arrow-right"></i></button>
				</div> 
			</div>

			<div class="row">
				<!-- Dynamic section to be built to retrieve Dynamic Calendar data from OverCoaching.com -->
				<div class="alert alert-info" id="dynCalMessage" style="display: none;">
				</div>
				<div id="dynCal" class="calendar-background">				<!-- dynCal.js will fill this div with the dynamic calendar HTML code -->
				</div> <!-- /#dynCal -->
			</div> <!-- /.col -->
			</div> <!-- /.row -->
		</div>

	</div> <!-- /.device -->

	<div class="container">
		<div class="row mt-5 mb-3">
			<p><span class="calendar">En bref</span></p>
		</div>
		<div class="row mb-3">
			<div class="title mb-3">Quelques chiffres-clés 
			</div>
			<?php 
				//$clubStats = (array)json_decode(file_get_contents("https://www.overcoaching.fr/api/v1/get_ext_club_nb_members.php"))
				$clubStats = (array)json_decode(file_get_contents("https://www.overcoaching.fr/api/club/club2_nb_members.json"));
			?>
		</div>
		<div class="row">
			<div class="infos-cles">
				<div class="mobile-hidden">
					<img src="img/logoIssyTriathlon2023.png" class="img-90" alt="Issy Triathlon">
				</div>
				<div class="oval-copy">
					<div class="oval-text">
					<span class="oval-text-small">Depuis</span> 1988
					</div>
				</div>
				<div class="oval-copy">
					<div class="oval-text">
					<?=$clubStats["total"]?><br><span class="oval-text-small">membres</span>
					</div>
				</div>
				<div class="oval-copy">
					<div class="oval-text">
					<?=$clubStats["y_percent"]?>%<br><span class="oval-text-small">de jeunes</span>
					</div>
				</div>
				<div class="oval-copy">
					<div class="oval-text">
					<?=$clubStats["f_percent"]?>%<br><span class="oval-text-small">de femmes</span>
					</div>
				</div>
				<div class="oval-copy">
					<div class="oval-text">
					<?=$clubStats["nb_coachs"]?><br><span class="oval-text-small">coachs</span>
					</div>
				</div>
				<div class="oval-copy">
					<div class="oval-text">
					<?=$clubStats["nb_adult_trainings"]?><br><span class="oval-text-small">entraînements /semaine</span>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row">
			<div class="rectangle-blue">
				<a href="index.php?class=Page&action=display&id=1"><div class="rectangle-blue-text">Je veux en savoir plus + &#128512;</div></a>
			</div>
		</div>
	</div>


	<div class="mt-3 mb-3 imgContainer">
		<a href="index.php?class=Page&action=display&id=8">
			<img src="img/d2du_h_2023.jpg" alt="" class="imgWide">
		</a>
		<div class="top-left">
			<span class="fondBlanc">Section Elites</span>
		</div>
		<div class="centered-left">
			<span class="title ml-0 text-white">Découvrez nos équipes élites Triathlon</span>
		</div>

		<div class="bottom-right">
			<span class="callToAction">Découvrez !</a></div>
		</div>

		<div class="top-right mobile-hidden">
			<img src="img/DoubleLine.png" alt="">
		</div>
	</div>

	<div class="container mt-3 mb-3">
		<div class="row">
			<div><span class="calendar">La vie du club</span></div>
		</div>
		<div class="row">
			<div class="mt-3 mb-3"><span class="title">IssyTri Inside </span></div>
		</div>

		<div class="row">
			<div class="dt-hidden mb-3 mt-3 center-content">
				<?php $randImages=Image::readManyRandom(9); ?>
				<div class="imgGallery">
					<?php foreach ($randImages as $randImage) :?>
							<img src="<?=$randImage->getImagePath()?>" alt="" class="imgMobile">
					<?php endforeach ?>
				</div>
			</div>
		
			<div class="mobile-hidden mt-3 mb-3">
				<?php $randImages=Image::readManyRandom(18); ?>
				<div class="imgGallery">
					<?php foreach ($randImages as $randImage) :?>
						<img src="<?=$randImage->getImagePath()?>" alt="" class="imgDT">
					<?php endforeach ?>
				</div>
			</div>
		</div>


		<div class="row mt-3 mb-3" >
			<div class="center-content mt-3">
				<a href="http://www.facebook.com/pages/Issy-Triathlon/230840167004050" target="_blank"><img src="img/social-fb.png" height="32" alt="Facebook"></a>
				<a href="https://twitter.com/issytriathlon" target="_blank"><img src="img/social-tw.png" class="ml-3 mr-3" height="32" alt="Twitter"></a>
				<a href="https://www.instagram.com/issy_triathlon/" target="_blank"><img src="img/social-ig.png" height="32" alt="Instagram"></a>
			</div>
		</div>

	</div>
	<a href="index.php?class=Page&action=display&id=7">
	<div class="mt-3 mb-3 imgContainer">

		<img src="img/run.jpg" alt="" class="imgWide">


			<div class="filter"></div>

		<div class="top-left">
			<span class="fondBlanc">Nouveaux adhérents</span>
		</div>
		<div class="centered-left">
			<span class="title ml-0 text-white"><br>Vous hésitez à vous inscrire ? <br><br>Venez rencontrer nos coachs lors d'une séance découverte</span>
		</div>
		<div class="bottom-right">
			<span class="callToAction">Inscrivez-vous !</span>
		</div>
		<div class="top-right mobile-hidden">
			<img src="img/DoubleLine.png" alt="">
		</div>
	</div>
	</a>

	<div class="container">
		<div class="row">
			<span class="calendar">Nos partenaires</span>
		</div>
	</div>

	<div class="container-fluid">
		<div class="partnerLogos">

		</div>
	</div>


		
<?php $content=ob_get_clean() ?>

<?php ob_start()?> 

	<!-- Include dynamic calendar JavaScript apps -->
	<script src="jsApps/dynCal3.js"></script>

<?php $js=ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";