<?php 
/**
 * Author : Philippe Roggeband - March 2020
 * Version 1.0
 * Change History
 * 
 * Display search results 
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Bienvenue sur le site de Issy Triathlon 3.0 - Recherche</title>
<?php $meta = ob_get_clean() ?>

<?php
    $pages=$params[0];
    $sections=$params[1];
    $articles=$params[2];
    $images=$params[3];
    $searchTags=$params[4];
?>

<?php ob_start() ?>  <!-- Build $content variable -->

    <div class="container">
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h5>Résultats de la recherche</h5>
            </div>
        </div>

        <div class="card">
            <div class="card-header bg-secondary text-white">
                    <h5>Pages</h5>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">

                <?php foreach ($pages as $page) :?>
                    <li class="list-group-item">
                        <a class="card-text" href="index.php?class=Page&action=display&id=<?=$page?>"><?=Page::read($page)->getPageTitle()?></a>
                    </li>
                <?php endforeach ?>
                </ul>
            </div>
        </div>

        <div class="Card">
            <div class="card-header bg-secondary text-white">
                <h5>Sections</h5>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <?php foreach ($sections as $section) :?>
                        <?php
                            $page=Section::read($section)->getSectionPageId()?>
                        <li class="list-group-item">
                            <a href="index.php?class=Page&action=display&id=<?=$page?>#section-<?=Section::read($section)->getSectionId()?>"><?=Section::read($section)->getSectionTitle()?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    
        <div class="Card">
            <div class="card-header bg-secondary text-white">
                <h5>Articles</h5>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                <?php foreach ($articles as $article) :?>
                    <?php 
                        $articleObj=Article::read($article);
                        $section=$articleObj->getArticleSectionId();
                        $page=Section::read($section)->getSectionPageId()?>
                    <li class="list-group-item">
                        <a href="index.php?class=Page&action=display&id=<?=$page?>#section-<?=Section::read($section)->getSectionId()?>"><?=Article::read($article)->getArticleTitle()?></a>  
                    </li>  
                <?php endforeach ?>
                </ul>
            </div>
        
        </div>

        <div class="card">
            <div class="card-header">
                <h5>Images</h5>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <?php foreach ($images as $image) :?>
                        <li class="list-group-item">
                        <fieldset>
                            <legend><?=Image::read($image)->getImageLegend()?></legend>
                            <a href="<?=Image::read($image)->getImagePath() ?>" target="_blank"><img src="<?= Image::read($image)->getImageMiniPath() ?>" alt=""></a>
                        </fieldset>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>

<?php $content=ob_get_clean() ?>

<?php ob_start()?> 

<?php $js=ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";