<?php 

/**
 * Author : Philippe Roggeband - April 2020
 * Version 1.0
 * Change history ; 
 * 
 * 
 * Contact form for visitors interested in joining the club
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
<title>Issy Triathlon 3.0 - Partenaires</title>
<?php $meta = ob_get_clean() ?>


<?php ob_start()?>  
        <!-- BUild sections display.  -->
            <div id="section-33"></div> <!-- Anchor for sub-menu links -->
        <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

        <!-- Get all section details -->
        

         <!-- Text top, articles below, square -->
            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <span class="title pageText">Partenaires institutionnels</span>
                        <div class="mt-3 pageText">
                            <table id="table-institution">
                            </table>
                            <p> </p>
                        </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                                    </div>

            </div>
        
        <div class="container">
            <!-- List tags assocated with the section, with automated search for related items -->
            <p>
                            </p>
        </div>

            <div id="section-37"></div> <!-- Anchor for sub-menu links -->
        <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

        <!-- Get all section details -->
        

         <!-- Wide --> 
            <!-- Assumptions : Section has ONE image, a title to be display in a "cartridge", and a very short text , and NO associated articles -->

            <div class="mt-3 mb-3 imgContainer" style="display: none;">
                                <img src="images/Equipementiers.jpg" alt="" class="imgWide">
                <div class="filter"></div>
                <div class="top-left">
                    <span class="fondBlanc">Partenaires </span>
                </div>
                <div class="centered-left">
                    <span class="title text-white"></span>
                </div>
                <div class="bottom-right">
                    <span class="callToAction">Equipementiers</span>
                </div>
                <div class="top-right mobile-hidden">
                    <img src="img/DoubleLine.png" alt="">
                </div>
            </div>


        
        <div class="container">
            <!-- List tags assocated with the section, with automated search for related items -->
            <p>
                            </p>
        </div>

            <div id="section-34"></div> <!-- Anchor for sub-menu links -->
        <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

        <!-- Get all section details -->
        

         <!-- Text top, articles below, square -->
            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <span class="title pageText">Equipement sportif</span>
                        <div class="mt-3 pageText">
                            <table id="table-sport">
                            </table>
                       </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                                    </div>

            </div>
        
        <div class="container">
            <!-- List tags assocated with the section, with automated search for related items -->
            <p>
                            </p>
        </div>

            <div id="section-40"></div> <!-- Anchor for sub-menu links -->
        <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

        <!-- Get all section details -->
        

         <!-- Text top, articles below, square -->
            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <span class="title pageText">Formation</span>
                        <div class="mt-3 pageText">
                            <table id="table-school">
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                                    </div>

            </div>
        
        <div class="container">
            <!-- List tags assocated with the section, with automated search for related items -->
            <p>
                            </p>
        </div>

            <div id="section-35"></div> <!-- Anchor for sub-menu links -->
        <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

        <!-- Get all section details -->
        

         <!-- Text top, articles below, square -->
            <div class="container">
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <span class="title pageText">Partenaires commerciaux</span>
                        <div class="mt-3 pageText">
                            <table id="table-business">
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card-deck mt-3">
                                    </div>

            </div>
<?php $content = ob_get_clean() ?>

<?php ob_start()?>

    <!-- Include dynamic calendar JavaScript apps -->
    <script src="jsApps/dynPartners.js"></script>

<?php $js=ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";