<?php 

/**
 * Author : Philippe Roggeband - April 2020
 * Version 1.0
 * Change history ; 
 * 
 * 
 * Contact form for visitors interested in joining the club
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
<title>Issy Triathlon 3.0 - Contactez-nous</title>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<?php $meta = ob_get_clean() ?>


<?php ob_start()?>  
<div class="container">
    <div class="row">
    <div class="card-deck">
        <div class="card mt-3 mb-3">
            <div class="card-header bg-IssyBlue text-white">
                Inscriptions 2024-2025
            </div> <!-- /.card-header -->
            <div class="card-body">
                Les inscriptions 2024-2025 (adultes, sport-santé, jeunes) ont ouvert le 1er septembre 2024 et se font exclusivement en ligne.<br>
                La procédure détaillée pas à pas est disponible sur notre site web, menu Adhérer.<br>
                Aucune limite de place chez les adultes. Les nouveaux jeunes doivent passer des tests obligatoires le mercredi 11 septembre avant de savoir s'ils sont retenus.<br><br>
                <!--
                <font color="red">Avant de nous contacter,</font> merci de bien consulter la <a href="https://www.issytriathlon.com/doc/2024/fiches/fiche_adultes_saison_2023_2024.pdf">fiche de renseignements adultes 2023-2024</a> ou la <a href="https://www.issytriathlon.com/doc/2024/fiches/fiche_jeunes_saison_2023_2024.pdf">fiche de renseignements jeunes 2023-2024</a>, svp !
            -->
 
            </div>
            <div class="card-header bg-IssyBlue text-white">
                Contactez-nous
            </div> <!-- /.card-header -->
            <div class="card-body">
                <form>
                    <div class="form-group row">

                        <label class="col-sm-2 col-form-label" for="lastName">Nom</label>
                        <div class="col-sm-10 mb-3">
                            <input type="text" class="form-control" name="lastName" id="lastName" required>
                        </div>
                        <label class="col-sm-2 col-form-label" for="firstName">Prénom</label>
                        <div class="col-sm-10 mb-3">
                            <input type="text" class="form-control" name="firstName" id="firstName" required>
                        </div>
                        <label class="col-sm-2 col-form-label" for="email">Adresse Mail</label>
                        <div class="col-sm-10 mb-3">
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                        <label class="col-sm-2 col-form-label" for="message">Texte</label>
                        <div class="col-sm-10 mb-3">
                            <textarea name="message" id="message" cols="35" class="form-control" rows="10" required></textarea>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="privacy" name="privacy" required>
                            <label class="form-check-label" for="privacy">J'accepte que Issy Triathlon collecte et utilise les données personnelles que je viens de communiquer dans ce formulaire dans le seul but qu'un responsable de l'association me contacte pour répondre à ma demande de renseignements.  Mes données ne seront pas utilisées à d'autres fins.</label>
                        </div>
                    </div>
                    <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LcoQvoZAAAAANKUMEr-tKBrhUZG4PFqCygjRiax"></div>
                    <br/>

                    <input type="button" class="btn btn-primary mt-3 mb-3" name="submit" id="submit" value="Envoyer" disabled>

                </form>
            </div> <!-- /.card-body -->
        </div> <!-- /.card -->
        <div class="card mt-3 mb-3">
            <div class="card-header bg-IssyBlue text-white">
                Issy Triathlon
            </div>
            <div class="card-body">
                <p>
                    <strong>Issy Triathlon,</strong> 
                    5 avenue Jean Bouin,
                    92130 Issy-les-Moulineaux
                </p>
                <p>
                Président : Didier SERRANO  <a href="mailto:serrano.didier@gmail.com"><i class="far fa-envelope"></i> serrano.didier@gmail.com</a>
                </p>
                <p>Webmaster : Christophe MATHIAS  <a href="mailto:webmaster@issytriathlon.com"><i class="far fa-envelope"></i> webmaster@issytriathlon.com</a> 
                </p>
                <p>Equipe secrétariat : <a href="mailto:secretariat@issytriathlon.com"><i class="far fa-envelope"></i> secretariat@issytriathlon.com</a> 
                </p>3
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2626.7102062168806!2d2.2626389159485214!3d48.82559047928431!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67a8508c26ce5%3A0xdb39b5dfe47b25ff!2sAvenue%20Jean%20Bouin%2C%2092130%20Issy-les-Moulineaux!5e0!3m2!1sen!2sfr!4v1587309057664!5m2!1sen!2sfr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div> <!-- /.card-body -->
        </div> <!-- /.card -->
    </div> <!-- /.card-deck --> 
    </div> <!-- /.row -->
</div> <!-- container -->
<script> 
function recaptchaCallback() {
    $('#submit').removeAttr('disabled');
};

document.getElementById('submit').addEventListener('click', function handleClick(event) {
    var request = new XMLHttpRequest();
    let params = [];
    params.push('o=2');
    params.push('email=' + document.getElementById('email').value);
    params.push('message=' + document.getElementById('message').value);
    params.push('last_name=' + document.getElementById('lastName').value);
    params.push('first_name=' + document.getElementById('firstName').value);
    let data = params.join('&');
    request.open('POST', 'https://www.overcoaching.fr/api/v1/external_contact.php');
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(data);
    request.onload = async function () {
        let resp = JSON.parse(this.response);
        if (resp.ok == 1) {
            alert("OK, message envoyé.");
            window.location = "https://www.issytriathlon.com";
        }

    }
});
</script>

<?php $content = ob_get_clean() ?>

<?php ob_start()?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";
