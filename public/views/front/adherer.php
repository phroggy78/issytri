<?php 

/**
 * Author : Christophe MATHIAS - August 2024
 * Version 1.0
 * Change history ; 
 * 
 * 
 * Subscription info
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
<title>Issy Triathlon 3.0 - Adhésion adultes</title>
<?php $meta = ob_get_clean() ?>


<?php ob_start()?> 
<div class="container">
    <!-- display images associated with the page -->
    <div> <p class="verticalSpace"></p></div> <!-- Need some space here -->
    <div class="row">
        <p class="pageText"><span class="title ml-3">Adhérer</span></p>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-5 pageText">
        	<span id="span_register_intro_text">
			</span>

			<span id="cost_a" class="cost">
			<p><strong>Combien coûte l&#039;adhésion adulte Issy Triathlon (licence comprise) ?</strong> <i class="i_fee_season"></i></p>

			<table>
				<thead>
					<tr>
						<th>type d&#039;adhésion</th>
						<th>tarif</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>standard</td>
						<td id="td_full_cost"></td>
					</tr>
					<tr>
						<td>étudiant de moins de 25 ans</td>
						<td id="td_discount_student_cost"></td>
					</tr>
					<tr>
						<td>chômeur</td>
						<td id="td_discount_nojob_cost"></td>
					</tr>
				</tbody>
			</table>
			</span>

			<span id="cost_j" class="cost">
			<p><strong>Combien coûte l&#039;adhésion jeune Issy Triathlon (licence comprise) ?</strong> <i class="i_fee_season"></i></p>

			<table border="1">
				<thead>
					<tr>
						<th>type d&#039;adhésion</th>
						<th>tarif</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>groupe Découverte (groupe par défaut)</td>
						<td id="td_discovery_cost"></td>
					</tr>
					<tr>
						<td>groupe Performance</td>
						<td id="td_performance_cost"></td>
					</tr>
					<tr>
						<td>section UNSS partenaire<br>(Découverte ou Performance)</td>
						<td id="td_unss_cost"></td>
					</tr>
				</tbody>
			</table>
			</span>

			<span id="cost_k" class="cost">
			<p><strong>Combien coûte l&#039;adhésion TriKid Issy Triathlon (licence comprise) ?</strong> <i class="i_fee_season"></i></p>

			<table border="1">
				<thead>
					<tr>
						<th>type d&#039;adhésion</th>
						<th>tarif</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>plein tarif</td>
						<td id="td_trikid_cost"></td>
					</tr>
				</tbody>
			</table>
			</span>

			<span id="cost_ssf" class="cost">
			<p><strong>Combien coûte l&#039;adhésion sport-santé femmes Issy Triathlon (licence comprise) ?</strong> <i class="i_fee_season"></i></p>

			<table>
				<thead>
					<tr>
						<th>type d&#039;adhésion</th>
						<th>tarif</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>plein tarif</td>
						<td id="td_ssf_cost"></td>
					</tr>
				</tbody>
			</table>
			</span>

			<span id="cost_ssm" class="cost">
			<p><strong>Combien coûte l&#039;adhésion sport-santé hommes Issy Triathlon (licence comprise) ?</strong> <i class="i_fee_season"></i></p>

			<table>
				<thead>
					<tr>
						<th>type d&#039;adhésion</th>
						<th>tarif</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>plein tarif</td>
						<td id="td_ssm_cost"></td>
					</tr>
				</tbody>
			</table>
			</span>
			<br>

			<p>Tarif couple/famille : au sein d&#039;une même famille/couple, si une personne a payé une adhésion plein tarif, les autres bénéficient de 20€ de réduction sur les adhésions plein tarif.</p>

			<p> </p>

			<p><strong>Que comprend l&#039;adhésion ?</strong></p>

			<p>L&#039;adhésion comprend :</p>

			<ul id="ul_subcription_includes">
			</ul>

			<p> </p>

			<span id="span_inter_season_fees" style="display: none;">
			<p><strong>Tarifs inscription en cours d&#039;année</strong></p>

			<p>A partir de mars, il est possible de s&#039;inscrire à un tarif réduit (prix incompressible de la licence &#43; prorata du prix de l&#039;adhésion club), ne donnant toutefois pas accès à la dotation club.</p>

			<p>Voici le tableau récapitulatif des prix adultes selon le mois d&#039;adhésion :</p>

			<table>
				<thead>
					<tr>
						<th>mois</th>
						<th>tarif</th>
					</tr>
				</thead>
				<tbody id="tbody_month_fees">
				</tbody>
			</table>

			<p> </p>
			</span>
        </div>

        <div class="col-sm-12 col-md-7 ">
                     
            <!-- Build list of captions -->
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <!-- Build list of images --> 
                <div class="carousel-inner">
                    <div class="carousel-item active" id="div_carou">

                        
                    </div>
                                           
                </div>
                <!--
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Suiv.</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Prec.</span>
                </a>
            -->
            </div>

        </div>
    </div>
</div>
    

<!-- BUild sections display.  -->
<div id="section-24"></div> <!-- Anchor for sub-menu links -->
<div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

<!-- Get all section details -->
     
<!-- Text top, articles below, round -->
<div class="container div_container_season_info">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <span class="title pageText" id="data_season_title"></span>
            <div class="mt-3 pageText">
                <p>Nous vous invitons à prendre connaissance de la <a href="javascript:void(0);" target="_blank" id="link_info_pdf">fiche de renseignements <span id="span_info_population"></span></a> pour la saison <span id="span_info_season"></span>, vous y trouverez très certainement les réponses à vos questions.</p>

				<p id="p_faq_adults" style="display: none;">Vous pouvez également consulter les rubriques &#34;<a href="index.php?class&#61;Page&amp;action&#61;display&amp;id&#61;1">Découvrez le club</a>&#34; et  &#34;<a href="index.php?class&#61;Page&amp;action&#61;display&amp;id&#61;16">Questions Fréquentes</a>&#34;</p>
            </div>
        </div>
    </div>
    <div class="card-deck mt-3">
    </div>
</div>
        
<div class="container div_container_season_info">
	<!-- List tags assocated with the section, with automated search for related items -->
    <p> </p>
</div>

<div id="section-5"></div> <!-- Anchor for sub-menu links -->
<div> <p class="verticalSpace"></p></div> <!-- Need some space here -->

<!-- Get all section details -->
<!-- Text top, articles below, square -->
<div class="container">
    <div class="row mt-5">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <span class="title pageText" id="span_procedure_title">Procédure d&#039;inscription adulte</span>
            <div class="mt-3 pageText">
                <p><span id="span_subscription_timing"></span> <span id="span_subscription_timing_precision"></span></p>

				<p>Comme vous le voyez ci-dessous, les inscriptions se font <strong>exclusivement en ligne</strong>, vous n&#039;avez aucun papier à nous remettre (hors éventuels chèques, si vous optez pour ce moyen de paiement).<br />
				<br />
				La procédure d&#039;inscription s&#039;effectue dans l&#039;ordre suivant :<br />
				<br />
				1) <strong>demande de licence FFTRI</strong> <i>sur le site de la Fédération</i></p>

				<ul>
					<li>connectez-vous à votre <a href="https://espacetri.fftri.com/" target="_blank">espace Tri 2.0</a>
						<ul>
							<li>pour les anciens licenciés (Issy Tri ou autres clubs) : cliquez sur &#34;Se connecter&#34;. Vous aurez alors besoin de votre identifiant FFTRI (une lettre suivie de 5 chiffres) et de votre mot de passe. Pour rappel, votre identifiant est composé des 6 premiers caractères de votre numéro de licence actuel. Ex : A12345<br />
							Si vous avez perdu votre mot de passe, cliquez sur &#34;mot de passe oublié ?&#34;.</li>
							<li>pour les néo-licenciés, cliquez sur &#34;Se licencier&#34; pour réaliser votre demande de licence.</li>
						</ul>
					</li>
					<li id="li_licence_type"></li>
					<li>il vous sera demandé de choisir entre 3 formules d&#039;assurance ; si vous hésitez entre la n°1 et la n°2, nous vous conseillons de choisir la <strong>formule d&#039;assurance n°2</strong> car la 1 et la 2 sont incluses dans le coût de l&#039;adhésion club, donc autant prendre la plus complète. Si vous choisissez la formule n°3, lors de votre inscription en ligne Issy Triathlon vous aurez un supplément à payer (la différence de tarif entre la formule 3 et la formule 2)</li>
					<li>à la fin de ce formulaire FFTRI de demande de licence, vous obtiendrez un document PDF &#34;Demande de licence FFTRI&#34;. Veuillez imprimer ce document et signer la page 2</li>
					<li>numérisez(**) les 2 premières pages du document rempli <strong>et signé</strong></li>
				</ul>

				<p>2) <strong>inscription en ligne Issy Triathlon</strong> <i>sur la plate-forme Overcoaching</i></p>

				<ul>
					<li>effectuez l&#039;<a href="https://issytri.overcoaching.fr/instances/issytri/register_home.php" target="_blank">inscription club en ligne</a>. <span id="span_coming_proof_s"></span>(**) :
						<ul id="ul_proofs">
						</ul>
						<span id="span_download_proof_s"></span></li>
				</ul>

				<p>3) <strong>paiement</strong></p>

				<ul>
					<li>pour régler votre cotisation(*), dont le montant vous sera indiqué à la fin de l&#039;étape 2, vous aurez là aussi le choix entre :
					<ul>
						<li>la régler directement en ligne lors de votre inscription club (étape 2)</li>
						<li>nous envoyer par mail (secretariat&#64;issytriathlon.com) votre règlement s&#039;il s&#039;agit d&#039;un paiement dématérialisé (e-chèques-vacances...)</li>
						<li>remettre votre règlement en mains propres à un encadrant (coach ou membre du comité directeur) lors de votre 1er entraînement</li>
					</ul>
					</li>
				</ul>

				<p>4) <strong>validation</strong></p>
				<ul>
					<li>une fois que vous aurez réalisé les 3 étapes précédentes, notre secrétariat validera votre demande d&#039;adhésion en ligne, et vous recevrez une confirmation de votre inscription.</li>
				</ul>

				<p> </p>

				<p> </p>

				<p>(*) quels sont les moyens de paiement acceptés ? un ou plusieurs parmi les suivants, au choix :</p>

				<ul>
					<li>paiement en ligne sécurisé (cartes Visa ou MasterCard)</li>
					<li>chèque(s), ordre : Issy Triathlon</li>
					<li>chèques-vacances (papier ou ANCV-Connect) ou coupons-sport ANCV</li>
					<li>participation financière COS, CAF ou CE d&#039;entreprise (fournir document à remplir et chèque de caution)</li>
					<li>espèces</li>
				</ul>

				<p>(**) pour numériser un document papier, vous pouvez le scanner ou simplement le prendre en photo avec votre téléphone. Chaque fichier obtenu ne devra pas dépasser la taille de 3 Mo.</p>
            </div>
        </div>
    </div>

    <div class="card-deck mt-3">
    	<?php
    	$popu = 'a';
    	if (isset($_SESSION['p']) && !empty($_SESSION['p'])) {
    		$popu = $_SESSION['p'];
    	}
    	echo '<input type="hidden" name="population" id="population" value="' . $popu . '">';
    	?>
	</div>

</div>
<?php $content = ob_get_clean() ?>

<?php ob_start()?>

    <!-- Include dynamic calendar JavaScript apps -->
    <script>
    	$(function () {
    		$('.cost').hide();
    		$('#cost_' + $('#population').val()).show();

          	let population = $('#population').val();
          	$.post('https://www.overcoaching.fr/api/v1/get_ext_club_info.php',
        	{
            	o: 2,
            	p: population,
        	},
        	function(data){
	            var resp = JSON.parse(data);
	            if (resp.month_fees.length > 0) {
		            $('#span_inter_season_fees').show();
	            }
	            if (population == 'a') {
	            	$('#p_faq_adults').show();
	            }
	            if (population == 'k') {
		            $('.div_container_season_info').hide();
	            }
	            $('#span_procedure_title').html("Procédure d'inscription " + resp.population_name);
	            $('#span_info_population').html(resp.population_name);
	            $('.i_fee_season').html('(données saison ' + (resp.fees_season - 1) + '-' + resp.fees_season + ')');
	            $('#td_full_cost').html(resp.full_cost + "&euro;");
	            $('#td_discount_student_cost').html(resp.discount_cost + "&euro;");
	            $('#td_discount_nojob_cost').html(resp.discount_cost + "&euro;");
	            $('#td_discovery_cost').html(resp.discovery_cost + "&euro;");
	            $('#td_performance_cost').html(resp.performance_cost + "&euro;");
	            $('#td_unss_cost').html(resp.unss_cost + "&euro;");
	            $('#td_trikid_cost').html(resp.trikid_cost + "&euro;");
	            $('#td_ssf_cost').html(resp.ssf_cost + "&euro;");
	            $('#td_ssm_cost').html(resp.ssm_cost + "&euro;");
	            let monthLines = '';
	            for (i = 0; i < resp.month_fees.length; i++) {
		            monthLines += '<tr><td>' + resp.month_fees[i].month + '</td><td>' + resp.month_fees[i].price + '&euro;</td></tr>';
	            }
	            $('#tbody_month_fees').html(monthLines);
	            $('#data_season_title').html("Renseignements " + (resp.info_season - 1) + "-" + resp.info_season);
	            $('#link_info_pdf').attr('href', resp.info_url);
	            $('#span_info_season').html((resp.info_season - 1) + "-" + resp.info_season);
	            $('#span_subscription_timing').html(resp.subscription_timing);
	            $('#span_subscription_timing_precision').html(resp.timing_precision);
	            $('#span_register_intro_text').html(resp.register_intro_text);
	            let sIncludes = '';
	            for (i = 0; i < resp.subscription_includes.length; i++) {
	            	sIncludes += "<li>" + resp.subscription_includes[i] + "</li>\n";
	            }
	            $('#ul_subcription_includes').html(sIncludes);
	            $('#li_licence_type').html(resp.licence_type_info);
	            let comingProofsTxt = "La pièce suivante vous sera demandée au format numérique";
	            let downloadProofsTxt = "Vous aurez à télécharger ce document lors de votre inscription en ligne (il vous le faut donc au format numérique)";
	            if (resp.proofs.length > 1) {
	            	comingProofsTxt = "Les pièces suivantes vous seront demandées au format numérique";
	            	downloadProofsTxt = "Vous aurez à télécharger ces documents lors de votre inscription en ligne (il vous les faut donc au format numérique)";
	            }
	            $('#span_coming_proof_s').html(comingProofsTxt);
	            $('#span_download_proof_s').html(downloadProofsTxt);
	            let proofLis = '';
	            for (i = 0; i < resp.proofs.length; i++) {
	            	proofLis += "<li>" + resp.proofs[i] + "</li>\n";
	            }
	            $('#ul_proofs').html(proofLis);
	            $('#div_carou').html('<img src="images/' + resp.picture_file + '" class="d-block w-100 img-fluid" alt="' + resp.picture_alt + '">');
        	});
	    });
    </script>

<?php $js=ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";