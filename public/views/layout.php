<!-- 

    Author : Philippe Roggeband
    V1.0 - February 2020
    Changes History - 

    Generic layout file used for every page in the web site, called by a "require" at the end of all pages
    Features : 

    - Call all dependencies (JS, CSS)
    - Build responsive navigation bar
    - Build the "tiles" which are used to display current highlighted content
    - Display the following sets of content : 
        $meta : page-specific elements to be included in file <head>
        $banner : page banner
        $content : page content 
        $js : page-specific javascript code 

-->
<?php 
    // Object to display any pending one-time flash messages
    $flash= new SessionFlash();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <!-- Bootstrap core CSS  and Jquery -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">

    <!-- JS scripts for date in-line editor plugin-->
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
    <!-- CSS file and JS script for tags plugin --> 
    <link href="css/tagsinput.css" rel="stylesheet" type="text/css">
    <script src="jsApps/tagsinput.js"></script>

    <!-- CSS file and JS scripts for cookies consent -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.1/cookieconsent.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.1/cookieconsent.min.js"></script>

    <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#000",
          "text": "#33bcf0"
        },
        "button": {
          "background": "#dc6f40"
        }
      },
      "theme": "classic",
      "content": {
        "message": "En poursuivant votre navigation, vous acceptez l'utilisation de cookies à des fins de personnalisation.",
        "dismiss": "OK",
        "link": "En savoir +",
        "href": ""
       }
    })});
    </script>

    <!-- Get access to icons library -->
    <script src="https://kit.fontawesome.com/ee6e3cf9c0.js"></script>
    <!-- Load site-specific CSS file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Retrieve view-specific meta data -->
    <?= $meta ?>

</head>

<body>
    <header>
        <!-- Navigation - build navbar dynamically -->
        <?php
            $menuC=new MenuController();
            $navBar=$menuC->buildNavBar();
        ?>
        
        <!-- Display navbar in responsive mode -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark menuBar">
            <a class="navbar-brand" href="index.php"><img src="images/logo2019_dark.jpg"  width="140px" height=auto alt="Issy Triathlon"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- id admin is logged in, display admin menu --> 

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <?=$navBar?>
                   <!-- Contact button -->
                   <a href="index.php?class=Contact&action=create" class="nav-link h5"aria-haspopup="true" aria-expanded="false">Contact</a>
                   <!-- MyIssyTri button -->
                   <a href="https://issytri.overcoaching.fr" target="_blank" class="nav-link h5"aria-haspopup="true" aria-expanded="false">MyIssyTri</a>

                    <?php if (UserController::isLogged()) : ?>
                        <li>
                            <a class="nav-link h5" href="admin.php"  aria-haspopup="true" aria-expanded="false">
                            <?=$_SESSION["role"] ?>
                            </a>
                        <li>
                            <a class="nav-link h5" href="admin.php?class=User&action=logout"  aria-haspopup="true" aria-expanded="false">
                            Logout
                            </a>
                        </li>
                    <?php endif ?>
 
                </ul> <!-- /.navbar -->


                <!-- Display field for search engines --> 
                <form class="form-inline my-2 my-lg-0" action="index.php?class=Search" method="post">
                    <input class="form-control mr-sm-2" name="searchString" id="searchString" type="search" placeholder="Mot-clé" aria-label="Search">
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Chercher</button>
                </form>
            </div>
        </nav>
    </header>

    <!-- Page Content -->
    <main>
        <!-- Load banner elements if any --> 
        <div class="row">
        <?php if(isset($banner))
            { echo $banner ;} ?>    
        </div>
        
        <div class="row">
            <!-- Main body, provided by calling view -->    
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Flash alert messages here -->
                <div class="container">
                    <?=$flash->flash()?>        <!-- Display any flash messages -->

                    <?= $content ?>             <!-- Display content from caller -->
                </div>
            </div>
        </div>

        <div class="row section">
            <!-- Display Strava statistics and links to social media  -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="container center-content mt-3">
                    <a href="http://www.facebook.com/pages/Issy-Triathlon/230840167004050" target="_blank"><img src="http://www.issytriathlon.com/img/follow/logo-facebook.png" height="32" alt="Facebook" /></a>
                    <a href="https://twitter.com/issytriathlon" target="_blank"><img src="http://www.issytriathlon.com/img/follow/logo-twitter.png" height="32" alt="Twitter" /></a>
                    <a href="https://www.instagram.com/issy_triathlon/" target="_blank"><img src="http://www.issytriathlon.com/img/follow/logo-insta.png" height="32" alt="Instagram" /></a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <p></p>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >  
                <div class="container center-content">

                    <div id="strava-club" class="container center-content mb-3">
                        <iframe height="160" frameborder='0' scrolling='no' src='https://www.strava.com/clubs/issy-triathlon/latest-rides/b67a66aa06c77690125c5b203a2b732087fc9f03?show_rides=false'></iframe>
                    </div>  
                </div>
            </div>
        </div> 
      </main>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Issy Triathlon <?php echo date('Y'); ?></p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript  -->
    <script src="js/jquery.min.js"></script>   
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>  


    <!-- Load view-specific JavaScript elemetns --> 
    <?php if(isset($js))
    { echo $js ;} ?>
</body>

</html>