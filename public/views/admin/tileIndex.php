
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Tiles </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Tiles</h2>
    <!-- Build table with list of Pages and Menus -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Tile Title</th>
            <th scope="col" class="Col"  data-sortable="true">Tile Order</th>
            <th scope="col" class="Col"  data-sortable="true">Status</th>
            <th scope="col" class="Col"  data-sortable="true">Importance</th>
            <th scope="col" class="Col"  data-sortable="true">Publication Date</th>
            <th scope="col" class="Col"  data-sortable="true">Expiry date</th>
        </thead>
        <tbody>
            <?php foreach ($params as $tile) :?>
                <tr>
                    <td><a href="admin.php?class=Tile&action=update&id=<?=$tile['tileId'] ?>"><?=$tile['tileId']?></a></td>
                    <td><?=$tile['tileTitle'] ?></td>
                    <td><?=$tile['tileOrder'] ?></td>
                    <td><?=$tile['tileStatus'] ?></td>
                    <td><?=$tile['tileImportance'] ?></td>
                    <td><?=$tile['tilePublicationDate']?></td>
                    <td><?=$tile['tileExpiryDate']?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

    <a href="admin.php?class=Tile&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add Tile</button></a>

</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";