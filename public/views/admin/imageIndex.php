
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Images </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Images</h2>
    <!-- Build table with list of Images -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <tr>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Image Legend</th>
            <th scope="col" class="Col"  data-sortable="true">Image Path</th>
            <th scope="col" class="Col"  data-sortable="true">Image</th>
            <th scope="col" class="Col"  data-sortable="true">Use</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($params as $image) :?>
                <tr>
                    <td><a href="admin.php?class=Image&action=update&id=<?=$image['imageId'] ?>"><?=$image['imageId']?></a></td>
                    <td><?=$image['imageLegend'] ?></td>
                    <td><?=$image['imagePath'] ?></td>
                    <td><img src="<?=$image['imageMiniPath']?>"></td>
                    <td><?=$image["imageUse"]?></td>
                </tr>
            <?php endforeach ?>
        </tbody>

    </table>

    <a href="admin.php?class=Image&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add Image</button></a>

</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";