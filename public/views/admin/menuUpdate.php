<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Edit Menu</title>
<?php $meta = ob_get_clean() ?>

<?php $menu=$params[0]; ?>           <!-- parameters array should only contain one item -->

<?php ob_start()?>  

    <div class="container">
        <h2>Edit Menu </h2>
        <form action="admin.php?class=Menu&action=update&id=<?=$menu->getMenuId()?>" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="menuId"> Id </label>
                <div class="col-sm-10">
                    <input class="form-control" id="menuId" name="menuId" type="text" value="<?=$menu->getMenuId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="menuTitle">Menu Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="menuTitle" id="menuTitle" type="text" value="<?=Lib::cleanHtml($menu->getMenuTitle())?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="menuOrder">Menu Order</label>
                <div class="col-sm-10">
                    <input class="form-control" type="number"  min="1" max="100" name="menuOrder" id="menuOrder" value="<?=$menu->getMenuOrder()?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Update</button>
        </form>
        
        <a href="admin.php?class=Menu&action=delete&id=<?=$menu->getMenuId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a> 
        <a href="admin.php?class=Menu&action=index"><button class="btn btn-warning mt-3">List</button></a>
    </div>

<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";