<?php 
/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * Render view to create an article in the database
 * This view is called by the article controller responsable for validity checks
 * 
 * An artice is always the child of a section
 * An aricle can be pointed to by Tag Objects, to enable keyword-based searches
 * An article can be associated with images, which will be displayed with the article text
 * 
 * An article has a publication and expiry date. It is only displayed if the current date is within range.
 * An article has a status :
 *      Preview : it is only displayed with the page if the user is logged in with admin or editor profile, regardless of the date
 *      Active : it is displayed if within the date range
 *      Hidden : it is not displayed to users, even if within the date range
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create Article </title>
<?php $meta = ob_get_clean() ?>

<!-- Body code  --> 
<?php ob_start()?>  
    <div class="container">
        <h2>Create Article</h2>
        <form action="admin.php?class=Article&action=create" method="post" enctype="multipart/form-data">
            <div class="form-group row mb-3">
                <label class="col-sm-2 col-form-label" for="articleTitle">Article Title</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="articleTitle" id="articleTitle" type="text" required>
                </div>
                <label class="col-sm-2 col-form-label" for="articleOrder">Article Order</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="number"  min="1" max="100" name="articleOrder" id="articleOrder" required>
                </div>
                <label class="col-sm-2 col-form-label" for="articleSectionId">Parent Section</label>
                <div class="col-sm-10 mb-3">
                    <select name="articleSectionId" id="articleSectionId" required>
                    <?php foreach ($params as $section) : ?>
                        <option value="<?=$section['sectionId'] ?>"><?=$section['sectionTitle'] ?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                
                <label class="col-sm-2 col-form-label" for="articleContent">Article Content</label>
                <div class="col-sm-10 mb-3">
                    <textarea name="articleContent" rows="10" cols="80" required></textarea>
                </div>
                <!-- Copy Paste for date pickers --> 
                <label class="col-sm-2 col-form-label" for="dp1">Publication date</label>
                <div class="col-sm-4 mb-3">
                    <input id="articlePublicationDate" name="articlePublicationDate" type="date" class="form-control">   
                </div>   
                <label class="col-sm-2 col-form-label" for="dp2">Expiry date</label>
                <div class="col-sm-4 mb-3">
                    <input id="articleExpiryDate" name="articleExpiryDate" type="date" class="form-control">
                </div>
                <label class="col-sm-2 col-form-label" for="articleStatus">Article Status</label>
                <div class="col-sm-10 mb-3">
                    <select name="articleStatus" id="articleStatus" required>
                        <option value="0">Hidden</option>           <!-- Noone sees the article -->
                        <option value="1">Active</option>           <!-- The article is visible if the current date is within range -->
                        <option value="2">Preview</option>          <!-- the article is visible if the viewer is logged in with admin profile -->
                    </select>
                </div>
                <!-- Article Tags -->
                <label class="col-sm-2 col-form-label" for="articleTags">Tags</label>
                <div class="col-sm-10 mb-3">
                    <input name="articleTags" id="articleTags" type="text" data-role="tagsinput"  />
                </div>
                <!-- Article images --> 
                <label class="col-sm-2 col-form-label" for="imageGallery">Images</label>
                <div class="col-sm-10 mb-3">
                    <input type="hidden" name="objectImages" id="objectImages">
                    <div id="imageGallery" class="gallery">         <!-- div to display image library dynamically --> 
                    </div>
                </div>
            
                <button class="btn btn-warning" id="btnImages">Library Images</button>
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Create</button>
        </form>
    </div>
<?php $content = ob_get_clean() ?>

<!-- Scripts to be included at end of layout --> 
<?php ob_start()?>
    <script>  // WISYWIG editor script 
        CKEDITOR.replace('articleContent', {
        height: 260,
        allowedContent: true
        });
    </script>

    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 
<?php $js = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";