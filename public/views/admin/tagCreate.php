<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Add Tag </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  

    <div class="container">
        <h2>Add Tag</h2>
        <form action="admin.php?class=Tag&action=create" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="tagName">Tag Name</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="tagName" id="tagName" type="text" required>
                </div>
                <label class="col-sm-2 col-form-label" for="tagDescription">Tag Description</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="text"  name="tagDescription" id="tagDescription" required>
                </div>
            </div>
        <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Create</button>
        </form>
    </div>

<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";