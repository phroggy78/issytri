
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Tags </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Tags</h2>
    <!-- Build table with list of Tags -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Tag Name</th>
            <th scope="col" class="Col"  data-sortable="true">Tag Description</th>
        </thead>
        <tbody>
            <?php foreach ($params as $tag) :?>
                <tr>
                    <td><a href="admin.php?class=Tag&action=update&id=<?=$tag->getTagId() ?>"><?=$tag->getTagId()?></a></td>
                    <td><?=Lib::cleanHtml($tag->getTagName()) ?></td>
                    <td><?=Lib::cleanHtml($tag->getTagDescription()) ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

    <a href="admin.php?class=Tag&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Create Tag</button></a>

</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";