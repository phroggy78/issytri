
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Sections </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Sections</h2>
    <!-- Build table with list of Pages and Menus -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <tr>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Section Title</th>
            <th scope="col" class="Col"  data-sortable="true">Section Order</th>
            <th scope="col" class="Col"  data-sortable="true">Page</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($params as $section) :?>
                <tr>
                    <td><a href="admin.php?class=Section&action=update&id=<?=$section['sectionId'] ?>"><?=$section['sectionId']?></a></td>
                    <td><?=$section['sectionTitle'] ?></td>
                    <td><?=$section['sectionOrder'] ?></td>
                    <td><?=$section['sectionPage'] ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    
    <a href="admin.php?class=Section&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add Section</button></a>
    
</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";