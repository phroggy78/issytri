<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create Page </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  

    <div class="container">
        <h2>Add Page</h2>
        <form action="admin.php?class=Page&action=create" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="pageTitle">Page Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="pageTitle" id="pageTitle" type="text" required>
                </div>
                <label class="col-sm-2 col-form-label" for="pageOrder">Page Order</label>
                <div class="col-sm-10">
                    <input class="form-control" type="number"  min="1" max="100" name="pageOrder" id="pageOrder" required>
                </div>
                <label class="col-sm-2 col-form-label" for="menuId">Parent Menu</label>
                <div class="col-sm-10">
                    <select name="menuId" id="menuId" required>
                    <?php foreach ($params as $menu) : ?>
                        <option value="<?=$menu['menuId'] ?>"><?=$menu['menuTitle'] ?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="pageContent">Page Content</label>
                <div class="col-sm-10">
                    <textarea name="pageContent" rows="10" cols="80" required></textarea>
                </div>
                <!-- page Tags -->
                <label class="col-sm-2 col-form-label" for="pageTags">Tags</label>
                <div class="col-sm-10">
                    <input name="pageTags" id="pageTags" type="text" data-role="tagsinput"  />
                </div>
                <!-- page images --> 
                <input type="hidden" name="objectImages" id="objectImages">
                <div id="imageGallery" class="gallery">
                </div>
                <button class="btn btn-warning" id="btnImages">Library Images</button>
            </div>
        <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Create</button>
        </form>
    </div>
<?php $content = ob_get_clean() ?>

<?php ob_start()?>
    <script> 
        // WISYWIG editor script 
        CKEDITOR.replace('pageContent', {
        height: 260,
        allowedContent: true
        });
    </script>


    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 
<?php $js = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";