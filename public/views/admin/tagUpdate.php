<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Edit Tag</title>
<?php $meta = ob_get_clean() ?>

<?php $tag=$params[0]; ?>           <!-- parameters array should only contain one item -->

<?php ob_start()?>  

    <div class="container">
        <h2>Edit Tag </h2>
        <form action="admin.php?class=Tag&action=update&id=<?=$tag->getTagId()?>" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="tagId"> Id </label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" id="tagId" name="tagId" type="text" value="<?=$tag->getTagId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="tagName">Tag Name</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="tagName" id="tagName" type="text" value="<?=Lib::cleanHtml($tag->getTagName())?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="tagDescription">Tag Description</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="text" name="tagDescription" id="tagDescription" value="<?=Lib::cleanHtml($tag->getTagDescription())?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Update</button>
        </form>

        <a href="admin.php?class=Tag&action=delete&id=<?=$tag->getTagId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a> 
        <a href="admin.php?class=Tag&action=index"><button class="btn btn-warning mt-3">List</button></a>
        <br>
    </div>

<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";