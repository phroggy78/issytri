<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update Document </title>
<?php $meta = ob_get_clean() ?>
<?php $document=$params[0]; 
      $documentTags=$params[1];    
?>

<?php ob_start()?>  
<div class="container">
    <h2>Update Document</h2>
    <form action="admin.php?class=Document&action=update&id=<?=$document->getDocumentId()?>" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="articleId"> Id </label>
            <div class="col-sm-10 mb-3">
                <input class="form-control" name="documentId" id="documentId" type="text" value="<?=$document->getDocumentId()?>" readonly>
            </div>
            <label class="col-sm-2 col-form-label" for="documentLegend">Document Legend</label>
            <div class="col-sm-10 mb-3">
                <input class="form-control" name="documentLegend" id="documentLegend" type="text" value="<?=Lib::cleanHtml($document->getDocumentLegend())?>" required>
            </div>
            <label class="col-sm-2 col-form-label" for="documentText">Document Text</label>
            <div class="col-sm-10 mb-3">
                <textarea name="documentText" rows="10" cols="80" ><?=Lib::cleanHtml($document->getDocumentText())?></textarea>
            </div>
            <label  class="col-sm-2 col-form-label" for="documentTags">Tags</label>
            <div class="col-sm-10 mb-3">
                <input name="documentTags" id="documentTags" type="text" data-role="tagsinput"  value="<?=$documentTags?>" />
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-3" name="submit">Update</button>
    </form>
    <?php if (UserController::isLoggedAdmin()) :?>
        <a href="admin.php?class=Document&action=delete&id=<?=$document->getDocumentId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a>
    <?php endif ?>
    <a href="admin.php?class=Document&action=index"><button class="btn btn-warning mt-3">List</button></a>
</div> 
<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutindex.php";