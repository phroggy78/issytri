<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create Image </title>
<?php $meta = ob_get_clean() ?>


<?php ob_start()?>  
<div class="container">
    <h2>Create Image</h2>
    <form action="admin.php?class=Image&action=create" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="imageLegend">Image Legend</label>
            <div class="col-sm-10 mb-3">
                <input class="form-control" name="imageLegend" id="imageLegend" type="text" required>
            </div>
            <label class="col-sm-2 col-form-label" for="imageText">Image Text</label>
            <div class="col-sm-10 mb-3">
                <textarea name="imageText" rows="10" cols="80" ></textarea>
            </div>
            <label class="col-sm-2 col-form-label" for="imageFile">Image file</label>
            <div class="col-sm-10 mb-3">
                <input type="file" name="imageFile" id="imageFile" required>
            </div>
            <label  class="col-sm-2 col-form-label" for="imageTags">Tags</label>
            <div class="col-sm-10 mb-3">
                <input name="imageTags" id="imageTags" type="text" data-role="tagsinput">
            </div>
        </div>

        <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Create</button>

    </form>
</div> 
<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";