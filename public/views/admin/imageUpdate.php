<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update Image </title>
<?php $meta = ob_get_clean() ?>
<?php $image=$params[0]; 
      $imageTags=$params[1];    
?>

<?php ob_start()?>  
<div class="container">
    <h2>Update Image</h2>
    <form action="admin.php?class=Image&action=update&id=<?=$image->getImageId()?>" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="articleId"> Id </label>
            <div class="col-sm-10 mb-3">
                <input class="form-control" name="imageId" id="imageId" type="text" value="<?=$image->getImageId()?>" readonly>
            </div>
            <label class="col-sm-2 col-form-label" for="imageLegend">Image Legend</label>
            <div class="col-sm-10 mb-3">
                <input class="form-control" name="imageLegend" id="imageLegend" type="text" value="<?=Lib::cleanHtml($image->getImageLegend())?>" required>
            </div>
            <label class="col-sm-2 col-form-label" for="imageText">Image Text</label>
            <div class="col-sm-10 mb-3">
                <textarea name="imageText" rows="10" cols="80" ><?=Lib::cleanHtml($image->getImageText())?></textarea>
            </div>
            <label  class="col-sm-2 col-form-label" for="imageTags">Tags</label>
            <div class="col-sm-10 mb-3">
                <input name="imageTags" id="imageTags" type="text" data-role="tagsinput"  value="<?=$imageTags?>" />
            </div>
            <label class="col-sm-2 col-form-label" for="image">Image</label>
            <div class="col-sm-10 mb-3">
                <img src="<?=$image->getImageMiniPath()?>">
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-3" name="submit">Update</button>
    </form>
    <?php if (UserController::isLoggedAdmin()) :?>
        <a href="admin.php?class=Image&action=delete&id=<?=$image->getImageId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a>
    <?php endif ?>
    <a href="admin.php?class=Image&action=index"><button class="btn btn-warning mt-3">List</button></a>
</div> 
<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutindex.php";