<?php 
/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * Render view to create a tile in the database
 * This view is called by the TileController. 
 *      Preview : it is only displayed with the page if the user is logged in with admin or editor profile, regardless of the date
 *      Active : it is displayed if within the date range
 *      Hidden : it is not displayed to users, even if within the date range
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create Tile </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
<?php $meta = ob_get_clean() ?>


<!-- Body code  --> 
<?php ob_start()?>  
    <div class="container">
        <h2>Create Tile</h2>
        <form action="admin.php?class=Tile&action=create" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="tileTitle">Tile Title</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="tileTitle" id="tileTitle" type="text" required>
                </div>
                <label class="col-sm-2 col-form-label" for="tileOrder">Tile Order</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="number"  min="1" max="100" name="tileOrder" id="tileOrder" required>
                </div>
                <label class="col-sm-2 col-form-label" for="tileImportance">Tile Category</label>
                <div class="col-sm-10 mb-3">
                    <select name="tileImportance" id="tileImportance" required>
                        <option value="0">Actualités</option>           <!-- Main default background color -->
                        <option value="1">Vie du Club</option>         <!-- Secondary background color -->
                        <option value="2">Evénementiel</option>            <!-- Danger -->
                        <option value="3">Divers</option>           <!-- Warning color -->
                    </select>
                </div>

                <label class="col-sm-2 col-form-label" for="tileContent">Tile Content - will be displayed in modal when user clicks on tile</label>
                <div class="col-sm-10 mb-3">
                    <textarea name="tileContent" rows="10" cols="80" required></textarea>
                </div>
                <!-- Copy Paste for date pickers --> 

                <label class="col-sm-2 col-form-label" for="dp1">Publication date</label>
                <div class="col-sm-4 mb-3">
                    <input id="tilePublicationDate" name="tilePublicationDate" type="date" class="form-control"> 
                </div>     
                <label class="col-sm-2 col-form-label" for="dp2">Expiry date</label>
                <div class="col-sm-4 mb-3">
                    <input id="tileExpiryDate" name="tileExpiryDate" type="date" class="form-control">
                </div>
                <label class="col-sm-2 col-form-label" for="tileStatus">Tile Status</label>
                <div class="col-sm-10 mb-3">
                    <select name="tileStatus" id="tileStatus" required>
                        <option value="0">Hidden</option>           <!-- Noone sees the article -->
                        <option value="1">Active</option>           <!-- The article is visible if the current date is within range -->
                        <option value="2">Preview</option>          <!-- the article is visible if the viewer is logged in with admin profile -->
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="tileImage">Main tile Image - displayed in tile</label>
                <div class="col-sm-10 mb-3">
                    <input type="file" name="tileImage" id="tileImage" required>
                </div>
                <!-- Additional Tile images --> 
                <input type="hidden" name="objectImages" id="objectImages">
                <div id="imageGallery" class="gallery">         <!-- div to display image library dynamically --> 
                </div>

                <button class="btn btn-warning mt-3" id="btnImages">Library Images</button>
            </div>
        <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Create</button>
        </form>
    </div>
<?php $content = ob_get_clean() ?>

<!-- Scripts to be included at end of layout --> 
<?php ob_start()?>
    <script src="jsApps/datePick.js"> // date picker script
    </script>

    <script>  // WISYWIG editor script 
        CKEDITOR.replace('tileContent', {
        height: 260,
        allowedContent: true
        });
    </script>

    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 
<?php $js = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";
