
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Menus </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Menus</h2>
    <!-- Build table with list of Menus -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Menu Title</th>
            <th scope="col" class="Col"  data-sortable="true">Menu Order</th>    
        </thead>
        <tbody>
            <?php foreach ($params as $menu) :?>
                <tr>
                    <th><a href="admin.php?class=Menu&action=update&id=<?=$menu->getMenuId() ?>"><?=$menu->getMenuId()?></a></th>
                    <td><?=Lib::cleanHtml($menu->getMenuTitle()) ?></td>
                    <td><?=$menu->getMenuOrder() ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <a href="admin.php?class=Menu&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Create Menu</button></a>
</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";