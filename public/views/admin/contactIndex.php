<?php 

/**
 * Author : Philippe Roggeband - April 2020
 * Version 1.0
 * Change History : 
 * 
 * Admin page to display / manage contact requests
 * 
 */
?>
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - contact Requests </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?> 
<h2>Requests received</h2>

<?php $contacts=$params['contacts']; ?>

<div class="container">
    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <th class="Col"  data-sortable="true">Id</th>
            <th class="Col"  data-sortable="true">Date</th>
            <th class="Col"  data-sortable="true">Firstname</th>
            <th class="Col"  data-sortable="true">Lastname</th>
            <th class="Col"  data-sortable="true">email</th>
            <th class="Col"  data-sortable="true">status</th>
        </thead>
        <tbody>
            <?php foreach ($contacts as $contact) :?>
                <tr>
                    <td><a href="admin.php?class=Contact&action=update&id=<?=$contact->getContactId()?>"><?=$contact->getContactId()?></td>
                    <td><?=$contact->getContactDateCreated() ?></td> 
                    <td><?=Lib::cleanHtml($contact->getContactFirstName())?></td>
                    <td><?=Lib::cleanHtml($contact->getContactLastName())?></td>
                    <td><?=Lib::cleanHtml($contact->getContactEmail())?></td>
                    <td>
                        <?php if ($contact->getContactStatus()) echo("Open"); else echo("Closed"); ?>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>


<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";