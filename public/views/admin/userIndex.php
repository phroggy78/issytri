
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Users </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Webmaster users</h2>
    <!-- Build table with list of users -->

    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">First name</th>
            <th scope="col">Last Name</th>
            <th scope="col">email</th>
            <th class="scope">role</th>
            </tr>
        </thead>
    <?php foreach ($params as $user) :?>
        <tr>
            <th><a href="admin.php?class=User&action=update&id=<?=$user->getUserId() ?>"><?=$user->getUserId()?></a></th>
            <td><?=Lib::cleanHtml($user->getFirstName()) ?></td>
            <td><?=Lib::cleanHtml($user->getLastName()) ?></td>
            <td><?=Lib::cleanHtml($user->getEmail()) ?></td>
            <td><?=Lib::cleanHtml($user->getRole())?> </td>
        </tr>
    <?php endforeach ?>
    </table>

    <a href="admin.php?class=User&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add user</button></a>
</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";
