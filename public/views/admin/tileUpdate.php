<?php 
/**
 * Author : Philippe Roggeband - February 2020
 * Version 1.0
 * Change History :
 * 
 * Render view to create a tile in the database
 * This view is called by the TileController. 
 *      Preview : it is only displayed with the page if the user is logged in with admin or editor profile, regardless of the date
 *      Active : it is displayed if within the date range
 *      Hidden : it is not displayed to users, even if within the date range
 * 
 */
?>

<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update Tile </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
<?php $meta = ob_get_clean() ?>

<?php 
// Unpack display parameters
    $tile=$params['tile'];
    $tileImages=$params['tileImages'];
    $imagesList=[];
    // get a list of image Id's to pass to JavaScript
    foreach ($tileImages as $tileImage) {
        array_push($imagesList,$tileImage[0]);
    }
    $imagesStr=implode(",",$imagesList);
?>

<!-- Body code  --> 
<?php ob_start()?>  
    <div class="container">
        <h2>Update Tile</h2>
        <form action="admin.php?class=Tile&action=update&id=<?=$tile->getTileId()?>" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="tileId"> Id </label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="tileId" id="tileId" type="text" value="<?=$tile->getTileId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="tileTitle">Tile Title</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="tileTitle" id="tileTitle" type="text" required value="<?=Lib::cleanHtml($tile->getTileTitle())?>">
                </div>
                <label class="col-sm-2 col-form-label" for="tileOrder">Tile Order</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="number"  min="1" max="100" name="tileOrder" id="tileOrder" required value=<?=$tile->getTileOrder()?>>
                </div>
                <label class="col-sm-2 col-form-label" for="tileImportance">Tile catégory</label>
                <div class="col-sm-10 mb-3">
                    <select name="tileImportance" id="tileImportance" required>
                        <option value="0" <?php if ($tile->getTileImportance()==0) echo "selected" ?>>Actualités</option>           <!-- Main default background color -->
                        <option value="1" <?php if ($tile->getTileImportance()==1) echo "selected" ?>>Vie du club</option>         <!-- Secondary background color -->
                        <option value="2" <?php if ($tile->getTileImportance()==2) echo "selected" ?>>Evénementiel</option>            <!-- Danger -->
                        <option value="3" <?php if ($tile->getTileImportance()==3) echo "selected" ?>>Divers</option>           <!-- Warning color -->
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="tileContent">Tile Content - will be displayed in modal when user clicks on tile</label>
                <div class="col-sm-10 mb-3">
                    <textarea name="tileContent" id="tileContent" rows="10" cols="80" required><?=Lib::cleanHtml($tile->getTileContent())?></textarea>
                </div>
                <!-- Copy Paste for date pickers --> 
                <label class="col-sm-2 col-form-label" for="dp1">Publication date</label>
                <div class="col-sm-4 mb-3">
                    <input id="tilePublicationDate" name="tilePublicationDate" type="date" class="form-control" value="<?=$tile->getTilePublicationDate()?>">      
                </div>
                <label class="col-sm-2 col-form-label" for="dp2">Expiry date</label>
                <div class="col-sm-4 mb-3">
                    <input id="tileExpiryDate" name="tileExpiryDate" type="date" class="form-control" value="<?=$tile->getTileExpiryDate()?>">
                </div>
                <label class="col-sm-2 col-form-label" for="tileStatus">Tile Status</label>
                <div class="col-sm-10 mb-3">
                    <select name="tileStatus" id="tileStatus" required>
                        <option value="0" <?php if ($tile->getTileStatus()==0) echo "selected" ?>>Hidden</option>           <!-- Noone sees the tile -->
                        <option value="1" <?php if ($tile->getTileStatus()==1) echo "selected" ?>>Active</option>           <!-- The tile is visible if the current date is within range -->
                        <option value="2" <?php if ($tile->getTileStatus()==2) echo "selected" ?>>Preview</option>          <!-- the tile is visible if the viewer is logged in with admin profile -->
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="tileImage">Main tile Image - displayed in tile</label>
                <div class="col-sm-10 mb-3">
                    <img src="<?=$tile->getTileImage()?>" class="img-fluid img-thumbnail">
                    <input type="file" name="tileImage" id="tileImage">
                </div>
                <!-- Additional Tile images --> 
                <input type="hidden" name="objectImages" id="objectImages" value="<?=$imagesStr?>">
                <?php foreach ($tileImages as $tileImage) :?>
                    <img src="<?=$tileImage[1]?>" class="selected miniature" data-imageId="<?=$tileImage[0]?>">
                <?php endforeach ?>
                <div id="imageGallery" class="gallery">         <!-- div to display image library dynamically --> 
                </div>

                <button class="btn btn-warning" id="btnImages mt-3">Library Images</button>
            </div>
            <button type="submit" class="btn btn-primary mt-3" name="submit">Update</button>
        </form>
        <a href="admin.php?class=Tile&action=delete&id=<?=$tile->getTileId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3 mb-3">Delete</button></a>
        <a href="admin.php?class=Tile&action=index"><button class="btn btn-warning mt-3 mb-3">List</button></a>
    </div>
<?php $content = ob_get_clean() ?>

<!-- Scripts to be included at end of layout --> 
<?php ob_start()?>

    <script>  // WISYWIG editor script 
        CKEDITOR.replace('tileContent', {
        height: 260,
        allowedContent: true
        });
    </script>

    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 
<?php $js = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";
