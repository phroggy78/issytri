
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Documents </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Documents</h2>
    <!-- Build table with list of Documents -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <tr>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Document Legend</th>
            <th scope="col" class="Col"  data-sortable="true">Document Path</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($params as $document) :?>
                <tr>
                    <td><a href="admin.php?class=Document&action=update&id=<?=$document['documentId'] ?>"><?=$document['documentId']?></a></td>
                    <td><?=$document['documentLegend'] ?></td>
                    <td><?=$document['documentPath'] ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>

    </table>

    <a href="admin.php?class=Document&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add Document</button></a>

</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";