<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create Document </title>
<?php $meta = ob_get_clean() ?>


<?php ob_start()?>  
<div class="container">
    <h2>Create Document</h2>
    <form action="admin.php?class=Document&action=create" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="documentLegend">Document Legend</label>
            <div class="col-sm-10 mb-3">
                <input class="form-control" name="documentLegend" id="documentLegend" type="text" required>
            </div>
            <label class="col-sm-2 col-form-label" for="documentText">Document Text</label>
            <div class="col-sm-10 mb-3">
                <textarea name="documentText" rows="10" cols="80" ></textarea>
            </div>
            <label class="col-sm-2 col-form-label" for="documentFile">Document file</label>
            <div class="col-sm-10 mb-3">
                <input type="file" name="documentFile" id="documentFile" required>
            </div>
            <label  class="col-sm-2 col-form-label" for="documentTags">Tags</label>
            <div class="col-sm-10 mb-3">
                <input name="documentTags" id="documentTags" type="text" data-role="tagsinput">
            </div>
        </div>

        <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Create</button>

    </form>
</div> 
<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";