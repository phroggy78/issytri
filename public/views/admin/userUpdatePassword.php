<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update User Password</title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
        <h2>Update Password</h2>
        <form action="admin.php?class=User&action=updatePassword" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="User Email"> Id </label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="text" value="<?=$_SESSION['email']?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="oldPassword">Enter old Password</label>
                <div class="col-sm-10 mb-3">
                    <input type="password" class="form-control" id="oldPassword" name="oldPassword" required>
                </div>
                <label class="col-sm-2 col-form-label" for="newPassword">Enter new Password</label> 
                <div class="col-sm-10 mb-3">
                    <input type="password" class="form-control" id="newPassword" name="newPassword" required>
                </div>
                <label class="col-sm-2 col-form-label" for="newPasswordCheck">Re-enter new Password Check</label>
                <div class="col-sm-10 mb-3">
                    <input type="password" class="form-control" id="newPasswordCheck" name="newPasswordCheck" required>
                </div>
            </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Update</button>
        </form>
        <a href="admin.php"><button class="btn btn-warning mt-3 mb-3">Cancel</button></a>
<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";