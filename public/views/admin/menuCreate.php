<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Add Menu </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  

    <div class="container">
        <h2>Add menu</h2>
        <form action="admin.php?class=Menu&action=create" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="menuTitle">Menu Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="menuTitle" id="menuTitle" type="text" required>
                </div>
                <label class="col-sm-2 col-form-label" for="menuOrder">Menu Order</label>
                <div class="col-sm-10">
                    <input class="form-control" type="number"  min="1" max="100" name="menuOrder" id="menuOrder">
                </div>
            </div>
        <button type="submit" class="btn btn-primary" name="submit">create</button>
        </form>
    </div>

<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";