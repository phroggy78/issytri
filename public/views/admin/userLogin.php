<?php ob_start()?>   
<!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Admin login </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>   

<div class="container">
    <h2>Admin Login</h2>
    <form action="admin.php?class=User&action=login" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="email">Email</label>
            <div class="col-sm-10 mb-3">
                <input type="email" name="email" id="email" required>
            </div>
            <label class="col-sm-2 col-form-label" for="password">Password</label>
            <div class="col-sm-10 mb-3">
                <input type="password" name="password" id ="password"required>
            </div>
            <button type="submit" class="btn btn-primary mt-3 mb-3">Login</button>
        </div>
    </form>
</div>



<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";