
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Articles </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Articles</h2>
    <!-- Build table with list of Pages and Menus -->
    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <tr>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Article Title</th>
            <th scope="col" class="Col"  data-sortable="true">Article Order</th>
            <th scope="col" class="Col"  data-sortable="true">Parent Section</th>
            <th scope="col" class="Col"  data-sortable="true">Parent Page</th>
            <th scope="col" class="Col"  data-sortable="true">Parent Menu</th>
            <th scope="col" class="Col"  data-sortable="true">Status</th>
            <th scope="col" class="Col"  data-sortable="true">Publication Date</th>
            <th scope="col" class="Col"  data-sortable="true">Expiry date</th>
            </tr>
        </thead>
    <?php foreach ($params as $article) :?>
        <tr>
            <th><a href="admin.php?class=Article&action=update&id=<?=$article['articleId'] ?>"><?=$article['articleId']?></a></th>
            <td><?=$article['articleTitle'] ?></td>
            <td><?=$article['articleOrder'] ?></td>
            <td><?=$article['articleSection']?></td>
            <td><?=$article['articlePage']?></td>
            <td><?=$article['articleMenu']?></td>
            <?php 
                $articleStatus="";
                switch ($article['articleStatus']) {
                    case 1 :
                        $articleStatus="Active";
                        break;
                    case 0 : 
                        $articleStatus="Hidden";
                        break ;
                    case 2 : 
                        $articleStatus="Preview";
                        break;
                }
            ?>
            <td><?=$articleStatus?>
            </td>
            <td><?=$article['articlePublicationDate']?></td>
            <td><?=$article['articleExpiryDate']?></td>
        </tr>
    <?php endforeach ?>
    </table>

    <a href="admin.php?class=Article&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add Article</button></a>

</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";