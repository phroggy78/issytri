<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Administration du site de Issy Triathlon 3.0</title>
<?php $meta = ob_get_clean() ?>


<?php ob_start() ?> 
<div class="container">
    <?php if (UserController::isLoggedAdmin()) :?>
        <h2>Administration tools</h2>
        <ul class="list-group">
            <li class="list-group-item"><a href="admin.php?class=Contact"> Contacts</a></li>
            <li class="list-group-item"><a href="admin.php?class=User"> Users</a></li>
            <li class="list-group-item"><a href="admin.php?class=Menu"> Menus</a></li>
            <li class="list-group-item"><a href="admin.php?class=Page"> Pages</a></li>
            <li class="list-group-item"><a href="admin.php?class=Section"> Sections</a></li>
            <li class="list-group-item"><a href="admin.php?class=Article"> Articles</a></li>
            <li class="list-group-item"><a href="admin.php?class=Image"> Images</a></li>
            <li class="list-group-item"><a href="admin.php?class=Tag"> Tags</a></li>
            <li class="list-group-item"><a href="admin.php?class=Tile"> Tiles</a></li>
            <li class="list-group-item"><a href="admin.php?class=User&action=updatePassword"> Reset password</a></li>
        </ul>

    <?php elseif (UserController::isLoggedEditor()) :?>
        <h2>Editor tools</h2>
        <ul class="list-group">
        <li class="list-group-item"><a href="admin.php?class=Page"> Pages</a></li>
            <li class="list-group-item"><a href="admin.php?class=Section"> Sections</a></li>
            <li class="list-group-item"><a href="admin.php?class=Article"> Articles</a></li>
            <li class="list-group-item"><a href="admin.php?class=Image"> Images</a></li>
            <li class="list-group-item"><a href="admin.php?class=Tile"> Tiles</a></li>
            <li class="list-group-item"><a href="admin.php?class=User&action=updatePassword"> Reset password</a></li>
        </ul>
    <?php endif ?>
</div>

<?php $content = ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";
