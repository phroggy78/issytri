<?php 

/**
 * Author: Philippe Roggeband April 2020
 * Version 1.0
 * 
 * Form to update the status of a contact request
 * 
 */

 $contact=$params['contact'];

 ?>

 <?php ob_start() ?>
    <h2>Update Contact Request</h2>
    <form action="admin.php?class=Contact&action=update&id=<?=$contact->getContactId()?>" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="contactId">Contact Id</label>
            <div class="col-sm-10 mb-3">
                <input type="text" class="form-control" name="contactId" id="contactId" value="<?=$contact->getContactId()?>" readonly>
            </div>
            <label class="col-sm-2 col-form-label" for="lastName">Nom</label>
            <div class="col-sm-10 mb-3">
                <input type="text" class="form-control" name="lastName" id="lastName" value="<?= Lib::cleanHtml($contact->getContactLastName())?>" readonly>
            </div>
            <label class="col-sm-2 col-form-label" for="firstName">Prénom</label>
            <div class="col-sm-10 mb-3">
                <input type="text" class="form-control" name="firstName" id="firstName" value="<?= Lib::cleanHtml($contact->getContactFirstName())?>" readonly>
            </div>
            <label class="col-sm-2 col-form-label" for="email">Adresse Mail</label>
            <div class="col-sm-10 mb-3">
                <input type="email" class="form-control" name="email" id="email" value="<?=Lib::cleanHtml($contact->getContactEmail())?>" readonly>
            </div>
            <label class="col-sm-2 col-form-label" for="message">Message</label>
            <div class="col-sm-10 mb-3">
                <textarea name="message" id="message" cols="35" class="form-control" rows="10" readonly><?=Lib::cleanHtml($contact->getContactMessage())?></textarea>
            </div>
            <label class="col-sm-2 col-form-label" for="contactStatus">Status</label>
            <div class="col-sm-10 mb-3">
            <select name="contactStatus" id="contactStatus">
                    <option value="0" <?php if($contact->getContactStatus()==0) echo "selected" ?>>Closed</option>
                    <option value="1" <?php if($contact->getContactStatus()==1) echo "selected" ?>>Pending</option>
                </select>
            </div>
        </div> <!-- /.form-group -->
        <button type="submit" class="btn btn-primary mt-3" name="submit">Update</button>
    </form>
    <a href="admin.php?class=Contact&action=delete&id=<?=$contact->getContactId() ?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a>
    <a href="admin.php?class=Contact&action=index"><button class="btn btn-warning mt-3">List</button></a>

<?php $content=ob_get_clean(); ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";

