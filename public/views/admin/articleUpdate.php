<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update Article </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
<?php $meta = ob_get_clean() ?>

<?php 
    // retrieve input parameters : current article, list of sections, list of attached tags
    $article=$params['article'];                    
    $sections=$params['sections'];
    $articleTags=$params['articleTags'];
    $articleImages=$params['articleImages'];
    $imagesList=[];
    // get a list of image Id's to pass to JavaScript
    foreach ($articleImages as $articleImage) {
        array_push($imagesList,$articleImage[0]);
    }
    $imagesStr=implode(",",$imagesList);
?>

<?php ob_start()?>  
    <div class="container">
        <h2>Update Article</h2>
        <form action="admin.php?class=Article&action=update&id=<?=$article->getArticleId()?>" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="articleId"> Id </label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="articleId" id="articleId" type="text" value="<?=$article->getArticleId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="articleTitle">Article Title</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="articleTitle" id="articleTitle" type="text" value="<?=Lib::cleanHtml($article->getArticleTitle())?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="articleOrder">Article Order</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="number"  min="1" max="100" name="articleOrder" id="articleOrder" value="<?=$article->getArticleOrder()?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="articleSectionId">Parent Section</label>
                <div class="col-sm-10 mb-3">
                <select name="articleSectionId" id="articleSectionId" required>
                    <?php foreach ($sections as $section) : ?>
                        <option value="<?=$section['sectionId'] ?>" <?php if($section['sectionId'] == $article->getArticleSectionId()) echo "selected" ?>><?=$section['sectionTitle'] ?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="articleContent">Article Content</label>
                <div class="col-sm-10 mb-3">
                    <textarea name="articleContent" rows="10" cols="80" required><?=Lib::cleanHtml($article->getArticleContent())?></textarea>
                </div>
                <!-- Copy Paste for date pickers --> 

                <label class="col-sm-2 col-form-label" for="dp1">Publication date</label>
                <div class="col-sm-4 mb-3">
                    <input id="articlePublicationDate" name="articlePublicationDate" type="date" class="form-control" value="<?=$article->getArticlePublicationDate()?>">      
                </div>
                <label class="col-sm-2 col-form-label" for="dp2">Expiry date</label>
                <div class="col-sm-4 mb-3">
                    <input id="articleExpiryDate" name="articleExpiryDate" type="date" class="form-control" value="<?=$article->getArticleExpiryDate()?>">
                </div>
                <label class="col-sm-2 col-form-label" for="articleStatus">Article Status</label>
                <div class="col-sm-10 mb-3">
                    <select name="articleStatus" id="articleStatus" required>
                        <option value="0" <?php if ($article->getArticleStatus()==0) echo "selected" ?>>Hidden</option>
                        <option value="1" <?php if ($article->getArticleStatus()==1) echo "selected" ?>>Active</option>
                        <option value="2" <?php if ($article->getArticleStatus()==2) echo "selected" ?>>Preview</option>
                    </select>
                </div>
                <!-- Article Tags -->
                <label class="col-sm-2 col-form-label"  for="articleTags">Tags</label>
                <div class="col-sm-10 mb-3">
                    <input name="articleTags" id="articleTags" type="text" data-role="tagsinput"  value="<?=$articleTags?>" />
                </div>
                <!-- Article Images -->
                <input type="hidden" name="objectImages" id="objectImages" value="<?=$imagesStr?>">
                <?php foreach ($articleImages as $articleImage) :?>
                    <img src="<?=$articleImage[1]?>" class="selected miniature" data-imageId="<?=$articleImage[0]?>">
                <?php endforeach ?>
                <div id="imageGallery" class="gallery"></div>

                <button class="btn btn-warning mt-3" id="btnImages">Library Images</button>
            </div>
        <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Update</button>

        </form>
        <a href="admin.php?class=Article&action=delete&id=<?=$article->getArticleId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a>
        <a href="admin.php?class=Article&action=index"><button class="btn btn-warning mt-3">List</button></a>
    </div>
<?php $content = ob_get_clean() ?>

<?php ob_start()?>
     <script> 
        // WISYWIG editor script 
        CKEDITOR.replace('articleContent', {
            height: 260,
            allowedContent: true
        });
    </script>
    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 
<?php $js = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";