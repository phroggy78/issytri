<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update Section </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
<?php $meta = ob_get_clean() ?>

<?php 
    // retrieve input parameters
    $section=$params['section'];
    $pages=$params['pageTitles'];
    $sectionTags=$params['sectionTagsList'];
    $sectionImages=$params['sectionImages'];
    $imagesList=[];
    // get a list of image Id's to pass to JavaScript
    foreach ($sectionImages as $sectionImage) {
        array_push($imagesList,$sectionImage[0]);
    }
    $imagesStr=implode(",",$imagesList);
?>
<?php ob_start()?>  

    <div class="container">
        <h2>Update Section</h2>
        <form action="admin.php?class=Section&action=update&id=<?=$section->getSectionId() ?>" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="sectionId"> Id </label>
                <div class="col-sm-10">
                    <input class="form-control" name="sectionId" id="sectionId" type="text" value="<?=$section->getSectionId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="sectionTitle">Section Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="sectionTitle" id="sectionTitle" type="text" value="<?=Lib::cleanHtml($section->getSectionTitle()) ?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="sectionOrder">Section Order</label>
                <div class="col-sm-10">
                    <input class="form-control" type="number"  min="1" max="100" name="sectionOrder" id="sectionOrder" value="<?=$section->getSectionOrder() ?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="sectionLayout">Section Layout</label>
                <div class="col-sm-10">
                    <select name="sectionLayout" id="sectionLayout" required>
                            <option value="0"  <?php if ($section->getSectionLayout()==0) echo "selected" ?>>Wide</option>             <!-- Full-width image with text overlay -->
                            <option value="1"  <?php if ($section->getSectionLayout()==1) echo "selected" ?>>L: Text - R: Image</option>     <!-- Text is in left column, Image is on right -->
                            <option value="2"  <?php if ($section->getSectionLayout()==2) echo "selected" ?>>Image text text </option> 
                            <option value="3"  <?php if ($section->getSectionLayout()==3) echo "selected" ?>>L: Text - Articles</option>
                            <option value="4"  <?php if ($section->getSectionLayout()==4) echo "selected" ?>>Text top, articles below, round</option>
                            <option value="5"  <?php if ($section->getSectionLayout()==5) echo "selected" ?>>Text top, articles below, square</option>
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="pageId">Parent Page</label>
                <div class="col-sm-10">
                    <select name="pageId" id="pageId" required>
                    <?php foreach ($pages as $page) : ?>
                        <option value="<?=$page['pageId'] ?>" <?php if($page['pageId'] == $section->getSectionPageId()) echo "selected" ?>><?=$page['pageTitle'] ?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="sectionContent">Section Content</label>
                <div class="col-sm-10">
                <textarea name="sectionContent" rows="10" cols="80"  required><?=Lib::cleanHtml($section->getSectionContent())?></textarea>
                </div>
                <!-- section Tags -->
                <label class="col-sm-2 col-form-label" for="sectionTags">Tags</label>
                <div class="col-sm-10">
                    <input name="sectionTags" id="sectionTags" type="text" data-role="tagsinput"  value="<?=$sectionTags?>" />
                </div>
                <!-- section Images -->
                <input type="hidden" name="objectImages" id="objectImages" value="<?=$imagesStr?>">

                <label class="col-sm-2 col-form-label" for="imageGallery"> Images</label>
                <?php foreach ($sectionImages as $sectionImage) :?>
                    <img src="<?=$sectionImage[1]?>" class="selected miniature" data-imageId="<?=$sectionImage[0]?>">
                <?php endforeach ?>
                <div id="imageGallery" class="gallery">
                </div>
                <div class="col-sm-12">
                    <button class="btn btn-warning" id="btnImages">Library Images</button>
                </div>
            </div>
        <button type="submit" class="btn btn-primary mt-3" name="submit">Update</button>
        </form>
            <a href="admin.php?class=Section&action=delete&id=<?=$section->getSectionId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3">Delete</button></a>
            <a href="admin.php?class=Section&action=index"><button class="btn btn-warning mt-3">List</button></a>
    </div>
<?php $content = ob_get_clean() ?>

<?php ob_start()?>
    <script> 
        // WISYWIG editor script 
        CKEDITOR.replace('sectionContent', {
        height: 260,
        allowedContent: true
        });
    </script>

    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 

<?php $js = ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";