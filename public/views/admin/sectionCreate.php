<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create Section </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>

<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  

    <div class="container">
        <h2>Create Section</h2>
        <form action="admin.php?class=Section&action=create" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label"  for="sectionTitle">Section Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="sectionTitle" id="sectionTitle" type="text" required>
                </div>
                <label class="col-sm-2 col-form-label" for="sectionOrder">Section Order</label>
                <div class="col-sm-10">
                    <input class="form-control" type="number"  min="1" max="100" name="sectionOrder" id="sectionOrder" required>
                </div>
                <label class="col-sm-2 col-form-label" for="sectionLayout">Section Layout</label>
                <div class="col-sm-10">
                    <select name="sectionLayout" id="sectionLayout" required>
                            <option value="0">Wide</option>             <!-- Full-width image with text overlay -->
                            <option value="1">L: Text - R: Image</option>     <!-- Text is in left column, Image is on right -->
                            <option value="2">Image text text </option> 
                            <option value="3">L: Text - Articles</option>
                            <option value="4">Text top, articles below, round</option>
                            <option value="5">Text top, articles below, square</option>
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="pageId">Parent Page</label>
                <div class="col-sm-10">
                    <select name="pageId" id="pageId" required>
                    <?php foreach ($params as $page) : ?>
                        <option value="<?=$page['pageId'] ?>"><?=$page['pageTitle'] ?></option>
                    <?php endforeach ?>
                    </select>
                </div>
            
                <label class="col-sm-2 col-form-label" for="sectionContent">Section Content</label>
                <div class="col-sm-10">
                    <textarea name="sectionContent" id="sectionContent" rows="10" cols="80" required></textarea>
                </div>
                <!-- Section Tags -->
                <label class="col-sm-2 col-form-label"  for="sectionTags">Tags</label>
                <div class="col-sm-10">
                    <input name="sectionTags" id="sectionTags" type="text" data-role="tagsinput"  />
                </div>
                <!-- Section images --> 

                <label class="col-sm-2 col-form-label" for="objectImages">Images</label>
                <input type="hidden" name="objectImages" id="objectImages">
                <div id="imageGallery" class="gallery col-sm-10">         <!-- div to display image library dynamically --> 
                </div>
                <div class="col-sm-12">
                    <button class="btn btn-warning" id="btnImages">Library Images</button>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Create</button>
        </form>
    </div>

<?php $content = ob_get_clean() ?>

<?php ob_start()?>
    <script> 
        // WISYWIG editor script 
        CKEDITOR.replace('sectionContent', {
        height: 260,
        allowedContent: true
        });
    </script>
    <!--  Script to tie images to an object  -->
    <script src="jsApps/addImages.js"> </script> 

<?php $js = ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";