
<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Pages </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  
<div class="container">
    <h2>Issy Triathlon - Pages</h2>
    <!-- Build table with list of Pages and Menus -->

    <table class="table" data-toggle="table" data-pagination="true"  data-search="true">
        <thead>
            <tr>
            <th scope="col" class="Col"  data-sortable="true">#</th>
            <th scope="col" class="Col"  data-sortable="true">Page Title</th>
            <th scope="col" class="Col"  data-sortable="true">Page Order</th>
            <th scope="col" class="Col"  data-sortable="true">Menu</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($params as $page) :?>
                <tr>
                    <td><a href="admin.php?class=Page&action=update&id=<?=$page['pageId'] ?>"><?=$page['pageId']?></a></td>
                    <td><?=$page['pageTitle'] ?></td>
                    <td><?=$page['pageOrder'] ?></td>
                    <td><?=$page['pageMenu'] ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>

    </table>

    <a href="admin.php?class=Page&action=create"><button type="button" class="btn btn-primary mt-3 mb-3">Add Page</button></a>
</div>
<?php $content=ob_get_clean() ?>
<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";