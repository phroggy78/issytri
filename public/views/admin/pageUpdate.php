<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Update Page </title>
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
<?php $meta = ob_get_clean() ?>

<?php 
    // retrieve input parameters
    $page=$params['pageId'];
    $menus=$params['menuTitles'];
    $pageTags=$params['pageTags'];
    $pageImages=$params['pageImages'];
    $imagesList=[];
    // get a list of image Id's to pass to JavaScript
    foreach ($pageImages as $pageImage) {
        array_push($imagesList,$pageImage[0]);
    }
    $imagesStr=implode(",",$imagesList);
?>

<?php ob_start()?>  

    <div class="container">
        <h2>Update Page</h2>
        <form action="admin.php?class=Page&action=update&id=<?=$page->getPageId() ?>" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="pageId"> Id </label>
                <div class="col-sm-10">
                    <input class="form-control" id="pageId" name="pageId" type="text" value="<?=$page->getPageId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="pageTitle">Page Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="pageTitle" id="pageTitle" type="text" value='<?=Lib::cleanHtml($page->getPageTitle()) ?>' required>
                </div>
                <label class="col-sm-2 col-form-label" for="pageOrder">Page Order</label>
                <div class="col-sm-10">
                    <input class="form-control" type="number"  min="1" max="100" name="pageOrder" id="pageOrder" value="<?=$page->getPageOrder() ?>" required>
                </div>
                <label class="col-sm-2 col-form-label" for="menuId">Parent Menu</label>
                <div class="col-sm-10">
                    <select name="menuId" id="menuId" required>
                    <?php foreach ($menus as $menu) : ?>
                        <option value="<?=$menu['menuId'] ?>" <?php if($menu['menuId'] == $page->getPageMenuId()) echo "selected" ?>><?=$menu['menuTitle'] ?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <label class="col-sm-2 col-form-label" for="pageContent">Page Content</label>
                <div class="col-sm-10">
                    <textarea name="pageContent" rows="10" cols="80"  required><?=Lib::cleanHtml($page->getPageContent())?></textarea>
                </div>
                <label class="col-sm-2 col-form-label" for="pageTags">Tags</label>
                <div class="col-sm-10">
                    <input name="pageTags" id="pageTags" type="text" data-role="tagsinput"  value="<?=$pageTags?>" />
                </div>
                <input type="hidden" name="objectImages" id="objectImages" value="<?=$imagesStr?>">

                <label class="col-sm-2 col-form-label" for="imageGallery">Images</label>
                <?php foreach ($pageImages as $pageImage) :?>
                    <img src="<?=$pageImage[1]?>" class="selected miniature" data-imageId="<?=$pageImage[0]?>">
                <?php endforeach ?>
                <div id="imageGallery" class="gallery col-sm-10">

                </div>
                <div class="col-sm-12">
                <button class="btn btn-warning" id="btnImages">Library Images</button>
                </div>

             </div>
        <button type="submit" class="btn btn-primary" name="submit">Update Page</button>
        </form>
        <a href="admin.php?class=Page&action=delete&id=<?=$page->getPageId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger mt-3 mb-3">Delete</button></a>
        <a href="admin.php?class=Page&action=index"><button class="btn btn-primary mt-3 mb-3">List</button></a>
        <a href="admin.php?class=Image&action=create" target="_blank"><button class="btn btn-primary mt-3 mb-3">Add image to library</button></a>
    </div>
<?php $content = ob_get_clean() ?>

<?php ob_start()?>
    <script> 
        // WISYWIG editor script 
        CKEDITOR.replace('pageContent', {
        height: 260 ,
        allowedContent: true
        });
    </script>

    <!--  Script to tie images to an object  -->

    <script src="jsApps/addImages.js"> </script> 

<?php $js = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";