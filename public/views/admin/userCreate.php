<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Create User profile </title>
<?php $meta = ob_get_clean() ?>

<?php ob_start()?>  

    <div class="container">
        <h2>Create User Profile</h2>
        <form action="admin.php?class=User&action=create" method="post">
                <div class="form-group row ">
                    <label class="col-sm-2 col-form-label" for="firstName">First Name</label>
                    <div class="col-sm-10 mb-3">
                        <input class="form-control" name="firstName" id="firstName" type="text">
                    </div>
                    <label class="col-sm-2 col-form-label" for="lastName">Last Name</label>
                    <div class="col-sm-10 mb-3">
                        <input class="form-control" type="text" name="lastName" id="lastName">
                    </div>
                    <label class="col-sm-2 col-form-label" for="userEmail">Email address</label>
                    <div class="col-sm-10 mb-3">
                        <input type="email" class="form-control" id="userEmail" name="userEmail">
                    </div>
                    <label class="col-sm-2 col-form-label" for="userPassword">Password</label>
                    <div class="col-sm-10 mb-3">
                        <input type="password" class="form-control" id="userPassword" name="userPassword">
                    </div>
                    <label class="col-sm-2 col-form-label" for="role">Role</label>
                    <div class="col-sm-10 mb-3">
                        <input type="text" class="form-control" id="role" name="role" value="Admin">
                    </div>
                </div>
            <button type="submit" class="btn btn-primary" name="submit">Create</button>
        </form>
    </div>

<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";