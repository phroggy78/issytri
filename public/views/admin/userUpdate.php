<?php ob_start()?>   <!-- Build $meta variable to hold file title --> 
    <title>Admin Issy Triathlon 3.0 - Edit User profile </title>
<?php $meta = ob_get_clean() ?>

<?php $user=$params[0]; ?>           <!-- parameters array should only contain one item -->

<?php ob_start()?>  

    <div class="container">
        <h2>Edit User Profile</h2>
        <form action="admin.php?class=User&action=update&id=<?=$user->getUserId()?>" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="userId"> Id </label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" id="userId" name="userId" type="text" value="<?=$user->getUserId()?>" readonly>
                </div>
                <label class="col-sm-2 col-form-label" for="firstName">First Name</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" name="firstName" id="firstName" type="text" value="<?=Lib::cleanHtml($user->getFirstName())?>">
                </div>
                <label class="col-sm-2 col-form-label" for="lastName">Last Name</label>
                <div class="col-sm-10 mb-3">
                    <input class="form-control" type="text" name="lastName" id="lastName" value="<?=Lib::cleanHtml($user->getLastName())?>">
                </div>
                <label class="col-sm-2 col-form-label" for="userEmail">Email address</label>
                <div class="col-sm-10 mb-3">
                    <input type="email" class="form-control" id="userEmail" name="userEmail" value="<?=Lib::cleanHtml($user->getEmail())?>">
                </div>
                <label class="col-sm-2 col-form-label" for="userPassword">Password</label>
                <div class="col-sm-10 mb-3">
                    <input type="password" class="form-control" id="userPassword" name="userPassword" value="************">
                </div>
                <label class="col-sm-2 col-form-label" for="role">Role</label>
                <div class="col-sm-10 mb-3">
                    <input type="text" class="form-control" id="role" name="role" value="<?=Lib::cleanHtml($user->getRole())?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3 mb-3" name="submit">Update</button>
        </form>
        <!-- Add buttons for Delete & List functions -->
        <a href="admin.php?class=User&action=delete&id=<?=$user->getUserId()?>" onclick="return confirm('Are you sure?');"><button class="btn btn-danger">Delete</button></a>
        <a href="admin.php?class=User&action=index"><button class="btn btn-warning">List</button></a>
    </div>
    <br>
<?php $content = ob_get_clean() ?>

<?php 
// Call layout to display view in common format
require "views/layoutIndex.php";