<!-- 

    Author : Philippe Roggeband
    V1.0 - July 2020
    Changes History - 

    Layout file used for the home page of the web site, called by a "require" at the end of all pages
    Features : 

    - Call all dependencies (JS, CSS)
    - Build responsive navigation bar
    - Build the "tiles" which are used to display current highlighted content
    - Display the following sets of content : 
        $meta : page-specific elements to be included in file <head>
        $banner : page banner
        $content : page content 
        $js : page-specific javascript code 

-->
<?php 
    // Object to display any pending one-time flash messages
    $flash= new SessionFlash();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <!-- Bootstrap core CSS  and Jquery -->
        
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- CSS file and JS script for tags plugin --> 
    <link href="css/tagsinput.css" rel="stylesheet" type="text/css">
    <script src="jsApps/tagsinput.js"></script>

    <!-- CSS file and JS scripts for cookies consent -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.1/cookieconsent.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.1/cookieconsent.min.js"></script>

    <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#000",
          "text": "#33bcf0"
        },
        "button": {
          "background": "#dc6f40"
        }
      },
      "theme": "classic",
      "content": {
        "message": "En poursuivant votre navigation, vous acceptez l'utilisation de cookies à des fins de personnalisation.",
        "dismiss": "OK",
        "link": "En savoir +",
        "href": ""
       }
    })});
    </script>

    <!-- Get access to icons library -->
    <script src="https://kit.fontawesome.com/ee6e3cf9c0.js"></script>

    <!-- Load fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Load site-specific CSS file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Retrieve view-specific meta data -->
    <?= $meta ?>

</head>

<body>
    <header>
        <!-- Navigation - build navbar dynamically -->
        <?php
            $menuC=new MenuController();
            $navBar=$menuC->buildNavBar();
        ?>
        
        <!-- Display navbar in responsive mode -->
        <div class="rectangle">
            <nav class="navbar navbar-expand-lg navbar-dark menuBar mt-2">

                <div class="dt-hidden">
                    <div>
                        <button type="button" class="btn searchButton" data-toggle="modal" data-target="#searchModal"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="Rechercher" aria-hidden="true">
                        <div class="modal-dialog"  role="document">
                            <div class="modal-content">
                                <div class="modal-body bg-IssyBlue">    
                                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                    <form  action="index.php?class=Search" class="form-inline" method="post">
                                        <div class="form-group mobileSearch">
                                            <input name="searchString" id="searchString" type="search" placeholder="Rechercher" aria-label="Search">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>

                
                <a class="navbar-brand" href="index.php"><img src="img/ISSYTriathlon.svg" class="navbarIcon" alt="Issy Triathlon"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- id admin is logged in, display admin menu --> 

                <div class="collapse navbar-collapse bg-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <?=$navBar?>
                        <!-- Contact button -->
                        <a href="index.php?class=Contact&action=create" class="nav-link issyMenuBar"aria-haspopup="true" aria-expanded="false">Contact</a>

    
                        <?php if (UserController::isLogged()) : ?>
                            <li>
                                <a class="nav-link issyMenuBar" href="admin.php"  aria-haspopup="true" aria-expanded="false">
                                <?=$_SESSION["role"] ?>
                                </a>
                            <li>
                                <a class="nav-link issyMenuBar" href="admin.php?class=User&action=logout"  aria-haspopup="true" aria-expanded="false">
                                Logout
                                </a>
                            </li>
                        <?php endif ?>
                    </ul>
                    <ul class="navbar-nav mr-3">
                        <li>
                            <!-- MyIssyTri button -->
                            <div class="rectangle-myIssyTri mr-3 bg-Issy-Blue verticalCenter">
                                <a href="https://issytri.overcoaching.fr" target="_blank" class="nav-link verticalCenter" aria-haspopup="true" aria-expanded="false"><img src="img/overcoachingCercle.png" class="logo-myIssyTri ml-1"><span class="text-myIssytri">MyIssyTri</span></a>
                            </div>
                        </li>


                        <li>
                            <div class="mobile-hidden text-white">    
                                <!-- Display field for search engines --> 
                                <div class="form-group rectangle-search verticalCenter">
                                    <form  action="index.php?class=Search" class="form-inline" method="post">
                                        <label for="searchString"><i class="fas fa-search ml-3 "></i></label>
                                        <input class="mr-sm-2 input-search" name="searchString" id="searchString" type="search" placeholder="Rechercher" aria-label="Search">
                                    </form>
                                </div>
                            </div> <!-- /.mobile-hidden -->
                        </li>

                    </ul> <!-- /.navbar -->
                </div>
            </nav>
        </div>
    </header>

    <!-- Page Content -->
    <main>
        <!-- Load banner elements if any --> 


        <?php if(isset($banner)) { echo $banner ;} ?>            

        <div class="container">
            <?=$flash->flash()?>        <!-- Display any flash messages -->
        </div>

        <?= $content ?>             <!-- Display content from caller -->

        <div class="container">
            <!-- Display Strava statistics and links to social media  -->

            <div class="row center-content">

                <div id="strava-club" class="center-content mb-3">
                    <iframe height="160" frameborder='0' scrolling='no' src='https://www.strava.com/clubs/issy-triathlon/latest-rides/b67a66aa06c77690125c5b203a2b732087fc9f03?show_rides=false'></iframe>
                </div> 
            </div>
        </div> 
      </main>

    <!-- Footer -->
    <footer class="py-5 bg-IssyBlue">
        <div class="container">
            <p class="m-0 text-center text-white">
                Mentions légales :<br>
                Issy Triathlon, 5 av Jean Bouin, 92130 Issy-les-Moulineaux<br>
                Directeur de la publication : Didier Serrano<br>
                Hébergeur : OVH SAS, Roubaix - France - 09.72.10.10.07<br><br>
            </p>
            <p class="m-0 text-center text-white">Copyright &copy; Issy Triathlon <?php echo date('Y'); ?></p>
        </div>
    </footer>



    <!-- Load Jquery & Bootstrap elements -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

        <!-- Load view-specific JavaScript elemetns --> 
        <?php if(isset($js))
    { echo $js ;} ?>
</body>

</html>