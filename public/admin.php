<?php 

/**
 * Issy Triathlon 3.0 website
 * Author: Ph.Roggeband
 *  admin.php
 * 
 *  Access administration tools to create / update / modify all classes used 
 * 
 *  URL format : admin.php?class=<class>&action=<action>[&id=<record id>]
 * 
 *  id is specified for edit / delete / update functions
 */

require "../src/utilities/config.php";                         // load configuration file
require "../src/utilities/autoload.php";                       // load internal dependencies
require "../vendor/autoload.php";                          // load vendor packages

if(session_status() !== PHP_SESSION_ACTIVE) session_start();

if(isset($_GET["class"]))
{
    if(!empty($_GET["class"]))
    {
        $class = ucfirst(strtolower($_GET["class"]))."Controller";
        try {
            $c = new $class();                      // instanciate chosen class 
        } catch (Exception $e) {
            die($e->getMessage());
        }

        if(isset($_GET["action"]))
        {
            if(!empty($_GET["action"]))
            {
                $action = $_GET["action"];

                if (!empty($_GET["id"])) {
                    try {
                        $c->$action($_GET["id"]);      // if action and id are  set, call corresponding method
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }
                }
                
                else {
                    try {
                        $c->$action();                  // if only action is set, call corresponding method with no params
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }
                }
            }
        }else{
            $c->index();                            // if class is set and no action is specifiied, call index method for given class
        }
    }
} else {
    
    if (UserController::isLogged()) {
            $openContacts=Contact::readAllByStatus(1);
            if (count($openContacts)>0) {
                $flash=new SessionFlash();
                $flash->warning(count($openContacts)." Contact requests pending");
            }
            require "views/admin/index.php" ;
        }
    else {
         require "views/admin/userLogin.php";
    }
}

?>



