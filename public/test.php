<?php 
/**
 * Issy Triathlon 3.0 website
 * Author: Ph.Roggeband
 * 
 * PhP router 
 * 
 * Starter file called either as a base URL ,  or from one of the controllers and views using the following syntax; 
 * 
 *  index.php?class=<class>&action=<action>[&<params>]
 * 
 */

require "../src/utilities/config.php";                         // load configuration file
require "../src/utilities/autoload.php";                       // load internal dependencies
require "../vendor/autoload.php";                          // load vendor package dependencies

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);


?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <!-- Bootstrap core CSS  and Jquery -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">


    <!-- Get access to icons library -->
    <script src="https://kit.fontawesome.com/ee6e3cf9c0.js" crossorigin="anonymous"></script>

    <!-- Load fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Load site-specific CSS file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Retrieve view-specific meta data -->

</head>

<body>
    <header>


    </header>

    <!-- Page Content -->
    <main>

        <div class="container">
        <h1>Issy Triathlon - Page de test</h1>
        <div>


        </div>
          <div id="coachList">
          </div>
        </div>


    </main>

    <footer>
    </footer>

    <!-- Load Jquery & Bootstrap elements -->
    <script src="http://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    
    <!-- Javascript code -->

  <script src="jsApps/dynCoach.js"></script>

</body>


