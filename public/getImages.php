<?php

require "../src/utilities/config.php";                         // load configuration file
require "../src/utilities/autoload.php";                       // load internal dependencies
/**
 * Author : Philippe Roggeband - March 2020
 * Version 1.0
 * Change History :
 * 
 * Standalone PhP Script called by Ajax to retrieve list of images in library 
 *
 */
 // retrieve all images from the library
 $images=Image::readAll() ;
 $imagesArray=[];

 // build an array with the ImageID and the path to the miniature
 foreach ($images as $image) {
     $imagesArray[$image->getImageId()]=$image->getImageMiniPath();
 }
 //var_dump ($imagesArray);
 $result= json_encode($imagesArray);
 //var_dump("<br>$result");
 echo $result;
?>