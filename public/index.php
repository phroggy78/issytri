<?php 
/**
 * Issy Triathlon 3.0 website
 * Author: Ph.Roggeband
 * 
 * PhP router 
 * 
 * Starter file called either as a base URL ,  or from one of the controllers and views using the following syntax; 
 * 
 *  index.php?class=<class>&action=<action>[&<params>]
 * 
 */

require "../src/utilities/config.php";                         // load configuration file
require "../src/utilities/autoload.php";                       // load internal dependencies
require "../vendor/autoload.php";                          // load vendor package dependencies


// check if URL containtes parameters
if(isset($_GET["class"]))
{
    if(!empty($_GET["class"]))
    {
        $class = ucfirst(strtolower($_GET["class"]))."Controller";
        try {
            $c = new $class();                      // instanciate chosen class 
        } catch (Exception $e) {
            die($e->getMessage());
        }

        if(isset($_GET["action"]))
        {
            if(!empty($_GET["action"]))
            {
                $action = $_GET["action"];
                
                try {
                    $c->$action();                  // if action is set, call corresponding method
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            }
        }else{
            $c->index();                            // if class is set and no action is specifiied, call index method for given class
        }

    }
}else{
    // no class is set, call FrontController by default
    $c = new FrontController();               
    $c->index();
}
