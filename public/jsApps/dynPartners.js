"use strict"

/* declare constants */



/* declare global variables */

/* main code */
$.getJSON("https://www.overcoaching.fr/api/v1/get_ext_club_partners.php?o=2&f=web_list")
.done(function(jsonReturn){
    // retrieve return string in global variables
    let i = 0;
    let c = 0;
    let html = '';
    let categories = ['institution', 'sport', 'school', 'business'];
    let partners = jsonReturn.partners;
    for (c = 0; c < categories.length; c++) {
        html = "<tbody>\n";
        for (i = 0; i < partners.length; i++) {
            if (partners[i].category == categories[c]) {
                html += "<tr>\n";
                html += '<td><a href="' + partners[i].web_url + '" target="_blank"><img src="' + partners[i].img_url + '" width="' + partners[i].logo_width + '" alt="" /></a></td>' + "\n";
                html += '<td width="10"></td><td>' + "\n";
                html += '<p><strong>' + partners[i].name + '</strong></p>' + "\n";
                html += '<p><span>' + partners[i].description + '</span></p>' + "\n";
                html += '<p></p>' + "\n";
                html += '</td>' + "\n";
                html += "</tr>\n";
            }
        }
        html += "</tbody>\n";
        $('#table-' + categories[c]).html(html);       
    }
})
.fail(function(errMsg){
    console.log("DynPartners PhP Failed");
    console.log(errMsg);
})

