"use strict"

/* declare constants */



/* declare global variables */

var classHtml="";
var rankings=[];
var ranks=[];
var rank;
var season;

/* local functions */

function displayRank(rank, index, ranks) {
    classHtml+="<tr>";
        classHtml+="<td>"+rank.rank+"</td>";
        if (rank.club_name=="Issy Triathlon") {classHtml+="<td><strong>"+rank.club_name+"</strong></td>";}
        else {        classHtml+="<td>"+rank.club_name+"</td>";}

        classHtml+="<td>"+rank.points+"</td>";
    classHtml+="</tr>";
}


function displayRanking(ranking,index,arr){
    classHtml+="<div class='col-sm-12 col-md-6 col-lg-6'>";
        classHtml+="<div class='card mt-3 mb-3 h-100'>";
            classHtml+="<div class='card-header bg-darkIssyBlue text-white'>";
                classHtml+=ranking.name;
                classHtml+="<small class='text-light mr-0'> - "+ranking.after_txt+"</small>";
            classHtml+="</div> <!-- /.card-header'-->";
            classHtml+="<div class='card-body'>";
                classHtml+="<table class='table'>";
                    classHtml+="<thead>";
                        classHtml+="<th class='Col'>Rang</th>";
                        classHtml+="<th class='Col'>Club</th>";
                        classHtml+="<th class='Col'>Points</th>";
                    classHtml+="</thead>";
                    classHtml+="<tbody>"
                        ranking.ranks.forEach(displayRank);
                    classHtml+="</tbody>";
                classHtml+="</table>";
            classHtml+="</div><!--/.card-body> -->";
        classHtml+="</div> <!-- /.card -->";
    classHtml+="</div> <!-- /.col -->";
    return;
}

function displayClassement(rankings) {
    // Open card-deck;

    classHtml+="<div class='card-deck'>";
    rankings=season.rankings;
    rankings.forEach(displayRanking);
    classHtml+="</div>";
    $("#Classement").html(classHtml);             // display in dynCal div 
    return;
}

/* main code starts here */

if($('#Classement').length > 0){
    // create and inject HTML code for inital calendar (date = today by default)
    $.getJSON("https://www.overcoaching.fr/api/v1/get_ext_elite_rankings.php")
    .done(function(jsonReturn){
        // retrieve return string in global variables
        console.log(jsonReturn);
        season=jsonReturn;
        classHtml="";                       // Initialise global variable
        displayClassement(season);

    })
    .fail(function(errMsg){
        console.log("DynClassement PhP Failed");
        console.log(errMsg);
    })
}

