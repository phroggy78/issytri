"use strict";

/**
 * Declare constants
 */

 const CAL_INCREMENT_LG=7 ;
 const CAL_INCREMENT_MD=5 ;
 const CAL_INCREMENT_SM=1 ;
 const CAL_DAYS=21;

 const COL_WIDTH_LG="style='width:14.2%'";
 const COL_WIDTH_MD="style='width:19.9%'";
 const COL_WIDTH_SM="style='width:33.3%'";

 const SWIM_ICON="<i class='fas fa-swimmer dyncal-discipline dyncal-discipline-swim'></i>";
 const RUN_ICON="<i class='fas fa-running dyncal-discipline dyncal-discipline-run'></i>";
 const BIKE_ICON="<i class='fas fa-biking dyncal-discipline dyncal-discipline-bike'></i>";
 const GPP_ICON="<i class='fas fa-dumbbell dyncal-discipline dyncal-discipline-gpp'></i>";

/**
 *  Declare variables
 */
var weekdays=["Lun","Mar","Mer","Jeu","Ven","Sam","Dim"]; // table with weekdays
var currentIndex=0;                                                               // index in calendar table
var button_previous = document.querySelector("#calendarPrevious");                // Previous Button DOM - when clicked, go back in calendar display
var button_today = document.querySelector("#calendarToday");                      // Today button DOM - when clicked, return to calendar display for today
var button_next= document.querySelector("#calendarNext");                         // Next button DOM - when clicked, move to next 5 days
var calHtml="";
var index=0;
var dynCalData=[];
var emptyReason="";
var calWidth=0;
var columnWidth="";
var iconClass="";
var bgColorClass="";


 /**
  * Declare functions
  */

  function shortName(longName) {
      // return first name + first letter of surname 

      
  }

  function reverseDate (inDate) {
      var dateArray=inDate.split('-');
      return dateArray[2]+"/"+dateArray[1];
  }

 function displayCalendar(index) {
    // Set calendar increment based on device width
    // build table header with days / dates

    var coach;
    var coachName;
    var firstName;
    var secondName;
    var addToCalendar;
    if (emptyReason != '') {
      $('#dynCalMessage').html(emptyReason);
      $('#dynCalMessage').show();
    }
    var today = new Date();
    var todayYmd = today.toISOString().split('T')[0];

    if (screen.width<768) {
        /** Generate mobile-first code  */
        for (var i=index;i<index+calWidth;i++) {
        calHtml="<div class='dt-hidden m2'>";
        calHtml+="<div class='card card-transparent border-secondary mb-2'>";                 //start card definition
        // card header : insert day and date and close div
        calHtml+="<div class='card-header dyncal-day'>"+weekdays[dynCalData[i].w-1]+" "+reverseDate(dynCalData[i].date)+"</div>";
        // insert activities
        for (var j=0; j<dynCalData[i].trainings.length;j++) {
            // extract data from JSON to build activity display
            // switch case statement to select icon & color based on activity

            switch (dynCalData[i].trainings[j].discipline_l)  {
                case "swim":
                    iconClass=SWIM_ICON;
                    bgColorClass="bg-white text-primary";
                    break;
                case "run":
                    iconClass=RUN_ICON+dynCalData[i].trainings[j].mode;
                    bgColorClass="bg-white text-secondary";
                    break;
                case "bike":
                    iconClass=BIKE_ICON+dynCalData[i].trainings[j].mode;
                    bgColorClass="bg-white text-info";
                    break;
                case "gpp":
                    iconClass=GPP_ICON;
                    bgColorClass="bg-white text-dark";
                    break;
                case "multi":
                    iconClass = BIKE_ICON;
                    bgColorClass="bg-white text-info";
                    break;
            }
            // Get first name of the coach(es)
            coachName="";
            if (dynCalData[i].trainings[j].trainers.length==1){
                for (var k=0; k<dynCalData[i].trainings[j].trainers.length;k++) {
                    // Place holder : chop down coach name to first name + first letter of surname
                    firstName=dynCalData[i].trainings[j].trainers[k].surname;
                    coachName+=firstName+" ";
                }
            }
            else if (dynCalData[i].trainings[j].trainers.length>1){
                coachName="Team";
            }
            calHtml+='<div class="card-body p-2 dyncal-card' + (dynCalData[i].date < todayYmd ? '-past' : '') + '">' +
            '<div class="row dyncal-rectangle-mobile"><div class="dyncal-rectangle-1 dyncal-rectangle-1-' + dynCalData[i].trainings[j].discipline_l + '"></div>'
              +'<div class="dyncal-rectangle-2-mobile"><div class="dyncal-rectangle-2-inner"><span class="heure dyncal-discipline-'+dynCalData[i].trainings[j].discipline_l+'">'
              +
              dynCalData[i].trainings[j].hour+" "+
              iconClass+"</span><br>"+
              '<span class="heure-place">' + dynCalData[i].trainings[j].place_short+'</span>' + "<br>"+
              '<span class="heure-coach">'+coachName+'</span></div>';
          calHtml+="</div></div></div>";                            //end card body definition
        }
        calHtml+="</div>";                                //end card definition
    }

        calHtml+="</div>"; // close mobile first section
    }

    else {


        /** Generate HTML for desktop  */
        calHtml="<div class='card-group m-2'>" ;                  //open card-group with daily calendar entries
        for (var i=index;i<index+calWidth;i++) {
            calHtml+="<div class='card card-transparent border-secondary mb-2'>";                 //start card definition
            // card header : insert day and date
            calHtml+="<div class='card-header dyncal-day'>"+weekdays[dynCalData[i].w-1]+" "+reverseDate(dynCalData[i].date)+"</div>";

            // insert activities
            var j=0;
            var found=false;
            for (var p=1; p<=4; p++) {
              j=0;
              found=false;
              while (!found && j<dynCalData[i].trainings.length) {
                if (dynCalData[i].trainings[j].day_index == p) {
                  found = true;
                } else {
                  j++;
                }
              }
              if (found) {
                switch (dynCalData[i].trainings[j].discipline_l)  {
                    case "swim":
                        iconClass=SWIM_ICON;
                        break;
                    case "run":
                        iconClass=RUN_ICON;//+dynCalData[i].trainings[j].mode;
                        break;
                    case "bike":
                        iconClass=BIKE_ICON;//+dynCalData[i].trainings[j].mode;
                        break;
                    case "gpp":
                        iconClass=GPP_ICON;
                        break;
                    case "multi":
                        iconClass = BIKE_ICON;// + RUN_ICON;
                        break;
                }
                // Get first name of the coach(es)
                coachName="";
                if (dynCalData[i].trainings[j].trainers.length==1){
                    for (var k=0; k<dynCalData[i].trainings[j].trainers.length;k++) {
                        // Place holder : chop down coach name to first name + first letter of surname
                        firstName=dynCalData[i].trainings[j].trainers[k].surname;
                        coachName+=firstName;
                    }
                }
                else if (dynCalData[i].trainings[j].trainers.length>1){
                    coachName="Team";
                }
                calHtml+='<div class="card-body p-2 dyncal-card' + (dynCalData[i].date < todayYmd ? '-past' : '') + '">' +
                  '<div class="row dyncal-rectangle"><div class="dyncal-rectangle-1 dyncal-rectangle-1-' + dynCalData[i].trainings[j].discipline_l + '"></div>'
                    +'<div class="dyncal-rectangle-2"><div class="dyncal-rectangle-2-inner"><span class="heure dyncal-discipline-'+dynCalData[i].trainings[j].discipline_l+'">'
                    +
                    dynCalData[i].trainings[j].hour+" "+
                    iconClass+"</span><br>"+
                    '<span class="heure-place">' + dynCalData[i].trainings[j].place_short+'</span>' + "<br>"+
                    '<span class="heure-coach">'+coachName+'</span></div>';
                calHtml+="</div></div></div>";                            //end card body definition

              } else {
                calHtml+='<div class="card-body p-2 dyncal-card"><div class="row empty-dyncal-rectangle"></div></div>';
              }
            }
            calHtml+="</div>";                                //end card definition
        }
        calHtml+="</div>";                                    // close card group
    }

    $("#dynCal").html(calHtml);             // display in dynCal div 
}

  /**
   * display next n days
   */
  function nextDays() {
      if (currentIndex<CAL_DAYS-calWidth) {
          currentIndex+=calWidth;
          displayCalendar(currentIndex);
          // Enable previous button
          button_previous.classList.remove("disabled");
          // if at end of table, disable next button
          if (currentIndex==CAL_DAYS-calWidth) {
              button_next.classList.add("disabled");
          }
      } 
    }

    /**
     *  display previous n days 
     */
  function prevDays() {
    if (currentIndex>=calWidth) {
        currentIndex-=calWidth;
        displayCalendar(currentIndex);
        // Enable next button
        button_next.classList.remove("disabled");
        // if at beginning of table, disable previous buttno
        if (currentIndex<calWidth) {
            button_next.classList.add("disabled");
        }
    } 
  }

  /**
   *  Display calendary from today
   */
  function today() {
      currentIndex=0
      displayCalendar(currentIndex);
      button_next.classList.remove("disabled");
      button_previous.classList.add("disabled");
  }


/*************** MAIN CODE ******************
 * 
 * Get JSON string with calendar for the next  15 days
 * 
 * Decode it into a table
 * 
 * Display the first 5 days and set up listeners to "scroll" through the table horizontally
 * 
 */

 button_previous.classList.add("disabled")               // set up button listeners
 currentIndex=0;                                         // initialise calendar table index

 button_previous.addEventListener('click',prevDays);
 button_today.addEventListener('click',today);
 button_next.addEventListener('click',nextDays);
 

 if (screen.width<768) {
    calWidth=CAL_INCREMENT_SM;
    columnWidth=COL_WIDTH_SM;
 }
 else if (screen.width<1024) {
    calWidth=CAL_INCREMENT_MD;
    columnWidth=COL_WIDTH_MD;
 }
 else {
     calWidth=CAL_INCREMENT_LG;
     columnWidth=COL_WIDTH_LG;
} 

// create and inject HTML code for inital calendar (date = today by default)
$.getJSON("https://www.overcoaching.fr/api/v1/get_ext_club_calendar.php?nb_days="+CAL_DAYS)
    .done(function(jsonReturn){
        // retrieve return string in global variables
        dynCalData=jsonReturn.days;      
        console.log(dynCalData);                   
        emptyReason=jsonReturn.empty_reason;
        displayCalendar(currentIndex);
    })
    .fail(function(errMsg){
        console.log("DynCal PhP Failed");
        console.log(errMsg);
    })

// create and inject HTML code for partners
$.getJSON("https://www.overcoaching.fr/api/v1/get_ext_club_partners.php?o=2&f=web_footer")
    .done(function(jsonReturn){
        var pHtml = '<div class="row">';
        var partners = jsonReturn.partners;
        var i = 0;
        for (i = 0; i < partners.length; i++) {
            pHtml += '<a href="' + partners[i].web_url + '" target="_blank" class="verticalCenter"><img class="img-gray img-color" src="' + partners[i].img_url + '" alt="' + partners[i].name + '"></a>';
            //if (i == 7) {
            //    pHtml += '</div><div class="row">';
            //}
        }
        pHtml += '</div>';
        $('.partnerLogos').html(pHtml);
    })
    .fail(function(errMsg){
        console.log("DynCal Partners Failed");
        console.log(errMsg);
    })