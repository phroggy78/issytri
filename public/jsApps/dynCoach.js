"use strict";
/* 
    JS script to display list of active coaches, obtained from overcoaching site
*/
/* declare global variables */





var coachlist;
var coachHtml;
var coachArray;

/** Local functions */


function getDisciplines(disciplines) {
    // Get Icons for disciplines 
    console.log(disciplines);
    var swimIcon='<i class="fas fa-swimmer"></i> ';
    var runIcon='<i class="fas fa-running"></i> ';
    var cycleIcon='<i class="fas fa-biking"></i> ';
    var fitIcon='<i class="fas fa-dumbbell"></i> ';
    var reply="";
    for (var i=0; i<disciplines.length; i++) {
        switch (disciplines[i]) {
            case "natation":
                reply+=swimIcon;
            break;
            case "course à pied":
                reply+=runIcon;
            break;
            case "prépa physique":
                reply+=fitIcon;
            break;
            case "vélo":
                reply+=cycleIcon;
            break;
        }
    }
    console.log(reply);
    return reply;
}


function getPopulations(populations) {

    var adultIcon='<i class="far fa-male"></i> ';
    var juniorIcon='<i class="far fa-child"></i> ';
    var sportSanteIcon='<i class="far fa-female"></i> ';
    var reply="";

    // get Icons for target populations

    for (var i=0;i<populations.length;i++) {
        switch(populations[i]) {
            case "adultes":
                reply+=adultIcon;
            break;
            case "juniors 2022":
                reply+=juniorIcon;
            break;
            case "jeunes Performance 1 2022":
                reply+=juniorIcon+"P1";
            break;
            case "jeunes Performance 2 2022":
                reply+=juniorIcon+"P2";
            break;
            case "Sport-Santé 2022":
                reply+=sportSanteIcon;
            break;
        }
    }
    return reply;

}

function  displayCoachList(coachList) {

var icons;

console.log ("here");
console.log(coachList);
coachHtml="<table><thead><tr>";
coachHtml+="<th>Prénom</th><th>Photo</th><th>Populations</th><th>Disciplines</th>";
coachHtml+="</tr></thead>";

for (var i=0;i<coachList.length;i++) {
    coachHtml+="<tr>";
    coachHtml+="<td>"
        coachHtml+=coachList[i].name;
    coachHtml+="</td>"
    coachHtml+="<td><img class='articleCoverSq' src='"+coachList[i].picture+"'>"
    coachHtml+="</td>";
    icons=coachList[i].populations;
    coachHtml+="<td>"+icons;
    coachHtml+="</td>" 
    icons=getDisciplines(coachList[i].disciplines);
    coachHtml+="<td>"+icons;
    coachHtml+="</td>" 
    coachHtml+="</tr>";
}

coachHtml+="</table>"

$("#coachList").html(coachHtml);             // display in coachList div 
return;
}

function displayCoachList2(coachList) {
    var icons="";
    var coachHtml='<div class="card-deck mt-3">';
    for (var i=0;i<coachList.length;i++) {
        coachHtml+='<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">';
        coachHtml+='<div class="card mt-3 mb-3" >';
        coachHtml+='<img src="'+coachList[i].picture+'" class="articleCoverSq" alt="">'
        coachHtml+='<p><center>';
        coachHtml+=coachList[i].name+'<br>';
        icons=getDisciplines(coachList[i].disciplines);
        coachHtml+=icons;
        coachHtml+='</center></p>';
        coachHtml+='</div>';
        coachHtml+='</div>';
    }
    coachHtml+='</div>';
    $("#coachList").html(coachHtml);             // display in coachList div 
return;
}

// main code starts here 
if($('#coachList').length > 0) {
    $.getJSON("https://www.overcoaching.fr/api/v1/get_ext_club_generic_coachs.php")
    .done(function(jsonReturn){
        // retrieve return string in global variables
        console.log(jsonReturn);    // log JSON return
        displayCoachList2(jsonReturn); // Build HTML code from json return and display it
    })
    .fail(function(errMsg){
        console.log("coachlist PhP Failed");
        console.log(errMsg);
    })
}

