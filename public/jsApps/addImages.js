"use strict"
/**
 *  Author : Philippe Roggeband - March 2020
 *  Version 1.0
 *  Change History:
 * 
 *  JS / Ajax functions to add images to objects in the database. 
 * 
 *  Overall principles :  The calling form has a hidden input field ("objectImages") which is used to communicate with PhP. The field
 *  holds a CSV string of image ids which are currently associated with the object. 
 * 
 *  The calling form has a "div" with Id "imageGallery". This div will be filled with miniatures of available images in the library . 
 * 
 * When the user clicks on a library image, it's color changes to show that it is selected. It's Id is added to the string in the hidden 
 * field. The calling PHP controller is responsable for the addition of the images to the object in the database.
 * 
 */

/**
 * Define constants
 */

/**
 * define variables
 */

 var images=[];                                                     // set up empty array to receive images


/**
 * Define functions
 */

 /**
  * Function call when a user clicks on a library image to select / deselect it. 
  */
 function selectImage(){

     // toggle "selected" class
    $(this).toggleClass("selected");                        // CSS will alter image display to show that it has been selected / de-selected

     // if image is already in list, remove it. If it not, add it
     // oImages=$('#objectImages').val().split(",");
     
     console.log("image selected :"+$(this).attr("data-imageId") ); 
     
     if (oImages.includes($(this).attr("data-imageId")))
        {   console.log("remove image");
            oImages.splice(oImages.indexOf($(this).attr("data-imageId")),1);
        }
     else
        {   console.log("add image");
            oImages.push($(this).attr("data-imageId"))}
     // rebuild imageList Json and store it in the hidden input field 
     $('#objectImages').val(oImages.join()) ;
 }

 function displayImages(imagesArray) {
     var html="<br>";
     var index;
     for (index in imagesArray) {
        // only display image if it was not previously selected 
        if (! oImages.includes(index)) {
            html += "<img class='miniature libImage' data-imageId='"+index+"' src='"+imagesArray[index] +"'>"+" ";
        }
      }
     html+="<br><br>";
     $("#imageGallery").html(html);                     // display available images
     $(".libImage").click(selectImage);                // set up listener to select / deselect images
     $("#btnImages").addClass("hidden");

 }

 function getImages(){
    event.preventDefault()
    $.getJSON("getImages.php")
    .done(function(jsonReturn){
        images=jsonReturn ;                         // retrieve return string in global variable
        displayImages(images);                      // display images in 
    })
    .fail(function(errMsg){
        alert(errMsg);
        console.log(errMsg);
    })
 }

/**
* *********************************************** Main Code body starts here *********************************
*/

var oImages=$('#objectImages').val().split(",");                // get list of images already associated with objects
$('#btnImages').click(getImages);                               // Set up button listener for library display
$(".miniature").click(selectImage);

// Set up listener for previous image selection


